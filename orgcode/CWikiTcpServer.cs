﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;


namespace WikiOptics_Collimator
{
    public class CWikiTcpServer
    {
        string m_szWikiTcpServerSectionName     = "ServerConfiguration";
        string m_szKey_WikiTcpServerListenPort  = "ListenPort(5000~)";

        const Int32 DefaultIPPort = 5006;
        public Int32 m_nListenPort = DefaultIPPort;

        public List<CClientSocket> csock_list = new List<CClientSocket>();
        public List<byte[]> image_list = new List<byte[]>();
        public object m_csock_list_lock = new object();
        //TcpListener m_server = null;
        Socket m_server = null;

        public void SaveDefaultServerConfigFile(string filepath)
        {
            string defValue;
            defValue = DefaultIPPort.ToString();
            NativeMethods.WritePrivateProfileString(m_szWikiTcpServerSectionName, m_szKey_WikiTcpServerListenPort, defValue, filepath);            
        }

        public void LoadConfigFile()
        {
            try
            {
                // 현재 프로그램 실행 위치
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
                bool bExist = System.IO.File.Exists(filepath);
                if (false == bExist)
                {
                    SaveDefaultServerConfigFile(filepath);
                }
                else
                {
                    bExist = false;
                    string[] sec_names = CUtil.GetSectionNames(filepath);
                    int num_sections = sec_names.Length;
                    for (int i = 0; i < num_sections; i++)
                    {
                        bExist = sec_names[i].Equals(m_szWikiTcpServerSectionName);
                        if (bExist == true)
                            break;
                    }
                    if (false == bExist)
                    {
                        SaveDefaultServerConfigFile(filepath);
                    }
                }

                int nSize = 128;
                string defValue = DefaultIPPort.ToString();
                StringBuilder retVal1 = new StringBuilder(nSize);
                int result1 = 0;
                Int32 temp = 0;
                
                result1 = NativeMethods.GetPrivateProfileString(
                                m_szWikiTcpServerSectionName, 
                                m_szKey_WikiTcpServerListenPort, 
                                defValue, 
                                retVal1, 
                                nSize, 
                                filepath);
                if (true == Int32.TryParse(retVal1.ToString(), out temp))
                {
                    m_nListenPort = temp;
                }
                else
                {
                    m_nListenPort = DefaultIPPort;
                }
            }
            catch (Exception ex)
            {
                string msg = string.Format("[wiki] can't load config file, exception = {0}", ex.ToString());
                Program.log.Error(msg);
            }
        }

        //public void StartListen(IPAddress localaddr, Int32 port)
        public void StartListen()
        {
            if (m_server != null)
            {
                m_server.Close();
                m_server = null;
            }

            // https://www.csharpstudy.com/net/article/10-Socket-%EC%84%9C%EB%B2%84
            m_server = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
 
            // (2) 포트에 바인드
            IPEndPoint ep = new IPEndPoint(IPAddress.Any, m_nListenPort);
            m_server.Bind(ep);
            // (3) 포트 Listening 시작
            m_server.Listen(10);
            m_server.BeginAccept(new AsyncCallback(onAccept), this);

            //m_server = TcpListener.Create(m_nListenPort);
            //m_server.Start();
            //m_server.BeginAcceptSocket(new AsyncCallback(onAccept), this);
        }

        public void StopListen()
        {
            try
            {
                int count = csock_list.Count;
                for (int i = 0; i < count; i++)
                {
                    CClientSocket cs = csock_list[i];
                    cs.CloseSock();
                }

                m_server.Close();
                m_server = null;
            }
            catch (Exception ex)
            {
                Trace.WriteLine("[wiki] StopListen except = " + ex.ToString());
            }
        }

        void onAccept(IAsyncResult ar)
        {
            try
            {
                CWikiTcpServer daemon = (CWikiTcpServer)ar.AsyncState;
                if (daemon.m_server == null)
                    return;
                    
                Trace.WriteLine("[wiki] onAccept : a client want connection");

                CClientSocket csock = new CClientSocket();
                csock.m_clientsocket = daemon.m_server.EndAccept(ar);
                csock.m_daemon = daemon;
                csock.InitOption();                
                lock (m_csock_list_lock)
                {
                    csock_list.Add(csock);
                }

                //daemon.m_server.BeginAcceptSocket(new AsyncCallback(onAccept), daemon);
                daemon.m_server.BeginAccept(new AsyncCallback(onAccept), daemon);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("[wiki] onAccept except = " + ex.ToString());
            }
        }

        public void SendImageData(byte[] new_image)
        {
            Int32 i = 0;
            lock (m_csock_list_lock)
            {
                Int32 count = csock_list.Count;
                if (count <= 0)
                    return;

                if (image_list.Count > 2)
                {
                    image_list.RemoveAt(image_list.Count - 1);
                }
                image_list.Add(new_image);
                   
                for (i = 0; i < count; i++)
                {   
                    if (false == csock_list[i].m_bClosed)                 
                        csock_list[i].SendImage(image_list[0]);
                }
                image_list.RemoveAt(0);

                for (i = 0; i < count; i++)
                {
                    if (true == csock_list[i].m_bClosed)
                    {
                        csock_list.RemoveAt(i);
                        count = csock_list.Count;
                        i = 0;
                    }                        
                }
            }
        }

    } // public class CWikiTcpServer
}
