// WikiAngleAutoCollimator.frmGreyScaleSetting
//0323
using System;
using System.ComponentModel;
using System.Windows.Forms;
using WikiOptics_Collimator;
using System.Drawing;

namespace WikiOptics_Collimator
{
    public class frmGreyScaleSetting : Form
    {
        private IContainer components = null;

        private Button cmdSetMinMax;

        private Label label1;

        private Label label2;

        private TextBox txtMin;

        private TextBox txtMax;

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new global::System.ComponentModel.ComponentResourceManager(typeof(WikiOptics_Collimator.frmGreyScaleSetting));
            this.cmdSetMinMax = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMin = new System.Windows.Forms.TextBox();
            this.txtMax = new System.Windows.Forms.TextBox();
            base.SuspendLayout();
            this.cmdSetMinMax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSetMinMax.Location = new System.Drawing.Point(217, 18);
            this.cmdSetMinMax.Name = "cmdSetMinMax";
            this.cmdSetMinMax.Size = new System.Drawing.Size(63, 48);
            this.cmdSetMinMax.TabIndex = 0;
            this.cmdSetMinMax.Text = "SET";
            this.cmdSetMinMax.UseVisualStyleBackColor = true;
            this.cmdSetMinMax.Click += new System.EventHandler(cmdSetMinMax_Click);
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "MIN Threshold :";
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "MAX Threshold :";
            this.txtMin.Location = new System.Drawing.Point(129, 18);
            this.txtMin.Name = "txtMin";
            this.txtMin.Size = new System.Drawing.Size(68, 21);
            this.txtMin.TabIndex = 3;
            this.txtMin.Text = "30";
            this.txtMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(txtMin_KeyPress);
            this.txtMin.Leave += new System.EventHandler(txtMin_Leave);
            this.txtMax.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtMax.Location = new System.Drawing.Point(129, 45);
            this.txtMax.Name = "txtMax";
            this.txtMax.Size = new System.Drawing.Size(68, 21);
            this.txtMax.TabIndex = 4;
            this.txtMax.Text = "255";
            this.txtMax.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtMax.KeyPress += new System.Windows.Forms.KeyPressEventHandler(txtMax_KeyPress);
            this.txtMax.Leave += new System.EventHandler(txtMax_Leave);
            base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 12f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            base.ClientSize = new System.Drawing.Size(291, 77);
            base.Controls.Add(this.txtMax);
            base.Controls.Add(this.txtMin);
            base.Controls.Add(this.label2);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.cmdSetMinMax);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            //base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon"); //Gray Scale Setting 하단 아이콘
            base.Name = "frmGreyScaleSetting";
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Grey Scale Setting";
            base.Load += new System.EventHandler(frmGreyScaleSetting_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public frmGreyScaleSetting()
        {
            InitializeComponent();
        }

        private void frmGreyScaleSetting_Load(object sender, EventArgs e)
        {
            txtMax.Text = CSetInfo.Setting_Gray_MaxLevel.ToString();
            txtMin.Text = CSetInfo.Setting_Gray_MinLevel.ToString();
        }

        private void cmdSetMinMax_Click(object sender, EventArgs e)
        {
            if (!clsETC.ISNumeric(txtMax.Text))
            {
                MessageBox.Show("Please, Check Max Threshold Value");
            }
            else if (!clsETC.ISNumeric(txtMin.Text))
            {
                MessageBox.Show("Please, Check Min Threshold Value");
            }
            else if (Convert.ToInt16(txtMax.Text) > 255)
            {
                MessageBox.Show("0 <= Threshold <= 255");
                txtMax.Text = "255";
            }
            else if (Convert.ToInt16(txtMin.Text) > 255)
            {
                MessageBox.Show("0 <= Threshold <= 255");
                txtMin.Text = "255";
            }
            else if (Convert.ToInt16(txtMax.Text) < 0)
            {
                MessageBox.Show("0 <= Threshold <= 255");
                txtMax.Text = "0";
            }
            else if (Convert.ToInt16(txtMin.Text) < 0)
            {
                MessageBox.Show("0 <= Threshold <= 255");
                txtMin.Text = "0";
            }
            else
            {
                CSetInfo.Setting_Gray_MaxLevel = Convert.ToInt16(txtMax.Text);
                CSetInfo.Setting_Gray_MinLevel = Convert.ToInt16(txtMin.Text);
            }
        }

        private void txtMin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void txtMax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != Convert.ToChar(Keys.Back))
            {
                e.Handled = true;
            }
        }

        private void txtMin_Leave(object sender, EventArgs e)
        {
            if (txtMin.Text == "")
            {
                txtMin.Text = "0";
            }
        }

        private void txtMax_Leave(object sender, EventArgs e)
        {
            if (txtMax.Text == "")
            {
                txtMax.Text = "0";
            }
        }
    }
}