// WikiAngleAutoCollimator.clsMatrixCalculator
using System;

public static class clsMatrixCalculator
{
	public static double[,] _Calculate(int lenght, double weight)
	{
		double[,] array = new double[lenght, lenght];
		double num = 0.0;
		int num2 = lenght / 2;
		double num3 = 0.0;
		double num4 = 1.0 / (Math.PI * 2.0 * Math.Pow(weight, 2.0));
		for (int i = -num2; i <= num2; i++)
		{
			for (int j = -num2; j <= num2; j++)
			{
				num3 = (double)(j * j + i * i) / (2.0 * (weight * weight));
				array[i + num2, j + num2] = num4 * Math.Exp(0.0 - num3);
				num += array[i + num2, j + num2];
			}
		}
		for (int k = 0; k < lenght; k++)
		{
			for (int l = 0; l < lenght; l++)
			{
				array[k, l] *= 1.0 / num;
			}
		}
		return array;
	}
}
