// WikiAngleAutoCollimator.clsDislpayInfo
using System;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using OpenCvSharp;
using WikiOptics_Collimator;
using System.Text;

namespace WikiOptics_Collimator
{
    internal class clsDislpayInfo
    {
        public static bool InsideDisplayFlag;
        public static bool OutsideDisplayFlag;
        public static bool CrossLineDisplayFlag;
        public static string ShapeType = "CIRCLE";
        public static CvColor CrossLineColor;
        public static int CrossLineThickness;
        public static CvColor InsideCircleColor;
        public static int InsideCircleThickness;
        public static int InsideCircleLength;
        public static CvColor OutsideCircleColor;
        public static int OutsideCircleThickness;
        public static int OutsideCircleLength;
        public static CvColor InsideSquareColor;
        public static int InsideSquareThickness;
        public static int InsideSquareLength;
        public static CvColor OutsideSquareColor;
        public static int OutsideSquareThickness;
        public static int OutsideSquareLength;
        public static CvColor FindSpotColor;
        public static int FindSpotLength;
        public static int FindSpotThickness;
        public static CvColor FindSpotTextFontColor;
        public static int FindSpotTextFontSize;
        
        const string m_szDisInfoSectionName = "SET_DISINFO";
        const string m_szKey_Inside_Display_flag = "Inside_Display_flag";
        const string m_szKey_Outside_Display_flag = "Outside_Display_flag";
        const string m_szKey_CrossLine_Display_flag = "CrossLine_Display_Flag";
        const string m_szKey_Shape_Type = "ShapType";
        const string m_szKey_Crossline_Color = "CrossLine_Color";
        const string m_szKey_CROSSLINE_THICKNESS = "CrossLine_Thickness";
        const string m_szKEY_INSIDE_CIRCLE_COLOR = "Inside_Circle_Color";
        const string m_szKEY_INSIDE_CIRCLE_THICKNESS = "Inside_Circle_Thickness";
        const string m_szKEY_INSIDE_CIRCLE_LENGTH = "Inside_Circle_Length";
        const string m_szKEY_OUTSIDE_CIRCLE_COLOR = "Outside_Circle_Color";
        const string m_szKEY_OUTSIDE_CIRCLE_THICKNESS = "Outside_Circle_Thickness";
        const string m_szKEY_OUTSIDE_CIRCLE_LENGTH = "Outside_Circle_Length";
        const string m_szKEY_INSIDE_SQUARE_COLOR = "Inside_Square_Color";
        const string m_szKEY_INSIDE_SQUARE_THICKNESS = "Inside_Square_Thickness";
        const string m_szKEY_INSIDE_SQUARE_LENGTH = "Inside_Square_Length";
        const string m_szKEY_OUTSIDE_SQUARE_COLOR = "Outside_Square_Color";
        const string m_szKEY_OUTSIDE_SQUARE_THICKNESS = "Outside_Square_Thickness";
        const string m_szKEY_OUTSIDE_SQUARE_LENGTH = "Outside_Square_Length";
        const string m_szKEY_FINDSPOT_COLOR = "Find_Spot_Color";
        const string m_szKEY_FINDSPOT_THICKNESS = "Find_Spot_Thickness";
        const string m_szKEY_FINDSPOT_LENGTH = "Find_Spot_Length";
        const string m_szKEY_FINDSPOT_TEXTURE_FONT_COLOR = "Find_Spot_Texture_Font_Color";
        const string m_szKEY_FINDSPOT_TEXTURE_FONT_SIZE = "Find_Spot_Texture_Font_Size";

        static void SaveDefaultDisplayInfo(string filepath)
        {
            // 추가된 부분
            bool writeResult = false;
            string defValue;

            defValue = "1";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Inside_Display_flag, defValue, filepath);

            defValue = "1";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Outside_Display_flag, defValue, filepath);

            defValue = "1";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_CrossLine_Display_flag, defValue, filepath);

            defValue = "CIRCLE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Shape_Type, defValue, filepath);

            defValue = "BLUE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Crossline_Color, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_CROSSLINE_THICKNESS, defValue, filepath);

            defValue = "BLUE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_COLOR, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_THICKNESS, defValue, filepath);

            defValue = "500";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_LENGTH, defValue, filepath);

            defValue = "BLUE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_COLOR, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_THICKNESS, defValue, filepath);

            defValue = "500";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_LENGTH, defValue, filepath);

            defValue = "BLUE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_COLOR, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_THICKNESS, defValue, filepath);

            defValue = "500";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_LENGTH, defValue, filepath);

            defValue = "BLUE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_COLOR, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_THICKNESS, defValue, filepath);

            defValue = "500";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_LENGTH, defValue, filepath);

            defValue = "BLUE";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_COLOR, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_THICKNESS, defValue, filepath);


            defValue = "10";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_LENGTH, defValue, filepath);


            defValue = "WHITH";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_TEXTURE_FONT_COLOR, defValue, filepath);

            defValue = "10";
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_TEXTURE_FONT_SIZE, defValue, filepath);
        }

        public static void LoadDisplayInfo()
        {
            try
            {
                // 현재 프로그램 실행 위치
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
                bool bExist = System.IO.File.Exists(filepath);
                if (false == bExist)
                {
                    SaveDefaultDisplayInfo(filepath);
                }
                else
                {
                    bExist = false;
                    string[] sec_names = CUtil.GetSectionNames(filepath);
                    int num_sections = sec_names.Length;
                    for (int i = 0; i < num_sections; i++)
                    {
                        bExist = sec_names[i].Equals(m_szDisInfoSectionName);
                        if (bExist == true)
                            break;
                    }
                    if (false == bExist)
                    {
                        SaveDefaultDisplayInfo(filepath);
                    }
                }

                string defValue;
                StringBuilder retVal1 = new StringBuilder(128);
                int nSize = 128;
                int result1 = 0;
                bool temp2 = false;

                defValue = "1";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKey_Inside_Display_flag, defValue, retVal1, nSize, filepath);
                if (true == bool.TryParse(retVal1.ToString(), out temp2))
                {
                    InsideDisplayFlag = temp2;
                }

                defValue = "1";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKey_Outside_Display_flag, defValue, retVal1, nSize, filepath);
                if (true == bool.TryParse(retVal1.ToString(), out temp2))
                {
                    OutsideDisplayFlag = temp2;
                }

                defValue = "1";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKey_CrossLine_Display_flag, defValue, retVal1, nSize, filepath);
                if (true == bool.TryParse(retVal1.ToString(), out temp2))
                {
                    CrossLineDisplayFlag = temp2;
                }

                defValue = "CIRCLE";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKey_Shape_Type, defValue, retVal1, nSize, filepath);
                if (true == bool.TryParse(retVal1.ToString(), out temp2))
                {
                    ShapeType = temp2.ToString();
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKey_CROSSLINE_THICKNESS, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    CrossLineThickness = result1;
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_THICKNESS, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    InsideCircleThickness = result1;
                }

                defValue = "500";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_LENGTH, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    InsideCircleLength = result1;
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_THICKNESS, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    OutsideCircleThickness = result1;
                }

                defValue = "500";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_LENGTH, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    OutsideCircleLength = result1;
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_THICKNESS, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    InsideSquareThickness = result1;
                }

                defValue = "500";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_LENGTH, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    InsideSquareLength = result1;
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_THICKNESS, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    OutsideSquareThickness = result1;
                }

                 defValue = "500";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_LENGTH, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    OutsideSquareLength = result1;
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_THICKNESS, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    FindSpotThickness = result1;
                }

                defValue = "500";
                result1 = NativeMethods.GetPrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_LENGTH, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    FindSpotLength = result1;
                }
            }
            catch
            {
                MessageBox.Show(new Form
                {
                    TopMost = true
                }, "DISPLAY INFO FILE LOAD FAIL");
            }
        }

        public static void SaveDisplayInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
          
            bool writeResult = false;
            string value;

            value = string.Format("{0}", InsideDisplayFlag);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Inside_Display_flag, value, filepath);

            value = string.Format("{0}", OutsideDisplayFlag);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Outside_Display_flag, value, filepath);

            value = string.Format("{0}", CrossLineDisplayFlag);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_CrossLine_Display_flag, value, filepath);

            value = string.Format("{0}", ShapeType);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_Shape_Type, value, filepath);

            value = string.Format("{0}", CrossLineThickness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKey_CROSSLINE_THICKNESS, value, filepath);

            value = string.Format("{0}", InsideCircleThickness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_THICKNESS, value, filepath);

            value = string.Format("{0}", InsideCircleLength);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_CIRCLE_LENGTH, value, filepath);

            value = string.Format("{0}", OutsideCircleThickness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_THICKNESS, value, filepath);

            value = string.Format("{0}", OutsideCircleLength);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_CIRCLE_LENGTH, value, filepath);

            value = string.Format("{0}", InsideSquareThickness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_THICKNESS, value, filepath);

            value = string.Format("{0}", InsideSquareLength);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_INSIDE_SQUARE_LENGTH, value, filepath);

            value = string.Format("{0}", OutsideSquareThickness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_THICKNESS, value, filepath);

            value = string.Format("{0}", OutsideSquareLength);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_OUTSIDE_SQUARE_LENGTH, value, filepath);

            value = string.Format("{0}", FindSpotThickness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_THICKNESS, value, filepath);

            value = string.Format("{0}", FindSpotLength);
            writeResult = NativeMethods.WritePrivateProfileString(m_szDisInfoSectionName, m_szKEY_FINDSPOT_LENGTH, value, filepath);
        }
    }
}