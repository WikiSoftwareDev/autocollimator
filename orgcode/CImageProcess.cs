﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using OpenCvSharp;
using OpenCvSharp.Blob;
using OpenCvSharp.Extensions;


namespace WikiOptics_Collimator
{
    internal class CImageProcess
    {
        #region variables
        public static bool AnalysisToleranceFlag = false;
        public static bool AnalysisDisplayFlag = false;
        public static int nAnalysisAverage = 0;
        public static int nAnalysisCurrent = 0;
        public static double[] AnalysisAreaSize = new double[3];
        public static double[] sumAnalysisSize = new double[3];
        public static float[] sumAnalysisX = new float[3];
        public static float[] sumAnalysisY = new float[3];
        public static double[] AngleHourX = new double[3];
        public static double[] AngleMinuteX = new double[3];
        public static double[] AngleSecondX = new double[3];
        public static double[] AngleHourY = new double[3];
        public static double[] AngleMinuteY = new double[3];
        public static double[] AngleSecondY = new double[3];
        public static double[] AngleHourXY = new double[3];
        public static double[] AngleMinuteXY = new double[3];
        public static double[] AngleSecondXY = new double[3];
        public static double[] AngleX = new double[3];
        public static double[] AngleY = new double[3];
        public static double[] AngleD = new double[3];
        public static double Angle1to2;
        public static double Angle2to3;
        public static double Angle3to1;
        public static double Angle1to2HourXY;
        public static double Angle1to2MinuteXY;
        public static double Angle1to2SecondXY;
        public static double Angle2to3HourXY;
        public static double Angle2to3MinuteXY;
        public static double Angle2to3SecondXY;
        public static double Angle3to1HourXY;
        public static double Angle3to1MinuteXY;
        public static double Angle3to1SecondXY;
        private static double[,] currentMatrix = null;
        private static Bitmap[] originalBitmap = new Bitmap[3];
        private static Bitmap[] previewBitmap = new Bitmap[3];
        private static Bitmap[] resultBitmap = new Bitmap[3];
        public static Bitmap OriginalBitmap;
        public static IplImage OriginalImage;
        public static IplImage MirrorImage;
        public static IplImage RotationImage;

        public static object DisplayImage_lock = new object();
        public static IplImage DisplayImage = null;

        public static IplImage BlackImage;
        public static IplImage ToleranceImage;
        public static IplImage GrayImage;
        public static IplImage HistoGrayImage = null;
        public static IplImage HistoThresholdImage = null;
        public static IplImage ThresholdImage;
        public static IplImage[] RoiResizeImage = new IplImage[3];
        public static IplImage[] RoiResizeGrayImage = new IplImage[3];
        public static IplImage[] RoiImage = new IplImage[3];
        public static IplImage[] RoiKernelImage = new IplImage[3];
        public static int CalibrationMode;
        public static bool CalibrationFlag = false;
        public static double[] CenterPointX = new double[3];
        public static double[] CenterPointY = new double[3];
        public static int SearchSpotNumber = 0;
        public static bool HistoSearchSpotFlag = false;
        public static int HistoTotalSpotNumber;
       

        public static int HistoXSpotNumber;
        public static int HistoYSpotNumber;

        public static int[] HistoSearchPointX = new int[3];
        public static int[] HistoSearchPointY = new int[3];

        public static int[] HistoSearchStartX = new int[3];
        public static int[] HistoSearchEndX = new int[3];

        public static int[] HistoSearchStartY = new int[3];
        public static int[] HistoSearchEndY = new int[3];

        public static int[] HistoCenterPointX = new int[3];
        public static int[] HistoCenterPointY = new int[3];

        public static int[] HistoCircleXLength = new int[3];
        public static int[] HistoCircleYLength = new int[3];

        public static int[] HistoCircleStartX = new int[3];
        public static int[] HistoCircleEndX = new int[3];

        public static int[] HistoCircleStartY = new int[3];
        public static int[] HistoCircleEndY = new int[3];

        public static float[] AnalysisResultX = new float[3];
        public static float[] AnalysisResultY = new float[3];

        public static double[] SpotDistance = new double[3];

        public static float[] AnalysisPeakX = new float[3];
        public static float[] AnalysisPeakY = new float[3];

        public static float[] AnalysisAreaX = new float[3];
        public static float[] AnalysisAreaY = new float[3];

        public static float[] AnalysisBrightnessX = new float[3];
        public static float[] AnalysisBrightnessY = new float[3];

        public static int AnalysisMaxIntensity;
        public static int nAnalysisMaxIntensity;

        private static float[] xs;
        private static float[] ys;

        public static int FrameTick1;
        public static int FrameTick2;

        public static double FrameFPS;

        public static IplImage WobblePlottingImage = null;

        public static int ZeroPointChangedFlag;

        private static bool[] SpotInZoomFlag = new bool[3];

        public static float[] ZoomX = new float[3];
        public static float[] ZoomY = new float[3];

        #endregion

        public static void CheckCoordinate()
        {
            CvPoint2D32f[] array = new CvPoint2D32f[3];
            CvPoint2D32f cvPoint2D32f = new CvPoint2D32f(CZeroSetInfo.ZeroSetCenterX, CZeroSetInfo.ZeroSetCenterY);
            CvPoint2D32f centerPoint = new CvPoint2D32f(CParameter.CenterXPos, CParameter.CenterYPos);

            // Tool_Coordinate_Original           = 0;
            // Tool_Coordinate_Rotating           = 1;
            // Tool_Coordinate_Mirroring          = 2;
            switch (CSetInfo.Tool_Coordinate)
            {
                case 0:
                    CZeroSetInfo.ZeroSetCenterX = CZeroSetInfo.ZeroSetOriginalCenterX;
                    CZeroSetInfo.ZeroSetCenterY = CZeroSetInfo.ZeroSetOriginalCenterY;
                    return;

                case 1:
                    if (CSetInfo.Tool_Coordinate_Rotating_State == 2)
                    {
                        DisplayImage = FuncRotateImage(centerPoint, OriginalImage, 90.0, 3);
                        if (ZeroPointChangedFlag == 0)
                        {
                            cvPoint2D32f = FuncRotatePosition(centerPoint, cvPoint2D32f, -90.0);
                            CZeroSetInfo.ZeroSetCenterX = cvPoint2D32f.X;
                            CZeroSetInfo.ZeroSetCenterY = cvPoint2D32f.Y;
                            ZeroPointChangedFlag = 1;
                        }
                        for (int i = 0; i < SearchSpotNumber; i++)
                        {
                            array[i].X = AnalysisResultX[i];
                            array[i].Y = AnalysisResultY[i];
                            array[i] = FuncRotatePosition(centerPoint, array[i], -90.0);
                            AnalysisResultX[i] = array[i].X;
                            AnalysisResultY[i] = array[i].Y;
                        }
                    }

                    if (CSetInfo.Tool_Coordinate_Rotating_State == 1)
                    {
                        DisplayImage = FuncRotateImage(centerPoint, OriginalImage, 270.0, 3);
                        if (ZeroPointChangedFlag == 0)
                        {
                            cvPoint2D32f = FuncRotatePosition(centerPoint, cvPoint2D32f, 90.0);
                            CZeroSetInfo.ZeroSetCenterX = cvPoint2D32f.X;
                            CZeroSetInfo.ZeroSetCenterY = cvPoint2D32f.Y;
                            ZeroPointChangedFlag = 1;
                        }
                        for (int j = 0; j < SearchSpotNumber; j++)
                        {
                            array[j].X = AnalysisResultX[j];
                            array[j].Y = AnalysisResultY[j];
                            array[j] = FuncRotatePosition(centerPoint, array[j], 90.0);
                            AnalysisResultX[j] = array[j].X;
                            AnalysisResultY[j] = array[j].Y;
                        }
                    }
                    break;

                case 2:
                    DisplayImage = BlackImage.Clone();
                    if (CSetInfo.Tool_Coordinate_Mirroring_State == 1)
                    {
                        if (ZeroPointChangedFlag == 0)
                        {
                            cvPoint2D32f = FuncMirrorPosition(centerPoint, cvPoint2D32f, "X");
                            CZeroSetInfo.ZeroSetCenterX = cvPoint2D32f.X;
                            CZeroSetInfo.ZeroSetCenterY = cvPoint2D32f.Y;
                            ZeroPointChangedFlag = 1;
                        }
                        for (int k = 0; k < SearchSpotNumber; k++)
                        {
                            array[k].X = AnalysisResultX[k];
                            array[k].Y = AnalysisResultY[k];
                            array[k] = FuncMirrorPosition(centerPoint, array[k], "X");
                            AnalysisResultX[k] = array[k].X;
                            AnalysisResultY[k] = array[k].Y;
                        }
                    }

                    if (CSetInfo.Tool_Coordinate_Mirroring_State == 2)
                    {
                        if (ZeroPointChangedFlag == 0)
                        {
                            cvPoint2D32f = FuncMirrorPosition(centerPoint, cvPoint2D32f, "Y");
                            CZeroSetInfo.ZeroSetCenterX = cvPoint2D32f.X;
                            CZeroSetInfo.ZeroSetCenterY = cvPoint2D32f.Y;
                            ZeroPointChangedFlag = 1;
                        }
                        for (int l = 0; l < SearchSpotNumber; l++)
                        {
                            array[l].X = AnalysisResultX[l];
                            array[l].Y = AnalysisResultY[l];
                            array[l] = FuncMirrorPosition(centerPoint, array[l], "Y");
                            AnalysisResultX[l] = array[l].X;
                            AnalysisResultY[l] = array[l].Y;
                        }
                        return;
                    }
                    break;

                default:
                    return;
            }
        }

        public static void ResetSumAnalysisData()
        {
            nAnalysisCurrent = 0;
            for (int i = 0; i < 3; i++)
            {
                sumAnalysisSize[i] = 0.0;
                sumAnalysisX[i] = 0f;
                sumAnalysisY[i] = 0f;
            }
        }

        public static void CheckCalibration()
        {
            if (nAnalysisCurrent == 0)
            {
                FrameTick1 = Environment.TickCount;
            }

            lock (CImageProcess.DisplayImage_lock)
            {
                DisplayImage = OriginalImage.Clone();
                GetContourHistogramData();
            }

            if (CParameter.FactorySetFlag && CalibrationFlag)
            {
                MessageBox.Show("Stop Analysis or Factory Setting");
                CParameter.FactorySetFlag = false;
                return;
            }

            if (CalibrationFlag || CParameter.FactorySetFlag)
            {
                if (CParameter.FactorySetFlag && CParameter.FactorySetStartFlag)
                {
                    nAnalysisCurrent = 0;
                    CParameter.FactorySetStartFlag = false;
                }

                // 1: Area
                // 2: Brightness
                // 3: Peak
                switch (CalibrationMode)
                {
                    case 1:
                        RoiImageResize();
                        AreaProcess();                            
                        break;

                    case 2:
                        RoiImageResize();
                        BrightnessProcess();
                        break;

                    case 3:
                        PeakProcess();
                        break;
                }

                if (nAnalysisAverage != nAnalysisCurrent)
                {
                    return;
                }

                for (int i = 0; i < SearchSpotNumber; i++)
                {
                    AnalysisResultX[i] = sumAnalysisX[i] / (float)nAnalysisAverage;
                    AnalysisResultY[i] = sumAnalysisY[i] / (float)nAnalysisAverage;
                }

                ResetSumAnalysisData();
                if (CParameter.FactorySetFlag)
                {
                    CalFocalLength();
                }
               //  CalFocalLength();
                CheckCoordinate();
                CalAngle();
                if (CPlotting.WobblePlottingFlag)
                {
                    UpdateWobblePlotting();
                }
                FrameTick2 = Environment.TickCount;
                FrameFPS = (double)((float)(FrameTick2 - FrameTick1) / 1000f);
                FrameFPS = 1.0 / FrameFPS;
                if (CSetInfo.Tool_Zoom_State != 0)
                {
                    if (CheckSpotInZoom())
                    {
                        DisplayImage = BlackImage.Clone();
                        DisplayZoomToleranceView();
                        DisplayToleranceOKNG();
                        DisplayZoomImage();
                    }
                    else
                    {
                        DisplayToleranceView();
                        DisplayNumbering();
                        DisplayToleranceOKNG();
                        DisplayZoomError();
                    }
                }
                else
                {
                    DisplayToleranceView();
                    DisplayNumbering();
                    DisplayToleranceOKNG();
                }
                AnalysisDisplayFlag = true;
            }
        }

        private static void GetZoomPoint(int SpotNo, float AnalysisX, float AnalysisY)
        {
            int tool_Zoom_State = CSetInfo.Tool_Zoom_State;
            if (tool_Zoom_State <= 8)
            {
                if (tool_Zoom_State != 4)
                {
                    if (tool_Zoom_State == 8)
                    {
                        ZoomX[SpotNo] = (AnalysisX - 448f) * 8f;
                        ZoomY[SpotNo] = (AnalysisY - 448f) * 8f;
                    }
                }
                else
                {
                    ZoomX[SpotNo] = (AnalysisX - 384f) * 4f;
                    ZoomY[SpotNo] = (AnalysisY - 384f) * 4f;
                }
            }
            else if (tool_Zoom_State != 16)
            {
                if (tool_Zoom_State == 32)
                {
                    ZoomX[SpotNo] = (AnalysisX - 496f) * 32f;
                    ZoomY[SpotNo] = (AnalysisY - 496f) * 32f;
                }
            }
            else
            {
                ZoomX[SpotNo] = (AnalysisX - 480f) * 16f;
                ZoomY[SpotNo] = (AnalysisY - 480f) * 16f;
            }
        }

        private static void DisplayZoomError()
        {
            double num = 0.0;
            double num2 = 0.0;
            double num3 = 0.0;
            double num4 = 0.0;
            int tool_Zoom_State = CSetInfo.Tool_Zoom_State;
            if (tool_Zoom_State <= 8)
            {
                if (tool_Zoom_State != 4)
                {
                    if (tool_Zoom_State == 8)
                    {
                        num = 448.0;
                        num3 = 448.0;
                        num2 = 576.0;
                        num4 = 576.0;
                    }
                }
                else
                {
                    num = 384.0;
                    num3 = 384.0;
                    num2 = 640.0;
                    num4 = 640.0;
                }
            }
            else if (tool_Zoom_State != 16)
            {
                if (tool_Zoom_State == 32)
                {
                    num = 496.0;
                    num3 = 496.0;
                    num2 = 528.0;
                    num4 = 528.0;
                }
            }
            else
            {
                num = 480.0;
                num3 = 480.0;
                num2 = 544.0;
                num4 = 544.0;
            }
            Cv.DrawRect(DisplayImage, new CvPoint((int)num, (int)num4), new CvPoint((int)num2, (int)num3), new CvScalar(0.0, 0.0, 255.0), 2);
        }

        private static void DisplayZoomImage()
        {
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                if (ZoomX[i] == 0f && ZoomY[i] == 0f)
                {
                    return;
                }
            }
            if (SearchSpotNumber == 1 && SpotInZoomFlag[0])
            {
                CvFont font;
                Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] + 30, (int)ZoomY[0] + 40), font, new CvColor(255, 255, 255));
            }

            if (SearchSpotNumber == 2)
            {
                if (AnalysisResultX[0] < AnalysisResultX[1])
                {
                    CvFont font;
                    Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                    if (SpotInZoomFlag[0])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] - 40, (int)ZoomY[0] + 10), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[1])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] + 20, (int)ZoomY[1] + 10), font, new CvColor(255, 255, 255));
                    }
                }
                else
                {
                    CvFont font;
                    Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                    if (SpotInZoomFlag[0])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] + 20, (int)ZoomY[0] + 10), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[1])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] - 40, (int)ZoomY[1] + 10), font, new CvColor(255, 255, 255));
                    }
                }
            }

            if (SearchSpotNumber == 3)
            {
                CvFont font;
                Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                if (AnalysisResultX[0] < AnalysisResultX[1])
                {
                    if (AnalysisResultX[0] < AnalysisResultX[2])
                    {
                        if (AnalysisResultX[1] < AnalysisResultX[2])
                        {
                            if (SpotInZoomFlag[0])
                            {
                                DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                                Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] - 40, (int)ZoomY[0] + 10), font, new CvColor(255, 255, 255));
                            }
                            if (SpotInZoomFlag[1])
                            {
                                DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                                Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] - 10, (int)ZoomY[1] + 50), font, new CvColor(255, 255, 255));
                            }
                            if (SpotInZoomFlag[2])
                            {
                                DisplayImage.DrawMarker((int)ZoomX[2], (int)ZoomY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                                Cv.PutText(DisplayImage, "3", new CvPoint((int)ZoomX[2] + 20, (int)ZoomY[2] + 10), font, new CvColor(255, 255, 255));
                                return;
                            }
                        }
                        else
                        {
                            if (SpotInZoomFlag[0])
                            {
                                DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                                Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] - 40, (int)ZoomY[0] + 10), font, new CvColor(255, 255, 255));
                            }
                            if (SpotInZoomFlag[1])
                            {
                                DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                                Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] + 20, (int)ZoomY[1] + 10), font, new CvColor(255, 255, 255));
                            }
                            if (SpotInZoomFlag[2])
                            {
                                DisplayImage.DrawMarker((int)ZoomX[2], (int)ZoomY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                                Cv.PutText(DisplayImage, "3", new CvPoint((int)ZoomX[2] - 10, (int)ZoomY[2] + 50), font, new CvColor(255, 255, 255));
                                return;
                            }
                        }
                    }
                    else
                    {
                        if (SpotInZoomFlag[0])
                        {
                            DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                            Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] - 10, (int)ZoomY[0] + 50), font, new CvColor(255, 255, 255));
                        }
                        if (SpotInZoomFlag[1])
                        {
                            DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                            Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] + 20, (int)ZoomY[1] + 10), font, new CvColor(255, 255, 255));
                        }
                        if (SpotInZoomFlag[2])
                        {
                            DisplayImage.DrawMarker((int)ZoomX[2], (int)ZoomY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                            Cv.PutText(DisplayImage, "3", new CvPoint((int)ZoomX[2] - 40, (int)ZoomY[2] + 10), font, new CvColor(255, 255, 255));
                            return;
                        }
                    }
                }
                else if (AnalysisResultX[0] < AnalysisResultX[2])
                {
                    if (SpotInZoomFlag[0])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] - 10, (int)ZoomY[0] + 50), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[1])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] - 40, (int)ZoomY[1] + 10), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[2])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[2], (int)ZoomY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)ZoomX[2] + 20, (int)ZoomY[2] + 10), font, new CvColor(255, 255, 255));
                        return;
                    }
                }
                else if (AnalysisResultX[1] < AnalysisResultX[2])
                {
                    if (SpotInZoomFlag[0])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] + 20, (int)ZoomY[0] + 10), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[1])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] - 10, (int)ZoomY[1] + 50), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[2])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[2], (int)ZoomY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)ZoomX[2] - 40, (int)ZoomY[2] + 10), font, new CvColor(255, 255, 255));
                        return;
                    }
                }
                else
                {
                    if (SpotInZoomFlag[0])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[0], (int)ZoomY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)ZoomX[0] + 20, (int)ZoomY[0] + 10), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[1])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[1], (int)ZoomY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)ZoomX[1] - 10, (int)ZoomY[1] + 50), font, new CvColor(255, 255, 255));
                    }
                    if (SpotInZoomFlag[2])
                    {
                        DisplayImage.DrawMarker((int)ZoomX[2], (int)ZoomY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)ZoomX[2] - 40, (int)ZoomY[2] + 10), font, new CvColor(255, 255, 255));
                    }
                }
            }
        }

        private static bool CheckSpotInZoom()
        {
            double num = 0.0;
            double num2 = 0.0;
            double num3 = 0.0;
            double num4 = 0.0;
            int tool_Zoom_State = CSetInfo.Tool_Zoom_State;
            if (tool_Zoom_State <= 8)
            {
                if (tool_Zoom_State != 4)
                {
                    if (tool_Zoom_State == 8)
                    {
                        num = 448.0;
                        num3 = 448.0;
                        num2 = 576.0;
                        num4 = 576.0;
                    }
                }
                else
                {
                    num = 384.0;
                    num3 = 384.0;
                    num2 = 640.0;
                    num4 = 640.0;
                }
            }
            else if (tool_Zoom_State != 16)
            {
                if (tool_Zoom_State == 32)
                {
                    num = 496.0;
                    num3 = 496.0;
                    num2 = 528.0;
                    num4 = 528.0;
                }
            }
            else
            {
                num = 480.0;
                num3 = 480.0;
                num2 = 544.0;
                num4 = 544.0;
            }
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                SpotInZoomFlag[i] = true;
                if ((double)AnalysisResultX[i] < num || (double)AnalysisResultX[i] > num2)
                {
                    SpotInZoomFlag[i] = false;
                }
                if ((double)AnalysisResultY[i] < num3 || (double)AnalysisResultY[i] > num4)
                {
                    SpotInZoomFlag[i] = false;
                }
            }
            int num5 = 0;
            for (int j = 0; j < SearchSpotNumber; j++)
            {
                if (SpotInZoomFlag[j])
                {
                    GetZoomPoint(j, AnalysisResultX[j], AnalysisResultY[j]);
                    num5++;
                }
            }
            return num5 != 0;
        }

        /*
        public static void GetHistogramData()
        {
            if (HistoGrayImage != null)
            {
                HistoGrayImage.Dispose();
                HistoGrayImage = null;
            }

            HistoGrayImage = new OpenCvSharp.IplImage(OriginalImage.Size, OpenCvSharp.BitDepth.U8, 1);
            OriginalImage.CvtColor(HistoGrayImage, OpenCvSharp.ColorConversion.BgrToGray);
            int tickCount = System.Environment.TickCount;
            int[] array = new int[CamParamInfo.ImageWidth];
            int[] array2 = new int[CamParamInfo.ImageHeight];
            int num = 0;
            double num2 = 0.0;            
            int num3 = 0;

            IntPtr HisoGrayData= HistoGrayImage.ImageData;

            while (num3 < CamParamInfo.ImageWidth)
            {
                int num4 = 0;
                int widthStep = HistoGrayImage.WidthStep;
                byte b = Marshal.ReadByte(HisoGrayData, widthStep);

                num2 = (int)b;                
                while (num4 < CamParamInfo.ImageHeight)
                {
                    widthStep = HistoGrayImage.WidthStep * num4 + num3;
                    b = Marshal.ReadByte(HisoGrayData, widthStep);
                    if (num2 < (double)(int)b)
                    {
                        num2 = (int)b;
                    }
                    num4++;
                }
                array[num3] = (int)num2;
                num3++;
            }

            int num5 = 0;
            while (num5 < CamParamInfo.ImageHeight)
            {
                int widthStep = HistoGrayImage.WidthStep;
                byte b = Marshal.ReadByte(HisoGrayData, widthStep);
                int num6 = 0;
                num = b;

                while (num6 < CamParamInfo.ImageWidth)
                {
                    widthStep = HistoGrayImage.WidthStep * num5 + num6;
                    b = Marshal.ReadByte(HisoGrayData, widthStep);
                    if (num < b)
                    {
                        num = b;
                    }
                    num6++;
                }
                array2[num5] = num;
                num5++;
            }

            SearchSpotNumber = 2;

            int num7 = 0;
            int num8 = 0;
            int num9 = 0;
            int num10 = 10;

            HistoXSpotNumber = 0;
            HistoSearchSpotFlag = false;
            
            while (num10 < CamParamInfo.ImageWidth - 10)
            {
                switch (num9)
                {
                    case 2:
                        HistoSearchPointX[HistoXSpotNumber] = (num8 + num7) / 2;
                        HistoCircleXLength[HistoXSpotNumber] = num8 - num7;
                        HistoSearchStartX[HistoXSpotNumber] = num7;
                        HistoSearchEndX[HistoXSpotNumber] = num8;
                        HistoXSpotNumber++;
                        if (SearchSpotNumber != HistoXSpotNumber)
                        {
                            num9 = 0;
                        }
                        break;
                }
                if (SearchSpotNumber == HistoXSpotNumber)
                {
                    HistoSearchSpotFlag = true;
                    break;
                }
                num10++;
            } // while (num10 < CamParamInfo.ImageWidth - 10)

            num9 = 0;
            HistoYSpotNumber = 0;
            int num11 = 10;
            while (num11 < CamParamInfo.ImageHeight - 10)
            {
                switch (num9)
                {
                    case 2:
                        HistoSearchPointY[HistoYSpotNumber] = (num8 + num7) / 2;
                        HistoCircleYLength[HistoYSpotNumber] = num8 - num7 + 1;
                        HistoSearchStartY[HistoYSpotNumber] = num7;
                        HistoSearchEndY[HistoYSpotNumber] = num8;
                        HistoYSpotNumber++;
                        if (SearchSpotNumber != HistoYSpotNumber)
                        {
                            num9 = 0;
                        }
                        break;
                }
                if (SearchSpotNumber == HistoYSpotNumber)
                {
                    break;
                }
                num11++;
            }
            int num12 = 0;
            int num13 = 0;
            while (num13 < HistoXSpotNumber)
            {
                int num14 = 0;
                while (num14 < HistoYSpotNumber)
                {
                    int num15 = HistoSearchStartX[num13];
                    while (num15 < HistoSearchEndX[num13])
                    {
                        int num16 = HistoSearchStartY[num14];
                        while (num16 < HistoSearchEndY[num14])
                        {
                            num16++;
                        }
                        num15++;
                    }
                    int num17 = HistoSearchStartY[num13];
                    while (num17 < HistoSearchEndY[num13])
                    {
                        int num18 = HistoSearchStartX[num14];
                        while (num18 < HistoSearchEndX[num14])
                        {
                            num18++;
                        }
                        num17++;
                    }
                    if ((double)HistoGrayImage[HistoSearchPointY[num14], HistoSearchPointX[num13]] > 100.0)
                    {
                        HistoCenterPointX[num12] = HistoSearchPointX[num13];
                        HistoCenterPointY[num12] = HistoSearchPointY[num14];
                        HistoCircleStartX[num12] = HistoSearchStartX[num13];
                        HistoCircleEndX[num12] = HistoSearchEndX[num13];
                        HistoCircleStartY[num12] = HistoSearchStartY[num14];
                        HistoCircleEndY[num12] = HistoSearchEndY[num14];
                        num12++;
                    }
                    num14++;
                }
                num13++;
            }
            HistoTotalSpotNumber = num12;
        }

        public static void GetMinROIHistogramData()
        {
            if (HistoGrayImage != null)
            {
                HistoGrayImage.Dispose();
            }

            if (HistoThresholdImage != null)
            {
                HistoThresholdImage.Dispose();
            }

            HistoGrayImage = new IplImage(OriginalImage.Size, BitDepth.U8, 1);
            OriginalImage.CvtColor(HistoGrayImage, ColorConversion.BgrToGray);
            HistoThresholdImage = new IplImage(OriginalImage.Size, BitDepth.U8, 1);
            CvMemStorage circleStorage = new CvMemStorage();
            CvSeq<CvCircleSegment> cvSeq = HistoGrayImage.HoughCircles(circleStorage, HoughCirclesMethod.Gradient, 1.0, 3.0, 50.0, 10.0, 2, 50);
            int num = 0;
            foreach (CvCircleSegment? cvCircleSegment in cvSeq)
            {
                CvCircleSegment value = cvCircleSegment.Value;
                HistoCenterPointX[num] = (int)value.Center.X;
                HistoCenterPointY[num] = (int)value.Center.Y;
                HistoCircleStartX[num] = (int)(value.Center.X - value.Radius - 1f);
                HistoCircleEndX[num] = (int)(value.Center.X + value.Radius + 1f);
                HistoCircleStartY[num] = (int)(value.Center.Y - value.Radius - 1f);
                HistoCircleEndY[num] = (int)(value.Center.Y + value.Radius + 1f);
                num++;
            }
            HistoTotalSpotNumber = num;
            if (HistoTotalSpotNumber == 3)
            {
                MessageBox.Show(HistoTotalSpotNumber.ToString());
            }
        }

        public static void GetBlobHistogramData()
        {
            CvBlobs cvBlobs = new CvBlobs();
            cvBlobs.FilterByArea(30, 1000);
            cvBlobs.Label(HistoThresholdImage);
            foreach (KeyValuePair<int, CvBlob> keyValuePair in cvBlobs)
            {
                int key = keyValuePair.Key;
                CvBlob value = keyValuePair.Value

                Console.WriteLine(value.Rect);
                HistoThresholdImage.DrawRect(value.Rect, new CvScalar(255.0, 255.0, 255.0));
            }
            Cv.ShowImage("blob", HistoThresholdImage);
            cvBlobs.Clear();
        }
        */

        public static void GetContourHistogramData()
        {
            int tickCount = Environment.TickCount;
            float[] array = new float[]
            {
                0f,
                1280f
            };
            HistoGrayImage = new IplImage(OriginalImage.Size, BitDepth.U8, 1);
            HistoThresholdImage = new IplImage(OriginalImage.Size, BitDepth.U8, 1);
            OriginalImage.CvtColor(HistoGrayImage, ColorConversion.BgrToGray);
            for (int i = 0; i < HistoGrayImage.Width; i++)
            {
                HistoGrayImage[0, i] = 0.0;
                HistoGrayImage[HistoGrayImage.Height - 1, i] = 0.0;
            }
            for (int j = 0; j < HistoGrayImage.Height; j++)
            {
                HistoGrayImage[j, 0] = 0.0;
                HistoGrayImage[j, HistoGrayImage.Width - 1] = 0.0;
            }
            if (CParameter.FactorySetFlag)
            {
                HistoGrayImage.Threshold(HistoThresholdImage, (double)CParameter.ReferenceThreshold, 255.0, ThresholdType.Binary);
            }
            else
            {
                HistoGrayImage.Threshold(HistoThresholdImage, (double)CSetInfo.Setting_Gray_MinLevel, 255.0, ThresholdType.Binary);
            }

            CvMemStorage storage = new CvMemStorage();
            IntPtr HisoGrayData = HistoGrayImage.ImageData;
            CvSeq<CvPoint> cvSeq;
            int num = Cv.FindContours(HistoThresholdImage, storage, out cvSeq, CvContour.SizeOf, ContourRetrieval.External, ContourChain.ApproxNone);
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            int num5 = 0;
            int[] array2 = new int[num];
            int[] array3 = new int[num];
            int[] array4 = new int[num];
            int[] array5 = new int[num];
            double[] array6 = new double[num];
            double[] array7 = new double[num];
            CvSeq<CvPoint> cvSeq2 = cvSeq;
            int num6 = 0;
            while (cvSeq2 != null)
            {
                if (Math.Abs(Cv.ContourArea(cvSeq2)) > 20.0)
                {
                    array6[num2] = Cv.ContourArea(cvSeq2);
                    array7[num2] = array6[num2];
                    array2[num2] = Cv.GetSeqElem<CvPoint>(cvSeq2, 0).Value.X;   // sx
                    array3[num2] = Cv.GetSeqElem<CvPoint>(cvSeq2, 0).Value.X;   // ex ,, index가 틀렸다. 리버스 오류로 보인다.
                    array4[num2] = Cv.GetSeqElem<CvPoint>(cvSeq2, 0).Value.Y;   // sy
                    array5[num2] = Cv.GetSeqElem<CvPoint>(cvSeq2, 0).Value.Y;   // ey ,, index가 틀렸다. 리버스 오류로 보인다.

                    for (int k = 0; k < cvSeq2.Total; k++)
                    {
                        CvPoint? seqElem = Cv.GetSeqElem<CvPoint>(cvSeq2, k);
                        num3 += seqElem.Value.X;
                        num4 += seqElem.Value.Y;
                        num5++;
                        if (array2[num2] > seqElem.Value.X)
                        {
                            array2[num2] = seqElem.Value.X;
                        }
                        if (array4[num2] > seqElem.Value.Y)
                        {
                            array4[num2] = seqElem.Value.Y;
                        }
                        if (array3[num2] < seqElem.Value.X)
                        {
                            array3[num2] = seqElem.Value.X;
                        }
                        if (array5[num2] < seqElem.Value.Y)
                        {
                            array5[num2] = seqElem.Value.Y;
                        }
                    }
                    num2++;
                }
                cvSeq2 = cvSeq2.HNext;
                num6++;
            }
            SearchSpotNumber = num2;
            bool flag = true;

            for (;;)
            {   // spot중 면적이 큰 것을 앞으로 보낸다
                for (int l = 0; l < SearchSpotNumber - 1; l++)
                {
                    if (array6[l] > array6[l + 1])
                    {
                        int num7 = array2[l];
                        int num8 = array3[l];
                        int num9 = array4[l];
                        int num10 = array5[l];
                        double num11 = array6[l];
                        array2[l] = array2[l + 1];
                        array2[l + 1] = num7;
                        array3[l] = array3[l + 1];
                        array3[l + 1] = num8;
                        array4[l] = array4[l + 1];
                        array4[l + 1] = num9;
                        array5[l] = array5[l + 1];
                        array5[l + 1] = num10;
                        array6[l] = array6[l + 1];
                        array6[l + 1] = num11;
                        flag = false;
                    }
                }
                if (flag)
                {
                    break;
                }
                flag = true;
            }
            flag = true;
            if (SearchSpotNumber > 3)
            {
                SearchSpotNumber = 3;
            }
            for (int m = 0; m < SearchSpotNumber; m++)
            {
                AnalysisAreaSize[m] = Math.Abs(array6[m]);
                HistoCircleStartX[m] = array2[m];
                HistoCircleEndX[m] = array3[m];
                HistoCircleStartY[m] = array4[m];
                HistoCircleEndY[m] = array5[m];
            }

            for (;;)
            {
                for (int n = 0; n < SearchSpotNumber - 1; n++)
                {
                    // x 좌표 위치가 작은 spot을 앞으로 이동 시킨다
                    if (HistoCircleStartX[n] > HistoCircleStartX[n + 1]
                        &&
                        Math.Abs(HistoCircleStartX[n] - HistoCircleStartX[n + 1]) > 10)
                    {
                        int num7 = HistoCircleStartX[n];
                        int num8 = HistoCircleEndX[n];
                        int num9 = HistoCircleStartY[n];
                        int num10 = HistoCircleEndY[n];
                        double num11 = AnalysisAreaSize[n];
                        HistoCircleStartX[n] = HistoCircleStartX[n + 1];
                        HistoCircleStartX[n + 1] = num7;
                        HistoCircleEndX[n] = HistoCircleEndX[n + 1];
                        HistoCircleEndX[n + 1] = num8;
                        HistoCircleStartY[n] = HistoCircleStartY[n + 1];
                        HistoCircleStartY[n + 1] = num9;
                        HistoCircleEndY[n] = HistoCircleEndY[n + 1];
                        HistoCircleEndY[n + 1] = num10;
                        AnalysisAreaSize[n] = AnalysisAreaSize[n + 1];
                        AnalysisAreaSize[n + 1] = num11;
                        flag = false;
                    }
                }
                if (flag)
                {
                    break;
                }
                flag = true;
            }
            flag = true;
            for (;;)
            {
                for (int num12 = 0; num12 < SearchSpotNumber - 1; num12++)
                {
                    // y 좌표 위치가 작은 spot을 앞으로 이동 시킨다
                    if (HistoCircleStartY[num12] > HistoCircleStartY[num12 + 1]
                        &&
                        Math.Abs(HistoCircleStartY[num12] - HistoCircleStartY[num12 + 1]) > 10)
                    {
                        int num7 = HistoCircleStartX[num12];
                        int num8 = HistoCircleEndX[num12];
                        int num9 = HistoCircleStartY[num12];
                        int num10 = HistoCircleEndY[num12];
                        double num11 = AnalysisAreaSize[num12];
                        HistoCircleStartX[num12] = HistoCircleStartX[num12 + 1];
                        HistoCircleStartX[num12 + 1] = num7;
                        HistoCircleEndX[num12] = HistoCircleEndX[num12 + 1];
                        HistoCircleEndX[num12 + 1] = num8;
                        HistoCircleStartY[num12] = HistoCircleStartY[num12 + 1];
                        HistoCircleStartY[num12 + 1] = num9;
                        HistoCircleEndY[num12] = HistoCircleEndY[num12 + 1];
                        HistoCircleEndY[num12 + 1] = num10;
                        AnalysisAreaSize[num12] = AnalysisAreaSize[num12 + 1];
                        AnalysisAreaSize[num12 + 1] = num11;
                        flag = false;
                    }
                }
                if (flag)
                {
                    break;
                }
                flag = true;
            }
            for (int num13 = 0; num13 < SearchSpotNumber; num13++)
            {
                int num14 = 0;
                for (int num15 = HistoCircleStartX[num13]; num15 < HistoCircleEndX[num13] + 1; num15++)
                {
                    for (int num16 = HistoCircleStartY[num13]; num16 < HistoCircleEndY[num13] + 1; num16++)
                    {
                        int ofs = HistoGrayImage.WidthStep * num16 + num15;
                        int num17 = (int)Marshal.ReadByte(HisoGrayData, ofs);
                        if (num14 < num17)
                        {
                            num14 = num17;
                        }
                    }
                }
                int num18 = 0;
                int num19 = 0;
                int num20 = 0;
                for (int num21 = HistoCircleStartX[num13]; num21 < HistoCircleEndX[num13] + 1; num21++)
                {
                    for (int num22 = HistoCircleStartY[num13]; num22 < HistoCircleEndY[num13] + 1; num22++)
                    {
                        int ofs = HistoGrayImage.WidthStep * num22 + num21;
                        int num17 = (int)Marshal.ReadByte(HisoGrayData, ofs);
                        if (num14 == num17)
                        {
                            num18 += num21;
                            num19 += num22;
                            num20++;
                        }
                    }
                }
                HistoCenterPointX[num13] = num18 / num20;
                HistoCenterPointY[num13] = num19 / num20;
            }
            HistoTotalSpotNumber = SearchSpotNumber;
        }

        public static void RoiImageResize()
        {
            AnalysisMaxIntensity = 0;
            nAnalysisMaxIntensity = 0;
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                int num = HistoCircleEndX[i] - HistoCircleStartX[i];
                int num2 = HistoCircleEndY[i] - HistoCircleStartY[i];
                if (RoiImage[i] != null)
                {
                    RoiImage[i].ReleaseData();
                    RoiImage[i].Dispose();
                }
                if (RoiResizeImage[i] != null)
                {
                    RoiResizeImage[i].ReleaseData();
                    RoiResizeImage[i].Dispose();
                }

                if (RoiResizeGrayImage[i] != null)
                {
                    RoiResizeGrayImage[i].ReleaseData();
                    RoiResizeGrayImage[i].Dispose();
                }

                RoiImage[i] = new IplImage(num, num2, BitDepth.U8, 1);
                HistoGrayImage.SetROI(HistoCircleStartX[i], HistoCircleStartY[i], num, num2);
                Cv.Copy(HistoGrayImage, RoiImage[i]);
                for (int j = 0; j < RoiImage[i].Width; j++)
                {
                    for (int k = 0; k < RoiImage[i].Height; k++)
                    {
                        int num3 = (int)RoiImage[i][k, j];
                        if (num3 > AnalysisMaxIntensity)
                        {
                            AnalysisMaxIntensity = num3;
                            nAnalysisMaxIntensity++;
                        }
                    }
                }

                RoiResizeImage[i] = new IplImage(num * 10, num2 * 10, BitDepth.U8, 3);
                originalBitmap[i] = new Bitmap(RoiImage[i].ToBitmap());
                previewBitmap[i] = originalBitmap[i].CopyToSquareCanvas(num * 10, num2 * 10);
                currentMatrix = CMatrixCalculator.Calculate(5, 1.0);
                resultBitmap[i] = previewBitmap[i].ConvolutionFilter(currentMatrix, 1.0, 0);
                RoiResizeImage[i].CopyFrom(resultBitmap[i]);
                RoiResizeGrayImage[i] = new IplImage(RoiResizeImage[i].Size, BitDepth.U8, 1);
                RoiResizeImage[i].CvtColor(RoiResizeGrayImage[i], ColorConversion.BgrToGray);
            }
        }

        public static void AreaProcess()
        {
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                int num = 0;
                int num2 = 0;
                int num3 = 0;
                for (int j = 0; j < RoiResizeImage[i].Width; j++)
                {
                    for (int k = 0; k < RoiResizeImage[i].Height; k++)
                    {
                        int num4 = (int)RoiResizeImage[i][k, j];
                        if (num4 > CSetInfo.Setting_Gray_MinLevel)
                        {
                            num3++;
                            num += j;
                            num2 += k;
                        }
                    }
                }
                AnalysisAreaX[i] = (float)HistoCircleStartX[i] + (float)(num / num3) / 10f;
                AnalysisAreaY[i] = (float)HistoCircleStartY[i] + (float)(num2 / num3) / 10f;
            }
            if (nAnalysisCurrent < nAnalysisAverage + 1)
            {
                for (int l = 0; l < SearchSpotNumber; l++)
                {
                    sumAnalysisSize[l] += AnalysisAreaSize[l];
                    sumAnalysisX[l] += AnalysisAreaX[l];
                    sumAnalysisY[l] += AnalysisAreaY[l];
                }
                nAnalysisCurrent++;
            }
        }

        public static void BrightnessProcess()
        {
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                int num = 0;
                int num2 = 0;
                int num3 = 0;
                int num4 = 0;
                int num5 = 0;
                int num6 = 0;
                for (int j = 0; j < RoiResizeImage[i].Width; j++)
                {
                    for (int k = 0; k < RoiResizeImage[i].Height; k++)
                    {
                        int num7 = (int)RoiResizeImage[i][k, j];
                        if (num7 > CSetInfo.Setting_Gray_MinLevel)
                        {
                            num += j * (int)RoiResizeImage[i][k, j];
                            num2 += k * (int)RoiResizeImage[i][k, j];
                            num5 += (int)RoiResizeImage[i][k, j];
                        }
                    }
                }
                AnalysisBrightnessX[i] = (float)HistoCircleStartX[i] + (float)num / (float)num5 / 10f;
                AnalysisBrightnessY[i] = (float)HistoCircleStartY[i] + (float)num2 / (float)num5 / 10f;
            }
            if (nAnalysisCurrent < nAnalysisAverage + 1)
            {
                for (int l = 0; l < SearchSpotNumber; l++)
                {
                    sumAnalysisSize[l] += AnalysisAreaSize[l];
                    sumAnalysisX[l] += AnalysisBrightnessX[l];
                    sumAnalysisY[l] += AnalysisBrightnessY[l];
                }
                nAnalysisCurrent++;
            }
        }

        public static void PeakProcess()
        {
            string debg_msg;
            AnalysisMaxIntensity = 0;
            nAnalysisMaxIntensity = 0;
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                int num = HistoCircleEndX[i] - HistoCircleStartX[i] + 1;
                float[] array = new float[num];
                float[] array2 = new float[num];
                int num2 = num * CParameter.SplineSampleFactor;
                xs = new float[num2];

                //debg_msg = string.Format("[wiki] PeakProcess : number of pixels = {0}", num);
                //Trace.WriteLine(debg_msg);

                for (int j = 0; j < num; j++)
                {
                    array[j] = (float)(HistoCircleStartX[i] + j);
                    array2[j] = (float)HistoGrayImage[HistoCenterPointY[i], (int)array[j]];
                }
                for (int k = 0; k < num2; k++)
                {
                    xs[k] = (float)HistoCircleStartX[i] + (float)k * (float)(num - 1) / (float)(num2 - 1);
                }
                ys = CSpline.CubicSpline.Compute(array, array2, xs, 0f, float.NaN, false);
                float num3 = ys.Max();
                AnalysisPeakX[i] = xs[ys.ToList<float>().IndexOf(num3)];
                if (num3 > (float)AnalysisMaxIntensity)
                {
                    AnalysisMaxIntensity = (int)num3;
                    nAnalysisMaxIntensity++;
                }
                num = HistoCircleEndY[i] - HistoCircleStartY[i] + 1;
                array = new float[num];
                array2 = new float[num];
                num2 = num * CParameter.SplineSampleFactor;
                xs = new float[num2];
                for (int l = 0; l < num; l++)
                {
                    array[l] = (float)(HistoCircleStartY[i] + l);
                    array2[l] = (float)HistoGrayImage[(int)array[l], HistoCenterPointX[i]];
                }
                for (int m = 0; m < num2; m++)
                {
                    xs[m] = (float)HistoCircleStartY[i] + (float)m * (float)(num - 1) / (float)(num2 - 1);
                }
                ys = CSpline.CubicSpline.Compute(array, array2, xs, 0f, float.NaN, false);
                num3 = ys.Max();
                if (num3 > (float)AnalysisMaxIntensity)
                {
                    AnalysisMaxIntensity = (int)num3;
                }
                AnalysisPeakY[i] = xs[ys.ToList<float>().IndexOf(num3)];
            }
            if (nAnalysisCurrent < nAnalysisAverage + 1)
            {
                for (int n = 0; n < SearchSpotNumber; n++)
                {
                    sumAnalysisSize[n] += AnalysisAreaSize[n];
                    sumAnalysisX[n] += AnalysisPeakX[n];
                    sumAnalysisY[n] += AnalysisPeakY[n];
                }
                nAnalysisCurrent++;
            }
        }

        public static void RoiPeakProcess()
        {
            for (int i = 0; i < HistoTotalSpotNumber; i++)
            {
                int[] array = new int[RoiResizeGrayImage[i].Width];
                int num = 0;
                int num2 = 0;
                int num3 = 0;
                int num4 = 0;
                float num6;
                float num7;
                for (int j = 0; j < RoiResizeGrayImage[i].Width; j++)
                {
                    int num5 = 0;
                    for (int k = 0; k < RoiResizeGrayImage[i].Height; k++)
                    {
                        if (num5 < (int)RoiResizeGrayImage[i][k, j])
                        {
                            num5 = (int)RoiResizeGrayImage[i][k, j];
                        }
                        if (num < (int)RoiResizeGrayImage[i][k, j])
                        {
                            num = (int)RoiResizeGrayImage[i][k, j];
                            num6 = (float)j;
                            num7 = (float)k;
                        }
                    }
                    array[j] = num5;
                }
                for (int l = 0; l < RoiResizeGrayImage[i].Width; l++)
                {
                    for (int m = 0; m < RoiResizeGrayImage[i].Height; m++)
                    {
                        if (num == (int)RoiResizeGrayImage[i][m, l])
                        {
                            num2 += l;
                            num3 += m;
                            num4++;
                        }
                    }
                }
                num6 = (float)num2 / (float)num4;
                num7 = (float)num3 / (float)num4;
                AnalysisPeakX[i] = (float)HistoCircleStartX[i] + num6 / 10f;
                AnalysisPeakY[i] = (float)HistoCircleStartY[i] + num7 / 10f;
            }
            DisplayImage.DrawMarker(640, 512, CvColor.Blue, OpenCvSharp.MarkerStyle.Cross, 2000, LineType.AntiAlias, 2);
            CalAngle();
        }

        private static void CalFocalLength()
        {
           if (SearchSpotNumber != 2)
           {
               CParameter.FactorySetFlag = false;
               MessageBox.Show("Please, Check SpotNo~!");
             return;
           }
            CParameter.FocalLength = Math.Sqrt(Math.Pow((double)(AnalysisResultX[1] - AnalysisResultX[0]), 2.0) + Math.Pow((double)(AnalysisResultY[1] - AnalysisResultY[0]), 2.0)) * CParameter.PixelPerDistance / Math.Tan(Math.PI * (CParameter.ReferenceAngle * (double)CParameter.AlphaValue) / 180.0);
            CParameter.CenterXPos = AnalysisResultX[0];
            CParameter.CenterYPos = AnalysisResultY[0];
            CParameter.TiltAngle = Math.Atan((double)((AnalysisResultY[1] - AnalysisResultY[0]) / (AnalysisResultX[1] - AnalysisResultX[0]))) * 180.0 / Math.PI;
            CParameter.FactorySetFlag = false;
        }

        private static void DisplayNumbering()
        {
            for (int i = 0; i < SearchSpotNumber; i++)
            {
                if (AnalysisResultX[i] == 0f && AnalysisResultY[i] == 0f)
                {
                    return;
                }
            }

            CvFont font;
            Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
            if (SearchSpotNumber == 1)
            {
                //Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] + 40, (int)AnalysisResultY[0] + 40), font, new CvColor(255, 255, 255));
            }
            if (SearchSpotNumber == 2)
            {
                if (AnalysisResultX[0] < AnalysisResultX[1])
                {
                    //Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                    DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] - 40, (int)AnalysisResultY[0] + 10), font, new CvColor(255, 255, 255));
                    DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] + 20, (int)AnalysisResultY[1] + 10), font, new CvColor(255, 255, 255));
                }
                else
                {
                    //Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
                    DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] + 20, (int)AnalysisResultY[0] + 10), font, new CvColor(255, 255, 255));
                    DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] - 40, (int)AnalysisResultY[1] + 10), font, new CvColor(255, 255, 255));
                }
            }
            //Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 3);
            if (SearchSpotNumber == 3)
            {
                if (AnalysisResultX[0] < AnalysisResultX[1])
                {
                    if (AnalysisResultX[0] >= AnalysisResultX[2])
                    {
                        DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] - 10, (int)AnalysisResultY[0] + 50), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] + 20, (int)AnalysisResultY[1] + 10), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[2], (int)AnalysisResultY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)AnalysisResultX[2] - 40, (int)AnalysisResultY[2] + 10), font, new CvColor(255, 255, 255));
                        font.Dispose();
                        return;
                    }
                    if (AnalysisResultX[1] < AnalysisResultX[2])
                    {
                        DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] - 40, (int)AnalysisResultY[0] + 10), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] - 10, (int)AnalysisResultY[1] + 50), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[2], (int)AnalysisResultY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)AnalysisResultX[2] + 20, (int)AnalysisResultY[2] + 10), font, new CvColor(255, 255, 255));
                        font.Dispose();
                        return;
                    }
                    DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] - 40, (int)AnalysisResultY[0] + 10), font, new CvColor(255, 255, 255));
                    DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] + 20, (int)AnalysisResultY[1] + 10), font, new CvColor(255, 255, 255));
                    DisplayImage.DrawMarker((int)AnalysisResultX[2], (int)AnalysisResultY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "3", new CvPoint((int)AnalysisResultX[2] - 10, (int)AnalysisResultY[2] + 50), font, new CvColor(255, 255, 255));
                    font.Dispose();
                    return;
                }
                else
                {
                    if (AnalysisResultX[0] < AnalysisResultX[2])
                    {
                        DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] - 10, (int)AnalysisResultY[0] + 50), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] - 40, (int)AnalysisResultY[1] + 10), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[2], (int)AnalysisResultY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)AnalysisResultX[2] + 20, (int)AnalysisResultY[2] + 10), font, new CvColor(255, 255, 255));
                        font.Dispose();
                        return;
                    }
                    if (AnalysisResultX[1] < AnalysisResultX[2])
                    {
                        DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] + 20, (int)AnalysisResultY[0] + 10), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] - 10, (int)AnalysisResultY[1] + 50), font, new CvColor(255, 255, 255));
                        DisplayImage.DrawMarker((int)AnalysisResultX[2], (int)AnalysisResultY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                        Cv.PutText(DisplayImage, "3", new CvPoint((int)AnalysisResultX[2] - 40, (int)AnalysisResultY[2] + 10), font, new CvColor(255, 255, 255));
                        font.Dispose();
                        return;
                    }
                    DisplayImage.DrawMarker((int)AnalysisResultX[0], (int)AnalysisResultY[0], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "1", new CvPoint((int)AnalysisResultX[0] + 20, (int)AnalysisResultY[0] + 10), font, new CvColor(255, 255, 255));
                    DisplayImage.DrawMarker((int)AnalysisResultX[1], (int)AnalysisResultY[1], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "2", new CvPoint((int)AnalysisResultX[1] - 10, (int)AnalysisResultY[1] + 50), font, new CvColor(255, 255, 255));
                    DisplayImage.DrawMarker((int)AnalysisResultX[2], (int)AnalysisResultY[2], CvColor.Red, OpenCvSharp.MarkerStyle.Cross, 20, LineType.AntiAlias, 2);
                    Cv.PutText(DisplayImage, "3", new CvPoint((int)AnalysisResultX[2] - 40, (int)AnalysisResultY[2] + 10), font, new CvColor(255, 255, 255));
                }
            }

            font.Dispose();
        }

        private static void DisplayToleranceOKNG()
        {
            CvFont font;
            bool flag = true;
            double num = (double)CZeroSetInfo.ZeroSetCenterX - CZeroSetInfo.ToleranceDistance;
            double num2 = (double)CZeroSetInfo.ZeroSetCenterX + CZeroSetInfo.ToleranceDistance;
            double num3 = (double)CZeroSetInfo.ZeroSetCenterY - CZeroSetInfo.ToleranceDistance;
            double num4 = (double)CZeroSetInfo.ZeroSetCenterY + CZeroSetInfo.ToleranceDistance;

            if (SearchSpotNumber == 0)
            {
                return;
            }

            for (int i = 0; i < SearchSpotNumber; i++)
            {
                if (AnalysisResultX[i] == 0f && AnalysisResultY[i] == 0f)
                {
                    return;
                }
            }

            if (CSetInfo.Setting_Tolerance_Mode == 1)
            {
                for (int j = 0; j < SearchSpotNumber; j++)
                {
                    double num5 = Math.Sqrt(Math.Pow((double)(CZeroSetInfo.ZeroSetCenterX - AnalysisResultX[j]), 2.0) + Math.Pow((double)(CZeroSetInfo.ZeroSetCenterY - AnalysisResultY[j]), 2.0));
                    if (num5 > CZeroSetInfo.ToleranceDistance)
                    {
                        flag = false;
                        break;
                    }
                }
            }
            else
            {
                for (int k = 0; k < SearchSpotNumber; k++)
                {
                    if ((double)AnalysisResultX[k] < num || (double)AnalysisResultX[k] > num2)
                    {
                        flag = false;
                        break;
                    }
                    if ((double)AnalysisResultY[k] < num3 || (double)AnalysisResultY[k] > num4)
                    {
                        flag = false;
                        break;
                    }
                }
            }

            Cv.InitFont(out font, FontFace.HersheySimplex, 2.0, 2.0, 0.0, 5);
            if (flag)
            {
                AnalysisToleranceFlag = true;
                Cv.PutText(DisplayImage, "OK", new CvPoint(900, 970), font, new CvColor(0, 255, 0));
                return;
            }
            AnalysisToleranceFlag = false;
            Cv.PutText(DisplayImage, "NG", new CvPoint(900, 970), font, new CvColor(255, 0, 0));
            font.Dispose();
        }

        private static void DisplayZoomToleranceView()
        {
            CvFont font;
            
            double num = 0.0;
            double num2 = 0.0;
            double num3 = 0.0;
            double num4 = 0.0;
            double num5 = 0.0;
            double num6 = 0.0;
            double num7 = 0.0;
            double num8 = 0.0;
            double num9 = 0.0;
            string text = "";
            double num10 = (double)CZeroSetInfo.ZeroSetCenterX - CZeroSetInfo.ToleranceDistance;
            double num11 = (double)CZeroSetInfo.ZeroSetCenterX + CZeroSetInfo.ToleranceDistance;
            double num12 = (double)CZeroSetInfo.ZeroSetCenterY - CZeroSetInfo.ToleranceDistance;
            double num13 = (double)CZeroSetInfo.ZeroSetCenterY + CZeroSetInfo.ToleranceDistance;
            double num14 = (double)CParameter.CenterXPos - CZeroSetInfo.ToleranceDistance;
            double num15 = (double)CParameter.CenterXPos + CZeroSetInfo.ToleranceDistance;
            double num16 = (double)CParameter.CenterYPos - CZeroSetInfo.ToleranceDistance;
            double num17 = (double)CParameter.CenterYPos + CZeroSetInfo.ToleranceDistance;
            int tool_Zoom_State = CSetInfo.Tool_Zoom_State;

            if (tool_Zoom_State <= 8)
            {
                if (tool_Zoom_State != 4)
                {
                    if (tool_Zoom_State == 8)
                    {
                        text = "X 8";
                        num = 448.0;
                        num3 = 448.0;
                        num2 = 576.0;
                        num4 = 576.0;
                    }
                }
                else
                {
                    text = "X 4";
                    num = 384.0;
                    num3 = 384.0;
                    num2 = 640.0;
                    num4 = 640.0;
                }
            }
            else if (tool_Zoom_State != 16)
            {
                if (tool_Zoom_State == 32)
                {
                    text = "X 32";
                    num = 496.0;
                    num3 = 496.0;
                    num2 = 528.0;
                    num4 = 528.0;
                }
            }
            else
            {
                text = "X 16";
                num = 480.0;
                num3 = 480.0;
                num2 = 544.0;
                num4 = 544.0;
            }

            int tool_Zoom_State2 = CSetInfo.Tool_Zoom_State;
            if (tool_Zoom_State2 <= 8)
            {
                if (tool_Zoom_State2 != 4)
                {
                    if (tool_Zoom_State2 == 8)
                    {
                        num5 = (double)((int)CZeroSetInfo.ToleranceDistance * 8);
                        num6 = (double)((int)(CZeroSetInfo.ZeroSetCenterX - 448f) * 8);
                        num7 = (double)((int)(CZeroSetInfo.ZeroSetCenterY - 448f) * 8);
                        num8 = (double)((int)(CParameter.CenterXPos - 448f) * 8);
                        num9 = (double)((int)(CParameter.CenterYPos - 448f) * 8);
                    }
                }
                else
                {
                    num5 = (double)((int)CZeroSetInfo.ToleranceDistance * 4);
                    num6 = (double)((int)(CZeroSetInfo.ZeroSetCenterX - 384f) * 4);
                    num7 = (double)((int)(CZeroSetInfo.ZeroSetCenterY - 384f) * 4);
                    num8 = (double)((int)(CParameter.CenterXPos - 384f) * 4);
                    num9 = (double)((int)(CParameter.CenterYPos - 384f) * 4);
                }
            }
            else if (tool_Zoom_State2 != 16)
            {
                if (tool_Zoom_State2 == 32)
                {
                    num5 = (double)((int)CZeroSetInfo.ToleranceDistance * 32);
                    num6 = (double)((int)(CZeroSetInfo.ZeroSetCenterX - 496f) * 32);
                    num7 = (double)((int)(CZeroSetInfo.ZeroSetCenterY - 496f) * 32);
                    num8 = (double)((int)(CParameter.CenterXPos - 496f) * 32);
                    num9 = (double)((int)(CParameter.CenterYPos - 496f) * 32);
                }
            }
            else
            {
                num5 = (double)((int)CZeroSetInfo.ToleranceDistance * 16);
                num6 = (double)((int)(CZeroSetInfo.ZeroSetCenterX - 480f) * 16);
                num7 = (double)((int)(CZeroSetInfo.ZeroSetCenterY - 480f) * 16);
                num8 = (double)((int)(CParameter.CenterXPos - 480f) * 16);
                num9 = (double)((int)(CParameter.CenterYPos - 480f) * 16);
            }

            if (num14 > num && num15 < num2 && num16 > num3 && num17 < num4)
            {
                Cv.DrawCircle(DisplayImage, new CvPoint((int)num8, (int)num9), (int)num5, new CvScalar(255.0, 255.0, 255.0), 2);
            }

            if (num10 > num && num11 < num2 && num12 > num3 && num13 < num4)
            {
                if (CSetInfo.Setting_Tolerance_Mode == 1)
                {
                    Cv.DrawCircle(DisplayImage, new CvPoint((int)num6, (int)num7), (int)num5, new CvScalar(255.0, 0.0, 0.0), 2);
                }
                else
                {
                    Cv.DrawRect(DisplayImage, new CvPoint((int)(num6 - num5), (int)(num7 - num5)), new CvPoint((int)(num6 + num5), (int)(num7 + num5)), new CvScalar(255.0, 0.0, 0.0), 2);
                }
            }

            Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 2);
            DisplayImage.DrawMarker((int)num6, (int)num7, CvColor.Blue, OpenCvSharp.MarkerStyle.Cross, 2000, LineType.AntiAlias, 2);
            if (CalibrationFlag)
            {
                if (CSetInfo.Tool_Angle_Definition == 1)
                {
                    Cv.PutText(DisplayImage, "EXT", new CvPoint(950, 40), font, new CvColor(255, 255, 255));
                }
                else
                {
                    Cv.PutText(DisplayImage, "INT", new CvPoint(950, 40), font, new CvColor(255, 255, 255));
                }
                Cv.PutText(DisplayImage, text, new CvPoint(950, 100), font, new CvColor(255, 255, 255));
            }
            font.Dispose();
        }

        private static void DisplayToleranceView()
        {
            Int32 thickness = 2;

            if (SearchSpotNumber <= 0)
                return;

            CvFont font;
            Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 2);

            // 영상의 중심을 중심으로 하는 원을 그린다 ( white )
            Cv.DrawCircle(DisplayImage, 
                new CvPoint((int)CParameter.CenterXPos, (int)CParameter.CenterYPos),    // center
                (int)CZeroSetInfo.ToleranceDistance,                                    // radius
                new CvScalar(255.0, 255.0, 255.0),                                      // color
                thickness);                                                                     // thickness

            if (CSetInfo.Setting_Tolerance_Mode == 1)
            {
                // peak point를 중심으로하는 원을 그린다. ( blue )
                Cv.DrawCircle(
                    DisplayImage, 
                    new CvPoint((int)CZeroSetInfo.ZeroSetCenterX, (int)CZeroSetInfo.ZeroSetCenterY), 
                    (int)CZeroSetInfo.ToleranceDistance, 
                    new CvScalar(255.0, 0.0, 0.0), 
                    thickness);
            }
            else
            {
                Cv.DrawRect(
                    DisplayImage, 
                    new CvPoint((int)((double)CZeroSetInfo.ZeroSetCenterX - CZeroSetInfo.ToleranceDistance), (int)((double)CZeroSetInfo.ZeroSetCenterY - CZeroSetInfo.ToleranceDistance)), 
                    new CvPoint((int)((double)CZeroSetInfo.ZeroSetCenterX + CZeroSetInfo.ToleranceDistance), (int)((double)CZeroSetInfo.ZeroSetCenterY + CZeroSetInfo.ToleranceDistance)), 
                    new CvScalar(255.0, 0.0, 0.0), 
                    thickness);
            }

            // x-axis, y-axis를 그린다
            DisplayImage.DrawMarker(
                (int)CZeroSetInfo.ZeroSetCenterX, 
                (int)CZeroSetInfo.ZeroSetCenterY, 
                CvColor.Blue, 
                OpenCvSharp.MarkerStyle.Cross, 
                2000, 
                LineType.AntiAlias, 
                thickness);

            if (CalibrationFlag)
            {
                if (CSetInfo.Tool_Angle_Definition == 1)
                {
                    Cv.PutText(DisplayImage, "EXT", new CvPoint(950, 40), font, new CvColor(255, 255, 255));
                    font.Dispose();
                    return;
                }
                Cv.PutText(DisplayImage, "INT", new CvPoint(950, 40), font, new CvColor(255, 255, 255));
            }
            font.Dispose();
        }

        private static void UpdateWobblePlotting()
        {
            int num = (int)(CZeroSetInfo.ZeroSetCenterX - CamParamInfo.ImageWidth / 2U);
            int num2 = (int)(CZeroSetInfo.ZeroSetCenterY - CamParamInfo.ImageHeight / 2U);
            WobblePlottingImage.DrawCircle(new CvPoint((int)AnalysisResultX[CPlotting.PlottingSpotNumber] - num, (int)AnalysisResultY[CPlotting.PlottingSpotNumber] - num2), 3, new CvScalar(0.0, 0.0, 255.0), 5, LineType.AntiAlias);
            if (CSetInfo.Setting_Tolerance_Mode == 1)
            {
                Cv.DrawCircle(WobblePlottingImage, new CvPoint((int)(CamParamInfo.ImageWidth / 2U), (int)(CamParamInfo.ImageHeight / 2U)), (int)CZeroSetInfo.ToleranceDistance, new CvScalar(255.0, 0.0, 0.0), 2);
            }
            else
            {
                Cv.DrawRect(WobblePlottingImage, new CvPoint((int)(CamParamInfo.ImageWidth / 2U - CZeroSetInfo.ToleranceDistance), (int)(CamParamInfo.ImageHeight / 2U - CZeroSetInfo.ToleranceDistance)), new CvPoint((int)(CamParamInfo.ImageWidth / 2U + CZeroSetInfo.ToleranceDistance), (int)(CamParamInfo.ImageHeight / 2U + CZeroSetInfo.ToleranceDistance)), new CvScalar(255.0, 0.0, 0.0), 2);
            }
            WobblePlottingImage.DrawMarker((int)(CamParamInfo.ImageWidth / 2U), (int)(CamParamInfo.ImageHeight / 2U), CvColor.Blue, OpenCvSharp.MarkerStyle.Cross, 2000, LineType.AntiAlias, 2);
        }

        private static void CalAngle()
        {
            if (SearchSpotNumber <= 0)
            {
                Trace.WriteLine("[wiki] Number of spots is zero. do nothing under this condition");
                return;
            }

            double[] array = new double[SearchSpotNumber];
            double[] array2 = new double[SearchSpotNumber];
            double[] array3 = new double[SearchSpotNumber];
            double[] array4 = new double[SearchSpotNumber];
            double[] array5 = new double[SearchSpotNumber];
            double[] array6 = new double[SearchSpotNumber];

            for (int i = 0; i < SearchSpotNumber; i++)
            {
                array4[i] = (double)(AnalysisResultX[i] - CZeroSetInfo.ZeroSetCenterX);
                array5[i] = (double)(AnalysisResultY[i] - CZeroSetInfo.ZeroSetCenterY);
                array6[i] = Math.Sqrt(Math.Pow(array4[i], 2.0) + Math.Pow(array5[i], 2.0));
                if (i < 3)
                {
                    SpotDistance[i] = array6[i];
                }

                array[i] = Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * array4[i] + 
                                Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * array4[i];
                array2[i] = -(Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * array5[i] + 
                                Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * array5[i]);

                array3[i] = Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * array6[i] + 
                                Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * array6[i];

                AngleX[i] = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(array[i] * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;
                AngleY[i] = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(array2[i] * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;
                AngleD[i] = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(array3[i] * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;

                AngleHourX[i] = (double)((int)AngleX[i]);
                AngleMinuteX[i] = (double)Math.Abs((int)(Math.Abs(AngleX[i] - AngleHourX[i]) * 60.0));
                AngleSecondX[i] = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(AngleX[i] - AngleHourX[i]) * 60.0 - AngleMinuteX[i]) * 60.0));
                AngleHourX[i] = Math.Abs(AngleHourX[i]);

                AngleHourY[i] = (double)((int)AngleY[i]);
                AngleMinuteY[i] = (double)Math.Abs((int)(Math.Abs(AngleY[i] - AngleHourY[i]) * 60.0));
                AngleSecondY[i] = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(AngleY[i] - AngleHourY[i]) * 60.0 - AngleMinuteY[i]) * 60.0));
                AngleHourY[i] = Math.Abs(AngleHourY[i]);

                AngleHourXY[i] = (double)((int)AngleD[i]);
                AngleMinuteXY[i] = (double)Math.Abs((int)(Math.Abs(AngleD[i] - AngleHourXY[i]) * 60.0));
                AngleSecondXY[i] = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(AngleD[i] - AngleHourXY[i]) * 60.0 - AngleMinuteXY[i]) * 60.0));
            }
            CheckNumbering();
            double num = 0.0;
            double num2 = 0.0;
            double num3 = 0.0;
            double num4 = 0.0;
            double num5 = 0.0;
            double num6 = 0.0;
            double num7 = 0.0;
            double num8 = 0.0;
            double num9 = 0.0;
            double num10 = 0.0;
            double num11 = 0.0;
            double num12 = 0.0;
            if (SearchSpotNumber == 2)
            {
                num = AnalysisResultX[1] - AnalysisResultX[0]; //X
                num2 = AnalysisResultY[1] - AnalysisResultY[0]; //Y
                num3 = Math.Sqrt(Math.Pow(num, 2.0) + Math.Pow(num2, 2.0)); // X및 Y 좌표 차이와 두점 사이 거리
                num4 = Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * num3 + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * num3; // 기울기 각도 보정후 두점사이 거리
                Angle1to2 = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(num4 * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;
                Angle1to2HourXY = (int)Angle1to2;
                Angle1to2MinuteXY = Math.Abs((int)(Math.Abs(Angle1to2 - Angle1to2HourXY) * 60.0));
                Angle1to2SecondXY = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(Angle1to2 - Angle1to2HourXY) * 60.0 - Angle1to2MinuteXY) * 60.0));
            }
            if (SearchSpotNumber == 3)
            {
                num = AnalysisResultX[1] - AnalysisResultX[0];
                num2 = AnalysisResultY[1] - AnalysisResultY[0];
                num3 = Math.Sqrt(Math.Pow(num, 2.0) + Math.Pow(num2, 2.0));
                num4 = Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * num3 + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * num3;
                Angle1to2 = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(num4 * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;
                num5 = AnalysisResultX[2] - AnalysisResultX[1];
                num6 = AnalysisResultY[2] - AnalysisResultY[1];
                num7 = Math.Sqrt(Math.Pow(num5, 2.0) + Math.Pow(num6, 2.0));
                num8 = Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * num7 + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * num7;
                Angle2to3 = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(num8 * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;
                num9 = AnalysisResultX[0] - AnalysisResultX[2];
                num10 = AnalysisResultY[0] - AnalysisResultY[2];
                num11 = Math.Sqrt(Math.Pow(num9, 2.0) + Math.Pow(num10, 2.0));
                num12 = Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) * num11 + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0) * num11;
                Angle3to1 = (double)CSetInfo.Tool_Angle_Definition_Value * (Math.Atan(num12 * CParameter.PixelPerDistance / CParameter.FocalLength) * 180.0 / Math.PI) / 2.0;
                Angle1to2HourXY = (int)Angle1to2;
                Angle1to2MinuteXY = Math.Abs((int)(Math.Abs(Angle1to2 - Angle1to2HourXY) * 60.0));
                Angle1to2SecondXY = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(Angle1to2 - Angle1to2HourXY) * 60.0 - Angle1to2MinuteXY) * 60.0));
                Angle2to3HourXY = (int)Angle2to3;
                Angle2to3MinuteXY = Math.Abs((int)(Math.Abs(Angle2to3 - Angle2to3HourXY) * 60.0));
                Angle2to3SecondXY = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(Angle2to3 - Angle2to3HourXY) * 60.0 - Angle2to3MinuteXY) * 60.0));
                Angle3to1HourXY = (int)Angle3to1;
                Angle3to1MinuteXY = Math.Abs((int)(Math.Abs(Angle3to1 - Angle3to1HourXY) * 60.0));
                Angle3to1SecondXY = Math.Truncate(Math.Abs(Math.Abs(Math.Abs(Angle3to1 - Angle3to1HourXY) * 60.0 - Angle3to1MinuteXY) * 60.0));
            }
        }

        private static void PlotSplineSolution(string title, float[] x, float[] y, float[] xs, float[] ys, string path, float[] qPrime = null)
        {
            Chart chart = new Chart();
            chart.Size = new System.Drawing.Size(600, 400);
            chart.Titles.Add(title);
            chart.Legends.Add(new Legend("Legend"));
            ChartArea chartArea = new ChartArea("DefaultChartArea");
            chartArea.AxisX.Title = "X";
            chartArea.AxisY.Title = "Y";
            chart.ChartAreas.Add(chartArea);
            Series item = CreateSeries(chart, "Spline", CreateDataPoints(xs, ys), Color.Blue, System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None);
            Series item2 = CreateSeries(chart, "Original", CreateDataPoints(x, y), Color.Green, System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Diamond);
            chart.Series.Add(item2);
            chart.Series.Add(item);
            if (qPrime != null)
            {
                Series item3 = CreateSeries(chart, "Slope", CreateDataPoints(xs, qPrime), Color.Red, System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None);
                chart.Series.Add(item3);
            }
            chartArea.RecalculateAxesScale();
            chartArea.AxisX.Minimum = Math.Floor(chartArea.AxisX.Minimum);
            chartArea.AxisX.Maximum = Math.Ceiling(chartArea.AxisX.Maximum);
            int num = x.Length - 1;
            num = Math.Max(4, num);
            chartArea.AxisX.Interval = (chartArea.AxisX.Maximum - chartArea.AxisX.Minimum) / (double)num;
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (FileStream fileStream = new FileStream(path, FileMode.CreateNew))
            {
                chart.SaveImage(fileStream, ChartImageFormat.Png);
            }
        }

        private static Series CreateSeries(Chart chart, string seriesName, IEnumerable<DataPoint> points, Color color, System.Windows.Forms.DataVisualization.Charting.MarkerStyle markerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None)
        {
            Series series = new Series
            {
                XValueType = ChartValueType.Double,
                YValueType = ChartValueType.Double,
                Legend = chart.Legends[0].Name,
                IsVisibleInLegend = true,
                ChartType = SeriesChartType.Line,
                Name = seriesName,
                ChartArea = chart.ChartAreas[0].Name,
                MarkerStyle = markerStyle,
                Color = color,
                MarkerSize = 8
            };
            foreach (DataPoint item in points)
            {
                series.Points.Add(item);
            }
            return series;
        }

        private static List<DataPoint> CreateDataPoints(float[] x, float[] y)
        {
            List<DataPoint> list = new List<DataPoint>();
            for (int i = 0; i < x.Length; i++)
            {
                list.Add(new DataPoint((double)x[i], (double)y[i]));
            }
            return list;
        }

        private static void SortingPosition()
        {
        }

        public static CvPoint2D32f FuncRotatePosition(CvPoint2D32f CenterPoint, CvPoint2D32f SourcePoint, double Angle)
        {
            CvPoint2D32f result = new CvPoint2D32f(0f, 0f);
            double num = Angle / 180.0 * Math.PI;
            result.X = (float)(Math.Cos(num) * (double)(SourcePoint.X - CenterPoint.X) - Math.Sin(num) * (double)(SourcePoint.Y - CenterPoint.Y) + (double)CenterPoint.X);
            result.Y = (float)(Math.Sin(num) * (double)(SourcePoint.X - CenterPoint.X) + Math.Cos(num) * (double)(SourcePoint.Y - CenterPoint.Y) + (double)CenterPoint.Y);
            return result;
        }

        public static IplImage FuncRotateImage(CvPoint2D32f CenterPoint, IplImage src, double degree, int Channel)
        {
            IplImage iplImage = new IplImage(src.Size, BitDepth.U8, Channel);
            CvMat rotationMatrix2D = Cv.GetRotationMatrix2D(CenterPoint, degree, 1.0);
            Cv.WarpAffine(src, iplImage, rotationMatrix2D, Interpolation.Linear, CvScalar.ScalarAll(0.0));
            return iplImage;
        }

        public static CvPoint2D32f FuncMirrorPosition(CvPoint2D32f CenterPoint, CvPoint2D32f OriginalPosition, string MirrorAxis)
        {
            CvPoint2D32f result = new CvPoint(0, 0);
            if (MirrorAxis == "X")
            {
                result.Y = 2f * CenterPoint.Y - OriginalPosition.Y;
                result.X = OriginalPosition.X;
            }
            if (MirrorAxis == "Y")
            {
                result.X = 2f * CenterPoint.X - OriginalPosition.X;
                result.Y = OriginalPosition.Y;
            }
            return result;
        }

        public static void ResetZeroPoint() //초기화
        {
            CZeroSetInfo.ZeroSetCenterX = CZeroSetInfo.ZeroSetOriginalCenterX;
            CZeroSetInfo.ZeroSetCenterY = CZeroSetInfo.ZeroSetOriginalCenterY;
            ZeroPointChangedFlag = 0;
        }

        public static void CheckNumbering()
        {
            bool flag = true;
            if (CSetInfo.Tool_Numbering == 1)
            {
                for (;;)
                {
                    for (int i = 0; i < SearchSpotNumber - 1; i++)
                    {
                        if (AngleD[i] > AngleD[i + 1])
                        {
                            int num = HistoCircleStartX[i];
                            int num2 = HistoCircleEndX[i];
                            int num3 = HistoCircleStartY[i];
                            int num4 = HistoCircleEndY[i];
                            double num5 = AnalysisAreaSize[i];
                            float num6 = AnalysisResultX[i];
                            float num7 = AnalysisResultY[i];
                            double num8 = AngleX[i];
                            double num9 = AngleY[i];
                            double num10 = AngleD[i];
                            double num11 = AngleHourX[i];
                            double num12 = AngleMinuteX[i];
                            double num13 = AngleSecondX[i];
                            double num14 = AngleHourY[i];
                            double num15 = AngleMinuteY[i];
                            double num16 = AngleSecondY[i];
                            double num17 = AngleHourXY[i];
                            double num18 = AngleMinuteXY[i];
                            double num19 = AngleSecondXY[i];
                            HistoCircleStartX[i] = HistoCircleStartX[i + 1];
                            HistoCircleStartX[i + 1] = num;
                            HistoCircleEndX[i] = HistoCircleEndX[i + 1];
                            HistoCircleEndX[i + 1] = num2;
                            HistoCircleStartY[i] = HistoCircleStartY[i + 1];
                            HistoCircleStartY[i + 1] = num3;
                            HistoCircleEndY[i] = HistoCircleEndY[i + 1];
                            HistoCircleEndY[i + 1] = num4;
                            AnalysisAreaSize[i] = AnalysisAreaSize[i + 1];
                            AnalysisAreaSize[i + 1] = num5;
                            AnalysisResultX[i] = AnalysisResultX[i + 1];
                            AnalysisResultY[i] = AnalysisResultY[i + 1];
                            AngleX[i] = AngleX[i + 1];
                            AngleY[i] = AngleY[i + 1];
                            AngleD[i] = AngleD[i + 1];
                            AngleHourX[i] = AngleHourX[i + 1];
                            AngleMinuteX[i] = AngleMinuteX[i + 1];
                            AngleSecondX[i] = AngleSecondX[i + 1];
                            AngleHourY[i] = AngleHourY[i + 1];
                            AngleMinuteY[i] = AngleMinuteY[i + 1];
                            AngleSecondY[i] = AngleSecondY[i + 1];
                            AngleHourXY[i] = AngleHourXY[i + 1];
                            AngleMinuteXY[i] = AngleMinuteXY[i + 1];
                            AngleSecondXY[i] = AngleSecondXY[i + 1];
                            AnalysisResultX[i + 1] = num6;
                            AnalysisResultY[i + 1] = num7;
                            AngleX[i + 1] = num8;
                            AngleY[i + 1] = num9;
                            AngleD[i + 1] = num10;
                            AngleHourX[i + 1] = num11;
                            AngleMinuteX[i + 1] = num12;
                            AngleSecondX[i + 1] = num13;
                            AngleHourY[i + 1] = num14;
                            AngleMinuteY[i + 1] = num15;
                            AngleSecondY[i + 1] = num16;
                            AngleHourXY[i + 1] = num17;
                            AngleMinuteXY[i + 1] = num18;
                            AngleSecondXY[i + 1] = num19;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        break;
                    }
                    flag = true;
                }
            }

            flag = true;
            if (CSetInfo.Tool_Numbering == 2)
            {
                for (;;)
                {
                    for (int j = 0; j < SearchSpotNumber - 1; j++)
                    {
                        if (AnalysisAreaSize[j] < AnalysisAreaSize[j + 1])
                        {
                            int num = HistoCircleStartX[j];
                            int num2 = HistoCircleEndX[j];
                            int num3 = HistoCircleStartY[j];
                            int num4 = HistoCircleEndY[j];
                            double num5 = AnalysisAreaSize[j];
                            HistoCircleStartX[j] = HistoCircleStartX[j + 1];
                            HistoCircleStartX[j + 1] = num;
                            HistoCircleEndX[j] = HistoCircleEndX[j + 1];
                            HistoCircleEndX[j + 1] = num2;
                            HistoCircleStartY[j] = HistoCircleStartY[j + 1];
                            HistoCircleStartY[j + 1] = num3;
                            HistoCircleEndY[j] = HistoCircleEndY[j + 1];
                            HistoCircleEndY[j + 1] = num4;
                            AnalysisAreaSize[j] = AnalysisAreaSize[j + 1];
                            AnalysisAreaSize[j + 1] = num5;
                            flag = false;
                        }
                    }
                    if (flag)
                    {
                        break;
                    }
                    flag = true;
                }
                return;
            }
        }
    }
}
