﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            m_SerialOut.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Measurement = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_ZeroSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Area = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Brightness = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Peak = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angledefinition = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angle_Ext = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angle_Int = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Xon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Yon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_L90 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_R90 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Original = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbering = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbring_Angle = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbring_Size = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_8 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_16 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_32 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_Unit = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Unit_Degree = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Unit_Sec = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_Tolerance = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Circle = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Square = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Value = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_GrayLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Information = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.picWikiLogo = new System.Windows.Forms.PictureBox();
            this.comboBox_serialport = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.AutoShutter = new System.Windows.Forms.Button();
            this.SaveAllConfig = new System.Windows.Forms.Button();
            this.cmdFactorySet = new System.Windows.Forms.Button();
            this.cmdAnalysis = new System.Windows.Forms.Button();
            this.btn_Capture = new System.Windows.Forms.Button();
            this.cmdClear = new System.Windows.Forms.Button();
            this.cmdExport = new System.Windows.Forms.Button();
            this.btn_SettingFileOpen = new System.Windows.Forms.Button();
            this.txtSamplingFreq = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cmdMeasureData = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.pic_Status15 = new System.Windows.Forms.PictureBox();
            this.pic_Status14 = new System.Windows.Forms.PictureBox();
            this.pic_Status13 = new System.Windows.Forms.PictureBox();
            this.pic_Status12 = new System.Windows.Forms.PictureBox();
            this.pic_Status11 = new System.Windows.Forms.PictureBox();
            this.pic_Status10 = new System.Windows.Forms.PictureBox();
            this.pic_Status9 = new System.Windows.Forms.PictureBox();
            this.pic_Status8 = new System.Windows.Forms.PictureBox();
            this.pic_Status7 = new System.Windows.Forms.PictureBox();
            this.pic_Status6 = new System.Windows.Forms.PictureBox();
            this.pic_Status5 = new System.Windows.Forms.PictureBox();
            this.pic_Status4 = new System.Windows.Forms.PictureBox();
            this.pic_Status3 = new System.Windows.Forms.PictureBox();
            this.pic_Status2 = new System.Windows.Forms.PictureBox();
            this.pic_Status1 = new System.Windows.Forms.PictureBox();
            this.lbStatusDescription = new System.Windows.Forms.Label();
            this.lbStatusResult = new System.Windows.Forms.Label();
            this.TextBox_ShutterSpeed = new System.Windows.Forms.TextBox();
            this.Track_ShutterSpeed = new System.Windows.Forms.TrackBar();
            this.label17 = new System.Windows.Forms.Label();
            this.panel_image = new System.Windows.Forms.Panel();
            this.pic_Camera = new System.Windows.Forms.PictureBox();
            this.lbFPS = new System.Windows.Forms.Label();
            this.lbCurrentSpotNo = new System.Windows.Forms.Label();
            this.lbCurrentMode = new System.Windows.Forms.Label();
            this.lbTolerance = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbFPSb = new System.Windows.Forms.Label();
            this.tab_LiveView = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.txtSpot3to1 = new System.Windows.Forms.TextBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSpot2to3 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtSpot1to2 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.txtSpotX1 = new System.Windows.Forms.TextBox();
            this.txtSpotXY3 = new System.Windows.Forms.TextBox();
            this.txtSpotY1 = new System.Windows.Forms.TextBox();
            this.txtSpotY3 = new System.Windows.Forms.TextBox();
            this.txtSpotXY1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSpotX3 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.txtSpotX2 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtSpotY2 = new System.Windows.Forms.TextBox();
            this.txtSpotXY2 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cmdWobbleFlattingClear = new System.Windows.Forms.Button();
            this.cmdPlotStartStop = new System.Windows.Forms.Button();
            this.cbPlotSpotNo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel_Plot = new System.Windows.Forms.Panel();
            this.pic_Plot = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.txtSerialSpotXY2 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotY2 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotX2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSerialSpotXY1 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotY1 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotX1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtStartingNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmdSnapDataClear = new System.Windows.Forms.Button();
            this.cbSerialSpotNo = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtCurrentPartName = new System.Windows.Forms.TextBox();
            this.gridSerialData = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdSnapMeasurement = new System.Windows.Forms.Button();
            this.cmdDataExport = new System.Windows.Forms.Button();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.txtPixelPerDistance = new System.Windows.Forms.TextBox();
            this.imageListButton = new System.Windows.Forms.ImageList(this.components);
            this.button_Live_View = new System.Windows.Forms.Button();
            this.button_XYPLOT = new System.Windows.Forms.Button();
            this.button_SERIAL = new System.Windows.Forms.Button();
            this.menuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_ShutterSpeed)).BeginInit();
            this.panel_image.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Camera)).BeginInit();
            this.tab_LiveView.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.panel_Plot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Plot)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSerialData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.BackColor = System.Drawing.Color.Transparent;
            this.menuBar.Dock = System.Windows.Forms.DockStyle.None;
            this.menuBar.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.menuBar.GripMargin = new System.Windows.Forms.Padding(0);
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.menu_Measurement,
            this.toolStripMenuItem2,
            this.menu_Tools,
            this.toolStripMenuItem3,
            this.menu_Settings,
            this.toolStripMenuItem4,
            this.menu_Information,
            this.toolStripMenuItem5});
            this.menuBar.Location = new System.Drawing.Point(12, 7);
            this.menuBar.Name = "menuBar";
            this.menuBar.Padding = new System.Windows.Forms.Padding(0);
            this.menuBar.Size = new System.Drawing.Size(361, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuBar";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripMenuItem1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.toolStripMenuItem1.Enabled = false;
            this.toolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripMenuItem1.Size = new System.Drawing.Size(16, 24);
            this.toolStripMenuItem1.Text = "|";
            this.toolStripMenuItem1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // menu_Measurement
            // 
            this.menu_Measurement.BackColor = System.Drawing.Color.Transparent;
            this.menu_Measurement.MergeIndex = 10;
            this.menu_Measurement.Name = "menu_Measurement";
            this.menu_Measurement.Padding = new System.Windows.Forms.Padding(0);
            this.menu_Measurement.Size = new System.Drawing.Size(102, 24);
            this.menu_Measurement.Text = "MEASUREMENT";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Enabled = false;
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripMenuItem2.Size = new System.Drawing.Size(16, 24);
            this.toolStripMenuItem2.Text = "|";
            // 
            // menu_Tools
            // 
            this.menu_Tools.BackColor = System.Drawing.Color.Transparent;
            this.menu_Tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_ZeroSet,
            this.menu_Tools_Mode,
            this.menu_Tools_Angledefinition,
            this.menu_Tools_Coordinate,
            this.menu_Tools_Numbering,
            this.menu_Tools_Zoom});
            this.menu_Tools.MergeIndex = 10;
            this.menu_Tools.Name = "menu_Tools";
            this.menu_Tools.Padding = new System.Windows.Forms.Padding(0);
            this.menu_Tools.Size = new System.Drawing.Size(52, 24);
            this.menu_Tools.Text = "TOOLS";
            // 
            // menu_Tools_ZeroSet
            // 
            this.menu_Tools_ZeroSet.BackColor = System.Drawing.Color.White;
            this.menu_Tools_ZeroSet.Name = "menu_Tools_ZeroSet";
            this.menu_Tools_ZeroSet.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_ZeroSet.Text = "ZERO SET";
            this.menu_Tools_ZeroSet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_Tools_ZeroSet.Click += new System.EventHandler(this.menu_Tools_ZeroSet_Click);
            // 
            // menu_Tools_Mode
            // 
            this.menu_Tools_Mode.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Mode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Mode_Area,
            this.menu_Tools_Mode_Brightness,
            this.menu_Tools_Mode_Peak});
            this.menu_Tools_Mode.Name = "menu_Tools_Mode";
            this.menu_Tools_Mode.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode.Text = "MODE";
            this.menu_Tools_Mode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Mode_Area
            // 
            this.menu_Tools_Mode_Area.CheckOnClick = true;
            this.menu_Tools_Mode_Area.Name = "menu_Tools_Mode_Area";
            this.menu_Tools_Mode_Area.Size = new System.Drawing.Size(202, 22);
            this.menu_Tools_Mode_Area.Text = "AREA CENTER";
            this.menu_Tools_Mode_Area.Click += new System.EventHandler(this.menu_Tools_Mode_Area_Click);
            // 
            // menu_Tools_Mode_Brightness
            // 
            this.menu_Tools_Mode_Brightness.CheckOnClick = true;
            this.menu_Tools_Mode_Brightness.Name = "menu_Tools_Mode_Brightness";
            this.menu_Tools_Mode_Brightness.Size = new System.Drawing.Size(202, 22);
            this.menu_Tools_Mode_Brightness.Text = "BRIGHTNESS CENTER";
            this.menu_Tools_Mode_Brightness.Click += new System.EventHandler(this.menu_Tools_Mode_Brightness_Click);
            // 
            // menu_Tools_Mode_Peak
            // 
            this.menu_Tools_Mode_Peak.CheckOnClick = true;
            this.menu_Tools_Mode_Peak.Name = "menu_Tools_Mode_Peak";
            this.menu_Tools_Mode_Peak.Size = new System.Drawing.Size(202, 22);
            this.menu_Tools_Mode_Peak.Text = "PEAK";
            this.menu_Tools_Mode_Peak.Click += new System.EventHandler(this.menu_Tools_Mode_Peak_Click);
            // 
            // menu_Tools_Angledefinition
            // 
            this.menu_Tools_Angledefinition.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Angledefinition.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Angle_Ext,
            this.menu_Tools_Angle_Int});
            this.menu_Tools_Angledefinition.Name = "menu_Tools_Angledefinition";
            this.menu_Tools_Angledefinition.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Angledefinition.Text = "ANGLE DEFINITION";
            this.menu_Tools_Angledefinition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Angle_Ext
            // 
            this.menu_Tools_Angle_Ext.CheckOnClick = true;
            this.menu_Tools_Angle_Ext.Name = "menu_Tools_Angle_Ext";
            this.menu_Tools_Angle_Ext.Size = new System.Drawing.Size(241, 22);
            this.menu_Tools_Angle_Ext.Text = "EXTERNAL LIGHT MODE(2α)";
            this.menu_Tools_Angle_Ext.Click += new System.EventHandler(this.menu_Tools_Angle_Ext_Click);
            // 
            // menu_Tools_Angle_Int
            // 
            this.menu_Tools_Angle_Int.CheckOnClick = true;
            this.menu_Tools_Angle_Int.Name = "menu_Tools_Angle_Int";
            this.menu_Tools_Angle_Int.Size = new System.Drawing.Size(241, 22);
            this.menu_Tools_Angle_Int.Text = "INTERNAL LIGHT MODE(α)";
            this.menu_Tools_Angle_Int.Click += new System.EventHandler(this.menu_Tools_Angle_Int_Click);
            // 
            // menu_Tools_Coordinate
            // 
            this.menu_Tools_Coordinate.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Coordinate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Mirroring,
            this.menu_Tools_Coordinate_Rotating,
            this.menu_Tools_Coordinate_Original});
            this.menu_Tools_Coordinate.Name = "menu_Tools_Coordinate";
            this.menu_Tools_Coordinate.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Coordinate.Text = "COORDINATE";
            this.menu_Tools_Coordinate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Coordinate_Mirroring
            // 
            this.menu_Tools_Coordinate_Mirroring.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Mirroring_Xon,
            this.menu_Tools_Coordinate_Mirroring_Yon,
            this.menu_Tools_Coordinate_Mirroring_Off});
            this.menu_Tools_Coordinate_Mirroring.Name = "menu_Tools_Coordinate_Mirroring";
            this.menu_Tools_Coordinate_Mirroring.Size = new System.Drawing.Size(145, 22);
            this.menu_Tools_Coordinate_Mirroring.Text = "MIRRORING";
            // 
            // menu_Tools_Coordinate_Mirroring_Xon
            // 
            this.menu_Tools_Coordinate_Mirroring_Xon.Name = "menu_Tools_Coordinate_Mirroring_Xon";
            this.menu_Tools_Coordinate_Mirroring_Xon.Size = new System.Drawing.Size(104, 22);
            this.menu_Tools_Coordinate_Mirroring_Xon.Text = "X-on";
            this.menu_Tools_Coordinate_Mirroring_Xon.Click += new System.EventHandler(this.menu_Tools_Coordinate_Mirroring_Xon_Click);
            // 
            // menu_Tools_Coordinate_Mirroring_Yon
            // 
            this.menu_Tools_Coordinate_Mirroring_Yon.Name = "menu_Tools_Coordinate_Mirroring_Yon";
            this.menu_Tools_Coordinate_Mirroring_Yon.Size = new System.Drawing.Size(104, 22);
            this.menu_Tools_Coordinate_Mirroring_Yon.Text = "Y-on";
            this.menu_Tools_Coordinate_Mirroring_Yon.Click += new System.EventHandler(this.menu_Tools_Coordinate_Mirroring_Yon_Click);
            // 
            // menu_Tools_Coordinate_Mirroring_Off
            // 
            this.menu_Tools_Coordinate_Mirroring_Off.Name = "menu_Tools_Coordinate_Mirroring_Off";
            this.menu_Tools_Coordinate_Mirroring_Off.Size = new System.Drawing.Size(104, 22);
            this.menu_Tools_Coordinate_Mirroring_Off.Text = "OFF";
            this.menu_Tools_Coordinate_Mirroring_Off.Click += new System.EventHandler(this.menu_Tools_Coordinate_Mirroring_Off_Click);
            // 
            // menu_Tools_Coordinate_Rotating
            // 
            this.menu_Tools_Coordinate_Rotating.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Rotating_L90,
            this.menu_Tools_Coordinate_Rotating_R90,
            this.menu_Tools_Coordinate_Rotating_Off});
            this.menu_Tools_Coordinate_Rotating.Name = "menu_Tools_Coordinate_Rotating";
            this.menu_Tools_Coordinate_Rotating.Size = new System.Drawing.Size(145, 22);
            this.menu_Tools_Coordinate_Rotating.Text = "ROTATING";
            // 
            // menu_Tools_Coordinate_Rotating_L90
            // 
            this.menu_Tools_Coordinate_Rotating_L90.Name = "menu_Tools_Coordinate_Rotating_L90";
            this.menu_Tools_Coordinate_Rotating_L90.Size = new System.Drawing.Size(99, 22);
            this.menu_Tools_Coordinate_Rotating_L90.Text = "L90";
            this.menu_Tools_Coordinate_Rotating_L90.Click += new System.EventHandler(this.menu_Tools_Coordinate_Rotating_L90_Click);
            // 
            // menu_Tools_Coordinate_Rotating_R90
            // 
            this.menu_Tools_Coordinate_Rotating_R90.Name = "menu_Tools_Coordinate_Rotating_R90";
            this.menu_Tools_Coordinate_Rotating_R90.Size = new System.Drawing.Size(99, 22);
            this.menu_Tools_Coordinate_Rotating_R90.Text = "R90";
            this.menu_Tools_Coordinate_Rotating_R90.Click += new System.EventHandler(this.menu_Tools_Coordinate_Rotating_R90_Click);
            // 
            // menu_Tools_Coordinate_Rotating_Off
            // 
            this.menu_Tools_Coordinate_Rotating_Off.Name = "menu_Tools_Coordinate_Rotating_Off";
            this.menu_Tools_Coordinate_Rotating_Off.Size = new System.Drawing.Size(99, 22);
            this.menu_Tools_Coordinate_Rotating_Off.Text = "OFF";
            this.menu_Tools_Coordinate_Rotating_Off.Click += new System.EventHandler(this.menu_Tools_Coordinate_Rotating_Off_Click);
            // 
            // menu_Tools_Coordinate_Original
            // 
            this.menu_Tools_Coordinate_Original.Name = "menu_Tools_Coordinate_Original";
            this.menu_Tools_Coordinate_Original.Size = new System.Drawing.Size(145, 22);
            this.menu_Tools_Coordinate_Original.Text = "ORIGINAL";
            this.menu_Tools_Coordinate_Original.Visible = false;
            this.menu_Tools_Coordinate_Original.Click += new System.EventHandler(this.menu_Tools_Coordinate_Original_Click);
            // 
            // menu_Tools_Numbering
            // 
            this.menu_Tools_Numbering.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Numbering.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Numbring_Angle,
            this.menu_Tools_Numbring_Size});
            this.menu_Tools_Numbering.Name = "menu_Tools_Numbering";
            this.menu_Tools_Numbering.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Numbering.Text = "NUMBERING";
            this.menu_Tools_Numbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Numbring_Angle
            // 
            this.menu_Tools_Numbring_Angle.CheckOnClick = true;
            this.menu_Tools_Numbring_Angle.Name = "menu_Tools_Numbring_Angle";
            this.menu_Tools_Numbring_Angle.Size = new System.Drawing.Size(116, 22);
            this.menu_Tools_Numbring_Angle.Text = "ANGLE";
            this.menu_Tools_Numbring_Angle.Click += new System.EventHandler(this.menu_Tools_Numbring_Angle_Click);
            // 
            // menu_Tools_Numbring_Size
            // 
            this.menu_Tools_Numbring_Size.CheckOnClick = true;
            this.menu_Tools_Numbring_Size.Name = "menu_Tools_Numbring_Size";
            this.menu_Tools_Numbring_Size.Size = new System.Drawing.Size(116, 22);
            this.menu_Tools_Numbring_Size.Text = "SIZE";
            this.menu_Tools_Numbring_Size.Click += new System.EventHandler(this.menu_Tools_Numbring_Size_Click);
            // 
            // menu_Tools_Zoom
            // 
            this.menu_Tools_Zoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Zoom_4,
            this.menu_Tools_Zoom_8,
            this.menu_Tools_Zoom_16,
            this.menu_Tools_Zoom_32,
            this.menu_Tools_Zoom_Off});
            this.menu_Tools_Zoom.Name = "menu_Tools_Zoom";
            this.menu_Tools_Zoom.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Zoom.Text = "ZOOM";
            // 
            // menu_Tools_Zoom_4
            // 
            this.menu_Tools_Zoom_4.Name = "menu_Tools_Zoom_4";
            this.menu_Tools_Zoom_4.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Zoom_4.Text = "x 4";
            this.menu_Tools_Zoom_4.Click += new System.EventHandler(this.menu_Tools_Zoom_4_Click);
            // 
            // menu_Tools_Zoom_8
            // 
            this.menu_Tools_Zoom_8.Name = "menu_Tools_Zoom_8";
            this.menu_Tools_Zoom_8.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Zoom_8.Text = "x 8";
            this.menu_Tools_Zoom_8.Click += new System.EventHandler(this.menu_Tools_Zoom_8_Click);
            // 
            // menu_Tools_Zoom_16
            // 
            this.menu_Tools_Zoom_16.Name = "menu_Tools_Zoom_16";
            this.menu_Tools_Zoom_16.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Zoom_16.Text = "x 16";
            this.menu_Tools_Zoom_16.Click += new System.EventHandler(this.menu_Tools_Zoom_16_Click);
            // 
            // menu_Tools_Zoom_32
            // 
            this.menu_Tools_Zoom_32.Name = "menu_Tools_Zoom_32";
            this.menu_Tools_Zoom_32.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Zoom_32.Text = "x 32";
            this.menu_Tools_Zoom_32.Click += new System.EventHandler(this.menu_Tools_Zoom_32_Click);
            // 
            // menu_Tools_Zoom_Off
            // 
            this.menu_Tools_Zoom_Off.Name = "menu_Tools_Zoom_Off";
            this.menu_Tools_Zoom_Off.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Zoom_Off.Text = "Off";
            this.menu_Tools_Zoom_Off.Click += new System.EventHandler(this.menu_Tools_Zoom_Off_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Enabled = false;
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripMenuItem3.Size = new System.Drawing.Size(16, 24);
            this.toolStripMenuItem3.Text = "|";
            // 
            // menu_Settings
            // 
            this.menu_Settings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Setting_Unit,
            this.menu_Setting_Tolerance,
            this.menu_Setting_GrayLevel});
            this.menu_Settings.Name = "menu_Settings";
            this.menu_Settings.Padding = new System.Windows.Forms.Padding(0);
            this.menu_Settings.Size = new System.Drawing.Size(71, 24);
            this.menu_Settings.Text = "SETTINGS";
            // 
            // menu_Setting_Unit
            // 
            this.menu_Setting_Unit.BackColor = System.Drawing.Color.White;
            this.menu_Setting_Unit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Settings_Unit_Degree,
            this.menu_Settings_Unit_Sec});
            this.menu_Setting_Unit.Name = "menu_Setting_Unit";
            this.menu_Setting_Unit.Size = new System.Drawing.Size(147, 22);
            this.menu_Setting_Unit.Text = "UNIT";
            // 
            // menu_Settings_Unit_Degree
            // 
            this.menu_Settings_Unit_Degree.CheckOnClick = true;
            this.menu_Settings_Unit_Degree.Name = "menu_Settings_Unit_Degree";
            this.menu_Settings_Unit_Degree.Size = new System.Drawing.Size(128, 22);
            this.menu_Settings_Unit_Degree.Text = "DEGREE";
            this.menu_Settings_Unit_Degree.Click += new System.EventHandler(this.menu_Settings_Unit_Degree_Click);
            // 
            // menu_Settings_Unit_Sec
            // 
            this.menu_Settings_Unit_Sec.CheckOnClick = true;
            this.menu_Settings_Unit_Sec.Name = "menu_Settings_Unit_Sec";
            this.menu_Settings_Unit_Sec.Size = new System.Drawing.Size(128, 22);
            this.menu_Settings_Unit_Sec.Text = "MIN+SEC";
            this.menu_Settings_Unit_Sec.Click += new System.EventHandler(this.menu_Settings_Unit_Sec_Click);
            // 
            // menu_Setting_Tolerance
            // 
            this.menu_Setting_Tolerance.BackColor = System.Drawing.Color.White;
            this.menu_Setting_Tolerance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Settings_Tolerance_Circle,
            this.menu_Settings_Tolerance_Square,
            this.menu_Settings_Tolerance_Value});
            this.menu_Setting_Tolerance.Name = "menu_Setting_Tolerance";
            this.menu_Setting_Tolerance.Size = new System.Drawing.Size(147, 22);
            this.menu_Setting_Tolerance.Text = "TOLERANCE";
            // 
            // menu_Settings_Tolerance_Circle
            // 
            this.menu_Settings_Tolerance_Circle.CheckOnClick = true;
            this.menu_Settings_Tolerance_Circle.Name = "menu_Settings_Tolerance_Circle";
            this.menu_Settings_Tolerance_Circle.Size = new System.Drawing.Size(171, 22);
            this.menu_Settings_Tolerance_Circle.Text = "Circle";
            this.menu_Settings_Tolerance_Circle.Click += new System.EventHandler(this.menu_Settings_Tolerance_Circle_Click);
            // 
            // menu_Settings_Tolerance_Square
            // 
            this.menu_Settings_Tolerance_Square.CheckOnClick = true;
            this.menu_Settings_Tolerance_Square.Name = "menu_Settings_Tolerance_Square";
            this.menu_Settings_Tolerance_Square.Size = new System.Drawing.Size(171, 22);
            this.menu_Settings_Tolerance_Square.Text = "Square";
            this.menu_Settings_Tolerance_Square.Click += new System.EventHandler(this.menu_Settings_Tolerance_Square_Click);
            // 
            // menu_Settings_Tolerance_Value
            // 
            this.menu_Settings_Tolerance_Value.Name = "menu_Settings_Tolerance_Value";
            this.menu_Settings_Tolerance_Value.Size = new System.Drawing.Size(171, 22);
            this.menu_Settings_Tolerance_Value.Text = "Tolerance Value";
            this.menu_Settings_Tolerance_Value.Click += new System.EventHandler(this.menu_Settings_Tolerance_Value_Click);
            // 
            // menu_Setting_GrayLevel
            // 
            this.menu_Setting_GrayLevel.BackColor = System.Drawing.Color.White;
            this.menu_Setting_GrayLevel.Name = "menu_Setting_GrayLevel";
            this.menu_Setting_GrayLevel.Size = new System.Drawing.Size(147, 22);
            this.menu_Setting_GrayLevel.Text = "GREY LEVEL";
            this.menu_Setting_GrayLevel.Click += new System.EventHandler(this.menu_Setting_GrayLevel_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Enabled = false;
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripMenuItem4.Size = new System.Drawing.Size(16, 24);
            this.toolStripMenuItem4.Text = "|";
            // 
            // menu_Information
            // 
            this.menu_Information.Name = "menu_Information";
            this.menu_Information.Padding = new System.Windows.Forms.Padding(0);
            this.menu_Information.Size = new System.Drawing.Size(54, 24);
            this.menu_Information.Text = "ABOUT";
            this.menu_Information.Click += new System.EventHandler(this.menu_Information_About_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Enabled = false;
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Padding = new System.Windows.Forms.Padding(0);
            this.toolStripMenuItem5.Size = new System.Drawing.Size(16, 24);
            this.toolStripMenuItem5.Text = "|";
            // 
            // picWikiLogo
            // 
            this.picWikiLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picWikiLogo.BackColor = System.Drawing.Color.Transparent;
            this.picWikiLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picWikiLogo.ErrorImage = ((System.Drawing.Image)(resources.GetObject("picWikiLogo.ErrorImage")));
            this.picWikiLogo.Image = ((System.Drawing.Image)(resources.GetObject("picWikiLogo.Image")));
            this.picWikiLogo.InitialImage = ((System.Drawing.Image)(resources.GetObject("picWikiLogo.InitialImage")));
            this.picWikiLogo.Location = new System.Drawing.Point(1015, 8);
            this.picWikiLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picWikiLogo.Name = "picWikiLogo";
            this.picWikiLogo.Size = new System.Drawing.Size(134, 25);
            this.picWikiLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picWikiLogo.TabIndex = 1;
            this.picWikiLogo.TabStop = false;
            // 
            // comboBox_serialport
            // 
            this.comboBox_serialport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_serialport.FormattingEnabled = true;
            this.comboBox_serialport.Location = new System.Drawing.Point(495, 10);
            this.comboBox_serialport.Name = "comboBox_serialport";
            this.comboBox_serialport.Size = new System.Drawing.Size(68, 23);
            this.comboBox_serialport.TabIndex = 13;
            this.comboBox_serialport.Visible = false;
            this.comboBox_serialport.SelectedIndexChanged += new System.EventHandler(this.comboBox_serialport_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(569, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Port (9600) : ";
            this.label1.Visible = false;
            // 
            // AutoShutter
            // 
            this.AutoShutter.Location = new System.Drawing.Point(393, 0);
            this.AutoShutter.Name = "AutoShutter";
            this.AutoShutter.Size = new System.Drawing.Size(96, 42);
            this.AutoShutter.TabIndex = 10;
            this.AutoShutter.Text = "AutoShutter";
            this.AutoShutter.UseVisualStyleBackColor = true;
            this.AutoShutter.Visible = false;
            this.AutoShutter.Click += new System.EventHandler(this.btn_AutoShutter_Click);
            // 
            // SaveAllConfig
            // 
            this.SaveAllConfig.BackColor = System.Drawing.Color.Transparent;
            this.SaveAllConfig.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SaveAllConfig.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.SaveAllConfig.FlatAppearance.BorderSize = 0;
            this.SaveAllConfig.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.SaveAllConfig.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.SaveAllConfig.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.SaveAllConfig.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.SaveAllConfig.Image = global::WikiOptics_Collimator.Properties.Resources._04_Save_Button_01_Off_21__234_;
            this.SaveAllConfig.Location = new System.Drawing.Point(19, 199);
            this.SaveAllConfig.Name = "SaveAllConfig";
            this.SaveAllConfig.Size = new System.Drawing.Size(89, 64);
            this.SaveAllConfig.TabIndex = 0;
            this.SaveAllConfig.TabStop = false;
            this.SaveAllConfig.UseVisualStyleBackColor = false;
            this.SaveAllConfig.Click += new System.EventHandler(this.OnSaveAllConfigClick);
            this.SaveAllConfig.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_SaveAllConfig_MouseDown);
            this.SaveAllConfig.MouseEnter += new System.EventHandler(this.btn_SaveAllConfig_MouseEnter);
            this.SaveAllConfig.MouseLeave += new System.EventHandler(this.btn_SaveAllConfig_MouseLeave);
            this.SaveAllConfig.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_SaveAllConfig_MouseUp);
            // 
            // cmdFactorySet
            // 
            this.cmdFactorySet.BackColor = System.Drawing.Color.Transparent;
            this.cmdFactorySet.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdFactorySet.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cmdFactorySet.FlatAppearance.BorderSize = 0;
            this.cmdFactorySet.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdFactorySet.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdFactorySet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFactorySet.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdFactorySet.ForeColor = System.Drawing.Color.White;
            this.cmdFactorySet.Image = global::WikiOptics_Collimator.Properties.Resources._08_Factory_Set_Button_01_Off_21__514_;
            this.cmdFactorySet.Location = new System.Drawing.Point(19, 487);
            this.cmdFactorySet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdFactorySet.Name = "cmdFactorySet";
            this.cmdFactorySet.Size = new System.Drawing.Size(89, 64);
            this.cmdFactorySet.TabIndex = 8;
            this.cmdFactorySet.UseVisualStyleBackColor = false;
            this.cmdFactorySet.Click += new System.EventHandler(this.cmdFactorySet_Click);
            this.cmdFactorySet.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdFactorySet_MouseDown);
            this.cmdFactorySet.MouseEnter += new System.EventHandler(this.btn_cmdFactorySet_MouseEnter);
            this.cmdFactorySet.MouseLeave += new System.EventHandler(this.btn_cmdFactorySet_MouseLeave);
            this.cmdFactorySet.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdFactorySet_MouseUp);
            // 
            // cmdAnalysis
            // 
            this.cmdAnalysis.BackColor = System.Drawing.Color.Transparent;
            this.cmdAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdAnalysis.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cmdAnalysis.FlatAppearance.BorderSize = 0;
            this.cmdAnalysis.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdAnalysis.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdAnalysis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnalysis.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdAnalysis.Image = global::WikiOptics_Collimator.Properties.Resources._01_Start_Button_01_Off_21__94_;
            this.cmdAnalysis.Location = new System.Drawing.Point(19, 56);
            this.cmdAnalysis.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdAnalysis.Name = "cmdAnalysis";
            this.cmdAnalysis.Size = new System.Drawing.Size(89, 64);
            this.cmdAnalysis.TabIndex = 0;
            this.cmdAnalysis.TabStop = false;
            this.cmdAnalysis.UseVisualStyleBackColor = false;
            this.cmdAnalysis.Click += new System.EventHandler(this.cmdAnalysis_Click);
            this.cmdAnalysis.MouseEnter += new System.EventHandler(this.cmdAnalysis_MouseEnter);
            this.cmdAnalysis.MouseLeave += new System.EventHandler(this.cmdAnalysis_MouseLeave);
            // 
            // btn_Capture
            // 
            this.btn_Capture.BackColor = System.Drawing.Color.Transparent;
            this.btn_Capture.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_Capture.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.btn_Capture.FlatAppearance.BorderSize = 0;
            this.btn_Capture.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_Capture.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_Capture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Capture.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_Capture.Image = global::WikiOptics_Collimator.Properties.Resources._07_Capture_Button_01_Off_21__444__1_;
            this.btn_Capture.Location = new System.Drawing.Point(19, 415);
            this.btn_Capture.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Capture.Name = "btn_Capture";
            this.btn_Capture.Size = new System.Drawing.Size(89, 64);
            this.btn_Capture.TabIndex = 5;
            this.btn_Capture.UseVisualStyleBackColor = false;
            this.btn_Capture.Click += new System.EventHandler(this.btn_Capture_Click);
            this.btn_Capture.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_Capture_MouseDown);
            this.btn_Capture.MouseEnter += new System.EventHandler(this.btn_Capture_MouseEnter);
            this.btn_Capture.MouseLeave += new System.EventHandler(this.btn_Capture_MouseLeave);
            this.btn_Capture.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_Capture_MouseUp);
            // 
            // cmdClear
            // 
            this.cmdClear.BackColor = System.Drawing.Color.Transparent;
            this.cmdClear.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdClear.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cmdClear.FlatAppearance.BorderSize = 0;
            this.cmdClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClear.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdClear.Image = global::WikiOptics_Collimator.Properties.Resources._06_Clear_Button_01_Off_21__374_;
            this.cmdClear.Location = new System.Drawing.Point(19, 343);
            this.cmdClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(89, 64);
            this.cmdClear.TabIndex = 0;
            this.cmdClear.TabStop = false;
            this.cmdClear.UseVisualStyleBackColor = false;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            this.cmdClear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdClear_MouseDown);
            this.cmdClear.MouseEnter += new System.EventHandler(this.btn_cmdClear_MouseEnter);
            this.cmdClear.MouseLeave += new System.EventHandler(this.btn_cmdClear_MouseLeave);
            this.cmdClear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdClear_MouseUp);
            // 
            // cmdExport
            // 
            this.cmdExport.BackColor = System.Drawing.Color.Transparent;
            this.cmdExport.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdExport.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cmdExport.FlatAppearance.BorderSize = 0;
            this.cmdExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExport.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdExport.Image = global::WikiOptics_Collimator.Properties.Resources._05_Export_Button_01_Off_21__304_;
            this.cmdExport.Location = new System.Drawing.Point(19, 270);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(89, 64);
            this.cmdExport.TabIndex = 0;
            this.cmdExport.TabStop = false;
            this.cmdExport.UseVisualStyleBackColor = false;
            this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
            this.cmdExport.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdExport_MouseDown);
            this.cmdExport.MouseEnter += new System.EventHandler(this.btn_cmdExport_MouseEnter);
            this.cmdExport.MouseLeave += new System.EventHandler(this.btn_cmdExport_MouseLeave);
            this.cmdExport.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdExport_MouseUp);
            // 
            // btn_SettingFileOpen
            // 
            this.btn_SettingFileOpen.BackColor = System.Drawing.Color.Transparent;
            this.btn_SettingFileOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btn_SettingFileOpen.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.btn_SettingFileOpen.FlatAppearance.BorderSize = 0;
            this.btn_SettingFileOpen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btn_SettingFileOpen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btn_SettingFileOpen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SettingFileOpen.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.btn_SettingFileOpen.ForeColor = System.Drawing.Color.Transparent;
            this.btn_SettingFileOpen.Image = global::WikiOptics_Collimator.Properties.Resources._03_Open_Button_01_Off_21__164_;
            this.btn_SettingFileOpen.Location = new System.Drawing.Point(19, 128);
            this.btn_SettingFileOpen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_SettingFileOpen.Name = "btn_SettingFileOpen";
            this.btn_SettingFileOpen.Size = new System.Drawing.Size(89, 64);
            this.btn_SettingFileOpen.TabIndex = 0;
            this.btn_SettingFileOpen.TabStop = false;
            this.btn_SettingFileOpen.UseVisualStyleBackColor = false;
            this.btn_SettingFileOpen.Click += new System.EventHandler(this.btn_SettingFileOpen_Click);
            this.btn_SettingFileOpen.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_SettingFileOpen_MouseDown);
            this.btn_SettingFileOpen.MouseEnter += new System.EventHandler(this.btn_SettingFileOpen_MouseEnter);
            this.btn_SettingFileOpen.MouseLeave += new System.EventHandler(this.btn_SettingFileOpen_MouseLeave);
            // 
            // txtSamplingFreq
            // 
            this.txtSamplingFreq.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSamplingFreq.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSamplingFreq.Location = new System.Drawing.Point(160, 605);
            this.txtSamplingFreq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSamplingFreq.Name = "txtSamplingFreq";
            this.txtSamplingFreq.Size = new System.Drawing.Size(110, 21);
            this.txtSamplingFreq.TabIndex = 12;
            this.txtSamplingFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.BackColor = System.Drawing.Color.Transparent;
            this.label37.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label37.Location = new System.Drawing.Point(156, 625);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(121, 19);
            this.label37.TabIndex = 11;
            this.label37.Text = "sec (0.1s ~ 3600s)";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdMeasureData
            // 
            this.cmdMeasureData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdMeasureData.BackColor = System.Drawing.Color.Transparent;
            this.cmdMeasureData.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.cmdMeasureData.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cmdMeasureData.FlatAppearance.BorderSize = 0;
            this.cmdMeasureData.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cmdMeasureData.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdMeasureData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMeasureData.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdMeasureData.Image = global::WikiOptics_Collimator.Properties.Resources._09_MS_Start_Button_01_Off_21__605_;
            this.cmdMeasureData.Location = new System.Drawing.Point(19, 580);
            this.cmdMeasureData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdMeasureData.Name = "cmdMeasureData";
            this.cmdMeasureData.Size = new System.Drawing.Size(89, 64);
            this.cmdMeasureData.TabIndex = 10;
            this.cmdMeasureData.UseVisualStyleBackColor = false;
            this.cmdMeasureData.Click += new System.EventHandler(this.cmdMeasureData_Click);
            this.cmdMeasureData.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdMeasureData_MouseDown);
            this.cmdMeasureData.MouseEnter += new System.EventHandler(this.btn_cmdMeasureData_MouseEnter);
            this.cmdMeasureData.MouseLeave += new System.EventHandler(this.btn_cmdMeasureData_MouseLeave);
            this.cmdMeasureData.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdMeasureData_MouseUp);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label5.Location = new System.Drawing.Point(157, 580);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 19);
            this.label5.TabIndex = 8;
            this.label5.Text = "SAMPLING FREQ.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pic_Status15
            // 
            this.pic_Status15.BackColor = System.Drawing.Color.Red;
            this.pic_Status15.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_15_B3_637__84_1;
            this.pic_Status15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status15.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_15_B3_637__84_;
            this.pic_Status15.Location = new System.Drawing.Point(635, 52);
            this.pic_Status15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 10);
            this.pic_Status15.MaximumSize = new System.Drawing.Size(19, 37);
            this.pic_Status15.Name = "pic_Status15";
            this.pic_Status15.Size = new System.Drawing.Size(19, 33);
            this.pic_Status15.TabIndex = 14;
            this.pic_Status15.TabStop = false;
            this.pic_Status15.Visible = false;
            // 
            // pic_Status14
            // 
            this.pic_Status14.BackColor = System.Drawing.Color.Red;
            this.pic_Status14.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_14_B2_637__119_1;
            this.pic_Status14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status14.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_14_B2_637__119_;
            this.pic_Status14.Location = new System.Drawing.Point(635, 87);
            this.pic_Status14.Margin = new System.Windows.Forms.Padding(3, 15, 3, 4);
            this.pic_Status14.MaximumSize = new System.Drawing.Size(19, 37);
            this.pic_Status14.Name = "pic_Status14";
            this.pic_Status14.Size = new System.Drawing.Size(19, 33);
            this.pic_Status14.TabIndex = 13;
            this.pic_Status14.TabStop = false;
            this.pic_Status14.Visible = false;
            // 
            // pic_Status13
            // 
            this.pic_Status13.BackColor = System.Drawing.Color.Red;
            this.pic_Status13.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_13_B1_637__153_1;
            this.pic_Status13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status13.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_13_B1_637__153_;
            this.pic_Status13.Location = new System.Drawing.Point(635, 121);
            this.pic_Status13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status13.MaximumSize = new System.Drawing.Size(19, 37);
            this.pic_Status13.Name = "pic_Status13";
            this.pic_Status13.Size = new System.Drawing.Size(19, 33);
            this.pic_Status13.TabIndex = 12;
            this.pic_Status13.TabStop = false;
            this.pic_Status13.Visible = false;
            // 
            // pic_Status12
            // 
            this.pic_Status12.BackColor = System.Drawing.Color.Green;
            this.pic_Status12.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_12_BG3_637__187_1;
            this.pic_Status12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status12.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_12_BG3_637__187_;
            this.pic_Status12.Location = new System.Drawing.Point(635, 155);
            this.pic_Status12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status12.MaximumSize = new System.Drawing.Size(19, 37);
            this.pic_Status12.Name = "pic_Status12";
            this.pic_Status12.Size = new System.Drawing.Size(19, 33);
            this.pic_Status12.TabIndex = 11;
            this.pic_Status12.TabStop = false;
            this.pic_Status12.Visible = false;
            // 
            // pic_Status11
            // 
            this.pic_Status11.BackColor = System.Drawing.Color.Green;
            this.pic_Status11.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_11_BG2_637__211_1;
            this.pic_Status11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status11.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_11_BG2_637__211_;
            this.pic_Status11.Location = new System.Drawing.Point(635, 189);
            this.pic_Status11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status11.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status11.Name = "pic_Status11";
            this.pic_Status11.Size = new System.Drawing.Size(19, 33);
            this.pic_Status11.TabIndex = 10;
            this.pic_Status11.TabStop = false;
            this.pic_Status11.Visible = false;
            // 
            // pic_Status10
            // 
            this.pic_Status10.BackColor = System.Drawing.Color.Green;
            this.pic_Status10.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_10_BG1_637__255_1;
            this.pic_Status10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status10.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_10_BG1_637__255_;
            this.pic_Status10.Location = new System.Drawing.Point(635, 223);
            this.pic_Status10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status10.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status10.Name = "pic_Status10";
            this.pic_Status10.Size = new System.Drawing.Size(19, 33);
            this.pic_Status10.TabIndex = 9;
            this.pic_Status10.TabStop = false;
            this.pic_Status10.Visible = false;
            // 
            // pic_Status9
            // 
            this.pic_Status9.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status9.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_09_G3_637__289_1;
            this.pic_Status9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status9.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_09_G3_637__289_;
            this.pic_Status9.Location = new System.Drawing.Point(635, 257);
            this.pic_Status9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status9.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status9.Name = "pic_Status9";
            this.pic_Status9.Size = new System.Drawing.Size(19, 33);
            this.pic_Status9.TabIndex = 8;
            this.pic_Status9.TabStop = false;
            this.pic_Status9.Visible = false;
            // 
            // pic_Status8
            // 
            this.pic_Status8.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status8.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_08_G2_637__323_1;
            this.pic_Status8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status8.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_08_G2_637__323_;
            this.pic_Status8.Location = new System.Drawing.Point(635, 291);
            this.pic_Status8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status8.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status8.Name = "pic_Status8";
            this.pic_Status8.Size = new System.Drawing.Size(19, 33);
            this.pic_Status8.TabIndex = 7;
            this.pic_Status8.TabStop = false;
            this.pic_Status8.Visible = false;
            // 
            // pic_Status7
            // 
            this.pic_Status7.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status7.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_07_G1_637__357_1;
            this.pic_Status7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status7.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_07_G1_637__357_;
            this.pic_Status7.Location = new System.Drawing.Point(635, 325);
            this.pic_Status7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status7.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status7.Name = "pic_Status7";
            this.pic_Status7.Size = new System.Drawing.Size(19, 33);
            this.pic_Status7.TabIndex = 6;
            this.pic_Status7.TabStop = false;
            this.pic_Status7.Visible = false;
            // 
            // pic_Status6
            // 
            this.pic_Status6.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status6.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_06_Y3_637__391_1;
            this.pic_Status6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status6.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_06_Y3_637__391_;
            this.pic_Status6.Location = new System.Drawing.Point(635, 359);
            this.pic_Status6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status6.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status6.Name = "pic_Status6";
            this.pic_Status6.Size = new System.Drawing.Size(19, 33);
            this.pic_Status6.TabIndex = 5;
            this.pic_Status6.TabStop = false;
            this.pic_Status6.Visible = false;
            // 
            // pic_Status5
            // 
            this.pic_Status5.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status5.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_05_Y2_637__425_1;
            this.pic_Status5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status5.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_05_Y2_637__425_;
            this.pic_Status5.Location = new System.Drawing.Point(635, 393);
            this.pic_Status5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status5.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status5.Name = "pic_Status5";
            this.pic_Status5.Size = new System.Drawing.Size(19, 33);
            this.pic_Status5.TabIndex = 4;
            this.pic_Status5.TabStop = false;
            this.pic_Status5.Visible = false;
            // 
            // pic_Status4
            // 
            this.pic_Status4.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status4.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_04_Y1_637__459_;
            this.pic_Status4.Location = new System.Drawing.Point(635, 427);
            this.pic_Status4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status4.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status4.Name = "pic_Status4";
            this.pic_Status4.Size = new System.Drawing.Size(19, 33);
            this.pic_Status4.TabIndex = 3;
            this.pic_Status4.TabStop = false;
            this.pic_Status4.Visible = false;
            // 
            // pic_Status3
            // 
            this.pic_Status3.BackColor = System.Drawing.Color.Coral;
            this.pic_Status3.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.Gauge_03_R3_637__493_;
            this.pic_Status3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status3.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_03_R3_637__493_;
            this.pic_Status3.Location = new System.Drawing.Point(635, 461);
            this.pic_Status3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status3.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status3.Name = "pic_Status3";
            this.pic_Status3.Size = new System.Drawing.Size(19, 33);
            this.pic_Status3.TabIndex = 2;
            this.pic_Status3.TabStop = false;
            this.pic_Status3.Visible = false;
            // 
            // pic_Status2
            // 
            this.pic_Status2.BackColor = System.Drawing.Color.Red;
            this.pic_Status2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status2.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_02_R2_637__527_1;
            this.pic_Status2.Location = new System.Drawing.Point(635, 495);
            this.pic_Status2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status2.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status2.Name = "pic_Status2";
            this.pic_Status2.Size = new System.Drawing.Size(19, 33);
            this.pic_Status2.TabIndex = 1;
            this.pic_Status2.TabStop = false;
            this.pic_Status2.Visible = false;
            // 
            // pic_Status1
            // 
            this.pic_Status1.BackColor = System.Drawing.Color.Red;
            this.pic_Status1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status1.Image = global::WikiOptics_Collimator.Properties.Resources.Gauge_01_R1_637__561_;
            this.pic_Status1.Location = new System.Drawing.Point(635, 529);
            this.pic_Status1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status1.MaximumSize = new System.Drawing.Size(19, 35);
            this.pic_Status1.Name = "pic_Status1";
            this.pic_Status1.Size = new System.Drawing.Size(19, 33);
            this.pic_Status1.TabIndex = 0;
            this.pic_Status1.TabStop = false;
            this.pic_Status1.Visible = false;
            // 
            // lbStatusDescription
            // 
            this.lbStatusDescription.AutoSize = true;
            this.lbStatusDescription.BackColor = System.Drawing.Color.Transparent;
            this.lbStatusDescription.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbStatusDescription.ForeColor = System.Drawing.Color.Black;
            this.lbStatusDescription.Location = new System.Drawing.Point(588, 620);
            this.lbStatusDescription.Name = "lbStatusDescription";
            this.lbStatusDescription.Size = new System.Drawing.Size(13, 19);
            this.lbStatusDescription.TabIndex = 20;
            this.lbStatusDescription.Text = ".";
            this.lbStatusDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbStatusDescription.Visible = false;
            // 
            // lbStatusResult
            // 
            this.lbStatusResult.AutoSize = true;
            this.lbStatusResult.BackColor = System.Drawing.Color.Transparent;
            this.lbStatusResult.Font = new System.Drawing.Font("Noto Sans KR Black", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbStatusResult.ForeColor = System.Drawing.Color.Green;
            this.lbStatusResult.Location = new System.Drawing.Point(615, 582);
            this.lbStatusResult.Name = "lbStatusResult";
            this.lbStatusResult.Size = new System.Drawing.Size(43, 29);
            this.lbStatusResult.TabIndex = 15;
            this.lbStatusResult.Text = "OK";
            this.lbStatusResult.Visible = false;
            // 
            // TextBox_ShutterSpeed
            // 
            this.TextBox_ShutterSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.TextBox_ShutterSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox_ShutterSpeed.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.TextBox_ShutterSpeed.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(32)))), ((int)(((byte)(136)))));
            this.TextBox_ShutterSpeed.Location = new System.Drawing.Point(672, 541);
            this.TextBox_ShutterSpeed.Name = "TextBox_ShutterSpeed";
            this.TextBox_ShutterSpeed.Size = new System.Drawing.Size(30, 18);
            this.TextBox_ShutterSpeed.TabIndex = 21;
            this.TextBox_ShutterSpeed.Text = "0.0";
            this.TextBox_ShutterSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnShutterSpeedTextBox_KeyUp);
            // 
            // Track_ShutterSpeed
            // 
            this.Track_ShutterSpeed.AutoSize = false;
            this.Track_ShutterSpeed.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(140)))), ((int)(((byte)(179)))), ((int)(((byte)(205)))));
            this.Track_ShutterSpeed.Cursor = System.Windows.Forms.Cursors.Default;
            this.Track_ShutterSpeed.Location = new System.Drawing.Point(691, 196);
            this.Track_ShutterSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Track_ShutterSpeed.Maximum = 30000;
            this.Track_ShutterSpeed.Name = "Track_ShutterSpeed";
            this.Track_ShutterSpeed.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.Track_ShutterSpeed.Size = new System.Drawing.Size(15, 312);
            this.Track_ShutterSpeed.TabIndex = 2;
            this.Track_ShutterSpeed.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Track_ShutterSpeed.Scroll += new System.EventHandler(this.Track_ShutterSpeed_Scroll);
            // 
            // label17
            // 
            this.label17.BackColor = System.Drawing.Color.Transparent;
            this.label17.Font = new System.Drawing.Font("Noto Sans KR", 8.999999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.Location = new System.Drawing.Point(698, 542);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(29, 17);
            this.label17.TabIndex = 17;
            this.label17.Text = "ms";
            // 
            // panel_image
            // 
            this.panel_image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_image.AutoScroll = true;
            this.panel_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_image.Controls.Add(this.pic_Camera);
            this.panel_image.Location = new System.Drawing.Point(114, 52);
            this.panel_image.Margin = new System.Windows.Forms.Padding(0);
            this.panel_image.Name = "panel_image";
            this.panel_image.Size = new System.Drawing.Size(512, 512);
            this.panel_image.TabIndex = 0;
            // 
            // pic_Camera
            // 
            this.pic_Camera.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_Camera.BackColor = System.Drawing.Color.Transparent;
            this.pic_Camera.Location = new System.Drawing.Point(0, 0);
            this.pic_Camera.Margin = new System.Windows.Forms.Padding(0);
            this.pic_Camera.MaximumSize = new System.Drawing.Size(512, 512);
            this.pic_Camera.MinimumSize = new System.Drawing.Size(512, 512);
            this.pic_Camera.Name = "pic_Camera";
            this.pic_Camera.Size = new System.Drawing.Size(512, 512);
            this.pic_Camera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Camera.TabIndex = 0;
            this.pic_Camera.TabStop = false;
            // 
            // lbFPS
            // 
            this.lbFPS.BackColor = System.Drawing.Color.Transparent;
            this.lbFPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbFPS.Location = new System.Drawing.Point(491, 579);
            this.lbFPS.Name = "lbFPS";
            this.lbFPS.Size = new System.Drawing.Size(80, 19);
            this.lbFPS.TabIndex = 10;
            this.lbFPS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentSpotNo
            // 
            this.lbCurrentSpotNo.BackColor = System.Drawing.Color.Transparent;
            this.lbCurrentSpotNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCurrentSpotNo.Location = new System.Drawing.Point(491, 602);
            this.lbCurrentSpotNo.Name = "lbCurrentSpotNo";
            this.lbCurrentSpotNo.Size = new System.Drawing.Size(80, 19);
            this.lbCurrentSpotNo.TabIndex = 9;
            this.lbCurrentSpotNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentMode
            // 
            this.lbCurrentMode.BackColor = System.Drawing.Color.Transparent;
            this.lbCurrentMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCurrentMode.Font = new System.Drawing.Font("Noto Sans KR", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbCurrentMode.Location = new System.Drawing.Point(297, 605);
            this.lbCurrentMode.Name = "lbCurrentMode";
            this.lbCurrentMode.Size = new System.Drawing.Size(98, 37);
            this.lbCurrentMode.TabIndex = 8;
            this.lbCurrentMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTolerance
            // 
            this.lbTolerance.BackColor = System.Drawing.Color.Transparent;
            this.lbTolerance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTolerance.Location = new System.Drawing.Point(491, 623);
            this.lbTolerance.Name = "lbTolerance";
            this.lbTolerance.Size = new System.Drawing.Size(80, 19);
            this.lbTolerance.TabIndex = 7;
            this.lbTolerance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.Transparent;
            this.label12.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(401, 625);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(87, 19);
            this.label12.TabIndex = 3;
            this.label12.Text = "TOLERANCE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(297, 583);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 19);
            this.label11.TabIndex = 2;
            this.label11.Text = "MODE";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(417, 601);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 19);
            this.label10.TabIndex = 1;
            this.label10.Text = "SPOT NO.";
            // 
            // lbFPSb
            // 
            this.lbFPSb.AutoSize = true;
            this.lbFPSb.BackColor = System.Drawing.Color.Transparent;
            this.lbFPSb.Font = new System.Drawing.Font("Noto Sans KR", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.lbFPSb.ForeColor = System.Drawing.Color.Black;
            this.lbFPSb.Location = new System.Drawing.Point(455, 578);
            this.lbFPSb.Name = "lbFPSb";
            this.lbFPSb.Size = new System.Drawing.Size(34, 19);
            this.lbFPSb.TabIndex = 0;
            this.lbFPSb.Text = "FPS";
            // 
            // tab_LiveView
            // 
            this.tab_LiveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tab_LiveView.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
            this.tab_LiveView.Controls.Add(this.tabPage1);
            this.tab_LiveView.Controls.Add(this.tabPage2);
            this.tab_LiveView.Controls.Add(this.tabPage3);
            this.tab_LiveView.HotTrack = true;
            this.tab_LiveView.ItemSize = new System.Drawing.Size(0, 1);
            this.tab_LiveView.Location = new System.Drawing.Point(772, 83);
            this.tab_LiveView.Margin = new System.Windows.Forms.Padding(0);
            this.tab_LiveView.Name = "tab_LiveView";
            this.tab_LiveView.SelectedIndex = 0;
            this.tab_LiveView.Size = new System.Drawing.Size(373, 569);
            this.tab_LiveView.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tab_LiveView.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.tabPage1.Controls.Add(this.pictureBox7);
            this.tabPage1.Controls.Add(this.pictureBox6);
            this.tabPage1.Controls.Add(this.txtSpot3to1);
            this.tabPage1.Controls.Add(this.pictureBox4);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.label38);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtSpot2to3);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.txtSpot1to2);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.txtSpotX1);
            this.tabPage1.Controls.Add(this.txtSpotXY3);
            this.tabPage1.Controls.Add(this.txtSpotY1);
            this.tabPage1.Controls.Add(this.txtSpotY3);
            this.tabPage1.Controls.Add(this.txtSpotXY1);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.txtSpotX3);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.txtSpotX2);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.txtSpotY2);
            this.tabPage1.Controls.Add(this.txtSpotXY2);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.pictureBox3);
            this.tabPage1.Controls.Add(this.pictureBox2);
            this.tabPage1.Location = new System.Drawing.Point(4, 5);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(365, 560);
            this.tabPage1.TabIndex = 0;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::WikiOptics_Collimator.Properties.Resources.Title_Point_777__131_2;
            this.pictureBox7.Location = new System.Drawing.Point(11, 295);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(15, 16);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 49;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::WikiOptics_Collimator.Properties.Resources.Title_Point_777__131_2;
            this.pictureBox6.Location = new System.Drawing.Point(11, 16);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(15, 16);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 48;
            this.pictureBox6.TabStop = false;
            // 
            // txtSpot3to1
            // 
            this.txtSpot3to1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpot3to1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpot3to1.Location = new System.Drawing.Point(98, 433);
            this.txtSpot3to1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpot3to1.Name = "txtSpot3to1";
            this.txtSpot3to1.Size = new System.Drawing.Size(80, 21);
            this.txtSpot3to1.TabIndex = 47;
            this.txtSpot3to1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox4.Image = global::WikiOptics_Collimator.Properties.Resources.Title_Point_777__131_1;
            this.pictureBox4.InitialImage = global::WikiOptics_Collimator.Properties.Resources.Title_Point_777__131_;
            this.pictureBox4.Location = new System.Drawing.Point(13, 295);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(14, 0);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 43;
            this.pictureBox4.TabStop = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.BackColor = System.Drawing.Color.Transparent;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label30.Image = ((System.Drawing.Image)(resources.GetObject("label30.Image")));
            this.label30.Location = new System.Drawing.Point(18, 435);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(73, 16);
            this.label30.TabIndex = 46;
            this.label30.Text = "SPOT 3-1 :";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Font = new System.Drawing.Font("Noto Sans KR", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label38.Location = new System.Drawing.Point(29, 288);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(141, 23);
            this.label38.TabIndex = 42;
            this.label38.Text = "RELATICE ANGLE";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Noto Sans KR", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.label6.Location = new System.Drawing.Point(30, 9);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 23);
            this.label6.TabIndex = 38;
            this.label6.Text = "ABSOLUTE ANGLE";
            // 
            // txtSpot2to3
            // 
            this.txtSpot2to3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpot2to3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpot2to3.Location = new System.Drawing.Point(98, 385);
            this.txtSpot2to3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpot2to3.Name = "txtSpot2to3";
            this.txtSpot2to3.Size = new System.Drawing.Size(80, 21);
            this.txtSpot2to3.TabIndex = 45;
            this.txtSpot2to3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.BackColor = System.Drawing.Color.Transparent;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label31.Image = ((System.Drawing.Image)(resources.GetObject("label31.Image")));
            this.label31.Location = new System.Drawing.Point(18, 385);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(73, 16);
            this.label31.TabIndex = 44;
            this.label31.Text = "SPOT 2-3 :";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::WikiOptics_Collimator.Properties.Resources.Title_Point_777__131_1;
            this.pictureBox1.InitialImage = global::WikiOptics_Collimator.Properties.Resources.Title_Point_777__131_;
            this.pictureBox1.Location = new System.Drawing.Point(13, 17);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(14, 0);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 39;
            this.pictureBox1.TabStop = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label29.Image = global::WikiOptics_Collimator.Properties.Resources.LCD_Large_778__428_;
            this.label29.Location = new System.Drawing.Point(18, 58);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 16);
            this.label29.TabIndex = 21;
            this.label29.Text = "SPOT 1";
            // 
            // txtSpot1to2
            // 
            this.txtSpot1to2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpot1to2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpot1to2.Location = new System.Drawing.Point(98, 335);
            this.txtSpot1to2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpot1to2.Name = "txtSpot1to2";
            this.txtSpot1to2.Size = new System.Drawing.Size(80, 21);
            this.txtSpot1to2.TabIndex = 43;
            this.txtSpot1to2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Image = ((System.Drawing.Image)(resources.GetObject("label28.Image")));
            this.label28.Location = new System.Drawing.Point(13, 91);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(23, 16);
            this.label28.TabIndex = 22;
            this.label28.Text = "X :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label27.Image = ((System.Drawing.Image)(resources.GetObject("label27.Image")));
            this.label27.Location = new System.Drawing.Point(127, 91);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(25, 16);
            this.label27.TabIndex = 23;
            this.label27.Text = "Y :";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.BackColor = System.Drawing.Color.Transparent;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Image = ((System.Drawing.Image)(resources.GetObject("label32.Image")));
            this.label32.Location = new System.Drawing.Point(18, 340);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(73, 16);
            this.label32.TabIndex = 42;
            this.label32.Text = "SPOT 1-2 :";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.BackColor = System.Drawing.Color.Transparent;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label26.Image = global::WikiOptics_Collimator.Properties.Resources.LCD_Large_778__428_;
            this.label26.Location = new System.Drawing.Point(240, 90);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 16);
            this.label26.TabIndex = 24;
            this.label26.Text = "X-Y :";
            // 
            // txtSpotX1
            // 
            this.txtSpotX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotX1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotX1.Location = new System.Drawing.Point(40, 89);
            this.txtSpotX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotX1.Name = "txtSpotX1";
            this.txtSpotX1.Size = new System.Drawing.Size(70, 21);
            this.txtSpotX1.TabIndex = 25;
            this.txtSpotX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotXY3
            // 
            this.txtSpotXY3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotXY3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotXY3.Location = new System.Drawing.Point(279, 219);
            this.txtSpotXY3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotXY3.Name = "txtSpotXY3";
            this.txtSpotXY3.Size = new System.Drawing.Size(70, 21);
            this.txtSpotXY3.TabIndex = 41;
            this.txtSpotXY3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotY1
            // 
            this.txtSpotY1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotY1.Location = new System.Drawing.Point(156, 89);
            this.txtSpotY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotY1.Name = "txtSpotY1";
            this.txtSpotY1.Size = new System.Drawing.Size(71, 21);
            this.txtSpotY1.TabIndex = 26;
            this.txtSpotY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotY3
            // 
            this.txtSpotY3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotY3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotY3.Location = new System.Drawing.Point(156, 219);
            this.txtSpotY3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotY3.Name = "txtSpotY3";
            this.txtSpotY3.Size = new System.Drawing.Size(70, 21);
            this.txtSpotY3.TabIndex = 40;
            this.txtSpotY3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotXY1
            // 
            this.txtSpotXY1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotXY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotXY1.Location = new System.Drawing.Point(279, 89);
            this.txtSpotXY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotXY1.Name = "txtSpotXY1";
            this.txtSpotXY1.Size = new System.Drawing.Size(70, 21);
            this.txtSpotXY1.TabIndex = 27;
            this.txtSpotXY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.Window;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label25.Image = ((System.Drawing.Image)(resources.GetObject("label25.Image")));
            this.label25.Location = new System.Drawing.Point(11, 121);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 16);
            this.label25.TabIndex = 28;
            this.label25.Text = "SPOT 2";
            // 
            // txtSpotX3
            // 
            this.txtSpotX3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotX3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotX3.Location = new System.Drawing.Point(40, 219);
            this.txtSpotX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotX3.Name = "txtSpotX3";
            this.txtSpotX3.Size = new System.Drawing.Size(70, 21);
            this.txtSpotX3.TabIndex = 39;
            this.txtSpotX3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label24.Image = ((System.Drawing.Image)(resources.GetObject("label24.Image")));
            this.label24.Location = new System.Drawing.Point(15, 156);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 16);
            this.label24.TabIndex = 29;
            this.label24.Text = "X :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label23.Image = ((System.Drawing.Image)(resources.GetObject("label23.Image")));
            this.label23.Location = new System.Drawing.Point(130, 156);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 16);
            this.label23.TabIndex = 30;
            this.label23.Text = "Y :";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label18.Image = ((System.Drawing.Image)(resources.GetObject("label18.Image")));
            this.label18.Location = new System.Drawing.Point(240, 222);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 16);
            this.label18.TabIndex = 38;
            this.label18.Text = "X-Y :";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label22.Image = ((System.Drawing.Image)(resources.GetObject("label22.Image")));
            this.label22.Location = new System.Drawing.Point(240, 159);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 16);
            this.label22.TabIndex = 31;
            this.label22.Text = "X-Y :";
            // 
            // txtSpotX2
            // 
            this.txtSpotX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotX2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotX2.Location = new System.Drawing.Point(40, 154);
            this.txtSpotX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotX2.Name = "txtSpotX2";
            this.txtSpotX2.Size = new System.Drawing.Size(70, 21);
            this.txtSpotX2.TabIndex = 32;
            this.txtSpotX2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label19.Image = ((System.Drawing.Image)(resources.GetObject("label19.Image")));
            this.label19.Location = new System.Drawing.Point(130, 222);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 16);
            this.label19.TabIndex = 37;
            this.label19.Text = "Y :";
            // 
            // txtSpotY2
            // 
            this.txtSpotY2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotY2.Location = new System.Drawing.Point(156, 154);
            this.txtSpotY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotY2.Name = "txtSpotY2";
            this.txtSpotY2.Size = new System.Drawing.Size(70, 21);
            this.txtSpotY2.TabIndex = 33;
            this.txtSpotY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotXY2
            // 
            this.txtSpotXY2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSpotXY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSpotXY2.Location = new System.Drawing.Point(279, 154);
            this.txtSpotXY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotXY2.Name = "txtSpotXY2";
            this.txtSpotXY2.Size = new System.Drawing.Size(70, 21);
            this.txtSpotXY2.TabIndex = 34;
            this.txtSpotXY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label20.Image = ((System.Drawing.Image)(resources.GetObject("label20.Image")));
            this.label20.Location = new System.Drawing.Point(15, 222);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 16);
            this.label20.TabIndex = 36;
            this.label20.Text = "X :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.Window;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label21.Image = ((System.Drawing.Image)(resources.GetObject("label21.Image")));
            this.label21.Location = new System.Drawing.Point(11, 192);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 16);
            this.label21.TabIndex = 35;
            this.label21.Text = "SPOT 3";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox3.Image = global::WikiOptics_Collimator.Properties.Resources.LCD_Large_778__428_;
            this.pictureBox3.Location = new System.Drawing.Point(3, 323);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(360, 212);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 41;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::WikiOptics_Collimator.Properties.Resources.LCD_Large_778__428_;
            this.pictureBox2.Location = new System.Drawing.Point(6, 37);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(354, 239);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 40;
            this.pictureBox2.TabStop = false;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.tabPage2.Controls.Add(this.cmdWobbleFlattingClear);
            this.tabPage2.Controls.Add(this.cmdPlotStartStop);
            this.tabPage2.Controls.Add(this.cbPlotSpotNo);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.panel_Plot);
            this.tabPage2.Location = new System.Drawing.Point(4, 5);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(365, 560);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "X-Y PLOT(WOBBLE, FLATNESS)";
            // 
            // cmdWobbleFlattingClear
            // 
            this.cmdWobbleFlattingClear.BackColor = System.Drawing.Color.White;
            this.cmdWobbleFlattingClear.FlatAppearance.BorderSize = 0;
            this.cmdWobbleFlattingClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdWobbleFlattingClear.Image = global::WikiOptics_Collimator.Properties.Resources._14_Clear_Button_01_Off_968__631_;
            this.cmdWobbleFlattingClear.Location = new System.Drawing.Point(214, 492);
            this.cmdWobbleFlattingClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdWobbleFlattingClear.Name = "cmdWobbleFlattingClear";
            this.cmdWobbleFlattingClear.Size = new System.Drawing.Size(111, 42);
            this.cmdWobbleFlattingClear.TabIndex = 9;
            this.cmdWobbleFlattingClear.UseVisualStyleBackColor = false;
            this.cmdWobbleFlattingClear.Click += new System.EventHandler(this.cmdWobbleFlattingClear_Click);
            this.cmdWobbleFlattingClear.MouseEnter += new System.EventHandler(this.cmdWobbleFlattingClear_MouseEnter);
            this.cmdWobbleFlattingClear.MouseLeave += new System.EventHandler(this.cmdWobbleFlattingClear_MouseLeave);
            // 
            // cmdPlotStartStop
            // 
            this.cmdPlotStartStop.BackColor = System.Drawing.Color.Transparent;
            this.cmdPlotStartStop.FlatAppearance.BorderSize = 0;
            this.cmdPlotStartStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cmdPlotStartStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPlotStartStop.ForeColor = System.Drawing.Color.Transparent;
            this.cmdPlotStartStop.Image = global::WikiOptics_Collimator.Properties.Resources._13_Start_Button_01_Off_826__631_;
            this.cmdPlotStartStop.Location = new System.Drawing.Point(48, 492);
            this.cmdPlotStartStop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdPlotStartStop.Name = "cmdPlotStartStop";
            this.cmdPlotStartStop.Size = new System.Drawing.Size(111, 42);
            this.cmdPlotStartStop.TabIndex = 8;
            this.cmdPlotStartStop.UseVisualStyleBackColor = false;
            this.cmdPlotStartStop.Click += new System.EventHandler(this.cmdPlotStartStop_Click);
            this.cmdPlotStartStop.MouseEnter += new System.EventHandler(this.cmdPlotStartStop_MouseEnter);
            this.cmdPlotStartStop.MouseLeave += new System.EventHandler(this.cmdPlotStartStop_MouseLeave);
            // 
            // cbPlotSpotNo
            // 
            this.cbPlotSpotNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cbPlotSpotNo.FormattingEnabled = true;
            this.cbPlotSpotNo.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbPlotSpotNo.Location = new System.Drawing.Point(72, 448);
            this.cbPlotSpotNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbPlotSpotNo.Name = "cbPlotSpotNo";
            this.cbPlotSpotNo.Size = new System.Drawing.Size(93, 23);
            this.cbPlotSpotNo.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 449);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 15);
            this.label14.TabIndex = 2;
            this.label14.Text = "SPOT NO.";
            // 
            // panel_Plot
            // 
            this.panel_Plot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Plot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Plot.Controls.Add(this.pic_Plot);
            this.panel_Plot.Location = new System.Drawing.Point(4, 15);
            this.panel_Plot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Plot.Name = "panel_Plot";
            this.panel_Plot.Size = new System.Drawing.Size(358, 427);
            this.panel_Plot.TabIndex = 0;
            // 
            // pic_Plot
            // 
            this.pic_Plot.BackColor = System.Drawing.Color.Transparent;
            this.pic_Plot.Location = new System.Drawing.Point(0, -3);
            this.pic_Plot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Plot.Name = "pic_Plot";
            this.pic_Plot.Size = new System.Drawing.Size(356, 428);
            this.pic_Plot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Plot.TabIndex = 0;
            this.pic_Plot.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.tabPage3.Controls.Add(this.txtSerialSpotXY2);
            this.tabPage3.Controls.Add(this.txtSerialSpotY2);
            this.tabPage3.Controls.Add(this.txtSerialSpotX2);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.txtSerialSpotXY1);
            this.tabPage3.Controls.Add(this.txtSerialSpotY1);
            this.tabPage3.Controls.Add(this.txtSerialSpotX1);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.txtStartingNo);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.txtPartName);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.pictureBox5);
            this.tabPage3.Location = new System.Drawing.Point(4, 5);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(365, 560);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "   SERIAL  ";
            // 
            // txtSerialSpotXY2
            // 
            this.txtSerialSpotXY2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSerialSpotXY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialSpotXY2.Location = new System.Drawing.Point(281, 144);
            this.txtSerialSpotXY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotXY2.Name = "txtSerialSpotXY2";
            this.txtSerialSpotXY2.ReadOnly = true;
            this.txtSerialSpotXY2.Size = new System.Drawing.Size(72, 21);
            this.txtSerialSpotXY2.TabIndex = 48;
            this.txtSerialSpotXY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotY2
            // 
            this.txtSerialSpotY2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSerialSpotY2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialSpotY2.Location = new System.Drawing.Point(155, 144);
            this.txtSerialSpotY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotY2.Name = "txtSerialSpotY2";
            this.txtSerialSpotY2.ReadOnly = true;
            this.txtSerialSpotY2.Size = new System.Drawing.Size(76, 21);
            this.txtSerialSpotY2.TabIndex = 47;
            this.txtSerialSpotY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotX2
            // 
            this.txtSerialSpotX2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSerialSpotX2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialSpotX2.Location = new System.Drawing.Point(40, 144);
            this.txtSerialSpotX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotX2.Name = "txtSerialSpotX2";
            this.txtSerialSpotX2.ReadOnly = true;
            this.txtSerialSpotX2.Size = new System.Drawing.Size(76, 21);
            this.txtSerialSpotX2.TabIndex = 46;
            this.txtSerialSpotX2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label2.Location = new System.Drawing.Point(239, 146);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "X-Y :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label3.Location = new System.Drawing.Point(127, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 16);
            this.label3.TabIndex = 44;
            this.label3.Text = "Y :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label4.Location = new System.Drawing.Point(12, 146);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 16);
            this.label4.TabIndex = 43;
            this.label4.Text = "X :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label15.Location = new System.Drawing.Point(15, 118);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 16);
            this.label15.TabIndex = 42;
            this.label15.Text = "SPOT 2";
            // 
            // txtSerialSpotXY1
            // 
            this.txtSerialSpotXY1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSerialSpotXY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialSpotXY1.Location = new System.Drawing.Point(281, 84);
            this.txtSerialSpotXY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotXY1.Name = "txtSerialSpotXY1";
            this.txtSerialSpotXY1.ReadOnly = true;
            this.txtSerialSpotXY1.Size = new System.Drawing.Size(72, 21);
            this.txtSerialSpotXY1.TabIndex = 41;
            this.txtSerialSpotXY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotY1
            // 
            this.txtSerialSpotY1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSerialSpotY1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialSpotY1.Location = new System.Drawing.Point(155, 84);
            this.txtSerialSpotY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotY1.Name = "txtSerialSpotY1";
            this.txtSerialSpotY1.ReadOnly = true;
            this.txtSerialSpotY1.Size = new System.Drawing.Size(76, 21);
            this.txtSerialSpotY1.TabIndex = 40;
            this.txtSerialSpotY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotX1
            // 
            this.txtSerialSpotX1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtSerialSpotX1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSerialSpotX1.Location = new System.Drawing.Point(40, 84);
            this.txtSerialSpotX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotX1.Name = "txtSerialSpotX1";
            this.txtSerialSpotX1.ReadOnly = true;
            this.txtSerialSpotX1.Size = new System.Drawing.Size(76, 21);
            this.txtSerialSpotX1.TabIndex = 39;
            this.txtSerialSpotX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label16.Location = new System.Drawing.Point(237, 86);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 16);
            this.label16.TabIndex = 38;
            this.label16.Text = "X-Y :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(127, 86);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 16);
            this.label33.TabIndex = 37;
            this.label33.Text = "Y :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(12, 86);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(23, 16);
            this.label34.TabIndex = 36;
            this.label34.Text = "X :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label35.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label35.Location = new System.Drawing.Point(15, 58);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 35;
            this.label35.Text = "SPOT 1";
            // 
            // txtStartingNo
            // 
            this.txtStartingNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtStartingNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtStartingNo.Location = new System.Drawing.Point(293, 27);
            this.txtStartingNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStartingNo.Name = "txtStartingNo";
            this.txtStartingNo.Size = new System.Drawing.Size(60, 21);
            this.txtStartingNo.TabIndex = 6;
            this.txtStartingNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label13.Location = new System.Drawing.Point(201, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 15);
            this.label13.TabIndex = 5;
            this.label13.Text = "STARTING NO";
            // 
            // txtPartName
            // 
            this.txtPartName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.txtPartName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPartName.Location = new System.Drawing.Point(97, 24);
            this.txtPartName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(98, 21);
            this.txtPartName.TabIndex = 4;
            this.txtPartName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(178)))), ((int)(((byte)(196)))));
            this.label9.Location = new System.Drawing.Point(15, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 15);
            this.label9.TabIndex = 3;
            this.label9.Text = "PART NAME ";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.cmdSnapDataClear);
            this.panel2.Controls.Add(this.cbSerialSpotNo);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.txtCurrentPartName);
            this.panel2.Controls.Add(this.gridSerialData);
            this.panel2.Controls.Add(this.cmdSnapMeasurement);
            this.panel2.Controls.Add(this.cmdDataExport);
            this.panel2.Location = new System.Drawing.Point(0, 196);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(369, 364);
            this.panel2.TabIndex = 50;
            // 
            // cmdSnapDataClear
            // 
            this.cmdSnapDataClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdSnapDataClear.FlatAppearance.BorderSize = 0;
            this.cmdSnapDataClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSnapDataClear.Image = global::WikiOptics_Collimator.Properties.Resources._16_Clear_Data_Button_01_Off_897__631_;
            this.cmdSnapDataClear.Location = new System.Drawing.Point(124, 295);
            this.cmdSnapDataClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdSnapDataClear.Name = "cmdSnapDataClear";
            this.cmdSnapDataClear.Size = new System.Drawing.Size(114, 41);
            this.cmdSnapDataClear.TabIndex = 54;
            this.cmdSnapDataClear.UseVisualStyleBackColor = true;
            this.cmdSnapDataClear.Click += new System.EventHandler(this.cmdSnapDataClear_Click);
            this.cmdSnapDataClear.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdSnapDataClear_MouseDown);
            this.cmdSnapDataClear.MouseEnter += new System.EventHandler(this.btn_cmdSnapDataClear_MouseEnter);
            this.cmdSnapDataClear.MouseLeave += new System.EventHandler(this.btn_cmdSnapDataClear_MouseLeave);
            this.cmdSnapDataClear.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdSnapDataClear_MouseUp);
            // 
            // cbSerialSpotNo
            // 
            this.cbSerialSpotNo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.cbSerialSpotNo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.cbSerialSpotNo.FormattingEnabled = true;
            this.cbSerialSpotNo.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cbSerialSpotNo.Location = new System.Drawing.Point(281, 4);
            this.cbSerialSpotNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSerialSpotNo.Name = "cbSerialSpotNo";
            this.cbSerialSpotNo.Size = new System.Drawing.Size(72, 23);
            this.cbSerialSpotNo.TabIndex = 52;
            this.cbSerialSpotNo.SelectedIndexChanged += new System.EventHandler(this.cbSerialSpotNo_SelectedIndexChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(210, 7);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(63, 15);
            this.label36.TabIndex = 51;
            this.label36.Text = "SPOT NO.";
            // 
            // txtCurrentPartName
            // 
            this.txtCurrentPartName.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            this.txtCurrentPartName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrentPartName.Enabled = false;
            this.txtCurrentPartName.Location = new System.Drawing.Point(9, 4);
            this.txtCurrentPartName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrentPartName.Name = "txtCurrentPartName";
            this.txtCurrentPartName.Size = new System.Drawing.Size(144, 21);
            this.txtCurrentPartName.TabIndex = 50;
            this.txtCurrentPartName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gridSerialData
            // 
            this.gridSerialData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gridSerialData.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(229)))), ((int)(((byte)(241)))));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSerialData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridSerialData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSerialData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.gridSerialData.Location = new System.Drawing.Point(2, 33);
            this.gridSerialData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridSerialData.Name = "gridSerialData";
            this.gridSerialData.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gridSerialData.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gridSerialData.RowTemplate.Height = 23;
            this.gridSerialData.Size = new System.Drawing.Size(360, 254);
            this.gridSerialData.TabIndex = 49;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "NO.";
            this.Column1.Name = "Column1";
            this.Column1.Width = 40;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "X";
            this.Column2.Name = "Column2";
            this.Column2.Width = 70;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Y";
            this.Column3.Name = "Column3";
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "X-Y";
            this.Column4.Name = "Column4";
            this.Column4.Width = 70;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "RE.";
            this.Column5.Name = "Column5";
            this.Column5.Width = 50;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "STATUS";
            this.Column6.Name = "Column6";
            this.Column6.Width = 60;
            // 
            // cmdSnapMeasurement
            // 
            this.cmdSnapMeasurement.FlatAppearance.BorderSize = 0;
            this.cmdSnapMeasurement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSnapMeasurement.Image = global::WikiOptics_Collimator.Properties.Resources._15_Snap_Button_01_Off_775__631_;
            this.cmdSnapMeasurement.Location = new System.Drawing.Point(5, 295);
            this.cmdSnapMeasurement.Name = "cmdSnapMeasurement";
            this.cmdSnapMeasurement.Size = new System.Drawing.Size(114, 41);
            this.cmdSnapMeasurement.TabIndex = 53;
            this.cmdSnapMeasurement.UseVisualStyleBackColor = true;
            this.cmdSnapMeasurement.Click += new System.EventHandler(this.cmdSnapMeasurement_Click);
            this.cmdSnapMeasurement.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdSnapMeasurement_MouseDown);
            this.cmdSnapMeasurement.MouseEnter += new System.EventHandler(this.btn_cmdSnapMeasurement_MouseEnter);
            this.cmdSnapMeasurement.MouseLeave += new System.EventHandler(this.btn_cmdSnapMeasurement_MouseLeave);
            this.cmdSnapMeasurement.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdSnapMeasurement_MouseUp);
            // 
            // cmdDataExport
            // 
            this.cmdDataExport.FlatAppearance.BorderSize = 0;
            this.cmdDataExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDataExport.Image = global::WikiOptics_Collimator.Properties.Resources._17_Data_Export_Button_01_Off_1019__631_;
            this.cmdDataExport.Location = new System.Drawing.Point(246, 295);
            this.cmdDataExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdDataExport.Name = "cmdDataExport";
            this.cmdDataExport.Size = new System.Drawing.Size(114, 41);
            this.cmdDataExport.TabIndex = 55;
            this.cmdDataExport.UseVisualStyleBackColor = true;
            this.cmdDataExport.Click += new System.EventHandler(this.cmdDataExport_Click);
            this.cmdDataExport.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btn_cmdDataExport_MouseDown);
            this.cmdDataExport.MouseEnter += new System.EventHandler(this.btn_cmdDataExport_MouseEnter);
            this.cmdDataExport.MouseLeave += new System.EventHandler(this.btn_cmdDataExport_MouseLeave);
            this.cmdDataExport.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btn_cmdDataExport_MouseUp);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::WikiOptics_Collimator.Properties.Resources.LCD_Middle_778__134_;
            this.pictureBox5.Location = new System.Drawing.Point(5, 5);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(361, 176);
            this.pictureBox5.TabIndex = 51;
            this.pictureBox5.TabStop = false;
            // 
            // txtPixelPerDistance
            // 
            this.txtPixelPerDistance.Location = new System.Drawing.Point(131, 10);
            this.txtPixelPerDistance.Name = "txtPixelPerDistance";
            this.txtPixelPerDistance.Size = new System.Drawing.Size(74, 21);
            this.txtPixelPerDistance.TabIndex = 34;
            this.txtPixelPerDistance.Text = "4.8";
            this.txtPixelPerDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPixelPerDistance.Visible = false;
            // 
            // imageListButton
            // 
            this.imageListButton.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButton.ImageStream")));
            this.imageListButton.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListButton.Images.SetKeyName(0, "icoCapture.png");
            this.imageListButton.Images.SetKeyName(1, "icoExport.png");
            this.imageListButton.Images.SetKeyName(2, "icoOpen.png");
            this.imageListButton.Images.SetKeyName(3, "IcoSave.png");
            this.imageListButton.Images.SetKeyName(4, "icoStart.png");
            this.imageListButton.Images.SetKeyName(5, "icoStop.png");
            this.imageListButton.Images.SetKeyName(6, "icoTrashcan.png");
            // 
            // button_Live_View
            // 
            this.button_Live_View.FlatAppearance.BorderSize = 0;
            this.button_Live_View.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Live_View.Image = global::WikiOptics_Collimator.Properties.Resources._10_Live_Button_01_Off_775__80_;
            this.button_Live_View.Location = new System.Drawing.Point(785, 48);
            this.button_Live_View.Name = "button_Live_View";
            this.button_Live_View.Size = new System.Drawing.Size(112, 36);
            this.button_Live_View.TabIndex = 35;
            this.button_Live_View.UseVisualStyleBackColor = true;
            this.button_Live_View.Click += new System.EventHandler(this.button1_Click);
            this.button_Live_View.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_Live_View_MouseDown);
            this.button_Live_View.MouseEnter += new System.EventHandler(this.button_Live_View_MouseEnter);
            this.button_Live_View.MouseLeave += new System.EventHandler(this.button_Live_View_MouseLeave);
            // 
            // button_XYPLOT
            // 
            this.button_XYPLOT.FlatAppearance.BorderSize = 0;
            this.button_XYPLOT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_XYPLOT.Image = global::WikiOptics_Collimator.Properties.Resources._11_X_Y_Button_01_Off_897__80_;
            this.button_XYPLOT.Location = new System.Drawing.Point(903, 48);
            this.button_XYPLOT.Name = "button_XYPLOT";
            this.button_XYPLOT.Size = new System.Drawing.Size(112, 36);
            this.button_XYPLOT.TabIndex = 36;
            this.button_XYPLOT.UseVisualStyleBackColor = true;
            this.button_XYPLOT.Click += new System.EventHandler(this.button2_Click);
            this.button_XYPLOT.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_button_XYPLOT_MouseDown);
            this.button_XYPLOT.MouseEnter += new System.EventHandler(this.button_button_XYPLOT_MouseEnter);
            this.button_XYPLOT.MouseLeave += new System.EventHandler(this.button_button_XYPLOT_MouseLeave);
            // 
            // button_SERIAL
            // 
            this.button_SERIAL.FlatAppearance.BorderSize = 0;
            this.button_SERIAL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_SERIAL.Image = global::WikiOptics_Collimator.Properties.Resources._12_Serial_Button_01_Off_1019__80_;
            this.button_SERIAL.Location = new System.Drawing.Point(1021, 48);
            this.button_SERIAL.Name = "button_SERIAL";
            this.button_SERIAL.Size = new System.Drawing.Size(112, 36);
            this.button_SERIAL.TabIndex = 37;
            this.button_SERIAL.UseVisualStyleBackColor = true;
            this.button_SERIAL.Click += new System.EventHandler(this.button3_Click);
            this.button_SERIAL.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_button_SERIAL_MouseDown);
            this.button_SERIAL.MouseEnter += new System.EventHandler(this.button_button_SERIAL_MouseEnter);
            this.button_SERIAL.MouseLeave += new System.EventHandler(this.button_button_SERIAL_MouseLeave);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1157, 666);
            this.Controls.Add(this.button_SERIAL);
            this.Controls.Add(this.button_XYPLOT);
            this.Controls.Add(this.button_Live_View);
            this.Controls.Add(this.TextBox_ShutterSpeed);
            this.Controls.Add(this.Track_ShutterSpeed);
            this.Controls.Add(this.pic_Status15);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.lbStatusDescription);
            this.Controls.Add(this.pic_Status14);
            this.Controls.Add(this.lbTolerance);
            this.Controls.Add(this.pic_Status13);
            this.Controls.Add(this.lbStatusResult);
            this.Controls.Add(this.pic_Status12);
            this.Controls.Add(this.lbCurrentSpotNo);
            this.Controls.Add(this.pic_Status11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pic_Status10);
            this.Controls.Add(this.lbFPS);
            this.Controls.Add(this.pic_Status9);
            this.Controls.Add(this.txtSamplingFreq);
            this.Controls.Add(this.pic_Status8);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.pic_Status7);
            this.Controls.Add(this.comboBox_serialport);
            this.Controls.Add(this.pic_Status6);
            this.Controls.Add(this.lbCurrentMode);
            this.Controls.Add(this.pic_Status5);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pic_Status4);
            this.Controls.Add(this.lbFPSb);
            this.Controls.Add(this.pic_Status3);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.pic_Status2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pic_Status1);
            this.Controls.Add(this.cmdMeasureData);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel_image);
            this.Controls.Add(this.AutoShutter);
            this.Controls.Add(this.cmdFactorySet);
            this.Controls.Add(this.SaveAllConfig);
            this.Controls.Add(this.btn_Capture);
            this.Controls.Add(this.cmdClear);
            this.Controls.Add(this.cmdAnalysis);
            this.Controls.Add(this.cmdExport);
            this.Controls.Add(this.picWikiLogo);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.txtPixelPerDistance);
            this.Controls.Add(this.btn_SettingFileOpen);
            this.Controls.Add(this.tab_LiveView);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(1173, 705);
            this.MinimumSize = new System.Drawing.Size(1173, 705);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = " WikiOptics Autocollimator";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Track_ShutterSpeed)).EndInit();
            this.panel_image.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Camera)).EndInit();
            this.tab_LiveView.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel_Plot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Plot)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSerialData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ImageList imageListButton;
        private TextBox txtPixelPerDistance;

        MenuStrip menuBar;
        ToolStripMenuItem menu_Measurement;
        ToolStripMenuItem menu_Tools;
        ToolStripMenuItem menu_Settings;
        ToolStripMenuItem menu_Tools_ZeroSet;
        ToolStripMenuItem menu_Tools_Mode;
        ToolStripMenuItem menu_Tools_Angledefinition;
        ToolStripMenuItem menu_Tools_Coordinate;
        ToolStripMenuItem menu_Tools_Numbering;
        ToolStripMenuItem menu_Setting_Unit;
        ToolStripMenuItem menu_Setting_Tolerance;
        ToolStripMenuItem menu_Setting_GrayLevel;
        ToolStripMenuItem menu_Information;
        //ToolStripMenuItem menu_Information_About;

        ToolStripMenuItem menu_Tools_Mode_Area;
        ToolStripMenuItem menu_Tools_Mode_Brightness;
        ToolStripMenuItem menu_Tools_Mode_Peak;

        ToolStripMenuItem menu_Tools_Angle_Ext;
        ToolStripMenuItem menu_Tools_Angle_Int;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring;
        ToolStripMenuItem menu_Tools_Numbring_Angle;
        ToolStripMenuItem menu_Tools_Numbring_Size;
        ToolStripMenuItem menu_Settings_Unit_Degree;
        ToolStripMenuItem menu_Settings_Unit_Sec;
        ToolStripMenuItem menu_Settings_Tolerance_Circle;
        ToolStripMenuItem menu_Settings_Tolerance_Square;

        ToolStripMenuItem menu_Tools_Zoom;
        ToolStripMenuItem menu_Tools_Zoom_4;
        ToolStripMenuItem menu_Tools_Zoom_8;
        ToolStripMenuItem menu_Tools_Zoom_16;

        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Xon;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Yon;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Off;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_L90;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_R90;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_Off;
        ToolStripMenuItem menu_Tools_Zoom_Off;
        ToolStripMenuItem menu_Tools_Zoom_32;
        ToolStripMenuItem menu_Tools_Coordinate_Original;
        ToolStripMenuItem menu_Settings_Tolerance_Value;
        Label label2;
        Label label3;
        Label label4;
        Label label5;
        Label label9;
        Label label10;
        Label label11;
        Label label12;
        Label label13;
        Label label14;
        Label label15;
        Label label16;
        Label label17;
        Label label18;
        Label label19;
        Label label20;
        Label label21;
        Label label22;
        Label label23;
        Label label24;
        Label label25;
        Label label26;
        Label label27;
        Label label28;
        Label label29;
        Label label30;
        Label label31;
        Label label32;
        Label label33;
        Label label34;
        Label label35;
        Label label37;

        Label lbCurrentSpotNo;
        Label lbCurrentMode;
        Label lbTolerance;
        Label lbFPSb;
        Label lbFPS;
        Label lbStatusDescription;
        Label lbStatusResult;

        PictureBox pic_Status1;
        PictureBox pic_Status2;
        PictureBox pic_Status3;
        PictureBox pic_Status4;
        PictureBox pic_Status5;
        PictureBox pic_Status6;
        PictureBox pic_Status7;
        PictureBox pic_Status8;
        PictureBox pic_Status9;
        PictureBox pic_Status10;
        PictureBox pic_Status11;
        PictureBox pic_Status12;
        PictureBox pic_Status13;
        PictureBox pic_Status14;
        PictureBox pic_Status15;

        PictureBox pic_Camera;
        PictureBox pic_Plot;
        Panel panel2;
        //Panel panel3;
        //Panel panel4;
        //Panel panel5;
        Panel panel_image;
        Panel panel_Plot;

        Button cmdFactorySet;
        Button cmdAnalysis;
        Button btn_Capture;
        Button cmdClear;
        Button cmdExport;
        Button btn_SettingFileOpen;
        Button cmdMeasureData;
        Button cmdWobbleFlattingClear;
        Button cmdPlotStartStop;

        TabControl tab_LiveView;
        TabPage tabPage1;
        TabPage tabPage2;
        TabPage tabPage3;

        TextBox txtSamplingFreq;
        TextBox txtSpot3to1;
        TextBox txtSpot2to3;
        TextBox txtSpot1to2;
        TextBox txtSpotXY3;
        TextBox txtSpotY3;
        TextBox txtSpotX3;
        TextBox txtSpotXY2;
        TextBox txtSpotX2;
        TextBox txtSpotY2;
        TextBox txtSpotXY1;
        TextBox txtSpotX1;
        TextBox txtSpotY1;

        TextBox txtSerialSpotXY1;
        TextBox txtSerialSpotX1;
        TextBox txtSerialSpotY1;

        TextBox txtSerialSpotXY2;
        TextBox txtSerialSpotX2;
        TextBox txtSerialSpotY2;

        TextBox txtCurrentPartName;

        TextBox txtStartingNo;
        TextBox txtPartName;

        ComboBox cbPlotSpotNo;
        ComboBox cbSerialSpotNo;

        TrackBar Track_ShutterSpeed;

        DataGridView gridSerialData;
        DataGridViewTextBoxColumn Column1;
        DataGridViewTextBoxColumn Column2;
        DataGridViewTextBoxColumn Column3;
        DataGridViewTextBoxColumn Column4;
        DataGridViewTextBoxColumn Column5;
        DataGridViewTextBoxColumn Column6;

        PictureBox picWikiLogo;

        private TextBox TextBox_ShutterSpeed;
        private Button SaveAllConfig;

        private Button cmdDataExport;
        private Button cmdSnapDataClear;
        private Button cmdSnapMeasurement;
        private Label label36;
        private Button AutoShutter;

        #endregion

        private ComboBox comboBox_serialport;
        private Label label1;
        private ToolStripMenuItem toolStripMenuItem1;
        private ToolStripMenuItem toolStripMenuItem2;
        private ToolStripMenuItem toolStripMenuItem3;
        private ToolStripMenuItem toolStripMenuItem4;
        private ToolStripMenuItem toolStripMenuItem5;
        private Label label6;
        private PictureBox pictureBox2;
        private PictureBox pictureBox3;
        private Label label38;
        private PictureBox pictureBox5;
        private PictureBox pictureBox4;
        private PictureBox pictureBox1;
        private Button button_Live_View;
        private Button button_XYPLOT;
        private Button button_SERIAL;
        private PictureBox pictureBox6;
        private PictureBox pictureBox7;
    }
}

