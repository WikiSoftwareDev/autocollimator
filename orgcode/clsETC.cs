﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WikiOptics_Collimator
{
    internal class clsETC
    {
        //object m_ErrStringLock = new object();
        public static string m_szErrString = "";
        
        public clsETC()
        {
        }

        public static bool FileExistStatus(string strFileName)
        {
            return File.Exists(strFileName);
        }

        public static string FuncFileOpen(string strFileName)
        {
            if (File.Exists(strFileName))
            {
                StreamReader streamReader = new StreamReader(strFileName);
                string result = streamReader.ReadToEnd();
                streamReader.Close();
                return result;
            }

            string msg = string.Format("{0} file not found", strFileName);
            MessageBox.Show(new Form
            {
                TopMost = true
            }, msg);
            return "FAIL";
        }

        public static bool FuncFileSave(string strFileName, string strBuffer)
        {
            File.WriteAllText(strFileName, strBuffer);
            return true;
        }

        public static bool CSVFuncFileSave(string strFileName, string strBuffer)
        {
            try
            {
                File.WriteAllText(strFileName, strBuffer, Encoding.Default);
            }
            catch (Exception ex)
            {
                m_szErrString = ex.ToString();
                return false;
            }
            
            return true;
        }

        public static bool FuncFileWrite(string strFileName, string strBuffer)
        {
            if (File.Exists(strFileName))
            {
                StreamWriter streamWriter = new StreamWriter(strFileName, true);
                streamWriter.WriteLine(strBuffer);
                streamWriter.Close();
                return true;
            }

            string msg = string.Format("{0} file not found", strFileName);
            MessageBox.Show(new Form
            {
                TopMost = true
            }, msg);
            return false;
        }

        public static bool FuncFileMove(string strOriginalFileName, string strCopyFileName)
        {
            if (File.Exists(strOriginalFileName))
            {
                File.Move(strOriginalFileName, strCopyFileName);
                return true;
            }

            string msg = string.Format("org : {0}, copy{1},  file not found", 
                strOriginalFileName, strCopyFileName);
            MessageBox.Show(new Form
            {
                TopMost = true
            }, msg);
            return false;
        }

        public static bool FuncFileDelete(string strFileName)
        {
            if (File.Exists(strFileName))
            {
                File.Delete(strFileName);
                return true;
            }

            string msg = string.Format("{0} file not found", strFileName);
            MessageBox.Show(new Form
            {
                TopMost = true
            }, msg);
            return false;
        }

        public static void Time_Delay(long Delay)
        {
            long num = Delay * 10000L;
            long ticks = DateTime.Now.Ticks;
            do
            {
                Application.DoEvents();
            }
            while (DateTime.Now.Ticks - ticks < num);
        }

        public static string ReplaceCommonEscapeSequences(string s)
        {
            return s.Replace("\\n", "\n").Replace("\\r", "\r").Replace("\0", "");
        }

        public static string InsertCommonEscapeSequences(string s)
        {
            return s.Replace("\n", "\\n").Replace("\r", "\\r");
        }

        public static bool ISNumeric(string value)
        {
            float num;
            return float.TryParse(value, out num);
        }

        public static void ConvertColorRichText(RichTextBox txtObject)
        {
            txtObject.Invoke(new MethodInvoker(delegate ()
            {
                txtObject.SelectionStart = 0;
                txtObject.SelectionLength = txtObject.TextLength;
                txtObject.SelectionColor = Color.White;
                txtObject.SelectionFont = new Font("Ariel", 10f, FontStyle.Bold);
                txtObject.ScrollToCaret();
            }));
        }

        public static void WarningMsg(string Message)
        {
            string caption = "Warning";
            MessageBoxButtons buttons = MessageBoxButtons.OK;
            MessageBox.Show(new Form
            {
                TopMost = true
            }, Message, caption, buttons);
        }

        public static string MakeDateString()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }

        public static string ExtractColon(string strLine)
        {
            int num = strLine.IndexOf(":", 0);
            return strLine.Substring(num + 1, strLine.Length - (num + 1));
        }

        public static string ExtractDoubleColon(string strLine)
        {
            int num = strLine.IndexOf("::", 0);
            return strLine.Substring(num + 2, strLine.Length - num - 2);
        }

        public static string ExtractDoubleNumber(string strLine)
        {
            int length = strLine.IndexOf("::", 0);
            return strLine.Substring(0, length);
        }

        public static string TextBoxExtract(RichTextBox txtObject, long SelectPosition)
        {
            string text = txtObject.Text;
            int i = 1;
            int num = i - 1;
            i = text.IndexOf("\n", i);
            if ((long)num <= SelectPosition && SelectPosition <= (long)i)
            {
                txtObject.SelectionStart = num;
                txtObject.SelectionLength = i - num;
                return text.Substring(num, i - num);
            }
            while (i > 0)
            {
                i += 2;
                num = i - 1;
                i = text.IndexOf("\n", i);
                if ((long)num <= SelectPosition && SelectPosition <= (long)i)
                {
                    txtObject.SelectionStart = num;
                    txtObject.SelectionLength = i - num;
                    return text.Substring(num, i - num);
                }
            }
            return "";
        }

        public static void TextHighLight(RichTextBox txtObject, long LinePosition)
        {
            int LineStart = 0;
            int LineEnd = 0;
            int i;
            txtObject.Invoke(new MethodInvoker(delegate ()
            {
                txtObject.SelectionStart = 0;
                txtObject.SelectionLength = txtObject.TextLength;
                txtObject.SelectionColor = Color.White;
                txtObject.SelectionFont = new Font("Ariel", 10f, FontStyle.Bold);
                i = 0;
                while ((long)i < LinePosition)
                {
                    LineStart = LineEnd;
                    LineEnd = txtObject.Text.IndexOf("\n", LineStart + 2);
                    i++;
                }
                txtObject.SelectionStart = LineStart;
                txtObject.SelectionLength = LineEnd - LineStart;
                txtObject.SelectionColor = Color.Red;
                txtObject.SelectionFont = new Font("Ariel", 14f, FontStyle.Bold);
                txtObject.ScrollToCaret();
            }));
        }

        public static string GetFileName(string FilePath)
        {
            return Path.GetFileNameWithoutExtension(FilePath);
        }

        public static void ScreenCapture(int intBitmapWidth, int intBitmapHeight, Point ptSource)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            Bitmap bitmap = new Bitmap(intBitmapWidth, intBitmapHeight);
            Graphics graphics = Graphics.FromImage(bitmap);
            graphics.CopyFromScreen(ptSource, new Point(0, 0), new Size(intBitmapWidth, intBitmapHeight));
            saveFileDialog.Filter = "All Files(*.*)|*.*|Bitmap File(*.bmp)|*.bmp|JPEG File(*.jpg)|*.jpg|PNG File(*.png)|*.png";
            saveFileDialog.FilterIndex = 2; //확장자 BMP default
            //saveFileDialog.InitialDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\Capture";
            saveFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveFileDialog.Title = "Save Capture File";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bitmap.Save(saveFileDialog.FileName);
            }
        }
    }
}
