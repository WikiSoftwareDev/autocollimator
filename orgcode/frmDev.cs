// WikiAngleAutoCollimator.frmDevice
//Device List Camera Select, Refresh 0322
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using WikiOptics_Collimator;

namespace WikiOptics_Collimator
{
    public class frmDevice : Form
    {
        private IContainer components = null;

        private Label label1;

        private ComboBox cbDevice;

        private Button cmdRefresh;

        private Button cmdSelect;

        private Button cmdInstall;

        private frmDevice mAutocollimatorUI;
        //private MainForm mainForm;

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WikiOptics_Collimator.frmDevice));
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.cbDevice = new System.Windows.Forms.ComboBox();
            this.cmdRefresh = new System.Windows.Forms.Button();
            this.cmdSelect = new System.Windows.Forms.Button();
            this.cmdInstall = new System.Windows.Forms.Button();
            base.SuspendLayout();
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(30, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(302, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "The following camera(s) are currently available.";
            this.cbDevice.FormattingEnabled = true;
            this.cbDevice.Location = new System.Drawing.Point(32, 39);
            this.cbDevice.Name = "cbDevice";
            this.cbDevice.Size = new System.Drawing.Size(121, 20);
            this.cbDevice.TabIndex = 1;

            this.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRefresh.Location = new System.Drawing.Point(174, 37);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(75, 23);
            this.cmdRefresh.TabIndex = 2;
            this.cmdRefresh.Text = "Refresh";
            this.cmdRefresh.UseVisualStyleBackColor = true;
            this.cmdRefresh.Click += new System.EventHandler(cmdRefresh_Click);

            this.cmdSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSelect.Location = new System.Drawing.Point(255, 37);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(75, 23);
            this.cmdSelect.TabIndex = 3;
            this.cmdSelect.Text = "Select";
            this.cmdSelect.UseVisualStyleBackColor = true;
            this.cmdSelect.Click += new System.EventHandler(cmdSelect_Click);

            this.cmdInstall.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdInstall.Location = new System.Drawing.Point(335, 37);
            this.cmdInstall.Name = "cmdInstall";
            this.cmdInstall.Size = new System.Drawing.Size(75, 23);
            this.cmdInstall.TabIndex = 4;
            this.cmdInstall.Text = "Install";
            this.cmdInstall.UseVisualStyleBackColor = true;
            this.cmdInstall.Click += new System.EventHandler(cmdInstall_Click);

            base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 12f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            base.ClientSize = new System.Drawing.Size(420, 81);
            base.Controls.Add(this.cmdSelect);
            base.Controls.Add(this.cmdRefresh);
            base.Controls.Add(this.cbDevice);
            base.Controls.Add(this.cmdInstall);
            base.Controls.Add(this.label1);
            base.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
          //  base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon"); //작업표시줄 아이콘인데 지금당장필요없어서 필요할때 수정할것
            base.Name = "frmDevice";
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Device List";
            base.Load += new System.EventHandler(frmDevice_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }

        public frmDevice(MainForm mainForm)
        {
            InitializeComponent();           
        }

        private void frmDevice_Load(object sender, EventArgs e)
        {
            CPGCamera.ShowDeviceList();
            for (int i = 0; i < CPGCamera.DeviceNumer; i++)
            {
                cbDevice.Items.Add(CPGCamera.DeviceSerial[i]);
            }
        }

        private void cmdSelect_Click(object sender, EventArgs e)
        {
            if (cbDevice.SelectedIndex >= 0)
            {
                CPGCamera.CameraSerial = Convert.ToUInt32(cbDevice.Items[cbDevice.SelectedIndex].ToString());
                CPGCamera.SelectDeviceNumber = (uint)cbDevice.SelectedIndex;
            }
           base.DialogResult = DialogResult.OK;
        }

        private void cmdInstall_Click(object sender, EventArgs e)
        {
            string StartupPath = System.Windows.Forms.Application.StartupPath;
            int size = 0;
            string infpath = string.Format("{0}\\driver\\pgrusbcam.inf", StartupPath);

            // INF 파일을 설치합니다.
            bool success = NativeMethods.SetupCopyOEMInf(
                infpath,
                null,
                OemSourceMediaType.SPOST_PATH,
                0,
                null,
                0,
                ref size,
                null);
            if (!success)
            {
                // INF 파일 설치 실패 시 발생한 오류 코드를 가져옵니다.
                int errorCode = Marshal.GetLastWin32Error();
                MessageBox.Show("The INF file installation failed. Error code: " + errorCode.ToString(), "Failure", MessageBoxButtons.OK, MessageBoxIcon.Error);

                // 오류 코드에 해당하는 예외를 생성하여 throw 합니다.
                throw new Win32Exception(errorCode);                
            }
            else //if (success)
            {
                MessageBox.Show("The driver has been successfully installed. Please restart your computer to apply the changes.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void cmdRefresh_Click(object sender, EventArgs e)
        {
            cbDevice.Items.Clear();
            CPGCamera.ShowDeviceList();
            for (int i = 0; i < CPGCamera.DeviceNumer; i++)
            {
                cbDevice.Items.Add(CPGCamera.DeviceSerial[i]);
            }
        }
    }
}
