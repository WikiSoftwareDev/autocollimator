﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.IO.Ports;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using FlyCapture2Managed;
using FlyCapture2Managed.Gui;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Threading;
using System.Timers;
using System.Runtime.InteropServices;
using System.Drawing.Drawing2D;

namespace WikiOptics_Collimator
{
    public partial class MainForm : Form
    {
        System.Timers.Timer MeasureTimer = null;
        System.Timers.Timer SaveDataTimer = null;

        public object m_LockObj = new object();

        static AutoResetEvent m_grabThreadExited;
        static BackgroundWorker m_grabThread;

        Graphics pic_camera_graphic;

        int PicOffsetX;
        int PicOffsetY;
        //string strFPS;
        //int nCount = 20;
        public static int Tabindex = 0;
        bool UpdateUIFlag = false;

        private delegate void TimerEventFiredDelegate();
        private delegate void SaveTimerEventFiredDelegate();

        PictureBox[] PicBrightness;

        CSerialOut m_SerialOut = new CSerialOut();
        CWikiTcpServer m_socket_server = new CWikiTcpServer();
       
        

        

        private object _lockObject_grab = new object();

        public MainForm()
        {
            InitializeComponent();
            
            CamParamInfo.LoadCameraInfo();

            CImageProcess.OriginalImage = new IplImage((int)CamParamInfo.ImageWidth, (int)CamParamInfo.ImageHeight, BitDepth.U8, 3);
            CImageProcess.GrayImage = new IplImage((int)CamParamInfo.ImageWidth, (int)CamParamInfo.ImageHeight, BitDepth.U8, 1);

            this.PicBrightness = new PictureBox[]
            {
                this.pic_Status1,
                this.pic_Status2,
                this.pic_Status3,
                this.pic_Status4,
                this.pic_Status5,
                this.pic_Status6,
                this.pic_Status7,
                this.pic_Status8,
                this.pic_Status9,
                this.pic_Status10,
                this.pic_Status11,
                this.pic_Status12,
                this.pic_Status13,
                this.pic_Status14,
                this.pic_Status15
            };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            pic_camera_graphic = pic_Camera.CreateGraphics();
            if (pic_Camera.Width > 512)
            {
                PicOffsetX = (pic_Camera.Width - 512) / 2;
            }
            else
            {
                PicOffsetX = 0;
            }

            if (pic_Camera.Height > 512)
            {
                PicOffsetY = (pic_Camera.Height - 512) / 2;
            }
            else
            {
                PicOffsetY = 0;
            }
            
            frmDevice frmDevice2 = new frmDevice(this);
            DialogResult dialogResult = frmDevice2.ShowDialog();
            if (dialogResult != DialogResult.OK)
            {
                Application.ExitThread();
                Environment.Exit(0);
            }

            CSetInfo.LoadSettingInfo();
            CZeroSetInfo.LoadZeroSetInfo();
            clsAboutInfo.LoadAboutInfo();
            CParameter.LoadParameterInfo();
            clsMeasureData.LoadMeasureInfo();

            UpdateCheckBox();
            SetDisplayValue();

            CImageProcess.nAnalysisAverage = CParameter.AverageNumber;
            CImageProcess.nAnalysisCurrent = 0;
            CImageProcess.WobblePlottingImage = new IplImage((int)CamParamInfo.ImageWidth, (int)CamParamInfo.ImageHeight, BitDepth.U8, 3);
            CImageProcess.BlackImage = new IplImage((int)CamParamInfo.ImageWidth, (int)CamParamInfo.ImageHeight, BitDepth.U8, 3);
            Cv.Set(CImageProcess.WobblePlottingImage, new CvScalar(255.0, 255.0, 255.0));

            if (clsAboutInfo.m_nAboutFactory == 1)
            {
                cmdFactorySet.Visible = true;
            }
            else
            {
                cmdFactorySet.Visible = false;
            }
           
            Track_ShutterSpeed.Value = (int)(CamParamInfo.ShutterSpeed * 1000f);
            //float ShutterValue = (float)Track_ShutterSpeed.Value / 1000f;            
            TextBox_ShutterSpeed.Text = string.Format("{0:f1}", CamParamInfo.ShutterSpeed);
            
            CPGCamera.SetShutterSpeed(CamParamInfo.ShutterSpeed);

            string[] ArrayComPortsNames = SerialPort.GetPortNames();
            int no_serial = ArrayComPortsNames.Length;
            if (no_serial > 0)
            {
                for (int i = 0; i < no_serial; i++)
                {
                    comboBox_serialport.Items.Add(ArrayComPortsNames[i]);
                }
            }
            else
            {
                comboBox_serialport.Items.Add("no serial");
            }
           
            ConnectCamera();
           
            m_socket_server.LoadConfigFile();
            m_socket_server.StartListen();
          


        }
   
             private void Track_ShutterSpeed_Scroll(object sender, EventArgs e)//Camera Shot 속도 스크롤 
        {

            float num = Track_ShutterSpeed.Value / 1000f;
            TextBox_ShutterSpeed.Text = num.ToString("F1");

            CamParamInfo.ShutterSpeed = num;
            CPGCamera.SetShutterSpeed(num);
        }
    

  


        private void SetDisplayValue()
        {
            Track_ShutterSpeed.Value = (int)(CamParamInfo.ShutterSpeed * 1000f);
            TextBox_ShutterSpeed.Text = $"{CamParamInfo.ShutterSpeed:f1}";
            
        }
       


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_socket_server.StopListen();

            CPGCamera.m_grabImages = false;
            if (m_grabThreadExited != null)
            {
                m_grabThreadExited.WaitOne(2 * 1000);      // 2 seconds
                m_grabThreadExited.Dispose();
                m_grabThreadExited = null;
            }

            if (CPGCamera.camera == null)
                return;

            CPGCamera.StopCameraCapture();
            try
            {                
                CPGCamera.camera.Disconnect();
            }
            catch
            {
            }            
        }

        private void InitCamera()
        {
            CPGCamera.initCamera();
            CPGCamera.StartCameraCapture();
        }

        private void ConnectCamera()
        {
            if (!CPGCamera.initCamera())
            {
                MessageBox.Show("There is no camera, check camera connection state.");
                return;
            }

            if (!CPGCamera.SetCustomVideoMode())
            {
                MessageBox.Show("SetCustomVideoMode error");
                return;
            }

            if (!CPGCamera.SetCameraSetting())
            {
                MessageBox.Show("SetCameraSetting error");
                return;
            }

            CPGCamera.m_rawImage = new ManagedImage();
            CPGCamera.m_processedImage = new ManagedImage();
            CPGCamera.m_AnalysisImage = new ManagedImage();
            CPGCamera.m_camCtlDlg = new CameraControlDialog();
            m_grabThreadExited = new AutoResetEvent(false);
            UpdateUIFlag = true;
            RunCamera();
        }

        void RunCamera()
        {
            try
            {
                EmbeddedImageInfo embeddedImageInfo = CPGCamera.camera.GetEmbeddedImageInfo();
                embeddedImageInfo.timestamp.onOff = true;
                CPGCamera.camera.SetEmbeddedImageInfo(embeddedImageInfo);
                CPGCamera.StartCameraCapture();
                CPGCamera.m_grabImages = true;

                m_grabThread = new BackgroundWorker();
                m_grabThread.DoWork += new DoWorkEventHandler(GrabLoop);
                m_grabThread.WorkerReportsProgress = true;
                m_grabThread.ProgressChanged += new ProgressChangedEventHandler(UpdateUI);
                m_grabThread.RunWorkerAsync();
            }
            catch (FC2Exception)
            {
                Environment.ExitCode = -1;
                Application.Exit();
            }
        }

        private void GrabLoop(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker backgroundWorker = sender as BackgroundWorker;
            int nFailure_count = 0;

            while (CPGCamera.m_grabImages == true)
            {
                if (nFailure_count > 10)
                {
                    DialogResult result = MessageBox.Show("USB Camera is not compatible with this system. This device require USB 3.0 port.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (result == DialogResult.OK)
                    {
                        Application.Exit();
                    }
                    return;
                }

                try
                {
                   CPGCamera.camera.RetrieveBuffer(CPGCamera.m_rawImage);
                    CPGCamera.m_rawImage.Convert(PixelFormat.PixelFormatBgr, CPGCamera.m_processedImage);
                    CImageProcess.OriginalImage.CopyFrom((Bitmap)(object)CPGCamera.m_processedImage.bitmap);

                    /*
                     * for grabbed image network transmission 
                     * 
                    if (m_socket_server.csock_list.Count > 0)
                    {
                        IplImage networkGrayImage = new IplImage(CImageProcess.OriginalImage.Size, BitDepth.U8, 1);
                        CImageProcess.OriginalImage.CvtColor(networkGrayImage, OpenCvSharp.ColorConversion.BgrToGray);
                        int length = networkGrayImage.Size.Width * networkGrayImage.Size.Height;
                        byte[] byteImg = new byte[length];
                        Marshal.Copy( networkGrayImage.ImageData, byteImg, 0, length);

                        m_socket_server.SendImageData(byteImg);
                        networkGrayImage.Dispose();
                    }
                    */

                    if (UpdateUIFlag == true)
                    {
                        lock (_lockObject_grab)
                        {
                            CImageProcess.CheckCalibration();
                        }

                        Application.DoEvents();
                        backgroundWorker.ReportProgress(0);
                    }
                }
                catch (FC2Exception)
                {
                    InitCamera();
                    nFailure_count++;
                    continue;
                }

                clsETC.Time_Delay(70L);
                nFailure_count = 0;                
            }
            m_grabThreadExited.Set();
        }

        private void UpdateUI(object sender, ProgressChangedEventArgs e)
        {
            this.UpdateUIFlag = false;
            this.lbTolerance.Text = string.Concat(new string[]
            {
                CSetInfo.Setting_Tolerance_Hour.ToString(),
                "° ",
                CSetInfo.Setting_Tolerance_Min.ToString(),
                "' ",
                string.Format("{0:f0}", CSetInfo.Setting_Tolerance_Sec),
                "''"
            });

            if (CSetInfo.Tool_Zoom_Flag == 1 && CSetInfo.Tool_Zoom_State == 0)
            {
                CSetInfo.Tool_Zoom_Flag = 0;
                this.UpdateCheckBox();
            }

            if (CImageProcess.AnalysisDisplayFlag)
            {
                UpdateResultSpotAngle();
                UpdateBrightnessStatus();
                lock (CImageProcess.DisplayImage_lock)
                {
                    DrawCameraImage();
                }
                    
                CImageProcess.AnalysisDisplayFlag = false;
            }

            if (!CImageProcess.CalibrationFlag)
            {
                lock (CImageProcess.DisplayImage_lock)
                {
                    this.pic_Camera.Image = CImageProcess.DisplayImage.ToBitmap();
                }
                this.pic_Camera.Invalidate();
            }

            if (CPlotting.WobblePlottingFlag)
            {
                this.pic_Plot.Image = CImageProcess.WobblePlottingImage.ToBitmap();
                this.pic_Plot.Invalidate();
            }
          

            if (!CPlotting.WobblePlottingFlag && CPlotting.WobblePlottingClearFlag)
            {
                this.pic_Plot.Image = CImageProcess.WobblePlottingImage.ToBitmap();
                this.pic_Plot.Invalidate();
                CPlotting.WobblePlottingClearFlag = false;
            }

            try
            {
                this.lbFPS.Text = string.Format("{0:f3}", CImageProcess.FrameFPS);
            }
            catch (FC2Exception)
            {
                this.lbFPS.Text = string.Format("{0:f3}", 0);
                MessageBox.Show("Disconnect Camera~!!");
                return;
            }
            this.UpdateUIFlag = true;
        }

        private void DrawCameraImage()
        {
            if (pic_Camera.Width != pic_Camera.Height)
            {
                if (pic_Camera.Width < pic_Camera.Height)
                {
                    pic_camera_graphic.DrawImage((Image)(object)CImageProcess.DisplayImage.ToBitmap(), PicOffsetX, PicOffsetY, pic_Camera.Width, pic_Camera.Width);
                }
                else
                {
                    pic_camera_graphic.DrawImage((Image)(object)CImageProcess.DisplayImage.ToBitmap(), PicOffsetX, PicOffsetY, pic_Camera.Height, pic_Camera.Height);
                }
            }
            else
            {
                pic_camera_graphic.DrawImage(
                    (Image)(object)CImageProcess.DisplayImage.ToBitmap(),
                    0, 
                    0, 
                    pic_Camera.Width, 
                    pic_Camera.Height);
            }
        }

        private void UpdateCheckBox()
        {
            this.menu_Tools_Mode_Area.Checked = false;
            this.menu_Tools_Mode_Brightness.Checked = false;
            this.menu_Tools_Mode_Peak.Checked = false;
            switch (CSetInfo.Tool_Mode)
            {
                case 1:
                    this.menu_Tools_Mode_Area.Checked = true;
                    this.lbCurrentMode.Text = "AREA CENTER";
                    break;
                case 2:
                    this.menu_Tools_Mode_Brightness.Checked = true;
                    this.lbCurrentMode.Text = "BRIGHTNESS CENTER";
                    break;
                case 3:
                    this.menu_Tools_Mode_Peak.Checked = true;
                    this.lbCurrentMode.Text = "PEAK CENTER";
                    break;
            }

            CImageProcess.CalibrationMode = CSetInfo.Tool_Mode;
            this.menu_Tools_Angle_Ext.Checked = false;
            this.menu_Tools_Angle_Int.Checked = false;
            switch (CSetInfo.Tool_Angle_Definition)
            {
                case 1:
                    this.menu_Tools_Angle_Ext.Checked = true;
                    CSetInfo.Tool_Angle_Definition_Value = 2;
                    break;
                case 2:
                    this.menu_Tools_Angle_Int.Checked = true;
                    CSetInfo.Tool_Angle_Definition_Value = 1;
                    break;
            }

            this.menu_Tools_Coordinate_Original.Checked = false;
            this.menu_Tools_Coordinate_Mirroring.Checked = false;
            this.menu_Tools_Coordinate_Mirroring_Xon.Checked = false;
            this.menu_Tools_Coordinate_Mirroring_Yon.Checked = false;
            this.menu_Tools_Coordinate_Mirroring_Off.Checked = false;
            this.menu_Tools_Coordinate_Rotating.Checked = false;
            this.menu_Tools_Coordinate_Rotating_L90.Checked = false;
            this.menu_Tools_Coordinate_Rotating_R90.Checked = false;
            this.menu_Tools_Coordinate_Rotating_Off.Checked = false;

            // Tool_Coordinate_Original           = 0;
            // Tool_Coordinate_Rotating           = 1;
            // Tool_Coordinate_Mirroring          = 2;
            switch (CSetInfo.Tool_Coordinate)
            {
                case 0:
                    this.menu_Tools_Coordinate_Original.Checked = true;
                    this.menu_Tools_Coordinate_Mirroring_Off.Checked = true;
                    this.menu_Tools_Coordinate_Rotating_Off.Checked = true;
                    break;
                case 1:
                    this.menu_Tools_Coordinate_Rotating.Checked = true;
                    if (CSetInfo.Tool_Coordinate_Rotating_State == 2)
                    {
                        this.menu_Tools_Coordinate_Rotating_L90.Checked = true;
                    }
                    if (CSetInfo.Tool_Coordinate_Rotating_State == 1)
                    {
                        this.menu_Tools_Coordinate_Rotating_R90.Checked = true;
                    }
                    this.menu_Tools_Coordinate_Mirroring_Off.Checked = true;
                    break;
                case 2:
                    this.menu_Tools_Coordinate_Mirroring.Checked = true;
                    if (CSetInfo.Tool_Coordinate_Mirroring_State == 1)
                    {
                        this.menu_Tools_Coordinate_Mirroring_Xon.Checked = true;
                    }
                    if (CSetInfo.Tool_Coordinate_Mirroring_State == 2)
                    {
                        this.menu_Tools_Coordinate_Mirroring_Yon.Checked = true;
                    }
                    this.menu_Tools_Coordinate_Rotating_Off.Checked = true;
                    break;
            }
            //ZOOM 확인
            this.menu_Tools_Zoom_4.Checked = false;
            this.menu_Tools_Zoom_8.Checked = false;
            this.menu_Tools_Zoom_16.Checked = false;
            this.menu_Tools_Zoom_32.Checked = false;
            this.menu_Tools_Zoom_Off.Checked = false;
            int tool_Zoom_State = CSetInfo.Tool_Zoom_State;
            if (tool_Zoom_State <= 4)
            {
                if (tool_Zoom_State != 0)
                {
                    if (tool_Zoom_State == 4)
                    {
                        this.menu_Tools_Zoom.Checked = true;
                        this.menu_Tools_Zoom_4.Checked = true;
                    }
                }
                else
                {
                    this.menu_Tools_Zoom.Checked = false;
                    this.menu_Tools_Zoom_Off.Checked = true;
                }
            }
            else if (tool_Zoom_State != 8)
            {
                if (tool_Zoom_State != 16)
                {
                    if (tool_Zoom_State == 32)
                    {
                        this.menu_Tools_Zoom.Checked = true;
                        this.menu_Tools_Zoom_32.Checked = true;
                    }
                }
                else
                {
                    this.menu_Tools_Zoom.Checked = true;
                    this.menu_Tools_Zoom_16.Checked = true;
                }
            }
            else
            {
                this.menu_Tools_Zoom.Checked = true;
                this.menu_Tools_Zoom_8.Checked = true;
            }
            this.menu_Tools_Numbring_Angle.Checked = false;
            this.menu_Tools_Numbring_Size.Checked = false;
            switch (CSetInfo.Tool_Numbering)
            {
                case 1:
                    this.menu_Tools_Numbring_Angle.Checked = true;
                    break;
                case 2:
                    this.menu_Tools_Numbring_Size.Checked = true;
                    break;
            }
            this.menu_Settings_Unit_Degree.Checked = false;
            this.menu_Settings_Unit_Sec.Checked = false;
            switch (CSetInfo.Setting_Unit)
            {
                case 1:
                    this.menu_Settings_Unit_Degree.Checked = true;
                    break;
                case 2:
                    this.menu_Settings_Unit_Sec.Checked = true;
                    break;
            }
            this.menu_Settings_Tolerance_Circle.Checked = false;
            this.menu_Settings_Tolerance_Square.Checked = false;
            switch (CSetInfo.Setting_Tolerance_Mode)
            {
                case 1:
                    this.menu_Settings_Tolerance_Circle.Checked = true;
                    return;
                case 2:
                    this.menu_Settings_Tolerance_Square.Checked = true;
                    return;
                default:
                    return;
            }
        }

        private void UpdateResultSpotAngle()
        {
            this.lbCurrentSpotNo.Text = CImageProcess.SearchSpotNumber.ToString();

            // set serial out data..  ys.lee
            if (CImageProcess.SearchSpotNumber == 1)
            {
                m_SerialOut.WriteResult(CImageProcess.AngleX[0], CImageProcess.AngleY[0], CImageProcess.AngleD[0]);
            }
            else if (CImageProcess.SearchSpotNumber > 1)
            {
                m_SerialOut.WriteResult(CImageProcess.AngleX, CImageProcess.AngleY, CImageProcess.AngleD);
            }

            if (CSetInfo.Setting_Unit == 1)
            {
                if (CImageProcess.SearchSpotNumber == 1)
                {
                    this.txtSpotX1.Text = string.Format("{0:f6}", CImageProcess.AngleX[0]) + " °";
                    this.txtSpotY1.Text = string.Format("{0:f6}", CImageProcess.AngleY[0]) + " °";
                    this.txtSpotXY1.Text = string.Format("{0:f6}", CImageProcess.AngleD[0]) + " °";
                    this.txtSerialSpotX1.Text = this.txtSpotX1.Text;
                    this.txtSerialSpotY1.Text = this.txtSpotY1.Text;
                    this.txtSerialSpotXY1.Text = this.txtSpotXY1.Text;
                    this.txtSpotX2.Text = "";
                    this.txtSpotY2.Text = "";
                    this.txtSpotXY2.Text = "";
                    this.txtSpotX3.Text = "";
                    this.txtSpotY3.Text = "";
                    this.txtSpotXY3.Text = "";
                    this.txtSerialSpotX2.Text = this.txtSpotX2.Text;
                    this.txtSerialSpotY2.Text = this.txtSpotY2.Text;
                    this.txtSerialSpotXY2.Text = this.txtSpotXY2.Text;
                    this.txtSpot1to2.Text = "";
                    this.txtSpot2to3.Text = "";
                    this.txtSpot3to1.Text = "";
                }

                if (CImageProcess.SearchSpotNumber == 2)
                {
                    this.txtSpotX1.Text = string.Format("{0:f6}", CImageProcess.AngleX[0]) + " °";
                    this.txtSpotY1.Text = string.Format("{0:f6}", CImageProcess.AngleY[0]) + " °";
                    this.txtSpotXY1.Text = string.Format("{0:f6}", CImageProcess.AngleD[0]) + " °";
                    this.txtSpotX2.Text = string.Format("{0:f6}", CImageProcess.AngleX[1]) + " °";
                    this.txtSpotY2.Text = string.Format("{0:f6}", CImageProcess.AngleY[1]) + " °";
                    this.txtSpotXY2.Text = string.Format("{0:f6}", CImageProcess.AngleD[1]) + " °";
                    this.txtSpot1to2.Text = string.Format("{0:f6}", CImageProcess.Angle1to2) + " °";
                    this.txtSerialSpotX1.Text = this.txtSpotX1.Text;
                    this.txtSerialSpotY1.Text = this.txtSpotY1.Text;
                    this.txtSerialSpotXY1.Text = this.txtSpotXY1.Text;
                    this.txtSerialSpotX2.Text = this.txtSpotX2.Text;
                    this.txtSerialSpotY2.Text = this.txtSpotY2.Text;
                    this.txtSerialSpotXY2.Text = this.txtSpotXY2.Text;
                    this.txtSpotX3.Text = "";
                    this.txtSpotY3.Text = "";
                    this.txtSpotXY3.Text = "";
                    this.txtSpot2to3.Text = "";
                    this.txtSpot3to1.Text = "";
                }

                if (CImageProcess.SearchSpotNumber == 3)
                {
                    this.txtSpotX1.Text = string.Format("{0:f6}", CImageProcess.AngleX[0]) + " °";
                    this.txtSpotY1.Text = string.Format("{0:f6}", CImageProcess.AngleY[0]) + " °";
                    this.txtSpotXY1.Text = string.Format("{0:f6}", CImageProcess.AngleD[0]) + " °";
                    this.txtSpotX2.Text = string.Format("{0:f6}", CImageProcess.AngleX[1]) + " °";
                    this.txtSpotY2.Text = string.Format("{0:f6}", CImageProcess.AngleY[1]) + " °";
                    this.txtSpotXY2.Text = string.Format("{0:f6}", CImageProcess.AngleD[1]) + " °";
                    this.txtSpotX3.Text = string.Format("{0:f6}", CImageProcess.AngleX[2]) + " °";
                    this.txtSpotY3.Text = string.Format("{0:f6}", CImageProcess.AngleY[2]) + " °";
                    this.txtSpotXY3.Text = string.Format("{0:f6}", CImageProcess.AngleD[2]) + " °";
                    this.txtSpot1to2.Text = string.Format("{0:f6}", CImageProcess.Angle1to2) + " °";
                    this.txtSpot2to3.Text = string.Format("{0:f6}", CImageProcess.Angle2to3) + " °";
                    this.txtSpot3to1.Text = string.Format("{0:f6}", CImageProcess.Angle3to1) + " °";
                    return;
                }
            } // if (CSetInfo.Setting_Unit == 1)
            else
            {
                if (CImageProcess.SearchSpotNumber == 1)
                {
                    if (CImageProcess.AngleX[0] < 0.0)
                    {
                        this.txtSpotX1.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourX[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[0]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotX1.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourX[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[0]),
                            " ''"
                        });
                    }
                    if (CImageProcess.AngleY[0] < 0.0)
                    {
                        this.txtSpotY1.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourY[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[0]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotY1.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourY[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[0]),
                            " ''"
                        });
                    }
                    this.txtSpotXY1.Text = string.Concat(new string[]
                    {
                        CImageProcess.AngleHourXY[0].ToString(),
                        " ° ",
                        CImageProcess.AngleMinuteXY[0].ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.AngleSecondXY[0]),
                        " ''"
                    });
                    this.txtSerialSpotX1.Text = this.txtSpotX1.Text;
                    this.txtSerialSpotY1.Text = this.txtSpotY1.Text;
                    this.txtSerialSpotXY1.Text = this.txtSpotXY1.Text;
                    this.txtSpotX2.Text = "";
                    this.txtSpotY2.Text = "";
                    this.txtSpotXY2.Text = "";
                    this.txtSpotX3.Text = "";
                    this.txtSpotY3.Text = "";
                    this.txtSpotXY3.Text = "";
                    this.txtSerialSpotX2.Text = this.txtSpotX2.Text;
                    this.txtSerialSpotY2.Text = this.txtSpotY2.Text;
                    this.txtSerialSpotXY2.Text = this.txtSpotXY2.Text;
                    this.txtSpot1to2.Text = "";
                    this.txtSpot2to3.Text = "";
                    this.txtSpot3to1.Text = "";
                }

                if (CImageProcess.SearchSpotNumber == 2)
                {
                    if (CImageProcess.AngleX[0] < 0.0)
                    {
                        this.txtSpotX1.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourX[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[0]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotX1.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourX[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[0]),
                            " ''"
                        });
                    }
                    if (CImageProcess.AngleY[0] < 0.0)
                    {
                        this.txtSpotY1.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourY[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[0]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotY1.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourY[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[0]),
                            " ''"
                        });
                    }
                    this.txtSpotXY1.Text = string.Concat(new string[]
                    {
                        CImageProcess.AngleHourXY[0].ToString(),
                        " ° ",
                        CImageProcess.AngleMinuteXY[0].ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.AngleSecondXY[0]),
                        " ''"
                    });
                    if (CImageProcess.AngleX[1] < 0.0)
                    {
                        this.txtSpotX2.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourX[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[1]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotX2.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourX[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[1]),
                            " ''"
                        });
                    }
                    if (CImageProcess.AngleY[1] < 0.0)
                    {
                        this.txtSpotY2.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourY[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[1]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotY2.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourY[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[1]),
                            " ''"
                        });
                    }
                    this.txtSpotXY2.Text = string.Concat(new string[]
                    {
                        CImageProcess.AngleHourXY[1].ToString(),
                        " ° ",
                        CImageProcess.AngleMinuteXY[1].ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.AngleSecondXY[1]),
                        " ''"
                    });
                    this.txtSpot1to2.Text = string.Concat(new string[]
                    {
                        CImageProcess.Angle1to2HourXY.ToString(),
                        " ° ",
                        CImageProcess.Angle1to2MinuteXY.ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.Angle1to2SecondXY),
                        " ''"
                    });
                    this.txtSerialSpotX1.Text = this.txtSpotX1.Text;
                    this.txtSerialSpotY1.Text = this.txtSpotY1.Text;
                    this.txtSerialSpotXY1.Text = this.txtSpotXY1.Text;
                    this.txtSerialSpotX2.Text = this.txtSpotX2.Text;
                    this.txtSerialSpotY2.Text = this.txtSpotY2.Text;
                    this.txtSerialSpotXY2.Text = this.txtSpotXY2.Text;
                    this.txtSpotX3.Text = "";
                    this.txtSpotY3.Text = "";
                    this.txtSpotXY3.Text = "";
                    this.txtSpot2to3.Text = "";
                    this.txtSpot3to1.Text = "";
                }
                if (CImageProcess.SearchSpotNumber == 3)
                {
                    if (CImageProcess.AngleX[0] < 0.0)
                    {
                        this.txtSpotX1.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourX[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[0]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotX1.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourX[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[0]),
                            " ''"
                        });
                    }
                    if (CImageProcess.AngleY[0] < 0.0)
                    {
                        this.txtSpotY1.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourY[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[0]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotY1.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourY[0].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[0].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[0]),
                            " ''"
                        });
                    }
                    this.txtSpotXY1.Text = string.Concat(new string[]
                    {
                        CImageProcess.AngleHourXY[0].ToString(),
                        " ° ",
                        CImageProcess.AngleMinuteXY[0].ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.AngleSecondXY[0]),
                        " ''"
                    });
                    if (CImageProcess.AngleX[1] < 0.0)
                    {
                        this.txtSpotX2.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourX[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[1]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotX2.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourX[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[1]),
                            " ''"
                        });
                    }
                    if (CImageProcess.AngleY[1] < 0.0)
                    {
                        this.txtSpotY2.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourY[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[1]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotY2.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourY[1].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[1].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[1]),
                            " ''"
                        });
                    }
                    this.txtSpotXY2.Text = string.Concat(new string[]
                    {
                        CImageProcess.AngleHourXY[1].ToString(),
                        " ° ",
                        CImageProcess.AngleMinuteXY[1].ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.AngleSecondXY[1]),
                        " ''"
                    });
                    if (CImageProcess.AngleX[2] < 0.0)
                    {
                        this.txtSpotX3.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourX[2].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[2].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[2]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotX3.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourX[2].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteX[2].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondX[2]),
                            " ''"
                        });
                    }
                    if (CImageProcess.AngleY[2] < 0.0)
                    {
                        this.txtSpotY3.Text = string.Concat(new string[]
                        {
                            "-",
                            CImageProcess.AngleHourY[2].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[2].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[2]),
                            " ''"
                        });
                    }
                    else
                    {
                        this.txtSpotY3.Text = string.Concat(new string[]
                        {
                            CImageProcess.AngleHourY[2].ToString(),
                            " ° ",
                            CImageProcess.AngleMinuteY[2].ToString(),
                            " '",
                            string.Format("{0:f0}", CImageProcess.AngleSecondY[2]),
                            " ''"
                        });
                    }
                    this.txtSpotXY3.Text = string.Concat(new string[]
                    {
                        CImageProcess.AngleHourXY[2].ToString(),
                        " ° ",
                        CImageProcess.AngleMinuteXY[2].ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.AngleSecondXY[2]),
                        " ''"
                    });
                    this.txtSpot1to2.Text = string.Concat(new string[]
                    {
                        CImageProcess.Angle1to2HourXY.ToString(),
                        " °",
                        CImageProcess.Angle1to2MinuteXY.ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.Angle1to2SecondXY),
                        " ''"
                    });
                    this.txtSpot2to3.Text = string.Concat(new string[]
                    {
                        CImageProcess.Angle2to3HourXY.ToString(),
                        " °",
                        CImageProcess.Angle2to3MinuteXY.ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.Angle2to3SecondXY),
                        " ''"
                    });
                    this.txtSpot3to1.Text = string.Concat(new string[]
                    {
                        CImageProcess.Angle3to1HourXY.ToString(),
                        " °",
                        CImageProcess.Angle3to1MinuteXY.ToString(),
                        " '",
                        string.Format("{0:f0}", CImageProcess.Angle3to1SecondXY),
                        " ''"
                    });
                }
            }
        }

        private void UpdateBrightnessStatus()
        {
            if (CImageProcess.CalibrationFlag)
            {
                this.lbStatusResult.Visible = true;
                this.lbStatusDescription.Visible = true;
            }
            else
            {
                this.lbStatusResult.Visible = false;
                this.lbStatusDescription.Visible = false;
            }

            if (CImageProcess.AnalysisMaxIntensity < CSetInfo.Setting_Gray_MinLevel)
            {
                this.lbStatusResult.ForeColor = Color.Red;                    
                this.lbStatusDescription.ForeColor = Color.Red;
                this.lbStatusDescription.Text = "Nothing is Detected";

                for (int i = 0; i < 15; i++)
                {
                    if (i < 3)
                    {
                        this.PicBrightness[i].Visible = true;
                    }
                    else
                    {
                        this.PicBrightness[i].Visible = false;
                    }
                }

                if (CImageProcess.AnalysisMaxIntensity == 0)
                {
                    this.lbStatusResult.Text = "ERROR";
                }
                else if (CImageProcess.AnalysisMaxIntensity != 0)
                {                     
                    this.lbStatusResult.Text = "ERROR";                    
                }
                return;
            } // if (CImageProcess.AnalysisMaxIntensity < CSetInfo.Setting_Gray_MinLevel)

            if (CImageProcess.AnalysisMaxIntensity >= CSetInfo.Setting_Gray_MaxLevel)
            {
                if (CImageProcess.nAnalysisMaxIntensity == 1)
                {
                    for (int j = 0; j < 15; j++)
                    {
                        if (j < 13)
                        {
                            this.PicBrightness[j].Visible = true;
                        }
                        else
                        {
                            this.PicBrightness[j].Visible = false;
                        }
                    }
                }
                else if (CImageProcess.nAnalysisMaxIntensity == 2)
                {
                    for (int k = 0; k < 15; k++)
                    {
                        if (k < 14)
                        {
                            this.PicBrightness[k].Visible = true;
                        }
                        else
                        {
                            this.PicBrightness[k].Visible = false;
                        }
                    }
                }
                else
                {
                    for (int l = 0; l < 15; l++)
                    {
                        this.PicBrightness[l].Visible = true;
                    }
                }
                this.lbStatusResult.ForeColor = Color.Red;
                this.lbStatusResult.Text = "ERROR";
                this.lbStatusDescription.ForeColor = Color.Red;
                this.lbStatusDescription.Text = "Saturation occur.";
                return;
            } // if (CImageProcess.AnalysisMaxIntensity >= CSetInfo.Setting_Gray_MaxLevel)

            // good condition
            if (
                (CImageProcess.AnalysisMaxIntensity > CSetInfo.Setting_Gray_MinLevel)
                &&
                (CImageProcess.AnalysisMaxIntensity < CSetInfo.Setting_Gray_MaxLevel)
                )
            {
                double num = (double)(100 * (CImageProcess.AnalysisMaxIntensity - CSetInfo.Setting_Gray_MinLevel) / (CSetInfo.Setting_Gray_MaxLevel - CSetInfo.Setting_Gray_MinLevel));
                int num2 = (int)(num / 10.0) + 1;
                for (int m = 0; m < 2 + num2; m++)
                {
                    this.PicBrightness[m].Visible = true;
                }

                for (int n = 2 + num2; n < 15; n++)
                {
                    this.PicBrightness[n].Visible = false;
                }

                if (num2 < 7)
                {
                    this.lbStatusResult.Text = "LOW";
                }
                else
                {
                    this.lbStatusResult.Text = "OK";
                }

                this.lbStatusResult.ForeColor = Color.Black;
                this.lbStatusDescription.ForeColor = Color.Black;
                this.lbStatusDescription.Text = "Problem is not found.";// "no problem";
            }
        }

        private void menu_Tools_ZeroSet_Click(object sender, EventArgs e)
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "FrmZeroSet")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                    }
                    openForm.Activate();
                    openForm.Location = new Point(base.Location.X + base.Width / 2 - openForm.Width / 2, base.Location.Y + base.Height / 2 - openForm.Height / 2);
                    return;
                }
            }
            FormZeroSet frmZeroSet = new FormZeroSet();
            frmZeroSet.StartPosition = FormStartPosition.Manual;
            frmZeroSet.Location = new Point(base.Location.X + base.Width / 2 - frmZeroSet.Width / 2, base.Location.Y + base.Height / 2 - frmZeroSet.Height / 2);
            frmZeroSet.Show();
        }

        private void menu_Tools_Mode_Area_Click(object sender, EventArgs e)
        {
            CImageProcess.ResetSumAnalysisData();
            CSetInfo.Tool_Mode = 1;
            UpdateCheckBox();
        }

        private void menu_Tools_Mode_Brightness_Click(object sender, EventArgs e)
        {
            CImageProcess.ResetSumAnalysisData();
            CSetInfo.Tool_Mode = 2;
            UpdateCheckBox();
        }

        private void menu_Tools_Mode_Peak_Click(object sender, EventArgs e)
        {
            CImageProcess.ResetSumAnalysisData();
            CSetInfo.Tool_Mode = 3;
            UpdateCheckBox();
        }

        private void menu_Tools_Angle_Ext_Click(object sender, EventArgs e)
        {
            if (CSetInfo.Tool_Angle_Definition != 1)
            {
                CZeroSetInfo.ModeChangeToleranceDistance();
            }
            CSetInfo.Tool_Angle_Definition = 1;
            UpdateCheckBox();
            CSetInfo.Tool_Angle_Definition_Value = 2;
        }

        private void menu_Tools_Angle_Int_Click(object sender, EventArgs e)
        {
            if (CSetInfo.Tool_Angle_Definition != 2)
            {
                CZeroSetInfo.ModeChangeToleranceDistance();
            }
            CSetInfo.Tool_Angle_Definition = 2;
            UpdateCheckBox();
            CSetInfo.Tool_Angle_Definition_Value = 1;
        }

        private void menu_Tools_Numbring_Angle_Click(object sender, EventArgs e)
        {
            CSetInfo.Tool_Numbering = 1;
            UpdateCheckBox();
        }

        private void menu_Tools_Numbring_Size_Click(object sender, EventArgs e)
        {
            CSetInfo.Tool_Numbering = 2;
            UpdateCheckBox();
        }

        private void menu_Settings_Unit_Degree_Click(object sender, EventArgs e)
        {
            CSetInfo.Setting_Unit = 1;
            this.UpdateCheckBox();
        }

        private void menu_Settings_Unit_Sec_Click(object sender, EventArgs e)
        {
            CSetInfo.Setting_Unit = 2;
            this.UpdateCheckBox();
        }

        private void btn_SettingFileOpen_Click(object sender, EventArgs e) //파일오픈
        {
            btn_SettingFileOpen.BackgroundImage = Properties.Resources._03_Open_Button_03_On_21__164_;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Configuration File (*.ini)|*.ini";
            openFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            //openFileDialog.InitialDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\Setting";
            openFileDialog.Title = "Select Configuration File";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                //CSetInfo.SettingInfoFileName = openFileDialog.FileName;
                CSetInfo.LoadInfoFile(openFileDialog.FileName);
                this.UpdateCheckBox();
            }
        }

        private void btn_SettingFileSave_Click(object sender, EventArgs e) // 세팅파일세이브 ? 지금 작동안됨 기존 폼이랑 완전히 바뀌어서 수정하거나 사실상 삭제해도될듯
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Config. File(*.ini)|*.ini";
            //saveFileDialog.InitialDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\Setting";
            saveFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveFileDialog.Title = "Save Configuration File";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
               CSetInfo.m_szSettingInfoFileName = saveFileDialog.FileName;
               // CSetInfo.LoadInfoFile(saveFileDialog.FileName);
                CSetInfo.SaveSettingInfo();
                this.UpdateCheckBox();
            }
        }

        private void btn_Capture_Click(object sender, EventArgs e) // 화면 캡쳐
        {
            clsETC.ScreenCapture(base.Width, base.Height, base.Location);
        }

        private void menu_Setting_GrayLevel_Click(object sender, EventArgs e) //GrayLevel 조정
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "frmGreyScaleSetting")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                    }
                    openForm.Activate();
                    openForm.Location = new Point(base.Location.X + base.Width / 2 - openForm.Width / 2, base.Location.Y + base.Height / 2 - openForm.Height / 2);
                    return;
                }
            }
            frmGreyScaleSetting frmGreyScaleSetting2 = new frmGreyScaleSetting();
            frmGreyScaleSetting2.StartPosition = FormStartPosition.Manual;
            frmGreyScaleSetting2.Location = new Point(base.Location.X + base.Width / 2 - frmGreyScaleSetting2.Width / 2, base.Location.Y + base.Height / 2 - frmGreyScaleSetting2.Height / 2);
            frmGreyScaleSetting2.Show();
        }

        private void menu_Tools_Coordinate_Mirroring_Xon_Click(object sender, EventArgs e) // X축 대칭 좌표계로 변경
        {
            if (CSetInfo.Tool_Coordinate_Mirroring_State != 1)
            {
                CImageProcess.ResetZeroPoint(); //0323
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 2;
            CSetInfo.Tool_Coordinate_Mirroring_State = 1;
            UpdateCheckBox();
        }

        private void menu_Tools_Coordinate_Mirroring_Yon_Click(object sender, EventArgs e) // Y축 대칭 좌표계로 변경
        {
            if (CSetInfo.Tool_Coordinate_Mirroring_State != 2)
            {
                CImageProcess.ResetZeroPoint();
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 2;
            CSetInfo.Tool_Coordinate_Mirroring_State = 2;
            UpdateCheckBox();
        }

        private void menu_Tools_Coordinate_Mirroring_Off_Click(object sender, EventArgs e) //대칭 좌표계를 해제
        {
            if (CSetInfo.Tool_Coordinate_Mirroring_State != 3)
            {
                CImageProcess.ResetZeroPoint();
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 0;
            CSetInfo.Tool_Coordinate_Mirroring_State = 3;
            UpdateCheckBox();
        }

        private void menu_Tools_Coordinate_Rotating_L90_Click(object sender, EventArgs e) //90도 왼쪽 방향으로 회전하는 좌표계를 설정
        {
            if (CSetInfo.Tool_Coordinate_Rotating_State != 2)
            {
                CImageProcess.ResetZeroPoint();
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 1;
            CSetInfo.Tool_Coordinate_Rotating_State = 2;
            UpdateCheckBox();
        }

        private void menu_Tools_Coordinate_Rotating_R90_Click(object sender, EventArgs e) //90도 오른쪽 방향으로 회전하는 좌표계를 설정
        {
            if (CSetInfo.Tool_Coordinate_Rotating_State != 1)
            {
                CImageProcess.ResetZeroPoint();
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 1;
            CSetInfo.Tool_Coordinate_Rotating_State = 1;
            UpdateCheckBox();
        }

        private void menu_Tools_Coordinate_Rotating_Off_Click(object sender, EventArgs e) //좌표 회전 기능 off
        {
            if (CSetInfo.Tool_Coordinate_Rotating_State != 3)
            {
                CImageProcess.ResetZeroPoint();
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 0;
            CSetInfo.Tool_Coordinate_Rotating_State = 3;
            UpdateCheckBox(); ;
        }

        private void menu_Tools_Coordinate_Original_Click(object sender, EventArgs e) //현재 좌표계 설정이 원점 기준인지 확인한 다음, 원점 기준 좌표계로 설정
        {
            if (CSetInfo.Tool_Coordinate != 0)
            {
                CSetInfo.Tool_Coordinate_ChangedFlag = true;
            }
            CSetInfo.Tool_Coordinate = 0;
            UpdateCheckBox();
        }

        private void menu_Tools_Zoom_4_Click(object sender, EventArgs e) //4 배 확대
        {
            if (CSetInfo.Tool_Zoom_State != 4)
            {
                CSetInfo.Tool_Zoom_Flag = 1;
                CSetInfo.Tool_Zoom_State = 4;
                this.UpdateCheckBox();
            }
        }

        private void menu_Tools_Zoom_8_Click(object sender, EventArgs e) //8배 확대
        {
            if (CSetInfo.Tool_Zoom_State != 8)
            {
                CSetInfo.Tool_Zoom_Flag = 1;
                CSetInfo.Tool_Zoom_State = 8;
                this.UpdateCheckBox();
            }
        }

        private void menu_Tools_Zoom_16_Click(object sender, EventArgs e)//16배 확대
        {
            if (CSetInfo.Tool_Zoom_State != 16)
            {
                CSetInfo.Tool_Zoom_Flag = 1;
                CSetInfo.Tool_Zoom_State = 16;
                this.UpdateCheckBox();
            }
        }

        private void menu_Tools_Zoom_32_Click(object sender, EventArgs e)//32배 확대
        {
            if (CSetInfo.Tool_Zoom_State != 32)
            {
                CSetInfo.Tool_Zoom_Flag = 1;
                CSetInfo.Tool_Zoom_State = 32;
                this.UpdateCheckBox();
            }
        }

        private void menu_Tools_Zoom_Off_Click(object sender, EventArgs e) //Zoom 초기화
        {
            if (CSetInfo.Tool_Zoom_State != 0)
            {
                CSetInfo.Tool_Zoom_Flag = 0;
                CSetInfo.Tool_Zoom_State = 0;
                this.UpdateCheckBox();
            }
        }

        private void menu_Settings_Tolerance_Circle_Click(object sender, EventArgs e)
        {
            CSetInfo.Setting_Tolerance_Mode = 1;
            this.UpdateCheckBox();
        }

        private void menu_Settings_Tolerance_Square_Click(object sender, EventArgs e)
        {
            CSetInfo.Setting_Tolerance_Mode = 2;
            this.UpdateCheckBox();
        }

        private void menu_Settings_Tolerance_Value_Click(object sender, EventArgs e) //허용 오차값을 설정하는 폼
        {
            /*
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "frmTolerance")
                {
                    if (openForm.WindowState == FormWindowState.Minimized) //열려있는 양식있으면 Normal폼으로 설정 후 배치
                    {
                        openForm.WindowState = FormWindowState.Normal;
                    }
                    openForm.Activate();
                    openForm.Location = new Point(base.Location.X + base.Width / 2 - openForm.Width / 2, base.Location.Y + base.Height / 2 - openForm.Height / 2);
                    return;
                }
            }
            frmTolerance frmTolerance2 = new frmTolerance();
            frmTolerance2.StartPosition = FormStartPosition.Manual;
            frmTolerance2.Location = new Point(base.Location.X + base.Width / 2 - frmTolerance2.Width / 2, base.Location.Y + base.Height / 2 - frmTolerance2.Height / 2);
            frmTolerance2.Show();
            */

	        System.Collections.IEnumerator enumerator = System.Windows.Forms.Application.OpenForms.GetEnumerator();
	        try
	        {
		        while (enumerator.MoveNext())
		        {
			        System.Windows.Forms.Form form = (System.Windows.Forms.Form)enumerator.Current;
			        if (form.Name == "frmTolerance")
			        {
				        if (form.WindowState == System.Windows.Forms.FormWindowState.Minimized)
				        {
					        form.WindowState = System.Windows.Forms.FormWindowState.Normal;
				        }
				        form.Activate();
				        form.Location = new System.Drawing.Point(base.Location.X + base.Width / 2 - form.Width / 2, base.Location.Y + base.Height / 2 - form.Height / 2);
				        return;
			        }
		        }
	        }
	        finally
	        {
		        (enumerator as System.IDisposable)?.Dispose();
	        }

	        frmTolerance frmTolerance2 = new frmTolerance();
	        frmTolerance2.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
	        frmTolerance2.Location = new System.Drawing.Point(base.Location.X + base.Width / 2 - frmTolerance2.Width / 2, base.Location.Y + base.Height / 2 - frmTolerance2.Height / 2);
	        frmTolerance2.Show();
        }

        private void menu_Information_About_Click(object sender, EventArgs e) //INFO
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "frmInfo")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                    }
                    openForm.Activate();
                    openForm.Location = new Point(base.Location.X + base.Width / 2 - openForm.Width / 2, base.Location.Y + base.Height / 2 - openForm.Height / 2);
                    return;
                }
            }
            frmInfo frmInfo2 = new frmInfo();
            frmInfo2.StartPosition = FormStartPosition.Manual;
            frmInfo2.Location = new Point(base.Location.X + base.Width / 2 - frmInfo2.Width / 2, base.Location.Y + base.Height / 2 - frmInfo2.Height / 2);
            frmInfo2.Show();
        }
        
        //Factory 세팅 클릭이벤트
        private void cmdFactorySet_Click(object sender, EventArgs e)
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "frmFactorySetting")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                    }
                    openForm.Activate();
                    openForm.Location = new Point(base.Location.X + base.Width / 2 - openForm.Width / 2, base.Location.Y + base.Height / 2 - openForm.Height / 2);
                    return;
                }
            }
            frmFactorySetting frmFactorySetting2 = new frmFactorySetting();
            frmFactorySetting2.StartPosition = FormStartPosition.Manual;
            frmFactorySetting2.Location = new Point(base.Location.X + base.Width / 2 - frmFactorySetting2.Width / 2, base.Location.Y + base.Height / 2 - frmFactorySetting2.Height / 2);
            frmFactorySetting2.Show();
        }

        //Start 버튼
        private void cmdAnalysis_Click(object sender, EventArgs e)
        {
            if (CImageProcess.CalibrationFlag)
            {
                CImageProcess.CalibrationFlag = false;
                cmdAnalysis.ImageIndex = 0;
                cmdAnalysis.Image = Properties.Resources._01_Start_Button_01_Off_21__94_;
                // cmdAnalysis.Text = "START";
            }
            else
            {
                CImageProcess.CalibrationFlag = true;
                cmdAnalysis.ImageIndex = 1;
                cmdAnalysis.Image = Properties.Resources._02_Stop_Button_03_On_21__94_;

               // cmdAnalysis.Text = "STOP";
            }
        }

        //MeasureData 클리어 클릭이벤트
        private void cmdClear_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("It will delete the recent saved data.", "Confirm", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                clsMeasureData.nMeasureDataCount = 0;
            }
        }

        private void cmdExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Live Measurement File (*.csv)|*.csv";
            //saveFileDialog.InitialDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\Data";
            saveFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveFileDialog.Title = "Save Live Measurement File";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                //SaveProgressbar.Minimum = 0;
               // SaveProgressbar.Maximum = clsMeasureData.nMeasureDataCount;
               // SaveProgressbar.Visible = true;
                SaveDataTimer = new System.Timers.Timer();
                SaveDataTimer.Interval = 100.0;
                SaveDataTimer.Elapsed += SaveDataTimer_Elapsed;
                SaveDataTimer.Start();
                clsMeasureData.SaveMeasureDataFileName = saveFileDialog.FileName;
                if (!clsMeasureData.SaveMeasureData())
                {
                    if (clsETC.m_szErrString.Length > 0)
                    {
                        string msg = "Failed. " + clsETC.m_szErrString;
                        clsETC.m_szErrString = "";
                        MessageBox.Show(msg);
                    }
                    else
                    {
                        MessageBox.Show("Failed.");
                    }
                    
                   // SaveProgressbar.Visible = false;
                    SaveDataTimer.Stop();
                    SaveDataTimer.Dispose();
                }
                else
                {
                    clsMeasureData.nMeasureDataCount = 0;
                  //  SaveProgressbar.Visible = false;
                    SaveDataTimer.Stop();
                    SaveDataTimer.Dispose();
                    MessageBox.Show("Saved.");
                }
            }
        }

        private void SaveDataTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            BeginInvoke(new SaveTimerEventFiredDelegate(UpdateSaveProgressBar));
        }

        private void UpdateSaveProgressBar()
        {
            //SaveProgressbar.Value = clsMeasureData.SaveDataCurrentNum;
        }

        private void cmdMeasureData_Click(object sender, EventArgs e) //측정 데이터 시간 을 받아서 유효하면 머지 데이터 업데이트
        {
            double num = 0.0;
            if (txtSamplingFreq.Text == "")
            {
                MessageBox.Show("Please, insert measure time");
                return;
            }

            //num = Convert.ToDouble(txtSamplingFreq.Text) * 1000.0;

            double result = 0;
            bool tf = double.TryParse(txtSamplingFreq.Text, out result);
            if (tf == true && result > 0)
            {
                num = result * 1000.0;
            }
            else
            {
                num = -1;
            }

            if (num < 100.0 || num > 3600000.0)
            {
                MessageBox.Show("100 <= Measure Time (second) =< 3600");
                return;
            }

            clsMeasureData.MeasureDelayTime = (int)num;
            if (clsMeasureData.MeasureDataFlag == true) //측정 데이터 수집 현재 진행중인지 확인
            {
                clsMeasureData.MeasureDataFlag = false;
                cmdMeasureData.Text = "MEASUREMENT START";
                MeasureTimer.Stop();
                MeasureTimer.Dispose();
                MeasureTimer = null;
            }
            else
            {
                MeasureTimer = new System.Timers.Timer();
                clsMeasureData.MeasureDataFlag = true;
                clsMeasureData.nMeasureDataCount = 0;
                cmdMeasureData.Text = "MEASUREMENT STOP";
                MeasureTimer.Interval = clsMeasureData.MeasureDelayTime;
                MeasureTimer.Elapsed += MeasureTimer_Elapsed;
                MeasureTimer.Start();
            }
        }

        private void MeasureTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            BeginInvoke(new TimerEventFiredDelegate(UpdateMeasureData));
        }

        private void UpdateMeasureData()
        {
            clsMeasureData.MeasureSpotNo = CImageProcess.SearchSpotNumber;
            if (clsMeasureData.nMeasureDataCount == clsMeasureData.LimitMeausreCount && clsMeasureData.MeasureDataFlag)
            {
                clsMeasureData.MeasureDataFlag = false;
                cmdMeasureData.Text = "MEASUREMENT START";
                MeasureTimer.Stop();
                MeasureTimer.Dispose();
                MessageBox.Show("Number of measured data are larger than " + clsMeasureData.LimitMeausreCount + ", Please clear data.");
            }
            else
            {
                clsMeasureData.UpdateMeasureData();
            }
        }

       

        private void cmdWobbleFlattingClear_Click(object sender, EventArgs e)
        {
            if (CImageProcess.WobblePlottingImage != null)
            {
                cmdWobbleFlattingClear.Image = Properties.Resources._14_Clear_Button_03_On_968__631_;
                Cv.Set(CImageProcess.WobblePlottingImage, new CvScalar(255.0, 255.0, 255.0));
                CPlotting.WobblePlottingClearFlag = true;
                MessageBox.Show("Wobble Data NULL Clear Success");
                cmdWobbleFlattingClear.Image = Properties.Resources._14_Clear_Button_01_Off_968__631_;
            }
        }

        private void cmdPlotStartStop_Click(object sender, EventArgs e)
        {
            if (CPlotting.WobblePlottingFlag)
            {
                CPlotting.WobblePlottingFlag = false;
                cmdPlotStartStop.Image = Properties.Resources._13_Start_Button_01_Off_826__631_; 
            }
            else
            {
                CPlotting.WobblePlottingFlag = true;
                cmdPlotStartStop.Image = Properties.Resources._13_Start_Button_03_On_826__631_;
            }
        }

        private void cmdSnapDataClear_Click(object sender, EventArgs e)
        {
            //스냅샷 측정과 관련된 값 재설정
            clsMeasureData.SnapMeasureFlag = false;
            clsMeasureData.SnapPartName = null;
            clsMeasureData.SnapStartNumber = -1001001;
            clsMeasureData.SnapMeasureSpotNo = -1;
            clsMeasureData.nSnapMeasureDataCount = 0;
            txtCurrentPartName.Text = "";
            if (gridSerialData.InvokeRequired)
            {
                gridSerialData.BeginInvoke((Action)delegate
                {
                    gridSerialData.Rows.Clear(); //그리드에서 행 지우기
                });
            }
            else
            {
                gridSerialData.Rows.Clear();
            }
        }

        private void cbSerialSpotNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            CPlotting.PlottingSpotNumber = cbPlotSpotNo.SelectedIndex;
            if (CPlotting.PlottingSpotNumber + 1 > CImageProcess.SearchSpotNumber)
            {
                CPlotting.WobblePlottingFlag = false;
                MessageBox.Show("Does not exist");
            }
        }

        private void cmdDataExport_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Serial Measurement File(*.csv)|*.csv";
            //saveFileDialog.InitialDirectory = Application.ExecutablePath.Substring(0, Application.ExecutablePath.LastIndexOf("\\")) + "\\SerialData";
            saveFileDialog.InitialDirectory = System.Reflection.Assembly.GetExecutingAssembly().Location;
            saveFileDialog.Title = "Save Serial Measurement File";
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                clsMeasureData.SaveSnapMeasureDataFileName = saveFileDialog.FileName;
                if (!clsMeasureData.SaveSnapMeasureData())
                {
                    if (clsETC.m_szErrString.Length > 0)
                    {
                        string msg = clsETC.m_szErrString + ". Failed.";
                        MessageBox.Show(msg);
                    }
                    else
                    {
                        MessageBox.Show("Serial Measurement File Save Failed.");
                    }
                    
                }
                else
                {
                    MessageBox.Show("Serial Measurement File Save Success.");
                }
            }
        }

        //0324
        private void cmdSnapMeasurement_Click(object sender, EventArgs e)
        {
            string text = txtPartName.Text;
            if (clsMeasureData.SnapPartName != null && text != clsMeasureData.SnapPartName)
            {
                MessageBox.Show("partname and savepartname are different, Check partname");
                return;
            }
            if (txtStartingNo.Text == "")
            {
                txtStartingNo.Text = "0";
            }
            if (clsMeasureData.SnapStartNumber != -1001001 && clsMeasureData.SnapStartNumber != Convert.ToInt16(txtStartingNo.Text))
            {
                MessageBox.Show("Changed start number");
                return;
            }
            if (clsMeasureData.SnapMeasureSpotNo != -1 && cbSerialSpotNo.SelectedIndex != clsMeasureData.SnapMeasureSpotNo)
            {
                MessageBox.Show("changed spotNo");
                return;
            }
            if (cbSerialSpotNo.SelectedIndex == -1)
            {
                MessageBox.Show("The spot number is not selected.");
                return;
            }

            clsMeasureData.SnapMeasureSpotNo = cbSerialSpotNo.SelectedIndex;
            if (clsMeasureData.SnapPartName == null && text != clsMeasureData.SnapPartName)
            {
                txtCurrentPartName.Text = text;
                clsMeasureData.SnapPartName = text;
                clsMeasureData.SnapStartNumber = Convert.ToInt16(txtStartingNo.Text);
            }
            clsMeasureData.nSnapMeasureDataCount++;
            InsertSerialData();
            UpdateSerialGrid();
            clsMeasureData.SnapMeasureFlag = true;
        }

        private void InsertSerialData()
        {
            if (CSetInfo.Setting_Unit == 1)
            {
                clsMeasureData.SnapMeasureDataXAngle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.AngleX[clsMeasureData.SnapMeasureSpotNo].ToString();

                clsMeasureData.SnapMeasureDataYAngle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.AngleY[clsMeasureData.SnapMeasureSpotNo].ToString();

                clsMeasureData.SnapMeasureDataXYAngle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.AngleD[clsMeasureData.SnapMeasureSpotNo].ToString();

                if (CImageProcess.SearchSpotNumber > 1)
                {
                    clsMeasureData.SnapMeasureDataSpot1to2Angle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.Angle1to2.ToString();
                }
                else
                {
                    clsMeasureData.SnapMeasureDataSpot1to2Angle[clsMeasureData.nSnapMeasureDataCount - 1] = "-";
                }
            }
            else
            {
                if (CImageProcess.AngleX[clsMeasureData.SnapMeasureSpotNo] < 0.0)
                {
                    clsMeasureData.SnapMeasureDataXAngle[clsMeasureData.nSnapMeasureDataCount - 1] = "-" + CImageProcess.AngleHourX[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteX[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondX[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''";
                }
                else
                {
                    clsMeasureData.SnapMeasureDataXAngle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.AngleHourX[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteX[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondX[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''";
                }

                if (CImageProcess.AngleY[clsMeasureData.SnapMeasureSpotNo] < 0.0)
                {
                    clsMeasureData.SnapMeasureDataYAngle[clsMeasureData.nSnapMeasureDataCount - 1] = "-" + CImageProcess.AngleHourY[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteY[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondY[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''";
                }
                else
                {
                    clsMeasureData.SnapMeasureDataYAngle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.AngleHourY[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteY[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondY[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''";
                }

                clsMeasureData.SnapMeasureDataXYAngle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.AngleHourXY[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteXY[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondXY[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''";

                if (CImageProcess.SearchSpotNumber > 1)
                {
                    clsMeasureData.SnapMeasureDataSpot1to2Angle[clsMeasureData.nSnapMeasureDataCount - 1] = CImageProcess.Angle1to2HourXY + " ° " + CImageProcess.Angle1to2MinuteXY + " '" + $"{CImageProcess.Angle1to2SecondXY:f0}" + " ''";
                }
                else
                {
                    clsMeasureData.SnapMeasureDataSpot1to2Angle[clsMeasureData.nSnapMeasureDataCount - 1] = "-";
                }
            }

            if (CImageProcess.AnalysisToleranceFlag)
            {
                clsMeasureData.SnapMeasureDataStatus[clsMeasureData.nSnapMeasureDataCount - 1] = 1;
            }
            else
            {
                clsMeasureData.SnapMeasureDataStatus[clsMeasureData.nSnapMeasureDataCount - 1] = 0;
            }
        }

        private void UpdateSerialGrid()
        {
            string text;
            string text2;
            string text3;
            string text4;

            if (CSetInfo.Setting_Unit == 1)
            {
                text = CImageProcess.AngleX[clsMeasureData.SnapMeasureSpotNo].ToString();
                text2 = CImageProcess.AngleY[clsMeasureData.SnapMeasureSpotNo].ToString();
                text3 = CImageProcess.AngleD[clsMeasureData.SnapMeasureSpotNo].ToString();
                text4 = ((CImageProcess.SearchSpotNumber <= 1) ? "-" : CImageProcess.Angle1to2.ToString());
            }
            else
            {
                text = ((!(CImageProcess.AngleX[clsMeasureData.SnapMeasureSpotNo] < 0.0)) ? (CImageProcess.AngleHourX[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteX[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondX[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''") : ("-" + CImageProcess.AngleHourX[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteX[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondX[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''"));

                text2 = ((!(CImageProcess.AngleY[clsMeasureData.SnapMeasureSpotNo] < 0.0)) ? (CImageProcess.AngleHourY[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteY[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondY[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''") : ("-" + CImageProcess.AngleHourY[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteY[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondY[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''"));

                text3 = CImageProcess.AngleHourXY[clsMeasureData.SnapMeasureSpotNo] + " ° " + CImageProcess.AngleMinuteXY[clsMeasureData.SnapMeasureSpotNo] + " '" + $"{CImageProcess.AngleSecondXY[clsMeasureData.SnapMeasureSpotNo]:f0}" + " ''";

                text4 = ((CImageProcess.SearchSpotNumber <= 1) ? "-" : (CImageProcess.Angle1to2HourXY + " ° " + CImageProcess.Angle1to2MinuteXY + " '" + $"{CImageProcess.Angle1to2SecondXY:f0}" + " ''"));
            }

            string text5 = ((!CImageProcess.AnalysisToleranceFlag) ? "NG" : "OK");
            string text6 = (gridSerialData.RowCount + clsMeasureData.SnapStartNumber - 1).ToString();
            string[] strRows = new string[6] { text6, text, text2, text3, text4, text5 };
            if (gridSerialData.InvokeRequired)
            {
                gridSerialData.BeginInvoke((Action)delegate
                {
                    gridSerialData.Rows.Add(strRows);
                });
            }
            else
            {
                gridSerialData.Rows.Add(strRows);
            }
        }

        private void OnShutterSpeedTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int num = Track_ShutterSpeed.Value;
                string szShutter = string.Format("{0:f3}", (float)num / 1000f);

                string szSpeed = TextBox_ShutterSpeed.Text;
                double nSpeed;

                bool tf = double.TryParse(szSpeed, out nSpeed);

                if (tf == true)
                {
                    nSpeed *= 1000;
                    if (nSpeed > Track_ShutterSpeed.Maximum)
                    {
                        MessageBox.Show("Shutter Max Speed 30ms Value");
                        TextBox_ShutterSpeed.Text = szShutter;
                        return;
                    }
                    if (nSpeed < Track_ShutterSpeed.Minimum)
                        nSpeed = Track_ShutterSpeed.Minimum;

                    Track_ShutterSpeed.Value = (int)nSpeed;

                    float ShutterValue = (float)nSpeed / 1000f;
                    CamParamInfo.ShutterSpeed = ShutterValue;
                    CPGCamera.SetShutterSpeed(ShutterValue);
                }
                else
                {
                    MessageBox.Show("Invalid shutter speed value");
                    TextBox_ShutterSpeed.Text = szShutter;
                }
            }
        }

        private void OnSaveAllConfigClick(object sender, EventArgs e)
        {
            CamParamInfo.SaveCameraInfo();
            CSetInfo.SaveSettingInfo();
            CZeroSetInfo.SaveZeroSetInfo();
            CParameter.SaveParameterInfo();
            clsDislpayInfo.SaveDisplayInfo();
            MessageBox.Show("Save가 완료되었습니다.", "알림", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btn_AutoShutter_Click(object sender, EventArgs e)
        {
            bool bAdjustShutterSpeed = false;

            if (CImageProcess.CalibrationFlag)
            {
                this.lbStatusResult.Visible = true;
                this.lbStatusDescription.Visible = true;
            }
            else
            {
                this.lbStatusResult.Visible = false;
                this.lbStatusDescription.Visible = false;
            }

            if (CImageProcess.AnalysisMaxIntensity <= CSetInfo.Setting_Gray_MinLevel)
            {               
                if (CImageProcess.AnalysisMaxIntensity != 0){ 
                for (int i = 0; i < 15; i++)
                {
                    if (i < 3)
                    {
                        this.PicBrightness[i].Visible = true;
                    }
                    else
                    {
                        this.PicBrightness[i].Visible = false;
                    }
                }
                this.lbStatusResult.ForeColor = Color.Red;
                this.lbStatusResult.Text = "ERROR";
                this.lbStatusDescription.ForeColor = Color.Red;
                this.lbStatusDescription.Text = "Nothing is Detected";
                }
                // ERROR일때 True 빠지게 설정
                bAdjustShutterSpeed = true;
            }
            else if (CImageProcess.AnalysisMaxIntensity >= CSetInfo.Setting_Gray_MaxLevel)
            {
                if (CImageProcess.nAnalysisMaxIntensity == 1)
                {
                    for (int j = 0; j < 15; j++)
                    {
                        if (j < 13)
                        {
                            this.PicBrightness[j].Visible = true;
                        }
                        else
                        {
                            this.PicBrightness[j].Visible = false;
                        }
                    }
                }
                else if (CImageProcess.nAnalysisMaxIntensity == 2)
                {
                    for (int k = 0; k < 15; k++)
                    {
                        if (k < 14)
                        {
                            this.PicBrightness[k].Visible = true;
                        }
                        else
                        {
                            this.PicBrightness[k].Visible = false;
                        }
                    }
                }
                else
                {
                    for (int l = 0; l < 15; l++)
                    {
                        this.PicBrightness[l].Visible = true;
                    }
                }

                this.lbStatusResult.ForeColor = Color.Red;
                this.lbStatusResult.Text = "ERROR";
                this.lbStatusDescription.ForeColor = Color.Red;
                this.lbStatusDescription.Text = "Saturation occur.";

                // ERROR일때 True 빠지게 설정
                bAdjustShutterSpeed = true;
            }
            else if ((CImageProcess.AnalysisMaxIntensity > CSetInfo.Setting_Gray_MinLevel) && (CImageProcess.AnalysisMaxIntensity < CSetInfo.Setting_Gray_MaxLevel))
            {
                double num = (double)(100 * (CImageProcess.AnalysisMaxIntensity - CSetInfo.Setting_Gray_MinLevel) / (CSetInfo.Setting_Gray_MaxLevel - CSetInfo.Setting_Gray_MinLevel));
                int num2 = (int)(num / 10.0) + 1;
                for (int m = 0; m < 2 + num2; m++)
                {
                    this.PicBrightness[m].Visible = true;
                }

                for (int n = 2 + num2; n < 15; n++)
                {
                    this.PicBrightness[n].Visible = false;
                }

                if (num2 < 5)
                {
                    this.lbStatusResult.Text = "LOW";
                    // LOW 일때 True 빠지게 설정
                    bAdjustShutterSpeed = true;
                }
                else
                {
                    this.lbStatusResult.Text = "OK";
                    MessageBox.Show("Shutter speed OK!");
                }
            }

            if (bAdjustShutterSpeed)
            {
                int num = Track_ShutterSpeed.Value;
                string szSpeed = TextBox_ShutterSpeed.Text;
                double nSpeed;
                bool success = false;
                double.TryParse(szSpeed, out nSpeed);
                nSpeed *= 1000;
                while (bAdjustShutterSpeed)
                {
                   // StatisReslut값 확인
                    if (this.lbStatusResult.Text == "LOW")
                    {
                        nSpeed += 1000.0;
                    }
                    else if (this.lbStatusResult.Text == "ERROR")
                    {
                        nSpeed -= 1000.0;
                    }

                    if (this.lbStatusResult.Text == "NO Light")
                    {
                        nSpeed += 1000.0;
                    }
                    // 셔터 스피드 적용
                   
                    if (nSpeed >= 30000 || nSpeed <= 0)
                    {
                        nSpeed = 15000;
                        break;
                        // while 루프 탈출
                    }

                    TextBox_ShutterSpeed.Text = $"{CamParamInfo.ShutterSpeed:f1}";
                    Track_ShutterSpeed.Value = (int)nSpeed;
                    float ShutterValue = (float)nSpeed / 1000f;
                    CamParamInfo.ShutterSpeed = ShutterValue;
                    CPGCamera.SetShutterSpeed(ShutterValue);
                    // OK 했을때 나가는구간
                    if (this.lbStatusResult.Text == "OK")
                    {
                        bAdjustShutterSpeed = false;
                        success = true;
                        break;
                    }
                    this.Invoke(new Action(() =>
                    {
                        UpdateBrightnessStatus();
                    }));
                    Thread.Sleep(100);
                }

                // 메시지박스 생성
                if (success)
                {
                    MessageBox.Show("Shutter speed successfully!");
                }
                else
                {
                    MessageBox.Show("Failed to shutter speed.");
                }
            }
        }

        private void comboBox_serialport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox_serialport.SelectedIndex >= 0)
            {
                string itemSelected = comboBox_serialport.SelectedItem as string;
                if (0 == itemSelected.CompareTo("no serial"))
                    return;
                m_SerialOut.OpenPort(itemSelected, 9600);
            }
        }

      

        private void BTN_LIVEVIEW_Click(object sender, EventArgs e)
        {
            pictureBox2.Visible = true;
            pictureBox3.Visible = true;
            label38.Visible = true;
            label6.Visible = true;
            pictureBox1.Visible = true;
            label29.Visible = true;
            label28.Visible = true;
            txtSpotX1.Visible = true;
            label27.Visible = true;
            txtSpotY1.Visible = true;
            label26.Visible = true;
            txtSpotXY1.Visible = true;
            label25.Visible = true;
            label24.Visible = true;
            txtSpotX2.Visible = true;
            label23.Visible = true;
            txtSpotY2.Visible = true;
            label22.Visible = true;
            txtSpotXY2.Visible = true;
            label21.Visible = true;
            label20.Visible = true;
            txtSpotX3.Visible = true;
            label19.Visible = true;
            txtSpotY3.Visible = true;
            label18.Visible = true;
            txtSpotXY3.Visible = true;
            pictureBox4.Visible = true;
            label32.Visible = true;
            txtSpot1to2.Visible = true;
            label31.Visible = true;
            txtSpot2to3.Visible = true;
            label30.Visible = true;
            txtSpot3to1.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            tab_LiveView.SelectedIndex = 0;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            tab_LiveView.SelectedIndex = 1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            tab_LiveView.SelectedIndex = 2;
        }
        private void button_Live_View_MouseEnter(object sender, EventArgs e) => button_Live_View.Image = Properties.Resources._10_Live_Button_02_Select_775__80_;

        private void button_Live_View_MouseLeave(object sender, EventArgs e)
        {
            if(Tabindex !=1)
            button_Live_View.Image = Properties.Resources._10_Live_Button_01_Off_775__80_;
            else
                button_Live_View.Image = Properties.Resources._10_Live_Button_03_On_775__80_;
        }

        private void button_Live_View_MouseDown(object sender, MouseEventArgs e)

        {
            Tabindex = 1;
            button_Live_View.Image = Properties.Resources._10_Live_Button_03_On_775__80_;
            if (Tabindex ==1)
            {
                button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_01_Off_897__80_;
                button_SERIAL.Image = Properties.Resources._12_Serial_Button_01_Off_1019__80_;
            }
           
        }

        private void button_Live_View_MouseUp(object sender, MouseEventArgs e) => button_Live_View.Image = Properties.Resources._10_Live_Button_01_Off_775__80_;

      

        private void button_button_XYPLOT_MouseEnter(object sender, EventArgs e) => button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_02_Select_897__80_;

        private void button_button_XYPLOT_MouseLeave(object sender, EventArgs e)
           
        { 
             if(Tabindex !=2)    
            button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_01_Off_897__80_;
             else
                button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_03_On_897__80_;
        }
         
    

        private void button_button_XYPLOT_MouseDown(object sender, MouseEventArgs e)
        {
            Tabindex = 2;
            button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_03_On_897__80_;
            if (Tabindex == 2)
            {
                button_Live_View.Image = Properties.Resources._10_Live_Button_01_Off_775__80_;
                button_SERIAL.Image = Properties.Resources._12_Serial_Button_01_Off_1019__80_;
            }

        }

       // private void button_button_XYPLOT_MouseUp(object sender, MouseEventArgs e) => button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_01_Off_897__80_;

      

        private void button_button_SERIAL_MouseEnter(object sender, EventArgs e) => button_SERIAL.Image = Properties.Resources._12_Serial_Button_02_Select_1019__80_;

        private void button_button_SERIAL_MouseLeave(object sender, EventArgs e)
        {
            if(Tabindex !=3)
            button_SERIAL.Image = Properties.Resources._12_Serial_Button_01_Off_1019__80_;
            else
                button_SERIAL.Image = Properties.Resources._12_Serial_Button_03_On_1019__80_;
        }

        private void button_button_SERIAL_MouseDown(object sender, MouseEventArgs e)
        {
            Tabindex = 3;
            button_SERIAL.Image = Properties.Resources._12_Serial_Button_03_On_1019__80_;
            if (Tabindex == 3)
            {
                button_Live_View.Image = Properties.Resources._10_Live_Button_01_Off_775__80_;
                button_XYPLOT.Image = Properties.Resources._11_X_Y_Button_01_Off_897__80_;

            }

        }


private void button_button_SERIAL_MouseUp(object sender, MouseEventArgs e) => button_SERIAL.Image = Properties.Resources._12_Serial_Button_01_Off_1019__80_;



        private void btn_SettingFileOpen_MouseEnter(object sender, EventArgs e) =>  btn_SettingFileOpen.Image = Properties.Resources._03_Open_Button_02_Select_21__164_;

        private void btn_SettingFileOpen_MouseLeave(object sender, EventArgs e) => btn_SettingFileOpen.Image = Properties.Resources._03_Open_Button_01_Off_21__164_;

        private void btn_SettingFileOpen_MouseDown(object sender, MouseEventArgs e) => btn_SettingFileOpen.Image = Properties.Resources._03_Open_Button_03_On_21__164_;

        private void btn_SettingFileOpen_MouseUp(object sender, MouseEventArgs e) =>btn_SettingFileOpen.Image = Properties.Resources._03_Open_Button_01_Off_21__164_;



        private void btn_SaveAllConfig_MouseEnter(object sender, EventArgs e) => SaveAllConfig.Image = Properties.Resources._04_Save_Button_02_Select_21__234_;

        private void btn_SaveAllConfig_MouseLeave(object sender, EventArgs e) => SaveAllConfig.Image = Properties.Resources._04_Save_Button_01_Off_21__234_;

        private void btn_SaveAllConfig_MouseDown(object sender, MouseEventArgs e) => SaveAllConfig.Image = Properties.Resources._04_Save_Button_03_On_21__234_;

        private void btn_SaveAllConfig_MouseUp(object sender, MouseEventArgs e) => SaveAllConfig.Image = Properties.Resources._04_Save_Button_01_Off_21__234_;


        private void btn_cmdExport_MouseEnter(object sender, EventArgs e) => cmdExport.Image = Properties.Resources._05_Export_Button_02_Select_21__304_;

        private void btn_cmdExport_MouseLeave(object sender, EventArgs e) => cmdExport.Image = Properties.Resources._05_Export_Button_01_Off_21__304_;

        private void btn_cmdExport_MouseDown(object sender, MouseEventArgs e) => cmdExport.Image = Properties.Resources._05_Export_Button_03_On_21__304_;

        private void btn_cmdExport_MouseUp(object sender, MouseEventArgs e) => cmdExport.Image = Properties.Resources._05_Export_Button_01_Off_21__304_;




        private void btn_cmdClear_MouseEnter(object sender, EventArgs e) => cmdClear.Image = Properties.Resources._06_Clear_Button_02_Select_21__374_;

        private void btn_cmdClear_MouseLeave(object sender, EventArgs e) => cmdClear.Image = Properties.Resources._06_Clear_Button_01_Off_21__374_;

        private void btn_cmdClear_MouseDown(object sender, MouseEventArgs e) => cmdClear.Image = Properties.Resources._06_Clear_Button_03_On_21__374_;

        private void btn_cmdClear_MouseUp(object sender, MouseEventArgs e) => cmdClear.Image = Properties.Resources._06_Clear_Button_01_Off_21__374_;


        private void btn_Capture_MouseEnter(object sender, EventArgs e) => btn_Capture.Image = Properties.Resources._07_Capture_Button_02_Select_21__444_;

        private void btn_Capture_MouseLeave(object sender, EventArgs e) => btn_Capture.Image = Properties.Resources._07_Capture_Button_01_Off_21__444__1_;

        private void btn_Capture_MouseDown(object sender, MouseEventArgs e) => btn_Capture.Image = Properties.Resources._07_Capture_Button_03_On_21__444_;

        private void btn_Capture_MouseUp(object sender, MouseEventArgs e) => btn_Capture.Image = Properties.Resources._07_Capture_Button_01_Off_21__444__1_;

        

        private void btn_cmdFactorySet_MouseEnter(object sender, EventArgs e) => cmdFactorySet.Image = Properties.Resources._08_Factory_Set_Button_02_Select_21__514_;

        private void btn_cmdFactorySet_MouseLeave(object sender, EventArgs e) => cmdFactorySet.Image = Properties.Resources._08_Factory_Set_Button_01_Off_21__514_;

        private void btn_cmdFactorySet_MouseDown(object sender, MouseEventArgs e) => cmdFactorySet.Image = Properties.Resources._08_Factory_Set_Button_03_On_21__514_;

        private void btn_cmdFactorySet_MouseUp(object sender, MouseEventArgs e) => cmdFactorySet.Image = Properties.Resources._08_Factory_Set_Button_01_Off_21__514_;

     

        private void btn_cmdMeasureData_MouseEnter(object sender, EventArgs e) => cmdMeasureData.Image = Properties.Resources._09_MS_Start_Button_02_Select_21__605_;

        private void btn_cmdMeasureData_MouseLeave(object sender, EventArgs e) => cmdMeasureData.Image = Properties.Resources._09_MS_Start_Button_01_Off_21__605_;

        private void btn_cmdMeasureData_MouseDown(object sender, MouseEventArgs e) => cmdMeasureData.Image = Properties.Resources._09_MS_Start_Button_03_On_21__605_;

        private void btn_cmdMeasureData_MouseUp(object sender, MouseEventArgs e) => cmdMeasureData.Image = Properties.Resources._09_MS_Start_Button_01_Off_21__605_;

   


        private void btn_cmdSnapMeasurement_MouseEnter(object sender, EventArgs e) => cmdSnapMeasurement.Image = Properties.Resources._15_Snap_Button_02_Select_775__631_;

        private void btn_cmdSnapMeasurement_MouseLeave(object sender, EventArgs e) => cmdSnapMeasurement.Image = Properties.Resources._15_Snap_Button_01_Off_775__631_;

        private void btn_cmdSnapMeasurement_MouseDown(object sender, MouseEventArgs e) => cmdSnapMeasurement.Image = Properties.Resources._15_Snap_Button_03_On_775__631_;

        private void btn_cmdSnapMeasurement_MouseUp(object sender, MouseEventArgs e) => cmdSnapMeasurement.Image = Properties.Resources._15_Snap_Button_01_Off_775__631_;

      

        private void btn_cmdSnapDataClear_MouseEnter(object sender, EventArgs e) => cmdSnapDataClear.Image = Properties.Resources._16_Clear_Data_Button_02_Select_897__631_;

        private void btn_cmdSnapDataClear_MouseLeave(object sender, EventArgs e) => cmdSnapDataClear.Image = Properties.Resources._16_Clear_Data_Button_01_Off_897__631_;

        private void btn_cmdSnapDataClear_MouseDown(object sender, MouseEventArgs e) => cmdSnapDataClear.Image = Properties.Resources._16_Clear_Data_Button_03_On_897__631_;

        private void btn_cmdSnapDataClear_MouseUp(object sender, MouseEventArgs e) => cmdSnapDataClear.Image = Properties.Resources._16_Clear_Data_Button_01_Off_897__631_;
       

        private void btn_cmdDataExport_MouseEnter(object sender, EventArgs e) => cmdDataExport.Image = Properties.Resources._17_Data_Export_Button_02_Select_1019__631_;

        private void btn_cmdDataExport_MouseLeave(object sender, EventArgs e) => cmdDataExport.Image = Properties.Resources._17_Data_Export_Button_01_Off_1019__631_;

        private void btn_cmdDataExport_MouseDown(object sender, MouseEventArgs e) => cmdDataExport.Image = Properties.Resources._17_Data_Export_Button_03_On_1019__631_;

        private void btn_cmdDataExport_MouseUp(object sender, MouseEventArgs e) => cmdDataExport.Image = Properties.Resources._17_Data_Export_Button_01_Off_1019__631_;


        private void cmdAnalysis_MouseEnter(object sender, EventArgs e)
        {
            if (CImageProcess.CalibrationFlag)
            {
                // CalibrationFlag가 true일 때 다른 이미지로 변경
                cmdAnalysis.Image = Properties.Resources._02_Stop_Button_02_Select_21__94_; 
            }
            else
            {
                // CalibrationFlag가 false일 때 다른 이미지로 변경
                cmdAnalysis.Image = Properties.Resources._01_Start_Button_02_Select_21__94_;
            }
        }

        private void cmdAnalysis_MouseLeave(object sender, EventArgs e)
        {
            if (CImageProcess.CalibrationFlag)
            {
                // CalibrationFlag가 true일 때 다른 이미지로 변경
                cmdAnalysis.Image = Properties.Resources._02_Stop_Button_03_On_21__94_;
            }
            else
            {
                // CalibrationFlag가 false일 때 다른 이미지로 변경
                cmdAnalysis.Image = Properties.Resources._01_Start_Button_01_Off_21__94_;
            }
        }

        private void cmdPlotStartStop_MouseEnter(object sender, EventArgs e) =>cmdPlotStartStop.Image = Properties.Resources._13_Start_Button_02_Select_826__631_;


        private void cmdPlotStartStop_MouseLeave(object sender, EventArgs e)
        {
            if (CPlotting.WobblePlottingFlag)
            {
                // CalibrationFlag가 true일 때 다른 이미지로 변경
                cmdPlotStartStop.Image = Properties.Resources._13_Start_Button_03_On_826__631_;
            }
            else
            {
                // CalibrationFlag가 false일 때 다른 이미지로 변경
                cmdPlotStartStop.Image = Properties.Resources._13_Start_Button_01_Off_826__631_; 
            }
        }

        private void cmdWobbleFlattingClear_MouseEnter(object sender, EventArgs e)
        {
            cmdWobbleFlattingClear.Image = Properties.Resources._14_Clear_Button_02_Select_968__631_;

        }

        private void cmdWobbleFlattingClear_MouseLeave(object sender, EventArgs e)
        {
            cmdWobbleFlattingClear.Image = Properties.Resources._14_Clear_Button_01_Off_968__631_;
        }
    }
}
    

