﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;


namespace WikiOptics_Collimator
{
    public class CClientSocket
    {        
        public Socket m_clientsocket = null;
        public CWikiTcpServer m_daemon = null;

        string  m_szSend_data = "";
        Int32   m_nDataLength = 0;
        Int32   m_nOffset = 0;
        byte[]  host_data;        
        object  m_output_lock = new object();

        byte[] m_ReceiverBuff = new byte[8192];

        List<string> m_szOuputList = new List<string>();

        public List<byte[]> m_image_list = new List<byte[]>();
        byte[]  m_image_data = null;
        public object m_image_list_lock = new object();
        AutoResetEvent TerminatedEvent = new AutoResetEvent(false);
        AutoResetEvent ActivateEvent = new AutoResetEvent(false);
        Thread m_SendThread = null;
        bool m_bTerminateThread = false;
        public bool m_bClosed = false;
        bool m_bReadyToRecv = false;

        public CClientSocket()
        {
            m_bClosed = false;
        }

        public void CloseSock()
        {
            m_bTerminateThread = true;
            TerminatedEvent.WaitOne(1000 * 3);
            m_SendThread = null;

            lock (m_output_lock)
            {
                m_bClosed = true;
                if (m_clientsocket != null)
                {
                    try
                    {
                        // https://ckbcorp.tistory.com/887
                        // disconnect(); close();   => gracefully disconnect
                        // 데이터 송수신중이라면 모든 데이터의 송수신이 완결된 후 종료가 실행
                        // 그만큼 대기 시간이 소요되거나 먹통이 된것으로 간주된다

                        // shutdown(); close();
                        // 전송중인 데이터가 있어도 바로 끊고 종료한다

                        //m_clientsocket.Shutdown(SocketShutdown.Both);
                        //m_clientsocket.Disconnect(true);
                        m_clientsocket.Close();
                        m_clientsocket = null;
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine("[wiki] CloseSock except = " + ex.ToString());
                    }
                }
            }
        }

        // 바이트 배열을 String으로 변환 
        private string ByteToString(byte[] strByte) 
        { 
            //string str = Encoding.Default.GetString(strByte); 
            string str = Encoding.ASCII.GetString(strByte); 
            return str; 
        }
        
        // string을 바이트 배열로 변환
        private byte[] stringToByte(string str)
        {
            byte[] StrByte = Encoding.UTF8.GetBytes(str); 
            return StrByte; 
        }

        public void InitOption()
        {
            m_clientsocket.SetSocketOption(SocketOptionLevel.Socket, 
            SocketOptionName.SendTimeout, 
            2000);                              // send time out : 2 seconds

            m_clientsocket.SetSocketOption(SocketOptionLevel.Socket, 
            SocketOptionName.ReceiveTimeout, 
            2000);                              // receive time out : 2 seconds

            // https://www.sysnet.pe.kr/2/0/1825
            // 하지만, Timeout으로 인해 클라이언트 측에서 연결을 끊는 것이 좋은 선택은 아닙니다. 
            // 왜냐하면 상대방 소켓을 관리하는 스레드가 바빠서 데이터 전송을 못하는 걸 수도 있고, 
            // 때로는 패킷 정의를 하다 보면 각각의 요청/응답에 요구되는 timeout이 다르기 때문에 
            // 그것에 따라 연결 관리까지 함께 하는 것은 복잡도만 증가시킬 뿐입니다.
            // 이에 대한 대안으로 선택될 수 있는 옵션이 바로 Keep-Alive입니다. 다음과 같이 
            // KeepAlive 설정을 IOControl 메서드로 지정해 두면,

            int size = sizeof(UInt32);
            UInt32 on = 1;
            UInt32 keepAliveInterval = 10000;   // Send a packet once every 10 seconds.
            UInt32 retryInterval = 1000;        // If no response, resend every second.
            byte[] inArray = new byte[size * 3];
            Array.Copy(BitConverter.GetBytes(on), 0, inArray, 0, size);
            Array.Copy(BitConverter.GetBytes(keepAliveInterval), 0, inArray, size, size);
            Array.Copy(BitConverter.GetBytes(retryInterval), 0, inArray, size * 2, size);

            m_clientsocket.IOControl(IOControlCode.KeepAliveValues, inArray, null);

            m_SendThread = new Thread(SendImageDataThread);
            m_SendThread.Start(this);

            ReceiveData();
        }

        public void makeSend(string szOutput)
        {
            if (m_bClosed == true)
                return;

            Int32 host_length = szOutput.Length;            
            Int32 network_length = System.Net.IPAddress.HostToNetworkOrder(host_length);
            string str_out = network_length.ToString() + szOutput;
            host_data = stringToByte(str_out);
            m_nDataLength = host_data.Length;
            m_clientsocket.BeginSend(host_data, 0, host_data.Length, 0, 
                    new AsyncCallback(SendResultCallback), this);
        }

        public void SetResultData(string szOutput)
        {
            if (m_nDataLength != 0)
            {
                return;
            }

            lock (m_output_lock)
            {
                //m_szOuputList.Clear();
                if (m_szOuputList.Count > 2)
                {
                    m_szOuputList.RemoveAt(m_szOuputList.Count - 1);
                }
                m_szOuputList.Add(szOutput);
            }
            makeSend(m_szOuputList[0]);
        }

        void SendResultCallback(IAsyncResult ar)
        {
            CClientSocket csock = (CClientSocket)ar.AsyncState;

            if (csock.m_bClosed == true)
                return;

            Int32 send_data_length = csock.m_clientsocket.EndSend(ar);
            if (csock.m_nDataLength > send_data_length)
            {
                int len = csock.m_nDataLength - send_data_length;
                csock.m_clientsocket.BeginSend(csock.host_data, send_data_length, len, 0,
                        new AsyncCallback(SendResultCallback), csock);
            }
            else
            {
                Int32 cnt = 0;
                m_nDataLength = 0;
                lock (m_output_lock)
                {
                    m_szOuputList.RemoveAt(0);
                    cnt = m_szOuputList.Count;
                }

                if (cnt > 0)
                {
                    string str = m_szOuputList[0];
                    if(str.Length > 0 )
                        csock.makeSend(str);
                }
            }
        }

        public void SendImage(byte[] send_image)
        {
            if (m_clientsocket == null)
                return;

            if (m_bReadyToRecv == false)
            {
                return;
            }

            lock (m_image_list_lock)
            {
                Int32 cnt = m_image_list.Count;
                if (cnt > 3)
                    m_image_list.RemoveAt(2);
                m_image_list.Add(send_image);
            }

            ActivateEvent.Set();
        }

        static void SendImageDataThread(object param)
        {
            CClientSocket csock = (CClientSocket)param;
            bool tf = false;
            do
            {
                tf = csock.ActivateEvent.WaitOne(1000);
                if (true == csock.m_bTerminateThread)
                    return;

                if(tf == true)
                    csock.SendImageData();
            } while (csock.m_bTerminateThread == false);

            csock.TerminatedEvent.Set();
        }

        public void SendImageData()
        {
            string msg = "";
            byte[] send_image = null;

            lock (m_image_list_lock)
            {
                Int32 cnt = m_image_list.Count;
                if (cnt <= 0)
                    return;
                                    
                send_image = m_image_list[0];
                m_image_list.RemoveAt(0);
            }

            m_nDataLength = send_image.Length;

            if (m_bClosed == true)
                return;

            lock (m_output_lock)
            {
                m_bReadyToRecv = false;

                Int32 network_length = System.Net.IPAddress.HostToNetworkOrder(m_nDataLength);
                string str_out = network_length.ToString();
                host_data = stringToByte(str_out);

                m_image_data = new byte[m_nDataLength + 4];       // 4 (length) + payload
                Array.Copy(host_data, 0, m_image_data, 0, 4);
                Array.Copy(send_image, 0, m_image_data, 4, m_nDataLength);

                msg = string.Format("[wiki] data = {0} {1} {2} {3}",
                                m_image_data[0],
                                m_image_data[1],
                                m_image_data[2],
                                m_image_data[3]);
                //Trace.WriteLine(msg);

                m_nOffset = 0;
                m_nDataLength += 4;

                try
                {
                    m_clientsocket.BeginSend(m_image_data, 0, m_nDataLength, 0,
                            new AsyncCallback(SendImageCallback), this);
                    msg = string.Format("[wiki] network_length = {0}, payload_len = {1}",
                                    network_length, m_nDataLength);
                    //Trace.WriteLine(msg);
                }
                catch
                {
                }
            }
        }

        void SendImageCallback(IAsyncResult ar)
        {
            string msg = "";
            CClientSocket csock = (CClientSocket)ar.AsyncState;
            if (csock.m_bClosed == true)
                return;

            try
            {
                Int32 send_data_length = csock.m_clientsocket.EndSend(ar);

                msg = string.Format("[wiki] send_data_length = {0}, payload_len = {1}", 
                                            send_data_length, csock.m_nDataLength);
                //Trace.WriteLine(msg);

                if (send_data_length <= 0)
                    return;

                int len = csock.m_nDataLength - send_data_length;
                m_nOffset += send_data_length;
                //if (csock.m_nDataLength > send_data_length)
                if (len > 0)
                {
                    csock.m_clientsocket.BeginSend(csock.m_image_data, m_nOffset, len, 0,
                            new AsyncCallback(SendImageCallback), csock);
                }
                else if (len == 0)
                {                    
                    csock.ReceiveData();
                    Trace.WriteLine("[wiki] send complete..");
                    csock.m_nDataLength = 0;
                    m_nOffset = 0;
                    m_image_data = null;

                    lock (m_image_list_lock)
                    {
                        if ( (m_image_list.Count > 0) && (m_bTerminateThread == false) )
                            ActivateEvent.Set();
                    }
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Trace.WriteLine(ex.ToString());                
                lock (csock.m_daemon.m_csock_list_lock)
                {
                    csock.m_daemon.csock_list.Remove(csock);
                }
                csock.CloseSock();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }
        }

        public void ReceiveData()
        {
            int DataLength_toBeRead = 128;

            if (m_clientsocket == null)
                return;

            Trace.WriteLine("[wiki] ReceiveData +");
                        
            m_clientsocket.BeginReceive(
                    m_ReceiverBuff, 
                    0, 
                    DataLength_toBeRead, 
                    SocketFlags.None, 
                    new AsyncCallback(onReceiveCallback), 
                    this);
        }

        static void onReceiveCallback(IAsyncResult ar)
        {
            string msg = "";
            CClientSocket csock = (CClientSocket)ar.AsyncState;

            if (csock.m_clientsocket == null)
                return;

            SocketError socketError;
            Int32 bytesRead = 0;
            try
            {
                // Read data from the remote device.
                bytesRead = csock.m_clientsocket.EndReceive(ar, out socketError);
                if (socketError != SocketError.Success)
                {
                    csock.m_clientsocket.Close();
                    csock.m_clientsocket = null;
                    Trace.WriteLine("[wiki] onReceiveCallback : socketError != SocketError.Success");
                    return;
                }
            }
            catch
            {
                return;
            }

            if (bytesRead == 0)
            {
                csock.CloseSock();
                return;
            }

            msg = string.Format("[wiki] onReceiveCallback : bytesRead= {0}", bytesRead); 
            Trace.WriteLine(msg);

            if (
                (bytesRead > 4)
                &&
                (csock.m_ReceiverBuff[bytesRead - 4] == '\r')
                &&
                (csock.m_ReceiverBuff[bytesRead - 3] == '\n')
                &&
                (csock.m_ReceiverBuff[bytesRead - 2] == '\r')
                &&
                (csock.m_ReceiverBuff[bytesRead - 1] == '\n')
                )
            {
                Trace.WriteLine("[wiki] recv good.");
                string data2 = csock.ByteToString(csock.m_ReceiverBuff);
                string data = data2.Substring(0, bytesRead);
                if (true == data.Equals("get_next_image\r\n\r\n"))
                {
                    csock.m_bReadyToRecv = true;
                }

                bytesRead = 0;
            }

            try
            {
                int DataLength_toBeRead = 128 - bytesRead;
                csock.m_clientsocket.BeginReceive(
                        csock.m_ReceiverBuff, 
                        bytesRead, 
                        DataLength_toBeRead, 
                        SocketFlags.None, 
                        new AsyncCallback(onReceiveCallback), 
                        csock);
            }
            catch
            {
            }
        }
    }
}
