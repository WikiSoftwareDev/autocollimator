﻿using System.Text;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    internal class CParameter
    {
        #region
        public static bool      FactorySetStartFlag     = false;
        public static bool      FactorySetFlag          = false;
        public static double    PixelPerDistance        = 4.8f;
        public static int       ReferenceThreshold      = 20;
        public static double    ReferenceAngle          = 0.1485f;
        public static int       AlphaValue              = 2;
        public static int       AverageNumber           = 4;
        public static int       SplineSampleFactor      = 10;
        public static float     CenterXPos              = 511.6f;
        public static float     CenterYPos              = 512.6f;
        public static double    TiltAngle               = 1.0;
        public static double    FocalLength             = 99728.5484;
        public static string    m_szDeviceSerial        = "0";

        const string m_szSectionName               = "PARAMETER_INFO";
        const string m_szKey_DeviceSerial          = "DeviceSerial";
        const string m_szKey_PixelPerDistance      = "PIXEL_PER_DISTANCE(4.8)";
        const string m_szKey_ReferenceThreshold    = "REF_THRESHOLD(20)";
        const string m_szKey_ReferenceAngle        = "REF_ANGLE(0.1485)";
        const string m_szKey_AlphaValue            = "ALPHA(2)";
        const string m_szKey_AverageNumber         = "AVERAGE_NUMBER(4)";
        const string m_szKey_SplineSampleFactor    = "REGRESSION_FACTOR(10)";
        const string m_szKey_CenterXPos            = "CENTER_X";
        const string m_szKey_CenterYPos            = "CENTER_Y";
        const string m_szKey_TiltAngle             = "TILT_ANGLE";
        const string m_szKey_FocalLength           = "FOCAL_LENGTH";
        #endregion

        public CParameter()
        {
        }

        static void SaveDefaultParameter(string filepath)
        {
            bool writeResult = false;
            string defValue;

            defValue = "0";
            NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_DeviceSerial, defValue, filepath);

            defValue = "4.8";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_PixelPerDistance, defValue, filepath);

            defValue = "20";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ReferenceThreshold, defValue, filepath);

            defValue = "0.1485";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ReferenceAngle, defValue, filepath);

            defValue = "2";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_AlphaValue, defValue, filepath);

            defValue = "4";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_AverageNumber, defValue, filepath);

            defValue = "10";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_SplineSampleFactor, defValue, filepath);

            defValue = "511.6";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_CenterXPos, defValue, filepath);

            defValue = "512.6";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_CenterYPos, defValue, filepath);

            defValue = "1.0";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_TiltAngle, defValue, filepath);

            defValue = "99728.5484";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_FocalLength, defValue, filepath);
        }

        public static void LoadParameterInfo()
        {
            try
            {
                // 현재 프로그램 실행 위치
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_device.ini";
                bool bExist = System.IO.File.Exists(filepath);
                if( false == bExist )
                {
                    SaveDefaultParameter(filepath);
                }
                else
                {
                    bExist = false;
                    string[] sec_names = CUtil.GetSectionNames(filepath);
                    int num_sections = sec_names.Length;
                    for (int i = 0; i < num_sections; i++)
                    {
                        bExist = sec_names[i].Equals(m_szSectionName);
                        if (bExist == true)
                            break;
                    }
                    if (false == bExist)
                    {
                        SaveDefaultParameter(filepath);
                    }
                }

                string defValue;
                StringBuilder retVal1 = new StringBuilder(128);
                int nSize = 128;
                int result1 = 0;
                double temp = 0;

                defValue = "0";
                NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_DeviceSerial, defValue, retVal1, nSize, filepath);
                m_szDeviceSerial = retVal1.ToString();

                defValue = "4.8";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_PixelPerDistance, defValue, retVal1, nSize, filepath);
                if( true == double.TryParse(retVal1.ToString(), out temp) )
                {
                    PixelPerDistance = temp;
                }                

                defValue = "20";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_ReferenceThreshold, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    ReferenceThreshold = result1;
                }

                defValue = "0.1485";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_ReferenceAngle, defValue, retVal1, nSize, filepath);
                if (true == double.TryParse(retVal1.ToString(), out temp))
                {
                    ReferenceAngle = temp;
                }

                defValue = "2";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_AlphaValue, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    AlphaValue = result1;
                }

                defValue = "4";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_AverageNumber, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    AverageNumber = result1;
                }

                defValue = "10";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_SplineSampleFactor, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    SplineSampleFactor = result1;
                }

                float temp2 = 0;
                defValue = "511.6";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_CenterXPos, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    CenterXPos = temp2;
                }

                defValue = "512.6";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_CenterYPos, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    CenterYPos = temp2;
                }

                defValue = "1.0";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_TiltAngle, defValue, retVal1, nSize, filepath);
                if (true == double.TryParse(retVal1.ToString(), out temp))
                {
                    TiltAngle = temp;
                }

                defValue = "99728.5484";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_FocalLength, defValue, retVal1, nSize, filepath);
                if (true == double.TryParse(retVal1.ToString(), out temp))
                {
                    FocalLength = temp;
                }
            }
            catch
            {
                MessageBox.Show(new Form
                {
                    TopMost = true
                }, "PARAMETER INFO FILE LOAD FAIL");
            }
        }

        public static void SaveParameterInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_device.ini";

            bool writeResult = false;
            string value;

            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_DeviceSerial, m_szDeviceSerial, filepath);

            value = string.Format("{0:#.#####}", PixelPerDistance);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_PixelPerDistance, value, filepath);

            value = string.Format("{0}", ReferenceThreshold);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ReferenceThreshold, value, filepath);

            value = string.Format("{0:#.#####}", ReferenceAngle);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ReferenceAngle, value, filepath);

            value = string.Format("{0}", AlphaValue);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_AlphaValue, value, filepath);

            value = string.Format("{0}", AverageNumber);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_AverageNumber, value, filepath);

            value = string.Format("{0}", SplineSampleFactor);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_SplineSampleFactor, value, filepath);

            value = string.Format("{0:#.#####}", CenterXPos);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_CenterXPos, value, filepath);

            value = string.Format("{0:#.#####}", CenterYPos);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_CenterYPos, value, filepath);

            value = string.Format("{0:#.#####}", TiltAngle);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_TiltAngle, value, filepath);

            value = string.Format("{0:#.#####}", FocalLength);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_FocalLength, value, filepath);
        }
    }
}
