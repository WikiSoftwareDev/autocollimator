﻿using System.Text;

namespace WikiOptics_Collimator
{
    internal class CSetInfo
    {
        #region
        public const int Tool_Mode_AreaCenter               = 1;
        public const int Tool_Mode_BrightnessCenter         = 2;
        public const int Tool_Mode_PeakCenter               = 3;
        public const int Tool_Angle_Definition_Ext          = 1;
        public const int Tool_Angle_Definition_Int          = 2;
        public const int Tool_Coordinate_Original           = 0;
        public const int Tool_Coordinate_Rotating           = 1;
        public const int Tool_Coordinate_Mirroring          = 2;
        public const int Tool_Coordinate_Mirroring_X        = 1;
        public const int Tool_Coordinate_Mirroring_Y        = 2;
        public const int Tool_Coordinate_Mirroring_Off      = 3;

        public const int Tool_Coordinate_Rotating_R         = 1;
        public const int Tool_Coordinate_Rotating_L         = 2;
        public const int Tool_Coordinate_Rotating_Off       = 3;
        public const int Tool_Numbering_Angle               = 1;
        public const int Tool_Numbering_Size                = 2;
        public const int Tool_Zoom_Off                      = 0;
        public const int Tool_Zoom_4                        = 4;
        public const int Tool_Zoom_8                        = 8;
        public const int Tool_Zoom_16                       = 16;
        public const int Tool_Zoom_32                       = 32;
        public const int Setting_Unit_Degree                = 1;
        public const int Setting_Unit_MinSec                = 2;
        public const int Setting_Tolerance_Circle           = 1;
        public const int Setting_Tolerance_Square           = 2;

        public static int Tool_Mode;
        public static int Tool_Angle_Definition = Tool_Angle_Definition_Ext;
        public static int Tool_Angle_Definition_Value;
        public static int Tool_Coordinate;
        public static bool Tool_Coordinate_ChangedFlag = false;
        public static int Tool_Coordinate_Mirroring_State = 0;
        public static int Tool_Coordinate_Rotating_State = 0;
        public static int Tool_Numbering;
        public static int Tool_Zoom_Flag;
        public static int Tool_Zoom_State = 0;
        public static int Setting_Unit;
        public static int Setting_Tolerance_Mode;
        public static int Setting_Tolerance_Hour;
        public static int Setting_Tolerance_Min;
        public static int Setting_Tolerance_Sec;
        public static int Setting_Gray_MinLevel;
        public static int Setting_Gray_MaxLevel;
        public static string SettingInfoJustFileName;
        public static string m_szSettingInfoFileName;
        const string m_szSetInfoSectionName = "SET_INFO";
        const string m_szKey_Tool_Mode = "Tool_Mode";
        const string m_szKey_Tool_Angle_Definition = "Tool_Angle_Definition";
        const string m_szKey_Tool_Coordinate = "Tool_Coordinate";
        const string m_szKey_Tool_Numbering = "Tool_Numbering";
        const string m_szKey_Setting_Unit = "Setting_Unit";
        const string m_szKey_Setting_Tolerance_Mode = "Setting_Tolerance_Mode";
        const string m_szKey_Setting_Tolerance_Hour = "Setting_Tolerance_Hour";
        const string m_szKey_Setting_Tolerance_Min = "Setting_Tolerance_Min";
        const string m_szKey_Setting_Tolerance_Sec = "Setting_Tolerance_Sec";
        const string m_szKey_Setting_Gray_MinLevel = "Setting_Gray_MinLevel";
        const string m_szKey_Setting_Gray_MaxLevel = "Setting_Gray_MaxLevel";
        #endregion

        public CSetInfo()
        {
            Tool_Mode               = Tool_Mode_PeakCenter;
            Tool_Angle_Definition   = Tool_Angle_Definition_Ext;
            Tool_Coordinate         = Tool_Coordinate_Original;
            Tool_Numbering          = Tool_Numbering_Angle;            
            Setting_Unit            = Setting_Unit_MinSec;
            Setting_Tolerance_Mode  = Setting_Tolerance_Circle;
            Setting_Tolerance_Hour  = 0;
            Setting_Tolerance_Min   = 0;
            Setting_Tolerance_Sec   = 0;
            Setting_Gray_MinLevel   = 30;
            Setting_Gray_MaxLevel   = 255;
        }

        static void SaveDefaultSettingInfo(string filepath)
        {
            string value;

            value = string.Format("{0}", Tool_Mode_PeakCenter);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Mode,
                value,
                filepath);

            value = string.Format("{0}", Tool_Angle_Definition_Ext);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Angle_Definition,
                value,
                filepath);

            value = string.Format("{0}", Tool_Coordinate_Original);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Coordinate,
                value,
                filepath);

            value = string.Format("{0}", Tool_Numbering_Angle);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Numbering,
                value,
                filepath);

            value = string.Format("{0}", Setting_Unit_MinSec);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Unit,
                value,
                filepath);

            value = string.Format("{0}", Setting_Tolerance_Circle);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Mode,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Hour,
                value,
                filepath);

            value = "2";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Min,
                value,
                filepath);

            value = "32";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Sec,
                value,
                filepath);

            value = "30";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Gray_MinLevel,
                value,
                filepath);

            value = "255";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Gray_MaxLevel,
                value,
                filepath);
        }

        public static void LoadSettingInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";            
            bool bExist = System.IO.File.Exists(filepath);
            if (false == bExist)
            {
                SaveDefaultSettingInfo(filepath);
            }
            else
            {
                bExist = false;
                string[] sec_names = CUtil.GetSectionNames(filepath);
                int num_sections = sec_names.Length;
                for (int i = 0; i < num_sections; i++)
                {
                    bExist = sec_names[i].Equals(m_szSetInfoSectionName);
                    if (bExist == true)
                        break;
                }
                if (false == bExist)
                {
                    SaveDefaultSettingInfo(filepath);
                }
            }
            LoadInfoFile(filepath);
        }

        public static void LoadInfoFile(string filepath)
        {
            int num = 0;
            string defValue;
            StringBuilder retVal1 = new StringBuilder(128);
            int nSize = 128;

            defValue = "3";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Mode,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Tool_Mode = num;
            }

            defValue = "1";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Angle_Definition,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Tool_Angle_Definition = num;
            }

            defValue = "0";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Coordinate,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Tool_Coordinate = num;
            }

            defValue = "1";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Tool_Numbering,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Tool_Numbering = num;
            }

            defValue = "2";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Unit,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Unit = num;
            }

            defValue = "1";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Mode,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Tolerance_Mode = num;
            }

            defValue = "1";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Hour,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Tolerance_Hour = num;
            }

            defValue = "2";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Min,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Tolerance_Min = num;
            }

            defValue = "32";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_Sec,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Tolerance_Sec = num;
            }

            defValue = "30";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Gray_MinLevel,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Gray_MinLevel = num;
            }

            defValue = "255";
            NativeMethods.GetPrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Gray_MaxLevel,
                defValue,
                retVal1,
                nSize,
                filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                Setting_Gray_MaxLevel = num;
            }

            if (Tool_Angle_Definition == 1)
            //if (Tool_Angle_Definition == 0)
            {
                Tool_Angle_Definition_Value = 2;
            }
            else
            {
                Tool_Angle_Definition_Value = 1;
            }
        }

        public static void SaveSettingInfo()
        {
            // 현재 프로그램 실행 위치
            //string settingInfoFileName = m_szSettingInfoFileName;
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
            bool writeResult = false;
            string value;

            value = string.Format("{0}", Tool_Mode);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Tool_Mode, value, filepath);

            value = string.Format("{0}", Tool_Angle_Definition);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Tool_Angle_Definition, value, filepath);

            value = string.Format("{0}", Tool_Coordinate);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Tool_Coordinate, value, filepath);

            value = string.Format("{0}", Tool_Numbering);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Tool_Numbering, value, filepath);

            value = string.Format("{0}", Setting_Unit);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Unit, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_Mode);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_Mode, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_Hour);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_Hour, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_Min);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_Min, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_Sec);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_Sec, value, filepath);

            value = string.Format("{0}", Setting_Gray_MinLevel);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Gray_MinLevel, value, filepath);

            value = string.Format("{0}", Setting_Gray_MaxLevel);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Gray_MaxLevel, value, filepath);
        }
    }
}
