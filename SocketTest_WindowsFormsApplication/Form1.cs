﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;


namespace SocketTest_WindowsFormsApplication
{
    public partial class Form1 : Form
    {
        Socket m_sock = null;

        Int32 m_nInternal_Buffer_size = 8*1024;
        object m_buffer_lock = new object();
        byte[] m_ReceiverBuff = new byte[8*1024];
        List<byte[]> m_img_part = new List<byte[]>();

        StringBuilder m_szRecv = new StringBuilder(8192);

        Int32 m_nOffset = 0;
        Int32 m_nPayloadSize = 0;
        Int32 m_nRecvSize = 0;
        Int32 m_nDisplayDataFlag = 1;

        Int32 width = 1024;
        Int32 height = 1024;

        delegate void delegate_DisplayTextData(string fn);
        delegate void delegate_DisplayImageData();

        Int32 m_nRecvType = 1;      // 0 : result, 1 : image

        public Form1()
        {
            InitializeComponent();
        }

        private void onFormLoad(Object sender, EventArgs e)
        {
            textBox_target_ip_address.Text = "127.0.0.1";
            //textBox_target_ip_address.Text = "192.168.0.24";
            textBox_target_ip_port.Text = "5006";
        }

        private void onFormClosing(Object sender, FormClosingEventArgs e)
        {
            try
            {
                if (m_sock != null)
                {
                    // Release the socket.
                    //m_sock.Shutdown(SocketShutdown.Both);
                    m_sock.Dispose();
                    m_sock = null;
                }
            }
            catch (Exception ex)
            {
            }
        }

        private void onKeypress_target_ip_port(Object sender, KeyPressEventArgs e)
        {
            if (    
                    (char.IsDigit(e.KeyChar) == false)
                    &&
                    (e.KeyChar != Convert.ToChar(Keys.Back))
                )
            {
                e.Handled = true;
            }
        }

        private void button_stop_update_Click(Object sender, EventArgs e)
        {
            m_nDisplayDataFlag = 1 - m_nDisplayDataFlag;
            if (1 == m_nDisplayDataFlag)
            {
                button_stop_update.Text = "Stop updating";
            }
            else 
            {
                button_stop_update.Text = "Updating";
            }
        }

        private void button_connect_Click(Object sender, EventArgs e)
        {
            //textBox_target_ip_address.Text = "127.0.0.1";
            //textBox_target_ip_port.Text = "5006";

            if (m_sock != null)
            {
                m_sock.Dispose();
                m_sock = null;
            }

            //m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_sock = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.IP);

            try
            {
                IPAddress ipaddr = IPAddress.Parse(textBox_target_ip_address.Text);
                Int32 nport = -1;
                bool tf = Int32.TryParse(textBox_target_ip_port.Text, out nport);
                if (tf == true)
                {
                    IPEndPoint endpt = new IPEndPoint(ipaddr, nport);
                    m_sock.BeginConnect(endpt, new AsyncCallback(onConnect), this);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }

            button_connect.Visible = false;
        }

        static void onConnect(IAsyncResult ar)
        {
            try
            {
                Form1 frm = (Form1)ar.AsyncState;
                // Complete the connection.
                frm.m_sock.EndConnect(ar);

                Thread myThread = new Thread(FirstDataRequestAfterConnection); 
                myThread.Start(frm);
                string msg = string.Format("[wiki] Socket connected to {0}", 
                                frm.m_sock.RemoteEndPoint.ToString());
                Trace.WriteLine(msg);
            }
            catch (Exception ex)
            {
                string msg = string.Format("[wiki] onConnect : except = {}", 
                                ex.ToString());
                Trace.WriteLine(msg);
            }
        }

        static void FirstDataRequestAfterConnection(object param)
        {
            Form1 frm = (Form1)param;

            Thread.Sleep(1000);

            frm.m_sock.BeginReceive(
                frm.m_ReceiverBuff,
                0,
                5,
                SocketFlags.None,
                new AsyncCallback(onReceiveCallback),
                frm);

            Trace.WriteLine("[wiki] FirstDataRequestAfterConnection : send msg to the server");
            frm.SendNextData(0);
            frm.SendNextData(1);
        }

        static string byteToString(byte[] strByte, int len) 
        {
            string str = Encoding.ASCII.GetString(strByte, 0, len);
            return str;
        }

        static void onReceiveCallback(IAsyncResult ar)
        {
            string msg = "";
            Form1 frm = (Form1)ar.AsyncState;
            try
            {
                if (frm.m_sock == null)
                    return;

                if (frm.m_sock.Connected == false)
                    return;

                SocketError socketError;

                // Read data from the remote device.
                Int32 bytesRead = frm.m_sock.EndReceive(ar, out socketError);
                if (socketError != SocketError.Success)
                {
                    frm.m_sock.Close();
                    frm.m_sock = null;

                    Trace.WriteLine("[wiki] socketError != SocketError.Success");
                    return;
                }

                if (bytesRead <= 0)
                {
                    msg = string.Format("[wiki] bytesRead == 0");
                    //Trace.WriteLine(msg);

                    //frm.SendNextData(0);
                    //frm.SendNextData(1);

                    frm.m_sock.BeginReceive(frm.m_ReceiverBuff,
                                0, 
                                5,
                                SocketFlags.None, 
                                new AsyncCallback(onReceiveCallback), 
                                frm);
                    return;
                }

                if (frm.m_nPayloadSize == 0)
                {
                    msg = string.Format("[wiki] bytesRead == {0}", bytesRead);
                    //Trace.WriteLine(msg);

                    // data type & length : 5 byte check
                    bytesRead += frm.m_nOffset;                    
                    if (bytesRead < 5)
                    {
                        frm.m_nOffset = bytesRead;
                        frm.m_sock.BeginReceive(frm.m_ReceiverBuff, 
                                            bytesRead, 
                                            5 - bytesRead,
                                            SocketFlags.None, 
                                            new AsyncCallback(onReceiveCallback), 
                                            frm);
                    }
                    else if (bytesRead == 5)
                    {
                        //byte[] image_len = new byte[4];
                        //image_len[0] = frm.m_ReceiverBuff[1];
                        //image_len[1] = frm.m_ReceiverBuff[2];
                        //image_len[2] = frm.m_ReceiverBuff[3];
                        //image_len[3] = frm.m_ReceiverBuff[4];

                        //string str_len = Encoding.ASCII.GetString(frm.m_ReceiverBuff, 1, 4); 
                        //string str_len = Encoding.ASCII.GetString(image_len, 0, 4);
                        Int32 network_len = -1;
                        //Int32.TryParse(str_len, out network_len);
                        
                        unsafe
                        {
                            fixed (byte* ptr = frm.m_ReceiverBuff)
                            {
                                frm.m_nRecvType = ptr[0];
                                network_len = *(Int32*)(ptr + 1);
                            }
                        }                        

                        msg = string.Format("[wiki] type:{0}, payload len = {1} {2} {3} {4}, network_len={5}",
                                        frm.m_ReceiverBuff[0],
                                        frm.m_ReceiverBuff[1],
                                        frm.m_ReceiverBuff[2],
                                        frm.m_ReceiverBuff[3],
                                        frm.m_ReceiverBuff[4],
                                        network_len);
                        //Trace.WriteLine(msg);

                        frm.m_nOffset = 0;
                        //frm.m_nPayloadSize = System.Net.IPAddress.NetworkToHostOrder(network_len);
                        frm.m_nRecvSize = network_len;
                        frm.m_nPayloadSize = network_len;
                        network_len = frm.m_nPayloadSize;

                        //msg = string.Format("[wiki] payload_size = {0}", frm.m_nPayloadSize); 
                        //Trace.WriteLine(msg);

                        if (frm.m_nInternal_Buffer_size < network_len)
                            network_len = frm.m_nInternal_Buffer_size;

                        frm.m_sock.BeginReceive(frm.m_ReceiverBuff,
                                    0, 
                                    network_len,
                                    SocketFlags.None, 
                                    new AsyncCallback(onReceiveCallback), 
                                    frm);
                    }
                } // if (frm.m_nPayloadSize == 0)
                else if (frm.m_nPayloadSize > 0)
                {
                    Int32 toRead = 0;
                    if (frm.m_nRecvType == 0)
                    {
                        // result data
                        
                        //msg = string.Format("[wiki] result data recv : bytesRead = {0}", bytesRead); 
                        //Trace.WriteLine(msg);

                        string recv_str = byteToString(frm.m_ReceiverBuff, frm.m_nPayloadSize);
                        frm.m_szRecv.Append(recv_str);
                        frm.m_nRecvSize -= bytesRead;
                        if (frm.m_nRecvSize == 0)
                        {
                            frm.m_nOffset = 0;
                            frm.m_nPayloadSize = 0;
                            string szRecvData = frm.m_szRecv.ToString();
                            frm.m_szRecv.Clear();

                            //if (frm.richTextBox_result_date.InvokeRequired == true)
                            {
                                delegate_DisplayTextData dd = new delegate_DisplayTextData(frm.DisplayResultData);
                                frm.richTextBox_result_date.Invoke(dd, new object[] { szRecvData });
                            }
                            toRead = 5;
                        }
                        else if (frm.m_nRecvSize > 0)
                        {
                            // have to receive more data
                            frm.m_nOffset += bytesRead;
                            toRead = frm.m_nPayloadSize;
                            if (frm.m_nInternal_Buffer_size < toRead)
                                toRead = frm.m_nInternal_Buffer_size;
                        }

                        msg = string.Format("[wiki] result data : call recv : BeginReceive(toRead = {0})", toRead); 
                        Trace.WriteLine(msg);

                        frm.m_sock.BeginReceive(frm.m_ReceiverBuff, 
                                    frm.m_nOffset, 
                                    toRead, 
                                    SocketFlags.None, 
                                    new AsyncCallback(onReceiveCallback),
                                    frm);
                    } // if (frm.m_nRecvType == 0) // for result data
                    else if (frm.m_nRecvType == 1)
                    {
                        // image data

                        //byte[] beReadImageFromNetwork = frm.m_ReceiverBuff.ToArray();
                        byte[] beReadImageFromNetwork = new byte[bytesRead];
                        Array.Copy(frm.m_ReceiverBuff, 0, beReadImageFromNetwork, 0, bytesRead);
                        lock (frm.m_buffer_lock)
                        {
                            frm.m_img_part.Add(beReadImageFromNetwork);
                        }
                        
                        frm.m_nPayloadSize -= bytesRead;

                        //msg = string.Format("[wiki] bytesRead = {0}, payload_size = {1}",
                        //                    bytesRead, frm.m_nPayloadSize);
                        //Trace.WriteLine(msg);

                        if (frm.m_nPayloadSize == 0)
                        {
                            delegate_DisplayImageData dd = new delegate_DisplayImageData(frm.DisplayImageData);
                            //Trace.WriteLine("[wiki] call pictureBox_remoteCamera.Invoke");
                            frm.pictureBox_remoteCamera.Invoke(dd, new object[] { });
                            toRead = 5;
                        }
                        else if (frm.m_nPayloadSize > 0)
                        {
                            toRead = frm.m_nPayloadSize;
                            if (frm.m_nInternal_Buffer_size < toRead)
                            {
                                toRead = frm.m_nInternal_Buffer_size;
                            }
                            else if( toRead <= frm.m_nInternal_Buffer_size )
                            {
                                toRead = frm.m_nPayloadSize;
                            }
                        }

                        frm.m_sock.BeginReceive(frm.m_ReceiverBuff,
                                    0,
                                    toRead,
                                    SocketFlags.None, 
                                    new AsyncCallback(onReceiveCallback),
                                    frm);
                    } // else if (frm.m_nRecvType == 1)
                } // else if (frm.m_nPayloadSize > 0)
            }
            catch
            {
            }
        } // onReceiveCallback

        void DisplayImageData()
        {
            //Trace.WriteLine("[wiki] DisplayImageData +");

            string msg;
            byte[] image_data = null;
            Int32 cnt = m_img_part.Count;
            if (cnt <= 0)
            {
                Trace.WriteLine("[wiki] DisplayImageData #1 : m_img_part.Count <= 0");
                return;
            }

            Int32 i = 0;
            Int32 offset = 0;
            Int32 total_size = 0;
            lock (m_buffer_lock)
            {
                for (i = 0; i < cnt; i++)
                {
                    total_size += m_img_part[i].Length;
                }

                if (total_size <= 0)
                {
                    Trace.WriteLine("[wiki] DisplayImageData #3");
                    return;
                }

                image_data = new byte[total_size];
                offset = 0;
                for (i = 0; i < cnt; i++)
                {
                    Int32 len = m_img_part[i].Length;
                    Array.Copy(m_img_part[i], 0, image_data, offset, len);
                    offset += len;
                }

                //msg = string.Format("[wiki] DisplayImageData #2 : total_size = {0}", total_size);
                //Trace.WriteLine(msg);

                m_img_part.Clear();
            } // lock (m_buffer_lock)

            // offset == total_size;
            byte[] data = new byte[width * height * 3];
            Int32 bgr_inx = 0;
            for (i = 0; i < total_size; i++)
            {
                data[bgr_inx + 0] = image_data[i];
                data[bgr_inx + 1] = image_data[i];
                data[bgr_inx + 2] = image_data[i];

                bgr_inx += 3;
            }

            //Trace.WriteLine("[wiki] DisplayImageData : bmp is made..");

            unsafe
            {
                fixed (byte* ptr = data)
                {
                    //using (Bitmap img = new Bitmap(width, height, width * 3, PixelFormat.Format24bppRgb, new IntPtr(ptr) ))
                    {
                        Bitmap img = CopyDataToBitmap(data, width, height);
                        pictureBox_remoteCamera.Image = img;
                        pictureBox_remoteCamera.Refresh();
                    }
                }
            }

            //Trace.WriteLine("[wiki] image received successfully, call SendNextData(1);");
            SendNextData(1);
        }

        public void SendNextData(int RecvType)
        {
            Trace.WriteLine("[wiki] SendNextData +");

            if (m_sock == null)
            {
                return;
            }

            try
            {
                if (0 == RecvType)
                {
                    string str = "get_next_result\r\n\r\n";
                    byte[] send_buff = stringToByte(str);
                    m_sock.Send(send_buff);

                    Trace.WriteLine("[wiki] SendNextData : result");
                }
                else if (1 == RecvType)
                {
                    string str = "get_next_image\r\n\r\n";
                    byte[] send_buff = stringToByte(str);
                    m_sock.Send(send_buff);

                    Trace.WriteLine("[wiki] SendNextData : image");
                }
                else
                {
                    return;
                }                
            }
            catch (Exception ex)
            {
                Trace.WriteLine("[wiki] SendNextData : Except = {0}", ex.ToString());
                Trace.WriteLine("[wiki] SendNextData : stacktrace = {0}", ex.StackTrace);
            }

            Trace.WriteLine("[wiki] SendNextData -");
        }

        public Bitmap CopyDataToBitmap(byte[] data, Int32 w, Int32 h)
        {
            Bitmap bmp = new Bitmap(w, h, PixelFormat.Format24bppRgb);
            BitmapData bdata = bmp.LockBits(new Rectangle(0, 0, w, h), ImageLockMode.WriteOnly, bmp.PixelFormat);
            Marshal.Copy(data, 0, bdata.Scan0, data.Length);
            bmp.UnlockBits(bdata);
            return bmp;
        }

        void DisplayResultData(string szRecvData)
        {
            richTextBox_result_date.Text = szRecvData;

            //Trace.WriteLine("[wiki] DisplayResultData : call SendNextData(0)");
            SendNextData(0);
        }

        private byte[] stringToByte(string str)
        {
            byte[] StrByte = Encoding.ASCII.GetBytes(str); 
            return StrByte; 
        }
    }
}


// https://www.ridgesolutions.ie/index.php/2013/02/13/c-save-grayscale-bytearray-image-byte-as-bitmap-file-bmp-example/

// https://swharden.com/blog/2022-11-04-csharp-create-bitmap/
