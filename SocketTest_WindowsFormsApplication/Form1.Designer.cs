﻿namespace SocketTest_WindowsFormsApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_connect = new System.Windows.Forms.Button();
            this.textBox_target_ip_address = new System.Windows.Forms.TextBox();
            this.label_ip_address = new System.Windows.Forms.Label();
            this.label_target_port = new System.Windows.Forms.Label();
            this.textBox_target_ip_port = new System.Windows.Forms.TextBox();
            this.richTextBox_result_date = new System.Windows.Forms.RichTextBox();
            this.button_stop_update = new System.Windows.Forms.Button();
            this.pictureBox_remoteCamera = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_remoteCamera)).BeginInit();
            this.SuspendLayout();
            // 
            // button_connect
            // 
            this.button_connect.Location = new System.Drawing.Point(270, 28);
            this.button_connect.Name = "button_connect";
            this.button_connect.Size = new System.Drawing.Size(94, 23);
            this.button_connect.TabIndex = 0;
            this.button_connect.Text = "Connect.";
            this.button_connect.UseVisualStyleBackColor = true;
            this.button_connect.Click += new System.EventHandler(this.button_connect_Click);
            // 
            // textBox_target_ip_address
            // 
            this.textBox_target_ip_address.Location = new System.Drawing.Point(136, 12);
            this.textBox_target_ip_address.Name = "textBox_target_ip_address";
            this.textBox_target_ip_address.Size = new System.Drawing.Size(100, 21);
            this.textBox_target_ip_address.TabIndex = 1;
            this.textBox_target_ip_address.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label_ip_address
            // 
            this.label_ip_address.AutoSize = true;
            this.label_ip_address.Location = new System.Drawing.Point(12, 15);
            this.label_ip_address.Name = "label_ip_address";
            this.label_ip_address.Size = new System.Drawing.Size(118, 12);
            this.label_ip_address.TabIndex = 2;
            this.label_ip_address.Text = "Target IP address : ";
            // 
            // label_target_port
            // 
            this.label_target_port.AutoSize = true;
            this.label_target_port.Location = new System.Drawing.Point(12, 50);
            this.label_target_port.Name = "label_target_port";
            this.label_target_port.Size = new System.Drawing.Size(93, 12);
            this.label_target_port.TabIndex = 3;
            this.label_target_port.Text = "Target IP port : ";
            // 
            // textBox_target_ip_port
            // 
            this.textBox_target_ip_port.Location = new System.Drawing.Point(136, 47);
            this.textBox_target_ip_port.Name = "textBox_target_ip_port";
            this.textBox_target_ip_port.Size = new System.Drawing.Size(100, 21);
            this.textBox_target_ip_port.TabIndex = 4;
            this.textBox_target_ip_port.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBox_target_ip_port.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.onKeypress_target_ip_port);
            // 
            // richTextBox_result_date
            // 
            this.richTextBox_result_date.Location = new System.Drawing.Point(14, 77);
            this.richTextBox_result_date.Name = "richTextBox_result_date";
            this.richTextBox_result_date.Size = new System.Drawing.Size(348, 380);
            this.richTextBox_result_date.TabIndex = 5;
            this.richTextBox_result_date.Text = "";
            // 
            // button_stop_update
            // 
            this.button_stop_update.Location = new System.Drawing.Point(404, 383);
            this.button_stop_update.Name = "button_stop_update";
            this.button_stop_update.Size = new System.Drawing.Size(111, 23);
            this.button_stop_update.TabIndex = 6;
            this.button_stop_update.Text = "Stop updating";
            this.button_stop_update.UseVisualStyleBackColor = true;
            this.button_stop_update.Click += new System.EventHandler(this.button_stop_update_Click);
            // 
            // pictureBox_remoteCamera
            // 
            this.pictureBox_remoteCamera.Location = new System.Drawing.Point(404, 18);
            this.pictureBox_remoteCamera.Name = "pictureBox_remoteCamera";
            this.pictureBox_remoteCamera.Size = new System.Drawing.Size(428, 267);
            this.pictureBox_remoteCamera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox_remoteCamera.TabIndex = 7;
            this.pictureBox_remoteCamera.TabStop = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(844, 469);
            this.Controls.Add(this.pictureBox_remoteCamera);
            this.Controls.Add(this.button_stop_update);
            this.Controls.Add(this.richTextBox_result_date);
            this.Controls.Add(this.textBox_target_ip_port);
            this.Controls.Add(this.label_target_port);
            this.Controls.Add(this.label_ip_address);
            this.Controls.Add(this.textBox_target_ip_address);
            this.Controls.Add(this.button_connect);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.onFormClosing);
            this.Load += new System.EventHandler(this.onFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_remoteCamera)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_connect;
        private System.Windows.Forms.TextBox textBox_target_ip_address;
        private System.Windows.Forms.Label label_ip_address;
        private System.Windows.Forms.Label label_target_port;
        private System.Windows.Forms.TextBox textBox_target_ip_port;
        private System.Windows.Forms.RichTextBox richTextBox_result_date;
        private System.Windows.Forms.Button button_stop_update;
        private System.Windows.Forms.PictureBox pictureBox_remoteCamera;
    }
}

