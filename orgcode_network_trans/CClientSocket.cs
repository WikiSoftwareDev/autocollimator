﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using System.Xml;


namespace WikiOptics_Collimator
{
    public class CClientSocket : IDisposable
    {        
        public Socket m_clientsocket = null;
        public CWikiTcpServer m_daemon = null;

        public Int32   m_nRecvOffset = 0;
        public Int32   m_nResultDataLength = 0;
        public Int32   m_nImageDataLength = 0;
        public Int32   m_nImageDataSendOffset = 0;
        public Int32   m_nResultDataSendOffset = 0;
        
        object  m_output_lock = new object();

        byte[] m_ReceiverBuff = new byte[1024];

        // list of xml string
        byte[]  host_data = null;
        public List<string> m_szOuputList = new List<string>();
        public object m_result_input_list_lock = new object();
        public List<double> m_xt = new List<double>();
        public List<double> m_yt = new List<double>();
        public List<double> m_xyt = new List<double>();

        AutoResetEvent m_SendResultThreadActivateEvent = new AutoResetEvent(false);
        Thread m_SendResultThread = null;

        public List<byte[]> m_image_list = new List<byte[]>();
        public byte[] m_image_data = null;
        public object m_image_input_list_lock = new object();

        AutoResetEvent m_SendImageThreadActivateEvent = new AutoResetEvent(false);
        Thread m_SendImageThread = null;

        bool m_bTerminateThread = false;
        public bool m_bClosed = false;
        bool m_bReadyToSendImg = false;
        bool m_bReadyToSendResult = false;

        public CClientSocket()
        {
            m_bClosed = false;
        }

        public void CloseSock()
        {
            m_bTerminateThread = true;
            m_SendImageThreadActivateEvent.Set();
            m_SendResultThreadActivateEvent.Set();

            Trace.WriteLine("[wiki] CloseSock +");

            lock (m_output_lock)
            {
                m_bClosed = true;
                if (m_clientsocket != null)
                {
                    try
                    {
                        // https://ckbcorp.tistory.com/887
                        // disconnect(); close();   => gracefully disconnect
                        // 데이터 송수신중이라면 모든 데이터의 송수신이 완결된 후 종료가 실행
                        // 그만큼 대기 시간이 소요되거나 먹통이 된것으로 간주된다

                        // shutdown(); close();
                        // 전송중인 데이터가 있어도 바로 끊고 종료한다

                        //m_clientsocket.Shutdown(SocketShutdown.Both);
                        //m_clientsocket.Disconnect(true);
                        m_clientsocket.Close();
                        m_clientsocket = null;
                    }
                    catch (Exception ex)
                    {
                        Trace.WriteLine("[wiki] CloseSock except = " + ex.ToString());
                    }
                }
            }

            if( m_SendImageThread != null )
                m_SendImageThread.Join(1000 * 3);

            if( m_SendResultThread != null )
                m_SendResultThread.Join(1000 * 3);

            m_SendImageThread = null;
            m_SendResultThread = null;

            m_szOuputList.Clear();
            m_image_list.Clear();

            m_xt.Clear();
            m_yt.Clear();
            m_xyt.Clear();

            m_SendResultThreadActivateEvent.Close();
            m_SendImageThreadActivateEvent.Close();

            Trace.WriteLine("[wiki] CloseSock -");
        }

        // 바이트 배열을 String으로 변환 
        private string ByteToString(byte[] strByte) 
        { 
            //string str = Encoding.Default.GetString(strByte); 
            string str = Encoding.ASCII.GetString(strByte); 
            return str; 
        }
        
        // string을 바이트 배열로 변환
        private byte[] stringToByte(string str)
        {
            byte[] StrByte = Encoding.UTF8.GetBytes(str); 
            return StrByte; 
        }

        public void InitOption()
        {
            m_clientsocket.SetSocketOption(SocketOptionLevel.Socket, 
            SocketOptionName.SendTimeout, 
            2000);                              // send time out : 2 seconds

            m_clientsocket.SetSocketOption(SocketOptionLevel.Socket, 
            SocketOptionName.ReceiveTimeout, 
            2000);                              // receive time out : 2 seconds

            // https://www.sysnet.pe.kr/2/0/1825
            // 하지만, Timeout으로 인해 클라이언트 측에서 연결을 끊는 것이 좋은 선택은 아닙니다. 
            // 왜냐하면 상대방 소켓을 관리하는 스레드가 바빠서 데이터 전송을 못하는 걸 수도 있고, 
            // 때로는 패킷 정의를 하다 보면 각각의 요청/응답에 요구되는 timeout이 다르기 때문에 
            // 그것에 따라 연결 관리까지 함께 하는 것은 복잡도만 증가시킬 뿐입니다.
            // 이에 대한 대안으로 선택될 수 있는 옵션이 바로 Keep-Alive입니다. 다음과 같이 
            // KeepAlive 설정을 IOControl 메서드로 지정해 두면,

            int size = sizeof(UInt32);
            UInt32 on = 1;
            UInt32 keepAliveInterval = 10000;   // Send a packet once every 10 seconds.
            UInt32 retryInterval = 1000;        // If no response, resend every second.
            byte[] inArray = new byte[size * 3];
            Array.Copy(BitConverter.GetBytes(on), 0, inArray, 0, size);
            Array.Copy(BitConverter.GetBytes(keepAliveInterval), 0, inArray, size, size);
            Array.Copy(BitConverter.GetBytes(retryInterval), 0, inArray, size * 2, size);

            m_clientsocket.IOControl(IOControlCode.KeepAliveValues, inArray, null);

            m_SendImageThread = new Thread(SendImageDataThread);
            m_SendImageThread.Start(this);

            m_SendResultThread = new Thread(SendResultDataThread);
            m_SendResultThread.Start(this);

            ReceiveData();
        }

        static void SendResultDataThread(object param)
        {
            CClientSocket csock = (CClientSocket)param;
            bool tf = false;
            do
            {
                tf = csock.m_SendResultThreadActivateEvent.WaitOne(1000);
                if (true == csock.m_bTerminateThread)
                    return;

                if(tf == true)
                    csock.SendResultData();
            } while (csock.m_bTerminateThread == false);
        }

        public void PushResultBuffer(double[] xt, double[] yt, double[] xyt)
        {
            if (
                (m_bClosed == true)
                ||
                (m_bReadyToSendResult == false)
                ||
                (m_clientsocket == null)
                )
            {
                return;
            }
                
            lock (m_result_input_list_lock)
            {
                m_xt.Clear();
                m_yt.Clear();
                m_xyt.Clear();

                int array_len = xt.Length;
                if (array_len > 0)
                {
                    m_xt = new List<double>(xt);
                    m_yt = new List<double>(yt);
                    m_xyt = new List<double>(xyt);
                }
            }

            m_bReadyToSendResult = false;
            m_SendResultThreadActivateEvent.Set();
        }

        public void SendResultData()
        {
            if (m_xt.Count <= 0)
                return;

            int array_len = m_xt.Count;

            Utf8StringWriter writer = new Utf8StringWriter();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;

            lock (m_result_input_list_lock)
            {
                using (XmlWriter wr = XmlWriter.Create(writer, settings))
                {
                    wr.WriteStartDocument();
                    wr.WriteStartElement("results");

                    for (int i = 0; i < array_len; i++)
                    {
                        wr.WriteStartElement("result");

                        wr.WriteElementString("xt", m_xt[i].ToString());
                        wr.WriteElementString("yt", m_yt[i].ToString());
                        wr.WriteElementString("xyt", m_xyt[i].ToString());

                        wr.WriteEndElement();
                    }
                    wr.WriteEndElement();
                    wr.WriteEndDocument();
                }
            } // lock (m_result_input_list_lock)

            //lock (m_output_lock)
            {
                string szData = writer.ToString();
                if (szData.Length < 10)
                    return;
                
                //m_szOuputList.Clear();
                if (m_szOuputList.Count >= 2)
                {
                    m_szOuputList.RemoveAt(m_szOuputList.Count - 1);
                }
                m_szOuputList.Add(szData);
            } // lock (m_output_lock)

            //Trace.WriteLine("[wiki] SendResultData : call makeResultSend(m_szOuputList[0])");
            makeResultSend(m_szOuputList[0]);
            m_szOuputList.RemoveAt(0);
        }

        public void makeResultSend(string szOutput)
        {
            if (m_bClosed == true)
                return;

            //Int32 host_length = szOutput.Length;
            //Int32 network_length = System.Net.IPAddress.HostToNetworkOrder(host_length);
            Int32 payload_length = szOutput.Length;
            //string str_out = "0";       // packet type

            host_data = new byte[payload_length + 5];

            unsafe
            {
                fixed (byte* ptr = host_data)
                {
                    ptr[0] = 0;
                    Int32* len = (Int32*)(ptr + 1);
                    *len = payload_length;
                }
            }

            byte[] xml_data = stringToByte(szOutput);
            Array.Copy(xml_data, 0, host_data, 5, payload_length);
            //str_out += network_length.ToString() + szOutput;            

            m_nResultDataSendOffset = 0;            
            m_nResultDataLength = payload_length + 5;

            m_clientsocket.BeginSend(host_data, 0, m_nResultDataLength, 0,
                    new AsyncCallback(SendResultCallback), this);
        }

        void SendResultCallback(IAsyncResult ar)
        {
            CClientSocket csock = (CClientSocket)ar.AsyncState;
            if (csock.m_bClosed == true)
                return;

            Int32 send_data_length = csock.m_clientsocket.EndSend(ar);
            if (send_data_length <= 0)
                return;

            try
            {
                int len = csock.m_nResultDataLength - send_data_length;
                m_nResultDataSendOffset += send_data_length;
                if (len > 0)
                {
                    csock.m_clientsocket.BeginSend(csock.host_data, m_nResultDataSendOffset, len, 0,
                            new AsyncCallback(SendResultCallback), csock);
                }
                else if (len == 0)
                {
                    //csock.ReceiveData();
                    Trace.WriteLine("[wiki] SendResultCallback : result send complete..");
                    csock.m_nResultDataLength = 0;
                    m_nResultDataSendOffset = 0;
                    csock.host_data = null;

                    lock (m_result_input_list_lock)
                    {
                        if ( (m_szOuputList.Count > 0) && (m_bTerminateThread == false) )
                            m_SendResultThreadActivateEvent.Set();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void SendImage(byte[] send_image)
        {
            if (m_clientsocket == null)
                return;

            if (m_bReadyToSendImg == false)
            {
                return;
            }

            m_bReadyToSendImg = false;
            lock (m_image_input_list_lock)
            {
                Int32 cnt = m_image_list.Count;
                if (cnt > 2)
                {
                    m_image_list.RemoveAt(0);
                }                    
                m_image_list.Add(send_image);
            }

            m_SendImageThreadActivateEvent.Set();
        }

        static void SendImageDataThread(object param)
        {
            CClientSocket csock = (CClientSocket)param;
            bool tf = false;
            do
            {
                tf = csock.m_SendImageThreadActivateEvent.WaitOne(1000);
                //tf = csock.m_SendImageThreadActivateEvent.WaitOne();
                if (true == csock.m_bTerminateThread)
                    return;

                if(tf == true)
                    csock.SendImageData();
            } while (csock.m_bTerminateThread == false);
        }

        public void SendImageData()
        {
            string msg = "";
            byte[] send_image = null;

            lock (m_image_input_list_lock)
            {
                Int32 cnt = m_image_list.Count;
                if (cnt <= 0)
                    return;
                                    
                send_image = m_image_list[0];
                m_image_list.RemoveAt(0);
            }

            m_nImageDataLength = send_image.Length;

            lock (m_output_lock)
            {
                if (m_bClosed == true)
                    return;

                m_bReadyToSendImg = false;

                Int32 network_length = m_nImageDataLength;
                //Int32 network_length = System.Net.IPAddress.HostToNetworkOrder(m_nImageDataLength);
                //string str_out = network_length.ToString();
                //host_data = stringToByte(str_out);

                m_image_data = new byte[m_nImageDataLength + 5];        // 1(str(0)/img(1)) + 4 (length) + payload
                m_image_data[0] = 1;                                    // data type 1 : image
                //Array.Copy(host_data, 0, m_image_data, 1, 4);
                //Array.Copy(send_image, 0, m_image_data, 5, m_nImageDataLength);
                
                unsafe
                {
                    fixed (byte* ptr = m_image_data)
                    {
                        ptr[0] = 1;
                        Int32* len = (Int32*)(ptr + 1);
                        *len = network_length;
                    }
                }
                
                Array.Copy(send_image, 0, m_image_data, 5, m_nImageDataLength);

                msg = string.Format("[wiki] data = {0} {1} {2} {3} {4}",
                                m_image_data[0],
                                m_image_data[1],
                                m_image_data[2],
                                m_image_data[3],
                                m_image_data[4]);
                //Trace.WriteLine(msg);

                m_nImageDataSendOffset = 0;
                m_nImageDataLength += 5;

                if (null == m_clientsocket)
                    return;

                try
                {
                    m_clientsocket.BeginSend(m_image_data, 0, m_nImageDataLength, 0,
                            new AsyncCallback(SendImageCallback), this);
                    msg = string.Format("[wiki] network_length = {0}, payload_len = {1}",
                                    network_length, m_nImageDataLength);
                    //Trace.WriteLine(msg);
                }
                catch(Exception ex)
                {
                    msg = string.Format("[wiki] SendImageData except = {0}", ex.StackTrace);
                    Trace.WriteLine(msg);
                }
            } // lock (m_output_lock)
            //m_SendImageThreadActivateEvent.Set();
        }

        void SendImageCallback(IAsyncResult ar)
        {
            string msg = "";
            CClientSocket csock = (CClientSocket)ar.AsyncState;
            if (csock.m_bClosed == true)
                return;

            try
            {
                Int32 send_data_length = csock.m_clientsocket.EndSend(ar);

                msg = string.Format("[wiki] send_data_length = {0}, payload_len = {1}", 
                                            send_data_length, csock.m_nImageDataLength);
                //Trace.WriteLine(msg);

                if (send_data_length <= 0)
                    return;

                int len = csock.m_nImageDataLength - send_data_length;
                m_nImageDataSendOffset += send_data_length;
                if (len > 0)
                {
                    csock.m_clientsocket.BeginSend(csock.m_image_data, m_nImageDataSendOffset, len, 0,
                            new AsyncCallback(SendImageCallback), csock);
                }
                else if (len == 0)
                {                    
                    //csock.ReceiveData();
                    Trace.WriteLine("[wiki] image send complete..");
                    //csock.m_nImageDataLength = 0;
                    //m_nImageDataSendOffset = 0;
                    //m_image_data = null;

                    lock (m_image_input_list_lock)
                    {
                        if ( (m_image_list.Count > 0) && (m_bTerminateThread == false) )
                            m_SendImageThreadActivateEvent.Set();
                    }
                }
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                Trace.WriteLine(ex.ToString());
                lock (csock.m_daemon.m_csock_list_lock)
                {
                    csock.m_daemon.csock_list.Remove(csock);
                }
                csock.CloseSock();
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.ToString());
            }
        }

        public void ReceiveData()
        {
            int DataLength_toBeRead = 128;

            Trace.WriteLine("[wiki] ReceiveData +");

            if (m_clientsocket == null)
            {
                Trace.WriteLine("[wiki] ReceiveData - : m_clientsocket == null");
                return;
            }

            try
            {
                m_clientsocket.BeginReceive(
                        m_ReceiverBuff,
                        0,
                        DataLength_toBeRead,
                        SocketFlags.None,
                        new AsyncCallback(onReceiveCallback),
                        this);
            }
            catch(Exception ex)
            {
                Trace.WriteLine("[wiki] ReceiveData except = " + ex.StackTrace);
            }
            Trace.WriteLine("[wiki] ReceiveData -");
        }

        static void onReceiveCallback(IAsyncResult ar)
        {
            string msg = "";
            CClientSocket csock = (CClientSocket)ar.AsyncState;

            //Trace.WriteLine("[wiki] onReceiveCallback +");
            if (csock.m_clientsocket == null)
            {
                Trace.WriteLine("[wiki] onReceiveCallback - : csock.m_clientsocket == null");
                return;
            }

            SocketError socketError;
            Int32 bytesRead = 0;
            try
            {
                // Read data from the remote device.
                bytesRead = csock.m_clientsocket.EndReceive(ar, out socketError);
                if (socketError != SocketError.Success)
                {
                    csock.m_clientsocket.Close();
                    csock.m_clientsocket = null;
                    Trace.WriteLine("[wiki] onReceiveCallback : socketError != SocketError.Success");
                    return;
                }
            }
            catch
            {
                return;
            }

            if (bytesRead == 0)
            {
                Trace.WriteLine("[wiki] onReceiveCallback : bytesRead == 0");
                if (csock.m_bClosed == true)
                {
                    csock.m_bReadyToSendResult = false;
                    csock.m_bReadyToSendImg = false;
                }
                else
                {
                    csock.ReceiveData();
                }                
                return;
            }

            //msg = string.Format("[wiki] onReceiveCallback : bytesRead= {0}", bytesRead); 
            //Trace.WriteLine(msg);

            if (
                (bytesRead > 4)
                &&
                (csock.m_ReceiverBuff[bytesRead - 4] == '\r')
                &&
                (csock.m_ReceiverBuff[bytesRead - 3] == '\n')
                &&
                (csock.m_ReceiverBuff[bytesRead - 2] == '\r')
                &&
                (csock.m_ReceiverBuff[bytesRead - 1] == '\n')
                )
            {
                //Trace.WriteLine("[wiki] recv good.");

                csock.m_ReceiverBuff[bytesRead] = 0;

                string str_splitter = "\r\n\r\n";
                string data2 = "";
                string data1 = "";
                string data = csock.ByteToString(csock.m_ReceiverBuff);
                int inx = 0;

                data2 = data.Substring(0, bytesRead);
                do
                {
                    inx = data2.IndexOf(str_splitter);
                    if (inx == -1)
                    {
                        break;
                    }

                    data = data2.Substring(0, inx + 4);
                    if (true == data.Equals("get_next_image\r\n\r\n"))
                    {
                        csock.m_nImageDataLength = 0;
                        csock.m_nImageDataSendOffset = 0;
                        csock.m_image_data = null;
                        csock.m_bReadyToSendImg = true;
                        Trace.WriteLine("[wiki] onReceiveCallback : get_next_image");
                    }
                    else if (true == data.Equals("get_next_result\r\n\r\n"))
                    {
                        csock.m_bReadyToSendResult = true;
                        Trace.WriteLine("[wiki] onReceiveCallback : get_next_result");
                    }
                    else
                    {
                        Trace.WriteLine("[wiki] onReceiveCallback : hukkkk....");
                    }

                    data1 = data2.Substring(inx + 4);
                    data2 = data1;
                } while (true);
                bytesRead = 0;
                csock.m_nRecvOffset = data2.Length;

                //msg = string.Format("[wiki] onReceiveCallback : data2.Length = {0}", data2.Length);
                //Trace.WriteLine(msg);
            }
            else
            {
                Trace.WriteLine("[wiki] onReceiveCallback : (bytesRead < 4) or ... ");
            }

            try
            {
                if (csock.m_bClosed == true)
                    return;

                int DataLength_toBeRead = 128 - bytesRead;
                csock.m_nRecvOffset += bytesRead;
                csock.m_clientsocket.BeginReceive(
                        csock.m_ReceiverBuff, 
                        csock.m_nRecvOffset,
                        DataLength_toBeRead, 
                        SocketFlags.None, 
                        new AsyncCallback(onReceiveCallback), 
                        csock);
            }
            catch(Exception ex)
            {
                msg = string.Format("[wiki] onReceiveCallback exception = {0}", ex.StackTrace);
                Trace.WriteLine(msg);
            }

            //Trace.WriteLine("[wiki] onReceiveCallback -");
        }

        public void Dispose()
        {
            //throw new NotImplementedException();
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            CloseSock();
        }
    }
}
