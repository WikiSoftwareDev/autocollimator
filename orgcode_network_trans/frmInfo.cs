using System;
using System.ComponentModel;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    public class frmInfo : Form
    {
        private System.ComponentModel.IContainer components = null;

        private System.Windows.Forms.Button cmdFactorySetting;
        private System.Windows.Forms.Button cmdOK;
        private System.Windows.Forms.PictureBox picWikiLogo;
        private System.Windows.Forms.Label lbAboutTitle;
        private System.Windows.Forms.Label lbAboutSerial;
        private System.Windows.Forms.Label lbAboutConnect;
        private System.Windows.Forms.Label lbAboutTel;

        public frmInfo()
        {
            InitializeComponent();
        }

        private void frmInfo_Load(object sender, System.EventArgs e)
        {
            lbAboutTitle.Text = clsAboutInfo.m_szAboutTitle;
            lbAboutSerial.Text = string.Concat("Serial no. : ", clsAboutInfo.m_szAboutSerial);
            lbAboutConnect.Text = string.Concat("Contact Info. : ", clsAboutInfo.m_szAboutConnect);
            lbAboutTel.Text = string.Concat("Tel : ", clsAboutInfo.m_szAboutTel);
        }

        private void cmdOK_Click(object sender, System.EventArgs e)
        {
            Close();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.cmdFactorySetting = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.lbAboutTitle = new System.Windows.Forms.Label();
            this.lbAboutSerial = new System.Windows.Forms.Label();
            this.lbAboutConnect = new System.Windows.Forms.Label();
            this.lbAboutTel = new System.Windows.Forms.Label();
            this.picWikiLogo = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdFactorySetting
            // 
            this.cmdFactorySetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFactorySetting.Location = new System.Drawing.Point(278, 229);
            this.cmdFactorySetting.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdFactorySetting.Name = "cmdFactorySetting";
            this.cmdFactorySetting.Size = new System.Drawing.Size(121, 31);
            this.cmdFactorySetting.TabIndex = 2;
            this.cmdFactorySetting.Text = "Factory Setting";
            this.cmdFactorySetting.UseVisualStyleBackColor = true;
            this.cmdFactorySetting.Visible = false;
            // 
            // cmdOK
            // 
            this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdOK.Location = new System.Drawing.Point(141, 229);
            this.cmdOK.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(121, 31);
            this.cmdOK.TabIndex = 3;
            this.cmdOK.Text = "OK";
            this.cmdOK.UseVisualStyleBackColor = true;
            this.cmdOK.Click += new System.EventHandler(this.cmdOK_Click);
            // 
            // lbAboutTitle
            // 
            this.lbAboutTitle.AutoSize = true;
            this.lbAboutTitle.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAboutTitle.Location = new System.Drawing.Point(23, 86);
            this.lbAboutTitle.Name = "lbAboutTitle";
            this.lbAboutTitle.Size = new System.Drawing.Size(361, 18);
            this.lbAboutTitle.TabIndex = 5;
            this.lbAboutTitle.Text = "WAC-05 Laser Autocollimator Application Software";
            this.lbAboutTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAboutSerial
            // 
            this.lbAboutSerial.AutoSize = true;
            this.lbAboutSerial.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAboutSerial.Location = new System.Drawing.Point(97, 116);
            this.lbAboutSerial.Name = "lbAboutSerial";
            this.lbAboutSerial.Size = new System.Drawing.Size(213, 18);
            this.lbAboutSerial.TabIndex = 6;
            this.lbAboutSerial.Text = "Serial no. : xxxx-xxxx-xxxx-xxxx";
            this.lbAboutSerial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAboutConnect
            // 
            this.lbAboutConnect.AutoSize = true;
            this.lbAboutConnect.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAboutConnect.Location = new System.Drawing.Point(62, 146);
            this.lbAboutConnect.Name = "lbAboutConnect";
            this.lbAboutConnect.Size = new System.Drawing.Size(283, 18);
            this.lbAboutConnect.TabIndex = 7;
            this.lbAboutConnect.Text = "Conntec Info. : contact@wikitoptics.com";
            this.lbAboutConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAboutTel
            // 
            this.lbAboutTel.AutoSize = true;
            this.lbAboutTel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAboutTel.Location = new System.Drawing.Point(113, 178);
            this.lbAboutTel.Name = "lbAboutTel";
            this.lbAboutTel.Size = new System.Drawing.Size(180, 18);
            this.lbAboutTel.TabIndex = 8;
            this.lbAboutTel.Text = "Tel : 82-70-4667-4270~1";
            this.lbAboutTel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picWikiLogo
            // 
            this.picWikiLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picWikiLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.picWikiLogo.Image = global::WikiOptics_Collimator.Properties.Resources.picWikiLogo;
            this.picWikiLogo.Location = new System.Drawing.Point(101, 16);
            this.picWikiLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picWikiLogo.Name = "picWikiLogo";
            this.picWikiLogo.Size = new System.Drawing.Size(204, 52);
            this.picWikiLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picWikiLogo.TabIndex = 4;
            this.picWikiLogo.TabStop = false;
            // 
            // frmInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(411, 280);
            this.Controls.Add(this.lbAboutTel);
            this.Controls.Add(this.lbAboutConnect);
            this.Controls.Add(this.lbAboutSerial);
            this.Controls.Add(this.lbAboutTitle);
            this.Controls.Add(this.picWikiLogo);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdFactorySetting);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "About";
            this.Load += new System.EventHandler(this.frmInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
