﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using OpenCvSharp;


namespace WikiOptics_Collimator
{
    public partial class FormZeroSet : Form
    {
        public FormZeroSet()
        {
            InitializeComponent();
        }

        private void FrmZeroSet_Load(object sender, EventArgs e)
        {
            txtCenterX.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterX);
            txtCenterY.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterY);
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            CZeroSetInfo.ZeroSetCenterX = CParameter.CenterXPos;
            CZeroSetInfo.ZeroSetOriginalCenterX = CParameter.CenterXPos;
            CZeroSetInfo.ZeroSetCenterY = CParameter.CenterYPos;
            CZeroSetInfo.ZeroSetOriginalCenterY = CParameter.CenterYPos;
            txtCenterX.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterX);
            txtCenterY.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterY);
        }

        private void btn_ZeroSet_Click(object sender, EventArgs e)
        {
            CvPoint2D32f cvPoint2D32f = default(CvPoint2D32f);
            CvPoint2D32f centerPoint = new CvPoint2D32f(CParameter.CenterXPos, CParameter.CenterYPos);
            if (this.cbSpotNo.SelectedIndex < 0)
            {
                MessageBox.Show("Select No. Spots");
                return;
            }

            int selectedIndex = this.cbSpotNo.SelectedIndex;
            CZeroSetInfo.ZeroSetCenterX = CImageProcess.AnalysisResultX[selectedIndex];
            CZeroSetInfo.ZeroSetCenterY = CImageProcess.AnalysisResultY[selectedIndex];

            // Tool_Coordinate_Original           = 0;
            // Tool_Coordinate_Rotating           = 1;
            // Tool_Coordinate_Mirroring          = 2;
            switch (CSetInfo.Tool_Coordinate)
            {
                case 0:
                    CZeroSetInfo.ZeroSetOriginalCenterX = CZeroSetInfo.ZeroSetCenterX;
                    CZeroSetInfo.ZeroSetOriginalCenterY = CZeroSetInfo.ZeroSetCenterY;
                    break;
                case 1:
                    cvPoint2D32f.X = CZeroSetInfo.ZeroSetCenterX;
                    cvPoint2D32f.Y = CZeroSetInfo.ZeroSetCenterY;
                    if (CSetInfo.Tool_Coordinate_Rotating_State == 2)
                    {
                        cvPoint2D32f = CImageProcess.FuncRotatePosition(centerPoint, cvPoint2D32f, 90.0);
                        CZeroSetInfo.ZeroSetOriginalCenterX = cvPoint2D32f.X;
                        CZeroSetInfo.ZeroSetOriginalCenterY = cvPoint2D32f.Y;
                    }
                    if (CSetInfo.Tool_Coordinate_Rotating_State == 1)
                    {
                        cvPoint2D32f = CImageProcess.FuncRotatePosition(centerPoint, cvPoint2D32f, -90.0);
                        CZeroSetInfo.ZeroSetOriginalCenterX = cvPoint2D32f.X;
                        CZeroSetInfo.ZeroSetOriginalCenterY = cvPoint2D32f.Y;
                    }
                    break;
                case 2:
                    cvPoint2D32f.X = CZeroSetInfo.ZeroSetCenterX;
                    cvPoint2D32f.Y = CZeroSetInfo.ZeroSetCenterY;
                    if (CSetInfo.Tool_Coordinate_Mirroring_State == 1)
                    {
                        cvPoint2D32f = CImageProcess.FuncMirrorPosition(centerPoint, cvPoint2D32f, "X");
                        CZeroSetInfo.ZeroSetOriginalCenterX = cvPoint2D32f.X;
                        CZeroSetInfo.ZeroSetOriginalCenterY = cvPoint2D32f.Y;
                    }
                    if (CSetInfo.Tool_Coordinate_Mirroring_State == 2)
                    {
                        cvPoint2D32f = CImageProcess.FuncMirrorPosition(centerPoint, cvPoint2D32f, "Y");
                        CZeroSetInfo.ZeroSetOriginalCenterX = cvPoint2D32f.X;
                        CZeroSetInfo.ZeroSetOriginalCenterY = cvPoint2D32f.Y;
                    }
                    break;
            }
            this.txtCenterX.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterX);
            this.txtCenterY.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterY);
            CZeroSetInfo.SaveZeroSetInfo();
        }
    }
}
