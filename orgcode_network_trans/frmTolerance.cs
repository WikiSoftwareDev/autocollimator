using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
	// Token: 0x02000002 RID: 2
	public class frmTolerance : Form
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.components != null)
			{
				this.components.Dispose();
			}
			base.Dispose(disposing);
		}

		// Token: 0x06000002 RID: 2 RVA: 0x00002070 File Offset: 0x00000270
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();

            this.txtToleranceDo = new System.Windows.Forms.TextBox();
            this.btn_SetTolerance = new System.Windows.Forms.Button();
            this.txtToleranceMin = new System.Windows.Forms.TextBox();
            this.txtToleranceSec = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtToleranceDo
            // 
            this.txtToleranceDo.Location = new System.Drawing.Point(12, 18);
            this.txtToleranceDo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtToleranceDo.Name = "txtToleranceDo";
            this.txtToleranceDo.Size = new System.Drawing.Size(51, 22);
            this.txtToleranceDo.TabIndex = 1;
            this.txtToleranceDo.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // btn_SetTolerance
            // 
            this.btn_SetTolerance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_SetTolerance.Location = new System.Drawing.Point(258, 15);
            this.btn_SetTolerance.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_SetTolerance.Name = "btn_SetTolerance";
            this.btn_SetTolerance.Size = new System.Drawing.Size(62, 27);
            this.btn_SetTolerance.TabIndex = 2;
            this.btn_SetTolerance.Text = "Set";
            this.btn_SetTolerance.UseVisualStyleBackColor = true;
            this.btn_SetTolerance.Click += new System.EventHandler(this.btn_SetTolerance_Click);
            // 
            // txtToleranceMin
            // 
            this.txtToleranceMin.Location = new System.Drawing.Point(93, 18);
            this.txtToleranceMin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtToleranceMin.Name = "txtToleranceMin";
            this.txtToleranceMin.Size = new System.Drawing.Size(51, 22);
            this.txtToleranceMin.TabIndex = 3;
            this.txtToleranceMin.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtToleranceSec
            // 
            this.txtToleranceSec.Location = new System.Drawing.Point(174, 18);
            this.txtToleranceSec.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtToleranceSec.Name = "txtToleranceSec";
            this.txtToleranceSec.Size = new System.Drawing.Size(51, 22);
            this.txtToleranceSec.TabIndex = 4;
            this.txtToleranceSec.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(227, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 16);
            this.label1.TabIndex = 5;
            this.label1.Text = "\'\'";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(146, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 16);
            this.label2.TabIndex = 6;
            this.label2.Text = "\'";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(64, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 16);
            this.label3.TabIndex = 7;
            this.label3.Text = "��";
            // 
            // frmTolerance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(342, 63);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtToleranceSec);
            this.Controls.Add(this.txtToleranceMin);
            this.Controls.Add(this.btn_SetTolerance);
            this.Controls.Add(this.txtToleranceDo);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTolerance";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Tolerance Configuration";
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		// Token: 0x06000003 RID: 3 RVA: 0x000024C5 File Offset: 0x000006C5
		public frmTolerance()
		{
			this.InitializeComponent();
		}

		// Token: 0x06000004 RID: 4 RVA: 0x000024D4 File Offset: 0x000006D4
		private void btn_SetTolerance_Click(object sender, EventArgs e)
		{
			if (this.txtToleranceDo.Text == "")
			{
				this.txtToleranceDo.Text = "0";
			}
			if (!clsETC.ISNumeric(this.txtToleranceDo.Text))
			{
				MessageBox.Show("Please, Check Angle Value!!");
				return;
			}
			if (this.txtToleranceMin.Text == "")
			{
				this.txtToleranceMin.Text = "0";
			}
			if (!clsETC.ISNumeric(this.txtToleranceMin.Text))
			{
				MessageBox.Show("Please, Check Angle Value!!");
				return;
			}
			if (this.txtToleranceSec.Text == "")
			{
				this.txtToleranceSec.Text = "0";
			}
			if (!clsETC.ISNumeric(this.txtToleranceSec.Text))
			{
				MessageBox.Show("Please, Check Angle Value!!");
				return;
			}
			CSetInfo.Setting_Tolerance_Hour = (int)Convert.ToInt16(this.txtToleranceDo.Text);
			CSetInfo.Setting_Tolerance_Min = (int)Convert.ToInt16(this.txtToleranceMin.Text);
			CSetInfo.Setting_Tolerance_Sec = (int)Convert.ToInt16(this.txtToleranceSec.Text);
            CZeroSetInfo.CalToleranceDistance();
		}

		// Token: 0x06000005 RID: 5 RVA: 0x000025F4 File Offset: 0x000007F4
		private void frmTolerance_Load(object sender, EventArgs e)
		{
			this.txtToleranceDo.Text = CSetInfo.Setting_Tolerance_Hour.ToString();
			this.txtToleranceMin.Text = CSetInfo.Setting_Tolerance_Min.ToString();
			this.txtToleranceSec.Text = CSetInfo.Setting_Tolerance_Sec.ToString();
		}

		// Token: 0x04000001 RID: 1
		private IContainer components = null;

		// Token: 0x04000002 RID: 2
		private TextBox txtToleranceDo;

		// Token: 0x04000003 RID: 3
		private Button btn_SetTolerance;

		// Token: 0x04000004 RID: 4
		private TextBox txtToleranceMin;

		// Token: 0x04000005 RID: 5
		private TextBox txtToleranceSec;

		// Token: 0x04000006 RID: 6
		private Label label1;

		// Token: 0x04000007 RID: 7
		private Label label2;

		// Token: 0x04000008 RID: 8
		private Label label3;
	}
}
