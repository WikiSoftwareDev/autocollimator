﻿using System;
using System.Text;

namespace WikiOptics_Collimator
{
    internal class CTriDiagonalMatrixCal
    {
        public float[] A;
        public float[] B;
        public float[] C;

        public int N
        {
            get
            {
                if (this.A == null)
                {
                    return 0;
                }
                return this.A.Length;
            }
        }

        public float this[int row, int col]
        {
            get
            {
                int num = row - col;
                if (num == 0)
                {
                    return this.B[row];
                }
                if (num == -1)
                {
                    return this.C[row];
                }
                if (num == 1)
                {
                    return this.A[row];
                }
                return 0f;
            }
            set
            {
                int num = row - col;
                if (num == 0)
                {
                    this.B[row] = value;
                    return;
                }
                if (num == -1)
                {
                    this.C[row] = value;
                    return;
                }
                if (num == 1)
                {
                    this.A[row] = value;
                    return;
                }
                throw new ArgumentException("Only the main, super, and sub diagonals can be set.");
            }
        }

        public CTriDiagonalMatrixCal(int n)
        {
            this.A = new float[n];
            this.B = new float[n];
            this.C = new float[n];
        }

        public string ToDisplayString(string fmt = "", string prefix = "")
        {
            if (this.N > 0)
            {
                StringBuilder stringBuilder = new StringBuilder();
                string format = "{0" + fmt + "}";
                for (int i = 0; i < this.N; i++)
                {
                    stringBuilder.Append(prefix);
                    for (int j = 0; j < this.N; j++)
                    {
                        stringBuilder.AppendFormat(format, this[i, j]);
                        if (j < this.N - 1)
                        {
                            stringBuilder.Append(", ");
                        }
                    }
                    stringBuilder.AppendLine();
                }
                return stringBuilder.ToString();
            }
            return prefix + "0x0 Matrix";
        }

        public float[] Solve(float[] d)
        {
            int n = this.N;
            if (d.Length != n)
            {
                throw new ArgumentException("The input d is not the same size as this matrix.");
            }
            float[] array = new float[n];
            array[0] = this.C[0] / this.B[0];
            for (int i = 1; i < n; i++)
            {
                array[i] = this.C[i] / (this.B[i] - array[i - 1] * this.A[i]);
            }
            float[] array2 = new float[n];
            array2[0] = d[0] / this.B[0];
            for (int j = 1; j < n; j++)
            {
                array2[j] = (d[j] - array2[j - 1] * this.A[j]) / (this.B[j] - array[j - 1] * this.A[j]);
            }
            float[] array3 = new float[n];
            array3[n - 1] = array2[n - 1];
            for (int k = n - 2; k >= 0; k--)
            {
                array3[k] = array2[k] - array[k] * array3[k + 1];
            }
            return array3;
        }
    }
}
