﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace WikiOptics_Collimator
{
    internal class CUtil
    {
        // ini 파일 안의 모든 section 이름 가져오기
        public static string[] GetSectionNames(string path)
        {
            uint MAX_BUFFER = 32767;
            IntPtr pReturnedString = Marshal.AllocCoTaskMem((int)MAX_BUFFER);
            uint bytesReturned = NativeMethods.GetPrivateProfileSectionNames(pReturnedString, MAX_BUFFER, path);
            if (bytesReturned == 0)
                return null;

            //string result = Marshal.PtrToStringAnsi(pReturnedString, (int)(bytesReturned * 2));
            string result = Marshal.PtrToStringUni(pReturnedString, (int)(bytesReturned * 2));
            Marshal.FreeCoTaskMem(pReturnedString);

            int i = 0;
            int len = result.Length;
            for (i = 0; i < len - 2; i++)
            {
                if ((result[i] == '\0') && (result[i + 1] == '\0'))
                    break;
            }

            string ret = result.Substring(0, i+1);
            string[] sections = ret.Split(new char[] { '\0' }, StringSplitOptions.RemoveEmptyEntries);            
            return sections;
        }

        // 해당 section 안의 모든 키 값 가져오기
        public static string[] GetEntryNames(string path, string section)
        {
            //for (int maxsize = 500; true; maxsize *= 2)
            {
                int maxsize = 1024 * 10;
                StringBuilder section_names = new StringBuilder(maxsize);
                int size = NativeMethods.GetPrivateProfileString(section, null, "", section_names, maxsize, path);
                if (size < maxsize - 2)
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(section_names.ToString());
                    string entries = Encoding.ASCII.GetString(bytes, 0, size - (size > 0 ? 1 : 0));
                    return entries.Split(new char[] { '\0' });
                }
            }
            return null;
        }
    }
}
