﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;

namespace WikiOptics_Collimator
{
    public static class CExtBitmap
    {
        public static Bitmap CopyToSquareCanvas(this Bitmap sourceBitmap, 
            int canvasWidthLength, 
            int canvasHeightLength)
        {
            //if (sourceBitmap.Width <= sourceBitmap.Height)
            //{
            //    int height = sourceBitmap.Height;
            //}
            //else
            //{
            //    int width = sourceBitmap.Width;
            //}

            float num = (float)sourceBitmap.Width / (float)canvasWidthLength;
            float num2 = (float)sourceBitmap.Height / (float)canvasHeightLength;
            Bitmap bitmap = new Bitmap(canvasWidthLength, canvasHeightLength);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
                graphics.DrawImage(sourceBitmap, new Rectangle(0, 0, bitmap.Width, bitmap.Height), new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), GraphicsUnit.Pixel);
                graphics.Flush();
                return bitmap;
            }
        }

        public static Bitmap ConvolutionFilter(this Bitmap sourceBitmap, double[,] filterMatrix, double factor = 1.0, int bias = 0)
        {
            BitmapData bitmapData = sourceBitmap.LockBits(new Rectangle(0, 0, sourceBitmap.Width, sourceBitmap.Height), ImageLockMode.ReadOnly, PixelFormat.Format32bppArgb);
            byte[] array = new byte[bitmapData.Stride * bitmapData.Height];
            byte[] array2 = new byte[bitmapData.Stride * bitmapData.Height];
            Marshal.Copy(bitmapData.Scan0, array, 0, array.Length);
            sourceBitmap.UnlockBits(bitmapData);
            double num = 0.0;
            double num2 = 0.0;
            double num3 = 0.0;
            int length = filterMatrix.GetLength(1);
            filterMatrix.GetLength(0);
            int num4 = (length - 1) / 2;
            int num5 = 0;
            int num6 = 0;
            for (int i = num4; i < sourceBitmap.Height - num4; i++)
            {
                for (int j = num4; j < sourceBitmap.Width - num4; j++)
                {
                    num = 0.0;
                    num2 = 0.0;
                    num3 = 0.0;
                    num6 = i * bitmapData.Stride + j * 4;
                    for (int k = -num4; k <= num4; k++)
                    {
                        for (int l = -num4; l <= num4; l++)
                        {
                            num5 = num6 + l * 4 + k * bitmapData.Stride;
                            num += (double)(int)array[num5] * filterMatrix[k + num4, l + num4];
                            num2 += (double)(int)array[num5 + 1] * filterMatrix[k + num4, l + num4];
                            num3 += (double)(int)array[num5 + 2] * filterMatrix[k + num4, l + num4];
                        }
                    }
                    num = factor * num + (double)bias;
                    num2 = factor * num2 + (double)bias;
                    num3 = factor * num3 + (double)bias;
                    num = ((num > 255.0) ? 255.0 : ((num < 0.0) ? 0.0 : num));
                    num2 = ((num2 > 255.0) ? 255.0 : ((num2 < 0.0) ? 0.0 : num2));
                    num3 = ((num3 > 255.0) ? 255.0 : ((num3 < 0.0) ? 0.0 : num3));
                    array2[num6] = (byte)num;
                    array2[num6 + 1] = (byte)num2;
                    array2[num6 + 2] = (byte)num3;
                    array2[num6 + 3] = byte.MaxValue;
                }
            }
            Bitmap bitmap = new Bitmap(sourceBitmap.Width, sourceBitmap.Height);
            BitmapData bitmapData2 = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
            Marshal.Copy(array2, 0, bitmapData2.Scan0, array2.Length);
            bitmap.UnlockBits(bitmapData2);
            return bitmap;
        }
    }
}

