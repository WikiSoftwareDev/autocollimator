// WikiAngleAutoCollimator.clsMeasureData
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using WikiOptics_Collimator;

namespace WikiOptics_Collimator
{
    internal class clsMeasureData
    {
	    private static string[] strToolMode = new string[3] { "Area Center", "Brightness Center", "Peak Center" };

	    private static string[] strSettingUnit = new string[2] { "Degree", "Min+Sec" };

	    private static string[] strSnapStatus = new string[2] { "NG", "OK" };

	    public static int MeasureSpotNo = 0;

	    public static int MeasureDelayTime = 0;

	    public static bool MeasureDataFlag = false;

	    public static int nMeasureDataCount;

	    public static int SaveDataCurrentNum;

	    public static int[] MeasureDataSpotNo;

	    public static double[,] MeasureDataXAngle;

	    public static double[,] MeasureDataYAngle;

	    public static double[,] MeasureDataXYAngle;

	    public static double[,] MeasureDataXHour;

	    public static double[,] MeasureDataXMin;

	    public static double[,] MeasureDataXSec;

	    public static double[,] MeasureDataYHour;

	    public static double[,] MeasureDataYMin;

	    public static double[,] MeasureDataYSec;

	    public static double[,] MeasureDataXYHour;

	    public static double[,] MeasureDataXYMin;

	    public static double[,] MeasureDataXYSec;

	    public static double[] MeasureDataSpot1to2Angle;

	    public static double[] MeasureDataSpot2to3Angle;

	    public static double[] MeasureDataSpot3to1Angle;

	    public static double[] MeasureDataSpot1to2Hour;

	    public static double[] MeasureDataSpot1to2Min;

	    public static double[] MeasureDataSpot1to2Sec;

	    public static double[] MeasureDataSpot2to3Hour;

	    public static double[] MeasureDataSpot2to3Min;

	    public static double[] MeasureDataSpot2to3Sec;

	    public static double[] MeasureDataSpot3to1Hour;

	    public static double[] MeasureDataSpot3to1Min;

	    public static double[] MeasureDataSpot3to1Sec;

	    public static string SaveMeasureDataFileName;

	    public static string SaveSnapMeasureDataFileName = null;

	    public static bool SnapMeasureFlag = false;

	    public static string SnapPartName = null;

	    public static int SnapStartNumber = -1001001;

	    public static int nSnapMeasureDataCount = 0;

	    public static int SnapMeasureSpotNo = -1;

	    public static string[] SnapMeasureDataXAngle;

	    public static string[] SnapMeasureDataYAngle;

	    public static string[] SnapMeasureDataXYAngle;

	    public static string[] SnapMeasureDataSpot1to2Angle;

	    public static int[] SnapMeasureDataStatus;

	    public static long LimitMeausreCount = -1;

        const string m_szMeasureSectionName = "MEASURE_COUNT";
        const string m_szKey_MeasureCount = "MeasureCount";

        static void SaveDefaultMeasureInfo(string filepath)
        {
            string defValue;

            defValue = "10000";
            NativeMethods.WritePrivateProfileString(
                m_szMeasureSectionName,
                m_szKey_MeasureCount,
                defValue,
                filepath);
        }

        public static void LoadMeasureInfo()
	    {
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
            bool bExist = System.IO.File.Exists(filepath);
            if (false == bExist)
            {
                SaveDefaultMeasureInfo(filepath);
            }
            else
            {
                bExist = false;
                string[] sec_names = CUtil.GetSectionNames(filepath);
                int num_sections = sec_names.Length;
                for (int i = 0; i < num_sections; i++)
                {
                    bExist = sec_names[i].Equals(m_szMeasureSectionName);
                    if (bExist == true)
                        break;
                }
                if (false == bExist)
                {
                    SaveDefaultMeasureInfo(filepath);
                }
            }

            string defValue;
            StringBuilder retVal1 = new StringBuilder(128);
            int nSize = 128;
            int result1 = 0;
            long temp = 0;

            defValue = "10000";
            result1 = NativeMethods.GetPrivateProfileString(
                m_szMeasureSectionName,
                m_szKey_MeasureCount,
                defValue,
                retVal1,
                nSize,
                filepath);
            if (true == long.TryParse(retVal1.ToString(), out temp))
            {
                LimitMeausreCount = temp;
            }
            InitalMeasureDataNum();
        }

	    public static void InitalMeasureDataNum()
	    {
		    MeasureDataSpotNo = new int[LimitMeausreCount];
		    MeasureDataXAngle = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataYAngle = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXYAngle = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXHour = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXMin = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXSec = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataYHour = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataYMin = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataYSec = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXYHour = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXYMin = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataXYSec = new double[3, (int)checked((int)LimitMeausreCount)];
		    MeasureDataSpot1to2Angle = new double[LimitMeausreCount];
		    MeasureDataSpot2to3Angle = new double[LimitMeausreCount];
		    MeasureDataSpot3to1Angle = new double[LimitMeausreCount];
		    MeasureDataSpot1to2Hour = new double[LimitMeausreCount];
		    MeasureDataSpot1to2Min = new double[LimitMeausreCount];
		    MeasureDataSpot1to2Sec = new double[LimitMeausreCount];
		    MeasureDataSpot2to3Hour = new double[LimitMeausreCount];
		    MeasureDataSpot2to3Min = new double[LimitMeausreCount];
		    MeasureDataSpot2to3Sec = new double[LimitMeausreCount];
		    MeasureDataSpot3to1Hour = new double[LimitMeausreCount];
		    MeasureDataSpot3to1Min = new double[LimitMeausreCount];
		    MeasureDataSpot3to1Sec = new double[LimitMeausreCount];
		    SnapMeasureDataXAngle = new string[LimitMeausreCount];
		    SnapMeasureDataYAngle = new string[LimitMeausreCount];
		    SnapMeasureDataXYAngle = new string[LimitMeausreCount];
		    SnapMeasureDataSpot1to2Angle = new string[LimitMeausreCount];
		    SnapMeasureDataStatus = new int[LimitMeausreCount];
	    }

	    public static void UpdateMeasureData()
	    {
            if (nMeasureDataCount >= LimitMeausreCount)
                return;

            MeasureDataSpotNo[nMeasureDataCount] = CImageProcess.SearchSpotNumber;
		    for (int i = 0; i < 3; i++)
		    {
			    MeasureDataXAngle[i, nMeasureDataCount] = CImageProcess.AngleX[i];
			    MeasureDataYAngle[i, nMeasureDataCount] = CImageProcess.AngleY[i];
			    MeasureDataXYAngle[i, nMeasureDataCount] = CImageProcess.AngleD[i];
			    MeasureDataXHour[i, nMeasureDataCount] = CImageProcess.AngleHourX[i];
			    MeasureDataXMin[i, nMeasureDataCount] = CImageProcess.AngleMinuteX[i];
			    MeasureDataXSec[i, nMeasureDataCount] = CImageProcess.AngleSecondX[i];
			    MeasureDataYHour[i, nMeasureDataCount] = CImageProcess.AngleHourY[i];
			    MeasureDataYMin[i, nMeasureDataCount] = CImageProcess.AngleMinuteY[i];
			    MeasureDataYSec[i, nMeasureDataCount] = CImageProcess.AngleSecondY[i];
			    MeasureDataXYHour[i, nMeasureDataCount] = CImageProcess.AngleHourXY[i];
			    MeasureDataXYMin[i, nMeasureDataCount] = CImageProcess.AngleMinuteXY[i];
			    MeasureDataXYSec[i, nMeasureDataCount] = CImageProcess.AngleSecondXY[i];
		    }
		    MeasureDataSpot1to2Angle[nMeasureDataCount] = CImageProcess.Angle1to2;
		    MeasureDataSpot2to3Angle[nMeasureDataCount] = CImageProcess.Angle2to3;
		    MeasureDataSpot3to1Angle[nMeasureDataCount] = CImageProcess.Angle3to1;
		    MeasureDataSpot1to2Hour[nMeasureDataCount] = CImageProcess.Angle1to2HourXY;
		    MeasureDataSpot1to2Min[nMeasureDataCount] = CImageProcess.Angle1to2MinuteXY;
		    MeasureDataSpot1to2Sec[nMeasureDataCount] = CImageProcess.Angle1to2SecondXY;
		    MeasureDataSpot2to3Hour[nMeasureDataCount] = CImageProcess.Angle2to3HourXY;
		    MeasureDataSpot2to3Min[nMeasureDataCount] = CImageProcess.Angle2to3MinuteXY;
		    MeasureDataSpot2to3Sec[nMeasureDataCount] = CImageProcess.Angle2to3SecondXY;
		    MeasureDataSpot3to1Hour[nMeasureDataCount] = CImageProcess.Angle3to1HourXY;
		    MeasureDataSpot3to1Min[nMeasureDataCount] = CImageProcess.Angle3to1MinuteXY;
		    MeasureDataSpot3to1Sec[nMeasureDataCount] = CImageProcess.Angle3to1SecondXY;
		    nMeasureDataCount++;
	    }

	    public static bool SaveMeasureData() //SAVE 파일 정보
        {
		    string text = "";

		    //text = text + ",Product,,WAC-05 WikiAngle AutoCollimator" + Environment.NewLine;
            text = text + ",Product,," + clsAboutInfo.m_szAboutTitle + Environment.NewLine;

		    text = text + ",MeasureMent,,Live View Measurement" + Environment.NewLine;
		    text = text + ",MeasureMent Mode,," + strToolMode[CSetInfo.Tool_Mode - 1] + Environment.NewLine;
		    text = text + ",Unit,," + strSettingUnit[CSetInfo.Setting_Unit - 1] + Environment.NewLine;
		    object obj = text;
		    text = string.Concat(obj, ",Spot No,,", MeasureSpotNo, Environment.NewLine);
		    object obj2 = text;
		    text = string.Concat(obj2, ",Sampling Freq,,", MeasureDelayTime, "ms", Environment.NewLine);
		    text = text + ",Measurement Data" + Environment.NewLine;
		    text = text + ",Spot1,,,Spot2,,,Spot3,,,Spot 1-2,Spot 2-3,Spot 3-1" + Environment.NewLine;
		    text = text + "No,x,y,x-y,x,y,x-y,x,y,x-y" + Environment.NewLine;
		    for (int i = 0; i < nMeasureDataCount; i++)
		    {
			    SaveDataCurrentNum = i;
			    text = text + (i + 1) + ",";
			    for (int j = 0; j < 3; j++)
			    {
				    if (j > MeasureDataSpotNo[i] - 1)
				    {
					    text += "-,-,-,";
					    continue;
				    }
				    if (CSetInfo.Setting_Unit == 1)
				    {
					    object obj3 = text;
					    text = string.Concat(obj3, MeasureDataXAngle[j, i], ",", MeasureDataYAngle[j, i], ",", MeasureDataXYAngle[j, i], ",");
					    continue;
				    }
				    if (MeasureDataXAngle[j, i] < 0.0)
				    {
					    string text2 = text;
					    text = text2 + "-" + MeasureDataXHour[j, i] + " ° " + MeasureDataXMin[j, i] + " '" + $"{MeasureDataXSec[j, i]:f0}" + " '',";
				    }
				    else
				    {
					    string text3 = text;
					    text = text3 + MeasureDataXHour[j, i] + " ° " + MeasureDataXMin[j, i] + " '" + $"{MeasureDataXSec[j, i]:f0}" + " '',";
				    }
				    if (MeasureDataYAngle[j, i] < 0.0)
				    {
					    string text4 = text;
					    text = text4 + "-" + MeasureDataYHour[j, i] + " ° " + MeasureDataYMin[j, i] + " '" + $"{MeasureDataYSec[j, i]:f0}" + " '',";
				    }
				    else
				    {
					    string text5 = text;
					    text = text5 + MeasureDataYHour[j, i] + " ° " + MeasureDataYMin[j, i] + " '" + $"{MeasureDataYSec[j, i]:f0}" + " '',";
				    }
				    string text6 = text;
				    text = text6 + MeasureDataXYHour[j, i] + " ° " + MeasureDataXYMin[j, i] + " '" + $"{MeasureDataXYSec[j, i]:f0}" + " '',";
			    }
			    switch (MeasureDataSpotNo[i])
			    {
			    case 1:
				    text += "-,-,-";
				    break;
			    case 2:
			    {
				    if (CSetInfo.Setting_Unit == 1)
				    {
					    text = text + MeasureDataSpot1to2Angle[i] + ",-,-";
					    break;
				    }
				    string text10 = text;
				    text = text10 + MeasureDataSpot1to2Hour[i] + " ° " + MeasureDataSpot1to2Min[i] + " '" + $"{MeasureDataSpot1to2Sec[i]:f0}" + " '',-,-";
				    break;
			    }
			    case 3:
			    {
				    if (CSetInfo.Setting_Unit == 1)
				    {
					    object obj4 = text;
					    text = string.Concat(obj4, MeasureDataSpot1to2Angle[i], ",", MeasureDataSpot2to3Angle[i], ",", MeasureDataSpot3to1Angle[i]);
					    break;
				    }
				    string text7 = text;
				    text = text7 + MeasureDataSpot1to2Hour[i] + " ° " + MeasureDataSpot1to2Min[i] + " '" + $"{MeasureDataSpot1to2Sec[i]:f0}" + " '',";
				    string text8 = text;
				    text = text8 + MeasureDataSpot2to3Hour[i] + " ° " + MeasureDataSpot2to3Min[i] + " '" + $"{MeasureDataSpot2to3Sec[i]:f0}" + " '',";
				    string text9 = text;
				    text = text9 + MeasureDataSpot3to1Hour[i] + " ° " + MeasureDataSpot3to1Min[i] + " '" + $"{MeasureDataSpot3to1Sec[i]:f0}" + " ''";
				    break;
			    }
			    }
			    text += Environment.NewLine;
		    }
		    if (!clsETC.CSVFuncFileSave(SaveMeasureDataFileName, text))
		    {
			    return false;
		    }
		    return true;
	    }

	    public static bool SaveSnapMeasureData() //EVENT SAVE 파일 정보
	    {
		    string text = "";

            //사용중이 제품에 대한정보
            //text = text + ",Product,,WAC-05 WikiAngle AutoCollimator" + Environment.NewLine;
            text = text + ",Product,," + clsAboutInfo.m_szAboutTitle + Environment.NewLine;
            // 수행중인 축정 유형에 대한 정보
            text = text + ",MeasureMent,,Serial Measurement" + Environment.NewLine;
		
            // 사용중인 측정 모드에 대한 정보
            text = text + ",MeasureMent Mode,," + strToolMode[CSetInfo.Tool_Mode - 1] + Environment.NewLine;

            // 사용중인 장치에 대한 정보
		    text = text + ",Unit,," + strSettingUnit[CSetInfo.Setting_Unit - 1] + Environment.NewLine;
		    object obj = text;

		    //현재 스팟 번호
            text = string.Concat(obj, ",Spot No,,", SnapMeasureSpotNo + 1, Environment.NewLine);
		    text = text + ",Measurement Data" + Environment.NewLine;                                    
		    text = text + ",Pare Name,," + SnapPartName + Environment.NewLine;
		    text += "No,";
		    text += "X,Y,X-Y,";
		    text = text + "RE.,STATUS" + Environment.NewLine;
		    for (int i = 0; i < nSnapMeasureDataCount; i++)
		    {
			    Application.DoEvents();
			    text = text + (i + SnapStartNumber) + ",";
			    string text2 = text;
			    text = text2 + SnapMeasureDataXAngle[i] + "," + SnapMeasureDataYAngle[i] + "," + SnapMeasureDataXYAngle[i] + ",";
			    if (CImageProcess.SearchSpotNumber > 1)
			    {
				    string text3 = text;
				    text = text3 + SnapMeasureDataSpot1to2Angle[i] + "," + strSnapStatus[SnapMeasureDataStatus[i]] + Environment.NewLine;
			    }
			    else
			    {
				    text = text + "-," + strSnapStatus[SnapMeasureDataStatus[i]] + Environment.NewLine;
			    }
		    }

		    if (!clsETC.CSVFuncFileSave(SaveSnapMeasureDataFileName, text))
		    {
			    return false;
		    }
		    return true;
	    }
    }
}