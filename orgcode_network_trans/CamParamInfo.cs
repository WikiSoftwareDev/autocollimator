﻿using System.Text;
using System.Windows.Forms;
//ParamInfo Save 파일
namespace WikiOptics_Collimator
{
    internal class CamParamInfo
    {
        #region

        private static string   strAbsoluteMode = "ON";

        public static bool      AbsoluteMode = true;
        public static uint      Brightness = 0;
        public static float     Exposure = -2.585f;
        public static uint      Sharpness = 1270;
        public static uint      Hue = 0;
        public static uint      Saturation = 0;
        public static float     Gamma = 1.7f;
        public static float     ShutterSpeed = 20f;
        public static float     Gain = 15;
        public static uint      FrameRate = 40;
        public static uint      WhiteBalanceRed = 0;
        public static uint      WhiteBalanceBlue = 0;
        public static string    Power = "ON";
        public static string    PixelFormat = "Mono8";
        public static uint      ImageLeft = 128;
        public static uint      ImageTop = 0;
        public static uint      ImageWidth = 1024;
        public static uint      ImageHeight = 1024;
        public static uint      PacketSize = 6560;
      

        const string m_szSectionName           = "CAMERAINFO_SECTION";
        const string m_szKey_AbsoluteMode      = "AbsoluteMode(0(OFF),1(ON))";
        const string m_szKey_Brightness        = "Brightness(0)";
        const string m_szKey_Exposure          = "Exposure(-2.585)";
        const string m_szKey_Sharpness         = "Sharpness(1270)";
        const string m_szKey_Gamma             = "Gamma(1.7)";
        const string m_szKey_ShutterSpeed      = "ShutterSpeed(20)";
        const string m_szKey_Gain              = "Gain(15)";
        const string m_szKey_FPS               = "FrameRate(40)";
        const string m_szKey_Image_Left_Offset = "Image_Left_Offset(128)";
        const string m_szKey_Image_Top_Offset  = "Image_Top_Offset(0)";
        const string m_szKey_Image_Width       = "ImageWidth(1024)";
        const string m_szKey_Image_Height      = "ImageHeight(1024)";

        #endregion

        public CamParamInfo()
        {
        }

        static void SaveDefaultCameraInfo(string filepath)
        {
            bool writeResult = false;
            string defValue;

            defValue = "1";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_AbsoluteMode, defValue, filepath);

            defValue = "0";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Brightness, defValue, filepath);

            defValue = "-2.585";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Exposure, defValue, filepath);

            defValue = "1270";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Sharpness, defValue, filepath);

            defValue = "1.7";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Gamma, defValue, filepath);

            defValue = "20";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ShutterSpeed, defValue, filepath);

            defValue = "15";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Gain, defValue, filepath);

            defValue = "40";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_FPS, defValue, filepath);

            defValue = "128";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Left_Offset, defValue, filepath);

            defValue = "0";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Top_Offset, defValue, filepath);

            defValue = "1024";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Width, defValue, filepath);

            defValue = "1024";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Height, defValue, filepath);
        }

        public static void LoadCameraInfo()
        {
            try
            {
                // 현재 프로그램 실행 위치
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
                bool bExist = System.IO.File.Exists(filepath);
                if (false == bExist)
                {
                    SaveDefaultCameraInfo(filepath);
                }
                else
                {
                    bExist = false;
                    string[] sec_names = CUtil.GetSectionNames(filepath);
                    int num_sections = sec_names.Length;
                    for (int i = 0; i < num_sections; i++)
                    {
                        bExist = sec_names[i].Equals(m_szSectionName);
                        if (bExist == true)
                            break;
                    }
                    if (false == bExist)
                    {
                        SaveDefaultCameraInfo(filepath);
                    }
                }

                string defValue;
                StringBuilder retVal1 = new StringBuilder(128);
                int nSize = 128;
                int result1 = 0;
                uint result2 = 0;
                float temp2 = 0;
               

                defValue = "1";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_AbsoluteMode, defValue, retVal1, nSize, filepath);
                if (true == int.TryParse(retVal1.ToString(), out result1))
                {
                    AbsoluteMode = result1 == 1 ? true : false;
                }

                defValue = "0";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Brightness, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    Brightness = result2;
                }

                defValue = "-2.585";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Exposure, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    Exposure = temp2;
                }

                defValue = "1270";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Sharpness, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    Sharpness = result2;
                }

                defValue = "1.7";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Gamma, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    Gamma = temp2;
                }

                defValue = "20";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_ShutterSpeed, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    ShutterSpeed = temp2;
                }

                defValue = "15";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Gain, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    Gain = temp2;
                }

                defValue = "40";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_FPS, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    FrameRate = result2;
                }

                defValue = "128";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Image_Left_Offset, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    ImageLeft = result2;
                }

                defValue = "0";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Image_Top_Offset, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    ImageTop = result2;
                }

                defValue = "1024";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Image_Width, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    ImageWidth = result2;
                }

                defValue = "1024";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_Image_Height, defValue, retVal1, nSize, filepath);
                if (true == uint.TryParse(retVal1.ToString(), out result2))
                {
                    ImageHeight = result2;
                }
                if (strAbsoluteMode == "OFF")
                {
                    AbsoluteMode = false;
                }
                else
                {
                    AbsoluteMode = true;
                }
            }
            catch
            {
                MessageBox.Show(new Form
                {
                    TopMost = true
                }, "Camera Info. FILE LOAD FAIL");
            }
        }

        public static void SaveCameraInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";

            bool writeResult = false;
            string value;

            value = AbsoluteMode == true ? "1" : "0";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_AbsoluteMode, value, filepath);

            value = string.Format("{0}", Brightness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Brightness, value, filepath);

            value = string.Format("{0:#.#####}", Exposure);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Exposure, value, filepath);

            value = string.Format("{0}", Sharpness);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Sharpness, value, filepath);

            value = string.Format("{0:#.#####}", Gamma);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Gamma, value, filepath);

            value = string.Format("{0:#.#####}", ShutterSpeed);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ShutterSpeed, value, filepath);

            value = string.Format("{0:#.#####}", Gain);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Gain, value, filepath);

            value = string.Format("{0}", FrameRate);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_FPS, value, filepath);

            value = string.Format("{0}", ImageLeft);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Left_Offset, value, filepath);

            value = string.Format("{0}", ImageTop);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Top_Offset, value, filepath);

            value = string.Format("{0}", ImageWidth);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Width, value, filepath);

            value = string.Format("{0}", ImageHeight);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_Image_Height, value, filepath);
        }
    }
}
