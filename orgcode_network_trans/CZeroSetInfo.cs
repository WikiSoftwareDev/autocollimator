﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    internal class CZeroSetInfo
    {
        public static double    ToleranceDistance;
        public static float     ZeroSetCenterX = 150.6918f;
        public static float     ZeroSetCenterY = 406.5571f;
        public static float     ZeroSetOriginalCenterX;
        public static float     ZeroSetOriginalCenterY;
        public static bool      ZeroSetResetFlag = false;

        const string m_szZeroSectionName   = "ZeroSetInfo";
        const string m_szKey_ZeroX         = "CenterX";
        const string m_szKey_ZeroY         = "CenterY";

        public CZeroSetInfo()
        {
        }

        static void SaveDefaultZeroSetInfo(string filepath)
        {
            string defValue;

            defValue = "150.6918";
            NativeMethods.WritePrivateProfileString(
                m_szZeroSectionName, 
                m_szKey_ZeroX, 
                defValue, 
                filepath);

            defValue = "406.5571";
            NativeMethods.WritePrivateProfileString(
                m_szZeroSectionName, 
                m_szKey_ZeroY, 
                defValue, 
                filepath);
        }

        public static void LoadZeroSetInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
            bool bExist = System.IO.File.Exists(filepath);
            if (false == bExist)
            {
                SaveDefaultZeroSetInfo(filepath);
            }
            else
            {
                bExist = false;
                string[] sec_names = CUtil.GetSectionNames(filepath);
                int num_sections = sec_names.Length;
                for (int i = 0; i < num_sections; i++)
                {
                    bExist = sec_names[i].Equals(m_szZeroSectionName);
                    if (bExist == true)
                        break;
                }
                if (false == bExist)
                {
                    SaveDefaultZeroSetInfo(filepath);
                }
            }

            string defValue;
            StringBuilder retVal1 = new StringBuilder(128);
            int nSize = 128;
            int result1 = 0;
            float temp = 0;
            float temp2 = 0;

            defValue = "150.6918";
            result1 = NativeMethods.GetPrivateProfileString(
                m_szZeroSectionName,
                m_szKey_ZeroX,
                defValue,
                retVal1,
                nSize,
                filepath);
            if (true == float.TryParse(retVal1.ToString(), out temp))
            {
                ZeroSetCenterX = temp;
            }

            defValue = "406.5571";
            result1 = NativeMethods.GetPrivateProfileString(
                m_szZeroSectionName,
                m_szKey_ZeroY,
                defValue,
                retVal1,
                nSize,
                filepath);
            if (true == float.TryParse(retVal1.ToString(), out temp2))
            {
                ZeroSetCenterY = temp2;
            }
            ZeroSetOriginalCenterX = ZeroSetCenterX;
            ZeroSetOriginalCenterY = ZeroSetCenterY;

			CalToleranceDistance();
        }
        
        public static void SaveZeroSetInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
            string value;

            value = string.Format("{0:#.#####}", ZeroSetCenterX);
            NativeMethods.WritePrivateProfileString(
                m_szZeroSectionName, 
                m_szKey_ZeroX, 
                value, 
                filepath);

            value = string.Format("{0:#.#####}", ZeroSetCenterY);
            NativeMethods.WritePrivateProfileString(
                m_szZeroSectionName,
                m_szKey_ZeroY,
                value,
                filepath);
            CalToleranceDistance();
        }

        public static void CalToleranceDistance()
        {
            double num = (double)CSetInfo.Setting_Tolerance_Hour + (double)CSetInfo.Setting_Tolerance_Min / 60.0 + (double)CSetInfo.Setting_Tolerance_Sec / 3600.0;
            double num2 = Math.Tan(num * 2.0 / (double)CSetInfo.Tool_Angle_Definition_Value * Math.PI / 180.0) * CParameter.FocalLength / CParameter.PixelPerDistance;
            double num3 = num2 / (Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0));
            double toleranceDistance = num3;
            ToleranceDistance = toleranceDistance; //원그려주는 반지름 값
        }

        public static void ModeChangeToleranceDistance()
        {
            double num = (double)CSetInfo.Setting_Tolerance_Hour + (double)CSetInfo.Setting_Tolerance_Min / 60.0 + (double)CSetInfo.Setting_Tolerance_Sec / 3600.0;
            if (CSetInfo.Tool_Angle_Definition == 1)
            {
                num /= 2.0;
            }
            else
            {
                num *= 2.0;
            }
            CSetInfo.Setting_Tolerance_Hour = (int)num;
            CSetInfo.Setting_Tolerance_Min = Math.Abs((int)(Math.Abs(num - (double)CSetInfo.Setting_Tolerance_Hour) * 60.0));
            CSetInfo.Setting_Tolerance_Sec = (int)Math.Truncate(Math.Abs(Math.Abs(Math.Abs(num - (double)CSetInfo.Setting_Tolerance_Hour) * 60.0 - (double)CSetInfo.Setting_Tolerance_Min) * 60.0));
        }
    }
}

