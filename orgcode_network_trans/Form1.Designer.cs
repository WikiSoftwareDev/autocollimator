﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            m_SerialOut.Dispose();
            m_socket_server.Dispose();
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.menu_Measurement = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_ZeroSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Area = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Brightness = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Peak = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angledefinition = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angle_Ext = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angle_Int = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Xon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Yon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_L90 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_R90 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Original = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbering = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbring_Angle = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbring_Size = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_8 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_16 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_32 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_Unit = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Unit_Degree = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Unit_Sec = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_Tolerance = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Circle = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Square = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Value = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_GrayLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Information = new System.Windows.Forms.ToolStripMenuItem();
            this.picWikiLogo = new System.Windows.Forms.PictureBox();
            this.panel_Icon = new System.Windows.Forms.Panel();
            this.comboBox_serialport = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.InstallSerialDriver = new System.Windows.Forms.Button();
            this.AutoShutter = new System.Windows.Forms.Button();
            this.SaveAllConfig = new System.Windows.Forms.Button();
            this.cmdFactorySet = new System.Windows.Forms.Button();
            this.cmdAnalysis = new System.Windows.Forms.Button();
            this.btn_Capture = new System.Windows.Forms.Button();
            this.cmdClear = new System.Windows.Forms.Button();
            this.cmdPrint = new System.Windows.Forms.Button();
            this.cmdExport = new System.Windows.Forms.Button();
            this.btn_SettingFileSave = new System.Windows.Forms.Button();
            this.btn_SettingFileOpen = new System.Windows.Forms.Button();
            this.panel_Set = new System.Windows.Forms.Panel();
            this.txtSamplingFreq = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cmdMeasureData = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel_Status = new System.Windows.Forms.Panel();
            this.lbStatusDescription = new System.Windows.Forms.Label();
            this.lbStatusResult = new System.Windows.Forms.Label();
            this.pic_Status15 = new System.Windows.Forms.PictureBox();
            this.pic_Status14 = new System.Windows.Forms.PictureBox();
            this.pic_Status13 = new System.Windows.Forms.PictureBox();
            this.pic_Status12 = new System.Windows.Forms.PictureBox();
            this.pic_Status11 = new System.Windows.Forms.PictureBox();
            this.pic_Status10 = new System.Windows.Forms.PictureBox();
            this.pic_Status9 = new System.Windows.Forms.PictureBox();
            this.pic_Status8 = new System.Windows.Forms.PictureBox();
            this.pic_Status7 = new System.Windows.Forms.PictureBox();
            this.pic_Status6 = new System.Windows.Forms.PictureBox();
            this.pic_Status5 = new System.Windows.Forms.PictureBox();
            this.pic_Status4 = new System.Windows.Forms.PictureBox();
            this.pic_Status3 = new System.Windows.Forms.PictureBox();
            this.pic_Status2 = new System.Windows.Forms.PictureBox();
            this.pic_Status1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TextBox_ShutterSpeed = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lbShutter = new System.Windows.Forms.Label();
            this.Track_ShutterSpeed = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.panel_image = new System.Windows.Forms.Panel();
            this.pic_Camera = new System.Windows.Forms.PictureBox();
            this.panel_Info = new System.Windows.Forms.Panel();
            this.lbFPS = new System.Windows.Forms.Label();
            this.lbCurrentSpotNo = new System.Windows.Forms.Label();
            this.lbCurrentMode = new System.Windows.Forms.Label();
            this.lbTolerance = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbFPSb = new System.Windows.Forms.Label();
            this.tab_LiveView = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.txtSpot3to1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtSpot2to3 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtSpot1to2 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtSpotXY3 = new System.Windows.Forms.TextBox();
            this.txtSpotY3 = new System.Windows.Forms.TextBox();
            this.txtSpotX3 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.txtSpotXY2 = new System.Windows.Forms.TextBox();
            this.txtSpotY2 = new System.Windows.Forms.TextBox();
            this.txtSpotX2 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.txtSpotXY1 = new System.Windows.Forms.TextBox();
            this.txtSpotY1 = new System.Windows.Forms.TextBox();
            this.txtSpotX1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.cmdWobbleFlattingClear = new System.Windows.Forms.Button();
            this.cmdPlotStartStop = new System.Windows.Forms.Button();
            this.cbPlotSpotNo = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.panel_Plot = new System.Windows.Forms.Panel();
            this.pic_Plot = new System.Windows.Forms.PictureBox();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmdSnapDataClear = new System.Windows.Forms.Button();
            this.cbSerialSpotNo = new System.Windows.Forms.ComboBox();
            this.label36 = new System.Windows.Forms.Label();
            this.txtCurrentPartName = new System.Windows.Forms.TextBox();
            this.gridSerialData = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmdSnapMeasurement = new System.Windows.Forms.Button();
            this.cmdDataExport = new System.Windows.Forms.Button();
            this.txtSerialSpotXY2 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotY2 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotX2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtSerialSpotXY1 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotY1 = new System.Windows.Forms.TextBox();
            this.txtSerialSpotX1 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtStartingNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtPartName = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.SaveProgressbar = new System.Windows.Forms.ProgressBar();
            this.txtPixelPerDistance = new System.Windows.Forms.TextBox();
            this.imageListButton = new System.Windows.Forms.ImageList(this.components);
            this.menuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).BeginInit();
            this.panel_Icon.SuspendLayout();
            this.panel_Set.SuspendLayout();
            this.panel_Status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Track_ShutterSpeed)).BeginInit();
            this.panel_image.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Camera)).BeginInit();
            this.panel_Info.SuspendLayout();
            this.tab_LiveView.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panel_Plot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Plot)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSerialData)).BeginInit();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.BackColor = System.Drawing.Color.White;
            this.menuBar.Dock = System.Windows.Forms.DockStyle.None;
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Measurement,
            this.menu_Tools,
            this.menu_Settings,
            this.menu_Information});
            this.menuBar.Location = new System.Drawing.Point(9, 9);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(294, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuBar";
            // 
            // menu_Measurement
            // 
            this.menu_Measurement.MergeIndex = 10;
            this.menu_Measurement.Name = "menu_Measurement";
            this.menu_Measurement.Size = new System.Drawing.Size(104, 20);
            this.menu_Measurement.Text = "MEASUREMENT";
            // 
            // menu_Tools
            // 
            this.menu_Tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_ZeroSet,
            this.menu_Tools_Mode,
            this.menu_Tools_Angledefinition,
            this.menu_Tools_Coordinate,
            this.menu_Tools_Numbering,
            this.menu_Tools_Zoom});
            this.menu_Tools.MergeIndex = 10;
            this.menu_Tools.Name = "menu_Tools";
            this.menu_Tools.Size = new System.Drawing.Size(56, 20);
            this.menu_Tools.Text = "TOOLS";
            // 
            // menu_Tools_ZeroSet
            // 
            this.menu_Tools_ZeroSet.BackColor = System.Drawing.Color.White;
            this.menu_Tools_ZeroSet.Name = "menu_Tools_ZeroSet";
            this.menu_Tools_ZeroSet.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_ZeroSet.Text = "ZERO SET";
            this.menu_Tools_ZeroSet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.menu_Tools_ZeroSet.Click += new System.EventHandler(this.menu_Tools_ZeroSet_Click);
            // 
            // menu_Tools_Mode
            // 
            this.menu_Tools_Mode.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Mode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Mode_Area,
            this.menu_Tools_Mode_Brightness,
            this.menu_Tools_Mode_Peak});
            this.menu_Tools_Mode.Name = "menu_Tools_Mode";
            this.menu_Tools_Mode.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Mode.Text = "MODE";
            this.menu_Tools_Mode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Mode_Area
            // 
            this.menu_Tools_Mode_Area.CheckOnClick = true;
            this.menu_Tools_Mode_Area.Name = "menu_Tools_Mode_Area";
            this.menu_Tools_Mode_Area.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode_Area.Text = "AREA CENTER";
            this.menu_Tools_Mode_Area.Click += new System.EventHandler(this.menu_Tools_Mode_Area_Click);
            // 
            // menu_Tools_Mode_Brightness
            // 
            this.menu_Tools_Mode_Brightness.CheckOnClick = true;
            this.menu_Tools_Mode_Brightness.Name = "menu_Tools_Mode_Brightness";
            this.menu_Tools_Mode_Brightness.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode_Brightness.Text = "BRIGHTNESS CENTER";
            this.menu_Tools_Mode_Brightness.Click += new System.EventHandler(this.menu_Tools_Mode_Brightness_Click);
            // 
            // menu_Tools_Mode_Peak
            // 
            this.menu_Tools_Mode_Peak.CheckOnClick = true;
            this.menu_Tools_Mode_Peak.Name = "menu_Tools_Mode_Peak";
            this.menu_Tools_Mode_Peak.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode_Peak.Text = "PEAK";
            this.menu_Tools_Mode_Peak.Click += new System.EventHandler(this.menu_Tools_Mode_Peak_Click);
            // 
            // menu_Tools_Angledefinition
            // 
            this.menu_Tools_Angledefinition.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Angledefinition.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Angle_Ext,
            this.menu_Tools_Angle_Int});
            this.menu_Tools_Angledefinition.Name = "menu_Tools_Angledefinition";
            this.menu_Tools_Angledefinition.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Angledefinition.Text = "ANGLE DEFINITION";
            this.menu_Tools_Angledefinition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Angle_Ext
            // 
            this.menu_Tools_Angle_Ext.CheckOnClick = true;
            this.menu_Tools_Angle_Ext.Name = "menu_Tools_Angle_Ext";
            this.menu_Tools_Angle_Ext.Size = new System.Drawing.Size(227, 22);
            this.menu_Tools_Angle_Ext.Text = "EXTERNAL LIGHT MODE(2α)";
            this.menu_Tools_Angle_Ext.Click += new System.EventHandler(this.menu_Tools_Angle_Ext_Click);
            // 
            // menu_Tools_Angle_Int
            // 
            this.menu_Tools_Angle_Int.CheckOnClick = true;
            this.menu_Tools_Angle_Int.Name = "menu_Tools_Angle_Int";
            this.menu_Tools_Angle_Int.Size = new System.Drawing.Size(227, 22);
            this.menu_Tools_Angle_Int.Text = "INTERNAL LIGHT MODE(α)";
            this.menu_Tools_Angle_Int.Click += new System.EventHandler(this.menu_Tools_Angle_Int_Click);
            // 
            // menu_Tools_Coordinate
            // 
            this.menu_Tools_Coordinate.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Coordinate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Mirroring,
            this.menu_Tools_Coordinate_Rotating,
            this.menu_Tools_Coordinate_Original});
            this.menu_Tools_Coordinate.Name = "menu_Tools_Coordinate";
            this.menu_Tools_Coordinate.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Coordinate.Text = "COORDINATE";
            this.menu_Tools_Coordinate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Coordinate_Mirroring
            // 
            this.menu_Tools_Coordinate_Mirroring.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Mirroring_Xon,
            this.menu_Tools_Coordinate_Mirroring_Yon,
            this.menu_Tools_Coordinate_Mirroring_Off});
            this.menu_Tools_Coordinate_Mirroring.Name = "menu_Tools_Coordinate_Mirroring";
            this.menu_Tools_Coordinate_Mirroring.Size = new System.Drawing.Size(138, 22);
            this.menu_Tools_Coordinate_Mirroring.Text = "MIRRORING";
            // 
            // menu_Tools_Coordinate_Mirroring_Xon
            // 
            this.menu_Tools_Coordinate_Mirroring_Xon.Name = "menu_Tools_Coordinate_Mirroring_Xon";
            this.menu_Tools_Coordinate_Mirroring_Xon.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Coordinate_Mirroring_Xon.Text = "X-on";
            this.menu_Tools_Coordinate_Mirroring_Xon.Click += new System.EventHandler(this.menu_Tools_Coordinate_Mirroring_Xon_Click);
            // 
            // menu_Tools_Coordinate_Mirroring_Yon
            // 
            this.menu_Tools_Coordinate_Mirroring_Yon.Name = "menu_Tools_Coordinate_Mirroring_Yon";
            this.menu_Tools_Coordinate_Mirroring_Yon.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Coordinate_Mirroring_Yon.Text = "Y-on";
            this.menu_Tools_Coordinate_Mirroring_Yon.Click += new System.EventHandler(this.menu_Tools_Coordinate_Mirroring_Yon_Click);
            // 
            // menu_Tools_Coordinate_Mirroring_Off
            // 
            this.menu_Tools_Coordinate_Mirroring_Off.Name = "menu_Tools_Coordinate_Mirroring_Off";
            this.menu_Tools_Coordinate_Mirroring_Off.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Coordinate_Mirroring_Off.Text = "OFF";
            this.menu_Tools_Coordinate_Mirroring_Off.Click += new System.EventHandler(this.menu_Tools_Coordinate_Mirroring_Off_Click);
            // 
            // menu_Tools_Coordinate_Rotating
            // 
            this.menu_Tools_Coordinate_Rotating.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Rotating_L90,
            this.menu_Tools_Coordinate_Rotating_R90,
            this.menu_Tools_Coordinate_Rotating_Off});
            this.menu_Tools_Coordinate_Rotating.Name = "menu_Tools_Coordinate_Rotating";
            this.menu_Tools_Coordinate_Rotating.Size = new System.Drawing.Size(138, 22);
            this.menu_Tools_Coordinate_Rotating.Text = "ROTATING";
            // 
            // menu_Tools_Coordinate_Rotating_L90
            // 
            this.menu_Tools_Coordinate_Rotating_L90.Name = "menu_Tools_Coordinate_Rotating_L90";
            this.menu_Tools_Coordinate_Rotating_L90.Size = new System.Drawing.Size(95, 22);
            this.menu_Tools_Coordinate_Rotating_L90.Text = "L90";
            this.menu_Tools_Coordinate_Rotating_L90.Click += new System.EventHandler(this.menu_Tools_Coordinate_Rotating_L90_Click);
            // 
            // menu_Tools_Coordinate_Rotating_R90
            // 
            this.menu_Tools_Coordinate_Rotating_R90.Name = "menu_Tools_Coordinate_Rotating_R90";
            this.menu_Tools_Coordinate_Rotating_R90.Size = new System.Drawing.Size(95, 22);
            this.menu_Tools_Coordinate_Rotating_R90.Text = "R90";
            this.menu_Tools_Coordinate_Rotating_R90.Click += new System.EventHandler(this.menu_Tools_Coordinate_Rotating_R90_Click);
            // 
            // menu_Tools_Coordinate_Rotating_Off
            // 
            this.menu_Tools_Coordinate_Rotating_Off.Name = "menu_Tools_Coordinate_Rotating_Off";
            this.menu_Tools_Coordinate_Rotating_Off.Size = new System.Drawing.Size(95, 22);
            this.menu_Tools_Coordinate_Rotating_Off.Text = "OFF";
            this.menu_Tools_Coordinate_Rotating_Off.Click += new System.EventHandler(this.menu_Tools_Coordinate_Rotating_Off_Click);
            // 
            // menu_Tools_Coordinate_Original
            // 
            this.menu_Tools_Coordinate_Original.Name = "menu_Tools_Coordinate_Original";
            this.menu_Tools_Coordinate_Original.Size = new System.Drawing.Size(138, 22);
            this.menu_Tools_Coordinate_Original.Text = "ORIGINAL";
            this.menu_Tools_Coordinate_Original.Visible = false;
            this.menu_Tools_Coordinate_Original.Click += new System.EventHandler(this.menu_Tools_Coordinate_Original_Click);
            // 
            // menu_Tools_Numbering
            // 
            this.menu_Tools_Numbering.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Numbering.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Numbring_Angle,
            this.menu_Tools_Numbring_Size});
            this.menu_Tools_Numbering.Name = "menu_Tools_Numbering";
            this.menu_Tools_Numbering.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Numbering.Text = "NUMBERING";
            this.menu_Tools_Numbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Numbring_Angle
            // 
            this.menu_Tools_Numbring_Angle.CheckOnClick = true;
            this.menu_Tools_Numbring_Angle.Name = "menu_Tools_Numbring_Angle";
            this.menu_Tools_Numbring_Angle.Size = new System.Drawing.Size(111, 22);
            this.menu_Tools_Numbring_Angle.Text = "ANGLE";
            this.menu_Tools_Numbring_Angle.Click += new System.EventHandler(this.menu_Tools_Numbring_Angle_Click);
            // 
            // menu_Tools_Numbring_Size
            // 
            this.menu_Tools_Numbring_Size.CheckOnClick = true;
            this.menu_Tools_Numbring_Size.Name = "menu_Tools_Numbring_Size";
            this.menu_Tools_Numbring_Size.Size = new System.Drawing.Size(111, 22);
            this.menu_Tools_Numbring_Size.Text = "SIZE";
            this.menu_Tools_Numbring_Size.Click += new System.EventHandler(this.menu_Tools_Numbring_Size_Click);
            // 
            // menu_Tools_Zoom
            // 
            this.menu_Tools_Zoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Zoom_4,
            this.menu_Tools_Zoom_8,
            this.menu_Tools_Zoom_16,
            this.menu_Tools_Zoom_32,
            this.menu_Tools_Zoom_Off});
            this.menu_Tools_Zoom.Name = "menu_Tools_Zoom";
            this.menu_Tools_Zoom.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Zoom.Text = "ZOOM";
            // 
            // menu_Tools_Zoom_4
            // 
            this.menu_Tools_Zoom_4.Name = "menu_Tools_Zoom_4";
            this.menu_Tools_Zoom_4.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_4.Text = "x 4";
            this.menu_Tools_Zoom_4.Click += new System.EventHandler(this.menu_Tools_Zoom_4_Click);
            // 
            // menu_Tools_Zoom_8
            // 
            this.menu_Tools_Zoom_8.Name = "menu_Tools_Zoom_8";
            this.menu_Tools_Zoom_8.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_8.Text = "x 8";
            this.menu_Tools_Zoom_8.Click += new System.EventHandler(this.menu_Tools_Zoom_8_Click);
            // 
            // menu_Tools_Zoom_16
            // 
            this.menu_Tools_Zoom_16.Name = "menu_Tools_Zoom_16";
            this.menu_Tools_Zoom_16.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_16.Text = "x 16";
            this.menu_Tools_Zoom_16.Click += new System.EventHandler(this.menu_Tools_Zoom_16_Click);
            // 
            // menu_Tools_Zoom_32
            // 
            this.menu_Tools_Zoom_32.Name = "menu_Tools_Zoom_32";
            this.menu_Tools_Zoom_32.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_32.Text = "x 32";
            this.menu_Tools_Zoom_32.Click += new System.EventHandler(this.menu_Tools_Zoom_32_Click);
            // 
            // menu_Tools_Zoom_Off
            // 
            this.menu_Tools_Zoom_Off.Name = "menu_Tools_Zoom_Off";
            this.menu_Tools_Zoom_Off.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_Off.Text = "Off";
            this.menu_Tools_Zoom_Off.Click += new System.EventHandler(this.menu_Tools_Zoom_Off_Click);
            // 
            // menu_Settings
            // 
            this.menu_Settings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Setting_Unit,
            this.menu_Setting_Tolerance,
            this.menu_Setting_GrayLevel});
            this.menu_Settings.Name = "menu_Settings";
            this.menu_Settings.Size = new System.Drawing.Size(71, 20);
            this.menu_Settings.Text = "SETTINGS";
            // 
            // menu_Setting_Unit
            // 
            this.menu_Setting_Unit.BackColor = System.Drawing.Color.White;
            this.menu_Setting_Unit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Settings_Unit_Degree,
            this.menu_Settings_Unit_Sec});
            this.menu_Setting_Unit.Name = "menu_Setting_Unit";
            this.menu_Setting_Unit.Size = new System.Drawing.Size(139, 22);
            this.menu_Setting_Unit.Text = "UNIT";
            // 
            // menu_Settings_Unit_Degree
            // 
            this.menu_Settings_Unit_Degree.CheckOnClick = true;
            this.menu_Settings_Unit_Degree.Name = "menu_Settings_Unit_Degree";
            this.menu_Settings_Unit_Degree.Size = new System.Drawing.Size(126, 22);
            this.menu_Settings_Unit_Degree.Text = "DEGREE";
            this.menu_Settings_Unit_Degree.Click += new System.EventHandler(this.menu_Settings_Unit_Degree_Click);
            // 
            // menu_Settings_Unit_Sec
            // 
            this.menu_Settings_Unit_Sec.CheckOnClick = true;
            this.menu_Settings_Unit_Sec.Name = "menu_Settings_Unit_Sec";
            this.menu_Settings_Unit_Sec.Size = new System.Drawing.Size(126, 22);
            this.menu_Settings_Unit_Sec.Text = "MIN+SEC";
            this.menu_Settings_Unit_Sec.Click += new System.EventHandler(this.menu_Settings_Unit_Sec_Click);
            // 
            // menu_Setting_Tolerance
            // 
            this.menu_Setting_Tolerance.BackColor = System.Drawing.Color.White;
            this.menu_Setting_Tolerance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Settings_Tolerance_Circle,
            this.menu_Settings_Tolerance_Square,
            this.menu_Settings_Tolerance_Value});
            this.menu_Setting_Tolerance.Name = "menu_Setting_Tolerance";
            this.menu_Setting_Tolerance.Size = new System.Drawing.Size(139, 22);
            this.menu_Setting_Tolerance.Text = "TOLERANCE";
            // 
            // menu_Settings_Tolerance_Circle
            // 
            this.menu_Settings_Tolerance_Circle.CheckOnClick = true;
            this.menu_Settings_Tolerance_Circle.Name = "menu_Settings_Tolerance_Circle";
            this.menu_Settings_Tolerance_Circle.Size = new System.Drawing.Size(159, 22);
            this.menu_Settings_Tolerance_Circle.Text = "Circle";
            this.menu_Settings_Tolerance_Circle.Click += new System.EventHandler(this.menu_Settings_Tolerance_Circle_Click);
            // 
            // menu_Settings_Tolerance_Square
            // 
            this.menu_Settings_Tolerance_Square.CheckOnClick = true;
            this.menu_Settings_Tolerance_Square.Name = "menu_Settings_Tolerance_Square";
            this.menu_Settings_Tolerance_Square.Size = new System.Drawing.Size(159, 22);
            this.menu_Settings_Tolerance_Square.Text = "Square";
            this.menu_Settings_Tolerance_Square.Click += new System.EventHandler(this.menu_Settings_Tolerance_Square_Click);
            // 
            // menu_Settings_Tolerance_Value
            // 
            this.menu_Settings_Tolerance_Value.Name = "menu_Settings_Tolerance_Value";
            this.menu_Settings_Tolerance_Value.Size = new System.Drawing.Size(159, 22);
            this.menu_Settings_Tolerance_Value.Text = "Tolerance Value";
            this.menu_Settings_Tolerance_Value.Click += new System.EventHandler(this.menu_Settings_Tolerance_Value_Click);
            // 
            // menu_Setting_GrayLevel
            // 
            this.menu_Setting_GrayLevel.BackColor = System.Drawing.Color.White;
            this.menu_Setting_GrayLevel.Name = "menu_Setting_GrayLevel";
            this.menu_Setting_GrayLevel.Size = new System.Drawing.Size(139, 22);
            this.menu_Setting_GrayLevel.Text = "GREY LEVEL";
            this.menu_Setting_GrayLevel.Click += new System.EventHandler(this.menu_Setting_GrayLevel_Click);
            // 
            // menu_Information
            // 
            this.menu_Information.Name = "menu_Information";
            this.menu_Information.Size = new System.Drawing.Size(55, 20);
            this.menu_Information.Text = "About.";
            this.menu_Information.Click += new System.EventHandler(this.menu_Information_About_Click);
            // 
            // picWikiLogo
            // 
            this.picWikiLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picWikiLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picWikiLogo.Image = global::WikiOptics_Collimator.Properties.Resources.picWikiLogo;
            this.picWikiLogo.Location = new System.Drawing.Point(1015, 11);
            this.picWikiLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picWikiLogo.Name = "picWikiLogo";
            this.picWikiLogo.Size = new System.Drawing.Size(134, 25);
            this.picWikiLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picWikiLogo.TabIndex = 1;
            this.picWikiLogo.TabStop = false;
            // 
            // panel_Icon
            // 
            this.panel_Icon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel_Icon.BackColor = System.Drawing.Color.White;
            this.panel_Icon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Icon.Controls.Add(this.comboBox_serialport);
            this.panel_Icon.Controls.Add(this.label1);
            this.panel_Icon.Controls.Add(this.InstallSerialDriver);
            this.panel_Icon.Controls.Add(this.AutoShutter);
            this.panel_Icon.Controls.Add(this.SaveAllConfig);
            this.panel_Icon.Controls.Add(this.cmdFactorySet);
            this.panel_Icon.Controls.Add(this.cmdAnalysis);
            this.panel_Icon.Controls.Add(this.btn_Capture);
            this.panel_Icon.Controls.Add(this.cmdClear);
            this.panel_Icon.Controls.Add(this.cmdPrint);
            this.panel_Icon.Controls.Add(this.cmdExport);
            this.panel_Icon.Controls.Add(this.btn_SettingFileSave);
            this.panel_Icon.Controls.Add(this.btn_SettingFileOpen);
            this.panel_Icon.Location = new System.Drawing.Point(12, 36);
            this.panel_Icon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Icon.Name = "panel_Icon";
            this.panel_Icon.Size = new System.Drawing.Size(96, 649);
            this.panel_Icon.TabIndex = 2;
            // 
            // comboBox_serialport
            // 
            this.comboBox_serialport.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_serialport.FormattingEnabled = true;
            this.comboBox_serialport.Location = new System.Drawing.Point(19, 416);
            this.comboBox_serialport.Name = "comboBox_serialport";
            this.comboBox_serialport.Size = new System.Drawing.Size(68, 23);
            this.comboBox_serialport.TabIndex = 13;
            this.comboBox_serialport.SelectedIndexChanged += new System.EventHandler(this.comboBox_serialport_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(2, 390);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Port (9600) : ";
            // 
            // InstallSerialDriver
            // 
            this.InstallSerialDriver.ForeColor = System.Drawing.Color.Red;
            this.InstallSerialDriver.Location = new System.Drawing.Point(3, 502);
            this.InstallSerialDriver.Name = "InstallSerialDriver";
            this.InstallSerialDriver.Size = new System.Drawing.Size(87, 50);
            this.InstallSerialDriver.TabIndex = 11;
            this.InstallSerialDriver.Text = "Install Serial Driver";
            this.InstallSerialDriver.UseVisualStyleBackColor = true;
            this.InstallSerialDriver.Visible = false;
            // 
            // AutoShutter
            // 
            this.AutoShutter.Location = new System.Drawing.Point(0, 552);
            this.AutoShutter.Name = "AutoShutter";
            this.AutoShutter.Size = new System.Drawing.Size(96, 42);
            this.AutoShutter.TabIndex = 10;
            this.AutoShutter.Text = "AutoShutter";
            this.AutoShutter.UseVisualStyleBackColor = true;
            this.AutoShutter.Click += new System.EventHandler(this.btn_AutoShutter_Click);
            // 
            // SaveAllConfig
            // 
            this.SaveAllConfig.Location = new System.Drawing.Point(-1, 97);
            this.SaveAllConfig.Name = "SaveAllConfig";
            this.SaveAllConfig.Size = new System.Drawing.Size(96, 42);
            this.SaveAllConfig.TabIndex = 9;
            this.SaveAllConfig.Text = "SAVE";
            this.SaveAllConfig.UseVisualStyleBackColor = true;
            this.SaveAllConfig.Click += new System.EventHandler(this.OnSaveAllConfigClick);
            // 
            // cmdFactorySet
            // 
            this.cmdFactorySet.BackColor = System.Drawing.Color.Red;
            this.cmdFactorySet.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cmdFactorySet.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Fuchsia;
            this.cmdFactorySet.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Fuchsia;
            this.cmdFactorySet.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFactorySet.ForeColor = System.Drawing.Color.White;
            this.cmdFactorySet.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdFactorySet.Location = new System.Drawing.Point(-1, 588);
            this.cmdFactorySet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdFactorySet.Name = "cmdFactorySet";
            this.cmdFactorySet.Size = new System.Drawing.Size(96, 58);
            this.cmdFactorySet.TabIndex = 8;
            this.cmdFactorySet.Text = "FACTORY SET";
            this.cmdFactorySet.UseVisualStyleBackColor = false;
            this.cmdFactorySet.Click += new System.EventHandler(this.cmdFactorySet_Click);
            // 
            // cmdAnalysis
            // 
            this.cmdAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.cmdAnalysis.FlatAppearance.BorderSize = 0;
            this.cmdAnalysis.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdAnalysis.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdAnalysis.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnalysis.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdAnalysis.ImageIndex = 0;
            this.cmdAnalysis.Location = new System.Drawing.Point(-1, -1);
            this.cmdAnalysis.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdAnalysis.Name = "cmdAnalysis";
            this.cmdAnalysis.Size = new System.Drawing.Size(96, 42);
            this.cmdAnalysis.TabIndex = 0;
            this.cmdAnalysis.Text = "START";
            this.cmdAnalysis.UseVisualStyleBackColor = true;
            this.cmdAnalysis.Click += new System.EventHandler(this.cmdAnalysis_Click);
            // 
            // btn_Capture
            // 
            this.btn_Capture.FlatAppearance.BorderSize = 0;
            this.btn_Capture.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_Capture.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btn_Capture.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Capture.ImageIndex = 1;
            this.btn_Capture.Location = new System.Drawing.Point(0, 247);
            this.btn_Capture.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Capture.Name = "btn_Capture";
            this.btn_Capture.Size = new System.Drawing.Size(96, 44);
            this.btn_Capture.TabIndex = 5;
            this.btn_Capture.Text = "CAPTURE";
            this.btn_Capture.UseVisualStyleBackColor = true;
            this.btn_Capture.Click += new System.EventHandler(this.btn_Capture_Click);
            // 
            // cmdClear
            // 
            this.cmdClear.FlatAppearance.BorderSize = 0;
            this.cmdClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdClear.ImageIndex = 5;
            this.cmdClear.Location = new System.Drawing.Point(-1, 197);
            this.cmdClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(96, 42);
            this.cmdClear.TabIndex = 4;
            this.cmdClear.Text = "CLEAR";
            this.cmdClear.UseVisualStyleBackColor = true;
            this.cmdClear.Click += new System.EventHandler(this.cmdClear_Click);
            // 
            // cmdPrint
            // 
            this.cmdPrint.FlatAppearance.BorderSize = 0;
            this.cmdPrint.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdPrint.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdPrint.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdPrint.ImageIndex = 7;
            this.cmdPrint.Location = new System.Drawing.Point(0, 299);
            this.cmdPrint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(96, 42);
            this.cmdPrint.TabIndex = 6;
            this.cmdPrint.Text = "PRINT";
            this.cmdPrint.UseVisualStyleBackColor = true;
            this.cmdPrint.Visible = false;
            // 
            // cmdExport
            // 
            this.cmdExport.FlatAppearance.BorderSize = 0;
            this.cmdExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdExport.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdExport.ImageIndex = 4;
            this.cmdExport.Location = new System.Drawing.Point(-1, 147);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(96, 42);
            this.cmdExport.TabIndex = 3;
            this.cmdExport.Text = "EXPORT";
            this.cmdExport.UseVisualStyleBackColor = true;
            this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
            // 
            // btn_SettingFileSave
            // 
            this.btn_SettingFileSave.FlatAppearance.BorderSize = 0;
            this.btn_SettingFileSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_SettingFileSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btn_SettingFileSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SettingFileSave.ImageIndex = 3;
            this.btn_SettingFileSave.Location = new System.Drawing.Point(2, 453);
            this.btn_SettingFileSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_SettingFileSave.Name = "btn_SettingFileSave";
            this.btn_SettingFileSave.Size = new System.Drawing.Size(96, 42);
            this.btn_SettingFileSave.TabIndex = 2;
            this.btn_SettingFileSave.Text = "SAVE";
            this.btn_SettingFileSave.UseVisualStyleBackColor = true;
            this.btn_SettingFileSave.Visible = false;
            this.btn_SettingFileSave.Click += new System.EventHandler(this.btn_SettingFileSave_Click);
            // 
            // btn_SettingFileOpen
            // 
            this.btn_SettingFileOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_SettingFileOpen.FlatAppearance.BorderSize = 0;
            this.btn_SettingFileOpen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_SettingFileOpen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btn_SettingFileOpen.ImageIndex = 2;
            this.btn_SettingFileOpen.Location = new System.Drawing.Point(-1, 47);
            this.btn_SettingFileOpen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_SettingFileOpen.Name = "btn_SettingFileOpen";
            this.btn_SettingFileOpen.Size = new System.Drawing.Size(96, 42);
            this.btn_SettingFileOpen.TabIndex = 1;
            this.btn_SettingFileOpen.Text = "OPEN";
            this.btn_SettingFileOpen.UseVisualStyleBackColor = true;
            this.btn_SettingFileOpen.Click += new System.EventHandler(this.btn_SettingFileOpen_Click);
            // 
            // panel_Set
            // 
            this.panel_Set.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Set.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Set.Controls.Add(this.txtSamplingFreq);
            this.panel_Set.Controls.Add(this.label37);
            this.panel_Set.Controls.Add(this.cmdMeasureData);
            this.panel_Set.Controls.Add(this.label5);
            this.panel_Set.Location = new System.Drawing.Point(114, 637);
            this.panel_Set.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Set.Name = "panel_Set";
            this.panel_Set.Size = new System.Drawing.Size(641, 48);
            this.panel_Set.TabIndex = 3;
            // 
            // txtSamplingFreq
            // 
            this.txtSamplingFreq.Location = new System.Drawing.Point(149, 15);
            this.txtSamplingFreq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSamplingFreq.Name = "txtSamplingFreq";
            this.txtSamplingFreq.Size = new System.Drawing.Size(100, 21);
            this.txtSamplingFreq.TabIndex = 12;
            this.txtSamplingFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(255, 18);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(110, 15);
            this.label37.TabIndex = 11;
            this.label37.Text = "sec (0.1s ~ 3600s)";
            // 
            // cmdMeasureData
            // 
            this.cmdMeasureData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdMeasureData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMeasureData.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdMeasureData.Location = new System.Drawing.Point(409, 10);
            this.cmdMeasureData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdMeasureData.Name = "cmdMeasureData";
            this.cmdMeasureData.Size = new System.Drawing.Size(206, 26);
            this.cmdMeasureData.TabIndex = 10;
            this.cmdMeasureData.Text = "Start measurement";
            this.cmdMeasureData.UseVisualStyleBackColor = true;
            this.cmdMeasureData.Click += new System.EventHandler(this.cmdMeasureData_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Sampling frequency";
            // 
            // panel_Status
            // 
            this.panel_Status.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Status.Controls.Add(this.lbStatusDescription);
            this.panel_Status.Controls.Add(this.lbStatusResult);
            this.panel_Status.Controls.Add(this.pic_Status15);
            this.panel_Status.Controls.Add(this.pic_Status14);
            this.panel_Status.Controls.Add(this.pic_Status13);
            this.panel_Status.Controls.Add(this.pic_Status12);
            this.panel_Status.Controls.Add(this.pic_Status11);
            this.panel_Status.Controls.Add(this.pic_Status10);
            this.panel_Status.Controls.Add(this.pic_Status9);
            this.panel_Status.Controls.Add(this.pic_Status8);
            this.panel_Status.Controls.Add(this.pic_Status7);
            this.panel_Status.Controls.Add(this.pic_Status6);
            this.panel_Status.Controls.Add(this.pic_Status5);
            this.panel_Status.Controls.Add(this.pic_Status4);
            this.panel_Status.Controls.Add(this.pic_Status3);
            this.panel_Status.Controls.Add(this.pic_Status2);
            this.panel_Status.Controls.Add(this.pic_Status1);
            this.panel_Status.Location = new System.Drawing.Point(114, 595);
            this.panel_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Status.Name = "panel_Status";
            this.panel_Status.Size = new System.Drawing.Size(641, 34);
            this.panel_Status.TabIndex = 5;
            // 
            // lbStatusDescription
            // 
            this.lbStatusDescription.AutoSize = true;
            this.lbStatusDescription.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatusDescription.ForeColor = System.Drawing.Color.Green;
            this.lbStatusDescription.Location = new System.Drawing.Point(449, 10);
            this.lbStatusDescription.Name = "lbStatusDescription";
            this.lbStatusDescription.Size = new System.Drawing.Size(12, 17);
            this.lbStatusDescription.TabIndex = 16;
            this.lbStatusDescription.Text = ".";
            this.lbStatusDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbStatusDescription.Visible = false;
            // 
            // lbStatusResult
            // 
            this.lbStatusResult.AutoSize = true;
            this.lbStatusResult.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatusResult.ForeColor = System.Drawing.Color.Green;
            this.lbStatusResult.Location = new System.Drawing.Point(349, 5);
            this.lbStatusResult.Name = "lbStatusResult";
            this.lbStatusResult.Size = new System.Drawing.Size(36, 22);
            this.lbStatusResult.TabIndex = 15;
            this.lbStatusResult.Text = "OK";
            this.lbStatusResult.Visible = false;
            // 
            // pic_Status15
            // 
            this.pic_Status15.BackColor = System.Drawing.Color.Red;
            this.pic_Status15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status15.Location = new System.Drawing.Point(290, 4);
            this.pic_Status15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status15.Name = "pic_Status15";
            this.pic_Status15.Size = new System.Drawing.Size(19, 23);
            this.pic_Status15.TabIndex = 14;
            this.pic_Status15.TabStop = false;
            this.pic_Status15.Visible = false;
            // 
            // pic_Status14
            // 
            this.pic_Status14.BackColor = System.Drawing.Color.Red;
            this.pic_Status14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status14.Location = new System.Drawing.Point(270, 4);
            this.pic_Status14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status14.Name = "pic_Status14";
            this.pic_Status14.Size = new System.Drawing.Size(19, 23);
            this.pic_Status14.TabIndex = 13;
            this.pic_Status14.TabStop = false;
            this.pic_Status14.Visible = false;
            // 
            // pic_Status13
            // 
            this.pic_Status13.BackColor = System.Drawing.Color.Red;
            this.pic_Status13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status13.Location = new System.Drawing.Point(250, 4);
            this.pic_Status13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status13.Name = "pic_Status13";
            this.pic_Status13.Size = new System.Drawing.Size(19, 23);
            this.pic_Status13.TabIndex = 12;
            this.pic_Status13.TabStop = false;
            this.pic_Status13.Visible = false;
            // 
            // pic_Status12
            // 
            this.pic_Status12.BackColor = System.Drawing.Color.Green;
            this.pic_Status12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status12.Location = new System.Drawing.Point(230, 4);
            this.pic_Status12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status12.Name = "pic_Status12";
            this.pic_Status12.Size = new System.Drawing.Size(19, 23);
            this.pic_Status12.TabIndex = 11;
            this.pic_Status12.TabStop = false;
            this.pic_Status12.Visible = false;
            // 
            // pic_Status11
            // 
            this.pic_Status11.BackColor = System.Drawing.Color.Green;
            this.pic_Status11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status11.Location = new System.Drawing.Point(210, 4);
            this.pic_Status11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status11.Name = "pic_Status11";
            this.pic_Status11.Size = new System.Drawing.Size(19, 23);
            this.pic_Status11.TabIndex = 10;
            this.pic_Status11.TabStop = false;
            this.pic_Status11.Visible = false;
            // 
            // pic_Status10
            // 
            this.pic_Status10.BackColor = System.Drawing.Color.Green;
            this.pic_Status10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status10.Location = new System.Drawing.Point(190, 4);
            this.pic_Status10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status10.Name = "pic_Status10";
            this.pic_Status10.Size = new System.Drawing.Size(19, 23);
            this.pic_Status10.TabIndex = 9;
            this.pic_Status10.TabStop = false;
            this.pic_Status10.Visible = false;
            // 
            // pic_Status9
            // 
            this.pic_Status9.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status9.Location = new System.Drawing.Point(170, 4);
            this.pic_Status9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status9.Name = "pic_Status9";
            this.pic_Status9.Size = new System.Drawing.Size(19, 23);
            this.pic_Status9.TabIndex = 8;
            this.pic_Status9.TabStop = false;
            this.pic_Status9.Visible = false;
            // 
            // pic_Status8
            // 
            this.pic_Status8.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status8.Location = new System.Drawing.Point(150, 4);
            this.pic_Status8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status8.Name = "pic_Status8";
            this.pic_Status8.Size = new System.Drawing.Size(19, 23);
            this.pic_Status8.TabIndex = 7;
            this.pic_Status8.TabStop = false;
            this.pic_Status8.Visible = false;
            // 
            // pic_Status7
            // 
            this.pic_Status7.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status7.Location = new System.Drawing.Point(130, 4);
            this.pic_Status7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status7.Name = "pic_Status7";
            this.pic_Status7.Size = new System.Drawing.Size(19, 23);
            this.pic_Status7.TabIndex = 6;
            this.pic_Status7.TabStop = false;
            this.pic_Status7.Visible = false;
            // 
            // pic_Status6
            // 
            this.pic_Status6.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status6.Location = new System.Drawing.Point(110, 4);
            this.pic_Status6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status6.Name = "pic_Status6";
            this.pic_Status6.Size = new System.Drawing.Size(19, 23);
            this.pic_Status6.TabIndex = 5;
            this.pic_Status6.TabStop = false;
            this.pic_Status6.Visible = false;
            // 
            // pic_Status5
            // 
            this.pic_Status5.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status5.Location = new System.Drawing.Point(90, 4);
            this.pic_Status5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status5.Name = "pic_Status5";
            this.pic_Status5.Size = new System.Drawing.Size(19, 23);
            this.pic_Status5.TabIndex = 4;
            this.pic_Status5.TabStop = false;
            this.pic_Status5.Visible = false;
            // 
            // pic_Status4
            // 
            this.pic_Status4.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status4.Location = new System.Drawing.Point(70, 4);
            this.pic_Status4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status4.Name = "pic_Status4";
            this.pic_Status4.Size = new System.Drawing.Size(19, 23);
            this.pic_Status4.TabIndex = 3;
            this.pic_Status4.TabStop = false;
            this.pic_Status4.Visible = false;
            // 
            // pic_Status3
            // 
            this.pic_Status3.BackColor = System.Drawing.Color.Red;
            this.pic_Status3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status3.Location = new System.Drawing.Point(50, 4);
            this.pic_Status3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status3.Name = "pic_Status3";
            this.pic_Status3.Size = new System.Drawing.Size(19, 23);
            this.pic_Status3.TabIndex = 2;
            this.pic_Status3.TabStop = false;
            this.pic_Status3.Visible = false;
            // 
            // pic_Status2
            // 
            this.pic_Status2.BackColor = System.Drawing.Color.Red;
            this.pic_Status2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status2.Location = new System.Drawing.Point(30, 4);
            this.pic_Status2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status2.Name = "pic_Status2";
            this.pic_Status2.Size = new System.Drawing.Size(19, 23);
            this.pic_Status2.TabIndex = 1;
            this.pic_Status2.TabStop = false;
            this.pic_Status2.Visible = false;
            // 
            // pic_Status1
            // 
            this.pic_Status1.BackColor = System.Drawing.Color.Red;
            this.pic_Status1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status1.Location = new System.Drawing.Point(10, 4);
            this.pic_Status1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status1.Name = "pic_Status1";
            this.pic_Status1.Size = new System.Drawing.Size(19, 23);
            this.pic_Status1.TabIndex = 0;
            this.pic_Status1.TabStop = false;
            this.pic_Status1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.TextBox_ShutterSpeed);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.lbShutter);
            this.panel1.Controls.Add(this.Track_ShutterSpeed);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(114, 554);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(641, 34);
            this.panel1.TabIndex = 6;
            // 
            // TextBox_ShutterSpeed
            // 
            this.TextBox_ShutterSpeed.BackColor = System.Drawing.Color.Black;
            this.TextBox_ShutterSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox_ShutterSpeed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_ShutterSpeed.ForeColor = System.Drawing.Color.Lime;
            this.TextBox_ShutterSpeed.Location = new System.Drawing.Point(444, 8);
            this.TextBox_ShutterSpeed.MinimumSize = new System.Drawing.Size(70, 18);
            this.TextBox_ShutterSpeed.Name = "TextBox_ShutterSpeed";
            this.TextBox_ShutterSpeed.Size = new System.Drawing.Size(70, 15);
            this.TextBox_ShutterSpeed.TabIndex = 21;
            this.TextBox_ShutterSpeed.Text = "0.00";
            this.TextBox_ShutterSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextBox_ShutterSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnShutterSpeedTextBox_KeyUp);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label17.Location = new System.Drawing.Point(512, -2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 24);
            this.label17.TabIndex = 17;
            this.label17.Text = "ms";
            this.label17.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbShutter
            // 
            this.lbShutter.BackColor = System.Drawing.Color.Black;
            this.lbShutter.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShutter.ForeColor = System.Drawing.Color.Lime;
            this.lbShutter.Location = new System.Drawing.Point(566, 4);
            this.lbShutter.Name = "lbShutter";
            this.lbShutter.Size = new System.Drawing.Size(70, 26);
            this.lbShutter.TabIndex = 16;
            this.lbShutter.Text = "10.000";
            this.lbShutter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbShutter.Visible = false;
            // 
            // Track_ShutterSpeed
            // 
            this.Track_ShutterSpeed.Location = new System.Drawing.Point(93, 4);
            this.Track_ShutterSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Track_ShutterSpeed.Maximum = 30000;
            this.Track_ShutterSpeed.Name = "Track_ShutterSpeed";
            this.Track_ShutterSpeed.Size = new System.Drawing.Size(347, 45);
            this.Track_ShutterSpeed.TabIndex = 2;
            this.Track_ShutterSpeed.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Track_ShutterSpeed.Scroll += new System.EventHandler(this.Track_ShutterSpeed_Scroll);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(8, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Shutter speed";
            // 
            // panel_image
            // 
            this.panel_image.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_image.AutoScroll = true;
            this.panel_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_image.Controls.Add(this.pic_Camera);
            this.panel_image.Location = new System.Drawing.Point(117, 36);
            this.panel_image.Margin = new System.Windows.Forms.Padding(0);
            this.panel_image.Name = "panel_image";
            this.panel_image.Size = new System.Drawing.Size(512, 512);
            this.panel_image.TabIndex = 0;
            // 
            // pic_Camera
            // 
            this.pic_Camera.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pic_Camera.BackColor = System.Drawing.Color.Transparent;
            this.pic_Camera.Location = new System.Drawing.Point(0, 0);
            this.pic_Camera.Margin = new System.Windows.Forms.Padding(0);
            this.pic_Camera.Name = "pic_Camera";
            this.pic_Camera.Size = new System.Drawing.Size(512, 512);
            this.pic_Camera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Camera.TabIndex = 0;
            this.pic_Camera.TabStop = false;
            // 
            // panel_Info
            // 
            this.panel_Info.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Info.BackColor = System.Drawing.Color.White;
            this.panel_Info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Info.Controls.Add(this.lbFPS);
            this.panel_Info.Controls.Add(this.lbCurrentSpotNo);
            this.panel_Info.Controls.Add(this.lbCurrentMode);
            this.panel_Info.Controls.Add(this.lbTolerance);
            this.panel_Info.Controls.Add(this.label12);
            this.panel_Info.Controls.Add(this.label11);
            this.panel_Info.Controls.Add(this.label10);
            this.panel_Info.Controls.Add(this.lbFPSb);
            this.panel_Info.Location = new System.Drawing.Point(639, 36);
            this.panel_Info.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Info.Name = "panel_Info";
            this.panel_Info.Size = new System.Drawing.Size(116, 512);
            this.panel_Info.TabIndex = 8;
            // 
            // lbFPS
            // 
            this.lbFPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbFPS.Location = new System.Drawing.Point(42, 9);
            this.lbFPS.Name = "lbFPS";
            this.lbFPS.Size = new System.Drawing.Size(62, 19);
            this.lbFPS.TabIndex = 10;
            this.lbFPS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentSpotNo
            // 
            this.lbCurrentSpotNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCurrentSpotNo.Location = new System.Drawing.Point(78, 50);
            this.lbCurrentSpotNo.Name = "lbCurrentSpotNo";
            this.lbCurrentSpotNo.Size = new System.Drawing.Size(26, 19);
            this.lbCurrentSpotNo.TabIndex = 9;
            this.lbCurrentSpotNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentMode
            // 
            this.lbCurrentMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCurrentMode.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentMode.Location = new System.Drawing.Point(6, 112);
            this.lbCurrentMode.Name = "lbCurrentMode";
            this.lbCurrentMode.Size = new System.Drawing.Size(98, 37);
            this.lbCurrentMode.TabIndex = 8;
            this.lbCurrentMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTolerance
            // 
            this.lbTolerance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTolerance.Location = new System.Drawing.Point(6, 189);
            this.lbTolerance.Name = "lbTolerance";
            this.lbTolerance.Size = new System.Drawing.Size(98, 23);
            this.lbTolerance.TabIndex = 7;
            this.lbTolerance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(6, 169);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 15);
            this.label12.TabIndex = 3;
            this.label12.Text = "TOLERANCE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(6, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "MODE";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(6, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 15);
            this.label10.TabIndex = 1;
            this.label10.Text = "SPOT NO.";
            // 
            // lbFPSb
            // 
            this.lbFPSb.AutoSize = true;
            this.lbFPSb.ForeColor = System.Drawing.Color.Black;
            this.lbFPSb.Location = new System.Drawing.Point(6, 15);
            this.lbFPSb.Name = "lbFPSb";
            this.lbFPSb.Size = new System.Drawing.Size(30, 15);
            this.lbFPSb.TabIndex = 0;
            this.lbFPSb.Text = "FPS";
            // 
            // tab_LiveView
            // 
            this.tab_LiveView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tab_LiveView.Controls.Add(this.tabPage1);
            this.tab_LiveView.Controls.Add(this.tabPage2);
            this.tab_LiveView.Controls.Add(this.tabPage3);
            this.tab_LiveView.HotTrack = true;
            this.tab_LiveView.Location = new System.Drawing.Point(759, 45);
            this.tab_LiveView.Margin = new System.Windows.Forms.Padding(0);
            this.tab_LiveView.Name = "tab_LiveView";
            this.tab_LiveView.SelectedIndex = 0;
            this.tab_LiveView.Size = new System.Drawing.Size(393, 640);
            this.tab_LiveView.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.txtSpot3to1);
            this.tabPage1.Controls.Add(this.label30);
            this.tabPage1.Controls.Add(this.txtSpot2to3);
            this.tabPage1.Controls.Add(this.label31);
            this.tabPage1.Controls.Add(this.txtSpot1to2);
            this.tabPage1.Controls.Add(this.label32);
            this.tabPage1.Controls.Add(this.txtSpotXY3);
            this.tabPage1.Controls.Add(this.txtSpotY3);
            this.tabPage1.Controls.Add(this.txtSpotX3);
            this.tabPage1.Controls.Add(this.label18);
            this.tabPage1.Controls.Add(this.label19);
            this.tabPage1.Controls.Add(this.label20);
            this.tabPage1.Controls.Add(this.label21);
            this.tabPage1.Controls.Add(this.txtSpotXY2);
            this.tabPage1.Controls.Add(this.txtSpotY2);
            this.tabPage1.Controls.Add(this.txtSpotX2);
            this.tabPage1.Controls.Add(this.label22);
            this.tabPage1.Controls.Add(this.label23);
            this.tabPage1.Controls.Add(this.label24);
            this.tabPage1.Controls.Add(this.label25);
            this.tabPage1.Controls.Add(this.txtSpotXY1);
            this.tabPage1.Controls.Add(this.txtSpotY1);
            this.tabPage1.Controls.Add(this.txtSpotX1);
            this.tabPage1.Controls.Add(this.label26);
            this.tabPage1.Controls.Add(this.label27);
            this.tabPage1.Controls.Add(this.label28);
            this.tabPage1.Controls.Add(this.label29);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(385, 612);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "LIVE VIEW   ";
            // 
            // txtSpot3to1
            // 
            this.txtSpot3to1.Location = new System.Drawing.Point(119, 448);
            this.txtSpot3to1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpot3to1.Name = "txtSpot3to1";
            this.txtSpot3to1.Size = new System.Drawing.Size(80, 21);
            this.txtSpot3to1.TabIndex = 47;
            this.txtSpot3to1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label30.Location = new System.Drawing.Point(39, 450);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(73, 16);
            this.label30.TabIndex = 46;
            this.label30.Text = "SPOT 3-1 :";
            // 
            // txtSpot2to3
            // 
            this.txtSpot2to3.Location = new System.Drawing.Point(119, 399);
            this.txtSpot2to3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpot2to3.Name = "txtSpot2to3";
            this.txtSpot2to3.Size = new System.Drawing.Size(80, 21);
            this.txtSpot2to3.TabIndex = 45;
            this.txtSpot2to3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label31.Location = new System.Drawing.Point(39, 401);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(73, 16);
            this.label31.TabIndex = 44;
            this.label31.Text = "SPOT 2-3 :";
            // 
            // txtSpot1to2
            // 
            this.txtSpot1to2.Location = new System.Drawing.Point(119, 347);
            this.txtSpot1to2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpot1to2.Name = "txtSpot1to2";
            this.txtSpot1to2.Size = new System.Drawing.Size(80, 21);
            this.txtSpot1to2.TabIndex = 43;
            this.txtSpot1to2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(39, 349);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(73, 16);
            this.label32.TabIndex = 42;
            this.label32.Text = "SPOT 1-2 :";
            // 
            // txtSpotXY3
            // 
            this.txtSpotXY3.Location = new System.Drawing.Point(290, 231);
            this.txtSpotXY3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotXY3.Name = "txtSpotXY3";
            this.txtSpotXY3.Size = new System.Drawing.Size(80, 21);
            this.txtSpotXY3.TabIndex = 41;
            this.txtSpotXY3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotY3
            // 
            this.txtSpotY3.Location = new System.Drawing.Point(161, 231);
            this.txtSpotY3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotY3.Name = "txtSpotY3";
            this.txtSpotY3.Size = new System.Drawing.Size(80, 21);
            this.txtSpotY3.TabIndex = 40;
            this.txtSpotY3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotX3
            // 
            this.txtSpotX3.Location = new System.Drawing.Point(44, 231);
            this.txtSpotX3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotX3.Name = "txtSpotX3";
            this.txtSpotX3.Size = new System.Drawing.Size(80, 21);
            this.txtSpotX3.TabIndex = 39;
            this.txtSpotX3.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label18.Location = new System.Drawing.Point(249, 234);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(36, 16);
            this.label18.TabIndex = 38;
            this.label18.Text = "X-Y :";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label19.Location = new System.Drawing.Point(131, 234);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(25, 16);
            this.label19.TabIndex = 37;
            this.label19.Text = "Y :";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label20.Location = new System.Drawing.Point(16, 234);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(23, 16);
            this.label20.TabIndex = 36;
            this.label20.Text = "X :";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.BackColor = System.Drawing.SystemColors.Window;
            this.label21.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label21.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label21.Location = new System.Drawing.Point(12, 204);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 16);
            this.label21.TabIndex = 35;
            this.label21.Text = "SPOT 3";
            // 
            // txtSpotXY2
            // 
            this.txtSpotXY2.Location = new System.Drawing.Point(290, 153);
            this.txtSpotXY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotXY2.Name = "txtSpotXY2";
            this.txtSpotXY2.Size = new System.Drawing.Size(80, 21);
            this.txtSpotXY2.TabIndex = 34;
            this.txtSpotXY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotY2
            // 
            this.txtSpotY2.Location = new System.Drawing.Point(161, 153);
            this.txtSpotY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotY2.Name = "txtSpotY2";
            this.txtSpotY2.Size = new System.Drawing.Size(80, 21);
            this.txtSpotY2.TabIndex = 33;
            this.txtSpotY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotX2
            // 
            this.txtSpotX2.Location = new System.Drawing.Point(44, 153);
            this.txtSpotX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotX2.Name = "txtSpotX2";
            this.txtSpotX2.Size = new System.Drawing.Size(80, 21);
            this.txtSpotX2.TabIndex = 32;
            this.txtSpotX2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label22.Location = new System.Drawing.Point(249, 155);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(36, 16);
            this.label22.TabIndex = 31;
            this.label22.Text = "X-Y :";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label23.Location = new System.Drawing.Point(131, 155);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(25, 16);
            this.label23.TabIndex = 30;
            this.label23.Text = "Y :";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label24.Location = new System.Drawing.Point(16, 155);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(23, 16);
            this.label24.TabIndex = 29;
            this.label24.Text = "X :";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.BackColor = System.Drawing.SystemColors.Window;
            this.label25.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label25.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label25.Location = new System.Drawing.Point(12, 120);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(54, 16);
            this.label25.TabIndex = 28;
            this.label25.Text = "SPOT 2";
            // 
            // txtSpotXY1
            // 
            this.txtSpotXY1.Location = new System.Drawing.Point(290, 72);
            this.txtSpotXY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotXY1.Name = "txtSpotXY1";
            this.txtSpotXY1.Size = new System.Drawing.Size(80, 21);
            this.txtSpotXY1.TabIndex = 27;
            this.txtSpotXY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotY1
            // 
            this.txtSpotY1.Location = new System.Drawing.Point(161, 72);
            this.txtSpotY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotY1.Name = "txtSpotY1";
            this.txtSpotY1.Size = new System.Drawing.Size(80, 21);
            this.txtSpotY1.TabIndex = 26;
            this.txtSpotY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSpotX1
            // 
            this.txtSpotX1.Location = new System.Drawing.Point(44, 72);
            this.txtSpotX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSpotX1.Name = "txtSpotX1";
            this.txtSpotX1.Size = new System.Drawing.Size(80, 21);
            this.txtSpotX1.TabIndex = 25;
            this.txtSpotX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label26.Location = new System.Drawing.Point(249, 74);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(36, 16);
            this.label26.TabIndex = 24;
            this.label26.Text = "X-Y :";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label27.Location = new System.Drawing.Point(131, 74);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(25, 16);
            this.label27.TabIndex = 23;
            this.label27.Text = "Y :";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(16, 74);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(23, 16);
            this.label28.TabIndex = 22;
            this.label28.Text = "X :";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.SystemColors.Window;
            this.label29.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label29.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label29.Location = new System.Drawing.Point(12, 46);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 16);
            this.label29.TabIndex = 21;
            this.label29.Text = "SPOT 1";
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Orange;
            this.label8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 296);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(382, 22);
            this.label8.TabIndex = 1;
            this.label8.Text = "Relative angle";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.Orange;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(0, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(382, 22);
            this.label7.TabIndex = 0;
            this.label7.Text = "Absolute angle";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.White;
            this.tabPage2.Controls.Add(this.cmdWobbleFlattingClear);
            this.tabPage2.Controls.Add(this.cmdPlotStartStop);
            this.tabPage2.Controls.Add(this.cbPlotSpotNo);
            this.tabPage2.Controls.Add(this.label14);
            this.tabPage2.Controls.Add(this.panel_Plot);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(0);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(385, 612);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "X-Y PLOT(WOBBLE, FLATNESS)";
            // 
            // cmdWobbleFlattingClear
            // 
            this.cmdWobbleFlattingClear.BackColor = System.Drawing.Color.White;
            this.cmdWobbleFlattingClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdWobbleFlattingClear.Location = new System.Drawing.Point(238, 546);
            this.cmdWobbleFlattingClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdWobbleFlattingClear.Name = "cmdWobbleFlattingClear";
            this.cmdWobbleFlattingClear.Size = new System.Drawing.Size(111, 42);
            this.cmdWobbleFlattingClear.TabIndex = 9;
            this.cmdWobbleFlattingClear.Text = "CLEAR";
            this.cmdWobbleFlattingClear.UseVisualStyleBackColor = false;
            this.cmdWobbleFlattingClear.Click += new System.EventHandler(this.cmdWobbleFlattingClear_Click);
            // 
            // cmdPlotStartStop
            // 
            this.cmdPlotStartStop.BackColor = System.Drawing.Color.White;
            this.cmdPlotStartStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPlotStartStop.Location = new System.Drawing.Point(238, 480);
            this.cmdPlotStartStop.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdPlotStartStop.Name = "cmdPlotStartStop";
            this.cmdPlotStartStop.Size = new System.Drawing.Size(111, 42);
            this.cmdPlotStartStop.TabIndex = 8;
            this.cmdPlotStartStop.Text = "START";
            this.cmdPlotStartStop.UseVisualStyleBackColor = false;
            this.cmdPlotStartStop.Click += new System.EventHandler(this.cmdPlotStartStop_Click);
            // 
            // cbPlotSpotNo
            // 
            this.cbPlotSpotNo.FormattingEnabled = true;
            this.cbPlotSpotNo.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbPlotSpotNo.Location = new System.Drawing.Point(85, 480);
            this.cbPlotSpotNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbPlotSpotNo.Name = "cbPlotSpotNo";
            this.cbPlotSpotNo.Size = new System.Drawing.Size(93, 23);
            this.cbPlotSpotNo.TabIndex = 3;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(15, 484);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(63, 15);
            this.label14.TabIndex = 2;
            this.label14.Text = "SPOT NO.";
            // 
            // panel_Plot
            // 
            this.panel_Plot.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Plot.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Plot.Controls.Add(this.pic_Plot);
            this.panel_Plot.Location = new System.Drawing.Point(20, 12);
            this.panel_Plot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Plot.Name = "panel_Plot";
            this.panel_Plot.Size = new System.Drawing.Size(341, 427);
            this.panel_Plot.TabIndex = 0;
            // 
            // pic_Plot
            // 
            this.pic_Plot.BackColor = System.Drawing.Color.White;
            this.pic_Plot.Location = new System.Drawing.Point(0, 0);
            this.pic_Plot.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Plot.Name = "pic_Plot";
            this.pic_Plot.Size = new System.Drawing.Size(342, 428);
            this.pic_Plot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Plot.TabIndex = 0;
            this.pic_Plot.TabStop = false;
            // 
            // tabPage3
            // 
            this.tabPage3.BackColor = System.Drawing.Color.White;
            this.tabPage3.Controls.Add(this.panel2);
            this.tabPage3.Controls.Add(this.txtSerialSpotXY2);
            this.tabPage3.Controls.Add(this.txtSerialSpotY2);
            this.tabPage3.Controls.Add(this.txtSerialSpotX2);
            this.tabPage3.Controls.Add(this.label2);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Controls.Add(this.label4);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.txtSerialSpotXY1);
            this.tabPage3.Controls.Add(this.txtSerialSpotY1);
            this.tabPage3.Controls.Add(this.txtSerialSpotX1);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label33);
            this.tabPage3.Controls.Add(this.label34);
            this.tabPage3.Controls.Add(this.label35);
            this.tabPage3.Controls.Add(this.txtStartingNo);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.txtPartName);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(385, 612);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "   SERIAL  ";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.cmdSnapDataClear);
            this.panel2.Controls.Add(this.cbSerialSpotNo);
            this.panel2.Controls.Add(this.label36);
            this.panel2.Controls.Add(this.txtCurrentPartName);
            this.panel2.Controls.Add(this.gridSerialData);
            this.panel2.Controls.Add(this.cmdSnapMeasurement);
            this.panel2.Controls.Add(this.cmdDataExport);
            this.panel2.Location = new System.Drawing.Point(11, 181);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(371, 412);
            this.panel2.TabIndex = 50;
            // 
            // cmdSnapDataClear
            // 
            this.cmdSnapDataClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdSnapDataClear.Location = new System.Drawing.Point(128, 367);
            this.cmdSnapDataClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdSnapDataClear.Name = "cmdSnapDataClear";
            this.cmdSnapDataClear.Size = new System.Drawing.Size(114, 41);
            this.cmdSnapDataClear.TabIndex = 54;
            this.cmdSnapDataClear.Text = "CLEAR DATA";
            this.cmdSnapDataClear.UseVisualStyleBackColor = true;
            this.cmdSnapDataClear.Click += new System.EventHandler(this.cmdSnapDataClear_Click);
            // 
            // cbSerialSpotNo
            // 
            this.cbSerialSpotNo.FormattingEnabled = true;
            this.cbSerialSpotNo.Items.AddRange(new object[] {
            "1",
            "2"});
            this.cbSerialSpotNo.Location = new System.Drawing.Point(285, 21);
            this.cbSerialSpotNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSerialSpotNo.Name = "cbSerialSpotNo";
            this.cbSerialSpotNo.Size = new System.Drawing.Size(72, 23);
            this.cbSerialSpotNo.TabIndex = 52;
            this.cbSerialSpotNo.SelectedIndexChanged += new System.EventHandler(this.cbSerialSpotNo_SelectedIndexChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(215, 25);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(63, 15);
            this.label36.TabIndex = 51;
            this.label36.Text = "SPOT NO.";
            // 
            // txtCurrentPartName
            // 
            this.txtCurrentPartName.BackColor = System.Drawing.Color.White;
            this.txtCurrentPartName.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCurrentPartName.Enabled = false;
            this.txtCurrentPartName.Location = new System.Drawing.Point(8, 21);
            this.txtCurrentPartName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCurrentPartName.Name = "txtCurrentPartName";
            this.txtCurrentPartName.Size = new System.Drawing.Size(144, 21);
            this.txtCurrentPartName.TabIndex = 50;
            this.txtCurrentPartName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // gridSerialData
            // 
            this.gridSerialData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.gridSerialData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.gridSerialData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridSerialData.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.gridSerialData.Location = new System.Drawing.Point(6, 56);
            this.gridSerialData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridSerialData.Name = "gridSerialData";
            this.gridSerialData.RowHeadersVisible = false;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.gridSerialData.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.gridSerialData.RowTemplate.Height = 23;
            this.gridSerialData.Size = new System.Drawing.Size(360, 304);
            this.gridSerialData.TabIndex = 49;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "NO.";
            this.Column1.Name = "Column1";
            this.Column1.Width = 40;
            // 
            // Column2
            // 
            this.Column2.HeaderText = "X";
            this.Column2.Name = "Column2";
            this.Column2.Width = 70;
            // 
            // Column3
            // 
            this.Column3.HeaderText = "Y";
            this.Column3.Name = "Column3";
            this.Column3.Width = 70;
            // 
            // Column4
            // 
            this.Column4.HeaderText = "X-Y";
            this.Column4.Name = "Column4";
            this.Column4.Width = 70;
            // 
            // Column5
            // 
            this.Column5.HeaderText = "RE.";
            this.Column5.Name = "Column5";
            this.Column5.Width = 50;
            // 
            // Column6
            // 
            this.Column6.HeaderText = "STATUS";
            this.Column6.Name = "Column6";
            this.Column6.Width = 60;
            // 
            // cmdSnapMeasurement
            // 
            this.cmdSnapMeasurement.Location = new System.Drawing.Point(9, 367);
            this.cmdSnapMeasurement.Name = "cmdSnapMeasurement";
            this.cmdSnapMeasurement.Size = new System.Drawing.Size(114, 41);
            this.cmdSnapMeasurement.TabIndex = 53;
            this.cmdSnapMeasurement.Text = "SNAP MEASUREMENT";
            this.cmdSnapMeasurement.UseVisualStyleBackColor = true;
            this.cmdSnapMeasurement.Click += new System.EventHandler(this.cmdSnapMeasurement_Click);
            // 
            // cmdDataExport
            // 
            this.cmdDataExport.Location = new System.Drawing.Point(246, 367);
            this.cmdDataExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdDataExport.Name = "cmdDataExport";
            this.cmdDataExport.Size = new System.Drawing.Size(114, 41);
            this.cmdDataExport.TabIndex = 55;
            this.cmdDataExport.Text = "DATA EXPORT";
            this.cmdDataExport.UseVisualStyleBackColor = true;
            this.cmdDataExport.Click += new System.EventHandler(this.cmdDataExport_Click);
            // 
            // txtSerialSpotXY2
            // 
            this.txtSerialSpotXY2.Location = new System.Drawing.Point(288, 152);
            this.txtSerialSpotXY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotXY2.Name = "txtSerialSpotXY2";
            this.txtSerialSpotXY2.Size = new System.Drawing.Size(80, 21);
            this.txtSerialSpotXY2.TabIndex = 48;
            this.txtSerialSpotXY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotY2
            // 
            this.txtSerialSpotY2.Location = new System.Drawing.Point(155, 152);
            this.txtSerialSpotY2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotY2.Name = "txtSerialSpotY2";
            this.txtSerialSpotY2.Size = new System.Drawing.Size(80, 21);
            this.txtSerialSpotY2.TabIndex = 47;
            this.txtSerialSpotY2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotX2
            // 
            this.txtSerialSpotX2.Location = new System.Drawing.Point(40, 152);
            this.txtSerialSpotX2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotX2.Name = "txtSerialSpotX2";
            this.txtSerialSpotX2.Size = new System.Drawing.Size(80, 21);
            this.txtSerialSpotX2.TabIndex = 46;
            this.txtSerialSpotX2.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label2.Location = new System.Drawing.Point(249, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(36, 16);
            this.label2.TabIndex = 45;
            this.label2.Text = "X-Y :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label3.Location = new System.Drawing.Point(127, 154);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 16);
            this.label3.TabIndex = 44;
            this.label3.Text = "Y :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label4.Location = new System.Drawing.Point(12, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 16);
            this.label4.TabIndex = 43;
            this.label4.Text = "X :";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.Window;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label15.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label15.Location = new System.Drawing.Point(9, 126);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 16);
            this.label15.TabIndex = 42;
            this.label15.Text = "SPOT 2";
            // 
            // txtSerialSpotXY1
            // 
            this.txtSerialSpotXY1.Location = new System.Drawing.Point(288, 84);
            this.txtSerialSpotXY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotXY1.Name = "txtSerialSpotXY1";
            this.txtSerialSpotXY1.Size = new System.Drawing.Size(80, 21);
            this.txtSerialSpotXY1.TabIndex = 41;
            this.txtSerialSpotXY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotY1
            // 
            this.txtSerialSpotY1.Location = new System.Drawing.Point(155, 84);
            this.txtSerialSpotY1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotY1.Name = "txtSerialSpotY1";
            this.txtSerialSpotY1.Size = new System.Drawing.Size(80, 21);
            this.txtSerialSpotY1.TabIndex = 40;
            this.txtSerialSpotY1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSerialSpotX1
            // 
            this.txtSerialSpotX1.Location = new System.Drawing.Point(40, 84);
            this.txtSerialSpotX1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSerialSpotX1.Name = "txtSerialSpotX1";
            this.txtSerialSpotX1.Size = new System.Drawing.Size(80, 21);
            this.txtSerialSpotX1.TabIndex = 39;
            this.txtSerialSpotX1.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label16.Location = new System.Drawing.Point(249, 86);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(36, 16);
            this.label16.TabIndex = 38;
            this.label16.Text = "X-Y :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(127, 86);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(25, 16);
            this.label33.TabIndex = 37;
            this.label33.Text = "Y :";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(12, 86);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(23, 16);
            this.label34.TabIndex = 36;
            this.label34.Text = "X :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.BackColor = System.Drawing.SystemColors.Window;
            this.label35.Font = new System.Drawing.Font("Arial", 9.75F);
            this.label35.ForeColor = System.Drawing.SystemColors.WindowText;
            this.label35.Location = new System.Drawing.Point(9, 58);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 35;
            this.label35.Text = "SPOT 1";
            // 
            // txtStartingNo
            // 
            this.txtStartingNo.Location = new System.Drawing.Point(294, 20);
            this.txtStartingNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtStartingNo.Name = "txtStartingNo";
            this.txtStartingNo.Size = new System.Drawing.Size(51, 21);
            this.txtStartingNo.TabIndex = 6;
            this.txtStartingNo.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(201, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(86, 15);
            this.label13.TabIndex = 5;
            this.label13.Text = "STARTING NO";
            // 
            // txtPartName
            // 
            this.txtPartName.Location = new System.Drawing.Point(87, 20);
            this.txtPartName.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtPartName.Name = "txtPartName";
            this.txtPartName.Size = new System.Drawing.Size(98, 21);
            this.txtPartName.TabIndex = 4;
            this.txtPartName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(8, 25);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(76, 15);
            this.label9.TabIndex = 3;
            this.label9.Text = "PART NAME ";
            // 
            // SaveProgressbar
            // 
            this.SaveProgressbar.Location = new System.Drawing.Point(346, 9);
            this.SaveProgressbar.Name = "SaveProgressbar";
            this.SaveProgressbar.Size = new System.Drawing.Size(667, 20);
            this.SaveProgressbar.Step = 1;
            this.SaveProgressbar.TabIndex = 10;
            this.SaveProgressbar.Visible = false;
            // 
            // txtPixelPerDistance
            // 
            this.txtPixelPerDistance.Location = new System.Drawing.Point(131, 10);
            this.txtPixelPerDistance.Name = "txtPixelPerDistance";
            this.txtPixelPerDistance.Size = new System.Drawing.Size(74, 21);
            this.txtPixelPerDistance.TabIndex = 34;
            this.txtPixelPerDistance.Text = "4.8";
            this.txtPixelPerDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // imageListButton
            // 
            this.imageListButton.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButton.ImageStream")));
            this.imageListButton.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListButton.Images.SetKeyName(0, "icoCapture.png");
            this.imageListButton.Images.SetKeyName(1, "icoExport.png");
            this.imageListButton.Images.SetKeyName(2, "icoOpen.png");
            this.imageListButton.Images.SetKeyName(3, "IcoSave.png");
            this.imageListButton.Images.SetKeyName(4, "icoStart.png");
            this.imageListButton.Images.SetKeyName(5, "icoStop.png");
            this.imageListButton.Images.SetKeyName(6, "icoTrashcan.png");
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1157, 711);
            this.Controls.Add(this.SaveProgressbar);
            this.Controls.Add(this.tab_LiveView);
            this.Controls.Add(this.panel_Info);
            this.Controls.Add(this.panel_image);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel_Status);
            this.Controls.Add(this.panel_Set);
            this.Controls.Add(this.panel_Icon);
            this.Controls.Add(this.picWikiLogo);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.txtPixelPerDistance);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximumSize = new System.Drawing.Size(1173, 750);
            this.MinimumSize = new System.Drawing.Size(1173, 750);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "* WikiOptics Autocollimator *";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).EndInit();
            this.panel_Icon.ResumeLayout(false);
            this.panel_Icon.PerformLayout();
            this.panel_Set.ResumeLayout(false);
            this.panel_Set.PerformLayout();
            this.panel_Status.ResumeLayout(false);
            this.panel_Status.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Track_ShutterSpeed)).EndInit();
            this.panel_image.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Camera)).EndInit();
            this.panel_Info.ResumeLayout(false);
            this.panel_Info.PerformLayout();
            this.tab_LiveView.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.panel_Plot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Plot)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridSerialData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ImageList imageListButton;
        private TextBox txtPixelPerDistance;

        MenuStrip menuBar;
        ToolStripMenuItem menu_Measurement;
        ToolStripMenuItem menu_Tools;
        ToolStripMenuItem menu_Settings;
        ToolStripMenuItem menu_Tools_ZeroSet;
        ToolStripMenuItem menu_Tools_Mode;
        ToolStripMenuItem menu_Tools_Angledefinition;
        ToolStripMenuItem menu_Tools_Coordinate;
        ToolStripMenuItem menu_Tools_Numbering;
        ToolStripMenuItem menu_Setting_Unit;
        ToolStripMenuItem menu_Setting_Tolerance;
        ToolStripMenuItem menu_Setting_GrayLevel;
        ToolStripMenuItem menu_Information;
        //ToolStripMenuItem menu_Information_About;

        ToolStripMenuItem menu_Tools_Mode_Area;
        ToolStripMenuItem menu_Tools_Mode_Brightness;
        ToolStripMenuItem menu_Tools_Mode_Peak;

        ToolStripMenuItem menu_Tools_Angle_Ext;
        ToolStripMenuItem menu_Tools_Angle_Int;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring;
        ToolStripMenuItem menu_Tools_Numbring_Angle;
        ToolStripMenuItem menu_Tools_Numbring_Size;
        ToolStripMenuItem menu_Settings_Unit_Degree;
        ToolStripMenuItem menu_Settings_Unit_Sec;
        ToolStripMenuItem menu_Settings_Tolerance_Circle;
        ToolStripMenuItem menu_Settings_Tolerance_Square;

        ToolStripMenuItem menu_Tools_Zoom;
        ToolStripMenuItem menu_Tools_Zoom_4;
        ToolStripMenuItem menu_Tools_Zoom_8;
        ToolStripMenuItem menu_Tools_Zoom_16;

        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Xon;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Yon;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Off;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_L90;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_R90;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_Off;
        ToolStripMenuItem menu_Tools_Zoom_Off;
        ToolStripMenuItem menu_Tools_Zoom_32;
        ToolStripMenuItem menu_Tools_Coordinate_Original;
        ToolStripMenuItem menu_Settings_Tolerance_Value;
        Label label2;
        Label label3;
        Label label4;
        Label label5;
        Label label6;
        Label label7;
        Label label8;
        Label label9;
        Label label10;
        Label label11;
        Label label12;
        Label label13;
        Label label14;
        Label label15;
        Label label16;
        Label label17;
        Label label18;
        Label label19;
        Label label20;
        Label label21;
        Label label22;
        Label label23;
        Label label24;
        Label label25;
        Label label26;
        Label label27;
        Label label28;
        Label label29;
        Label label30;
        Label label31;
        Label label32;
        Label label33;
        Label label34;
        Label label35;
        Label label37;

        Label lbCurrentSpotNo;
        Label lbCurrentMode;
        Label lbTolerance;
        Label lbFPSb;
        Label lbFPS;
        Label lbShutter;
        Label lbStatusDescription;
        Label lbStatusResult;

        PictureBox pic_Status1;
        PictureBox pic_Status2;
        PictureBox pic_Status3;
        PictureBox pic_Status4;
        PictureBox pic_Status5;
        PictureBox pic_Status6;
        PictureBox pic_Status7;
        PictureBox pic_Status8;
        PictureBox pic_Status9;
        PictureBox pic_Status10;
        PictureBox pic_Status11;
        PictureBox pic_Status12;
        PictureBox pic_Status13;
        PictureBox pic_Status14;
        PictureBox pic_Status15;

        PictureBox pic_Camera;
        PictureBox pic_Plot;

        Panel panel1;
        Panel panel2;
        //Panel panel3;
        //Panel panel4;
        //Panel panel5;

        Panel panel_Status;
        Panel panel_image;
        Panel panel_Info;
        Panel panel_Icon;
        Panel panel_Set;
        Panel panel_Plot;

        Button cmdFactorySet;
        Button cmdAnalysis;
        Button btn_Capture;
        Button cmdClear;
        Button cmdPrint;
        Button cmdExport;
        Button btn_SettingFileSave;
        Button btn_SettingFileOpen;
        Button cmdMeasureData;
        Button cmdWobbleFlattingClear;
        Button cmdPlotStartStop;

        TabControl tab_LiveView;
        TabPage tabPage1;
        TabPage tabPage2;
        TabPage tabPage3;

        TextBox txtSamplingFreq;
        TextBox txtSpot3to1;
        TextBox txtSpot2to3;
        TextBox txtSpot1to2;
        TextBox txtSpotXY3;
        TextBox txtSpotY3;
        TextBox txtSpotX3;
        TextBox txtSpotXY2;
        TextBox txtSpotX2;
        TextBox txtSpotY2;
        TextBox txtSpotXY1;
        TextBox txtSpotX1;
        TextBox txtSpotY1;

        TextBox txtSerialSpotXY1;
        TextBox txtSerialSpotX1;
        TextBox txtSerialSpotY1;

        TextBox txtSerialSpotXY2;
        TextBox txtSerialSpotX2;
        TextBox txtSerialSpotY2;

        TextBox txtCurrentPartName;

        TextBox txtStartingNo;
        TextBox txtPartName;

        ComboBox cbPlotSpotNo;
        ComboBox cbSerialSpotNo;

        TrackBar Track_ShutterSpeed;

        ProgressBar SaveProgressbar;

        DataGridView gridSerialData;
        DataGridViewTextBoxColumn Column1;
        DataGridViewTextBoxColumn Column2;
        DataGridViewTextBoxColumn Column3;
        DataGridViewTextBoxColumn Column4;
        DataGridViewTextBoxColumn Column5;
        DataGridViewTextBoxColumn Column6;

        PictureBox picWikiLogo;

        private TextBox TextBox_ShutterSpeed;
        private Button SaveAllConfig;

        private Button cmdDataExport;
        private Button cmdSnapDataClear;
        private Button cmdSnapMeasurement;
        private Label label36;
        private Button AutoShutter;
        private Button InstallSerialDriver;

        #endregion

        private ComboBox comboBox_serialport;
        private Label label1;
    }
}

