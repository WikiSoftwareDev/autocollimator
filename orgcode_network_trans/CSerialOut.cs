﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Xml;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get
            {
                return Encoding.UTF8;
            }
        }
    }

    public class CSerialOut : IDisposable
    {
        bool m_bTerminateReceive = false;
        SerialPort m_ResultOut = new SerialPort();

        StringBuilder received_buffer = new StringBuilder(512);

        List<double> m_xt = new List<double>();
        List<double> m_yt = new List<double>();
        List<double> m_xyt = new List<double>();

        object lock_obj = new object();

        public void Dispose()
        {
            // call dispose on the context and any of its members here
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // Dispose managed resources.
                if (m_ResultOut != null)
                {
                    m_ResultOut.Dispose();
                    m_ResultOut = null;
                }
            }
            // Free native resources.
        }

        public int OpenPort(string port_name, int speed)
        {
            if (true == m_ResultOut.IsOpen)
            {
                m_ResultOut.DataReceived -= new SerialDataReceivedEventHandler(Serial_DataReceivedEventHandler);
                m_ResultOut.Close();
            }

            m_ResultOut.PortName = port_name;
            //m_ResultOut.BaudRate = 9600;
            m_ResultOut.BaudRate = speed;
            m_ResultOut.Parity = Parity.None;
            m_ResultOut.DataBits = 8;
            m_ResultOut.StopBits = StopBits.One;
            m_ResultOut.DataReceived += new SerialDataReceivedEventHandler(Serial_DataReceivedEventHandler);

            try
            {
                m_ResultOut.Open();
                m_ResultOut.DiscardInBuffer();
            }
            catch (Exception ex)
            {
                Trace.WriteLine("serial open exception : " + ex.ToString());                
                MessageBox.Show("serial open exception : " + ex.ToString());                
                return -1;
            }
            m_bTerminateReceive = false;
            return 1;
        }

        public void ClosePort()
        {
            m_bTerminateReceive = true;

            //m_ResultOut.DataReceived -= new SerialDataReceivedEventHandler(Serial_DataReceivedEventHandler);
            if (m_ResultOut.IsOpen == true)
            {
                m_ResultOut.Close();
            }            
        }

        // check "get\r\n\r\n"
        void Serial_DataReceivedEventHandler(object sender, SerialDataReceivedEventArgs e)
        {
            SerialPort port = (SerialPort)sender;

            if (m_bTerminateReceive == true)
            {
                return;
            }
            
            string received_data = port.ReadExisting();
            received_buffer.Append(received_data);
            int len = received_buffer.Length;
            if (len >= 7)
            {
                string temp = "get\r\n\r\n";
                bool bFound = false;
                received_data = received_buffer.ToString();
                while (len >= 7)
                {
                    bFound = received_data.Equals(temp);
                    if (true == bFound)
                    {
                        received_buffer.Clear();
                        received_data.Remove(0, temp.Length-1);
                        if( received_data.Length > 0 )
                            received_buffer.Append(received_data);
                        break;
                    }
                }
                
            }
        }

        public void PushResultBuffer(double[] xt, double[] yt, double[] xyt)
        {
            lock (lock_obj)
            {
                int array_len = xt.Length;
                if (array_len > 0)
                {
                    m_xt.Clear();
                    m_yt.Clear();
                    m_xyt.Clear();

                    m_xt = new List<double>(xt);
                    m_yt = new List<double>(yt);
                    m_xyt = new List<double>(xyt);
                }
            }
        }

        public void WriteResult()
        {
            lock (lock_obj)
            {
                if (
                    (m_xt.Count > 0)
                    &&
                    (m_yt.Count > 0)
                    &&
                    (m_xyt.Count > 0)
                    )
                {
                    double[] x_position = m_xt.ToArray();
                    double[] y_position = m_yt.ToArray();
                    double[] tilt_degree = m_xyt.ToArray();
                    WriteResult(x_position, y_position, tilt_degree);

                    m_xt.Clear();
                    m_yt.Clear();
                    m_xyt.Clear();
                }
                else
                {
                    m_xt.Clear();
                    m_yt.Clear();
                    m_xyt.Clear();
                }
            }
        }

        // for 2 point results
        /*
         * <?xml version="1.0" encoding="utf-8"?>
         * <results>
         *  <result>
         *      <xt></xt>
         *      <yt></yt>
         *      <xyt></xyt>
         *  </result>
         *  <result>
         *      <xt></xt>
         *      <yt></yt>
         *      <xyt></xyt>
         *  </result>  
         * </results>
         */
        public int WriteResult(double xt, double yt, double xyt)
        {
            if (false == m_ResultOut.IsOpen)
                return -1;

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;

            Utf8StringWriter writer = new Utf8StringWriter();
            {
                using (XmlWriter wr = XmlWriter.Create(writer, settings))
                {
                    wr.WriteStartDocument();
                    wr.WriteStartElement("results");

                    //for (int i = 0; i < array_len; i++)
                    {
                        wr.WriteStartElement("result");

                        wr.WriteElementString("xt", xt.ToString());
                        wr.WriteElementString("yt", yt.ToString());
                        wr.WriteElementString("xyt", xyt.ToString());

                        wr.WriteEndElement();
                    }

                    wr.WriteEndElement();
                    wr.WriteEndDocument();
                }
            }

            string szData = writer.ToString();
            Trace.WriteLine(szData);

            if (true == m_ResultOut.IsOpen)
            {
                int len = szData.Length;
                m_ResultOut.Write(szData);
                return len;
            }

            return 0;
        }

        public int WriteResult(double[] xt, double[] yt, double[] xyt)
        {
            int array_len = xt.Length;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.Encoding = Encoding.UTF8;

            Utf8StringWriter writer = new Utf8StringWriter();
            {
                using (XmlWriter wr = XmlWriter.Create(writer, settings))
                {
                    wr.WriteStartDocument();
                    wr.WriteStartElement("results");

                    for (int i = 0; i < array_len; i++)
                    {
                        wr.WriteStartElement("result");

                        wr.WriteElementString("xt", xt[i].ToString());
                        wr.WriteElementString("yt", yt[i].ToString());
                        wr.WriteElementString("xyt", xyt[i].ToString());

                        wr.WriteEndElement();
                    }

                    wr.WriteEndElement();
                    wr.WriteEndDocument();
                }
            }

            string szData = writer.ToString();
            Trace.WriteLine(szData);

            if (true == m_ResultOut.IsOpen)
            {
                int len = szData.Length;
                m_ResultOut.Write(szData);
                return len;
            }

            return 0;
        }
    }
}
