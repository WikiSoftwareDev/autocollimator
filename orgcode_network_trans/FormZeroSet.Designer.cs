﻿namespace WikiOptics_Collimator
{
    partial class FormZeroSet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();

            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCenterX = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cbSpotNo = new System.Windows.Forms.ComboBox();
            this.btn_Reset = new System.Windows.Forms.Button();
            this.btn_ZeroSet = new System.Windows.Forms.Button();
            this.txtCenterY = new System.Windows.Forms.TextBox();
            base.Load += new System.EventHandler(FrmZeroSet_Load);
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 16);
            this.label1.TabIndex = 29;
            this.label1.Text = "Spot No :";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(155, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 16);
            this.label6.TabIndex = 22;
            this.label6.Text = "pixel";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 69);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 16);
            this.label7.TabIndex = 20;
            this.label7.Text = "Center X :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(155, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(35, 16);
            this.label8.TabIndex = 25;
            this.label8.Text = "pixel";
            // 
            // txtCenterX
            // 
            this.txtCenterX.Location = new System.Drawing.Point(80, 65);
            this.txtCenterX.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCenterX.Name = "txtCenterX";
            this.txtCenterX.Size = new System.Drawing.Size(74, 22);
            this.txtCenterX.TabIndex = 21;
            this.txtCenterX.Text = "650";
            this.txtCenterX.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(12, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(67, 16);
            this.label9.TabIndex = 23;
            this.label9.Text = "Center Y :";
            // 
            // cbSpotNo
            // 
            this.cbSpotNo.FormattingEnabled = true;
            this.cbSpotNo.IntegralHeight = false;
            this.cbSpotNo.ItemHeight = 16;
            this.cbSpotNo.Items.AddRange(new object[] {
            "1",
            "2",
            "3"});
            this.cbSpotNo.Location = new System.Drawing.Point(80, 21);
            this.cbSpotNo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cbSpotNo.Name = "cbSpotNo";
            this.cbSpotNo.Size = new System.Drawing.Size(74, 24);
            this.cbSpotNo.TabIndex = 30;
            // 
            // btn_Reset
            // 
            this.btn_Reset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Reset.Location = new System.Drawing.Point(98, 153);
            this.btn_Reset.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Reset.Name = "btn_Reset";
            this.btn_Reset.Size = new System.Drawing.Size(78, 31);
            this.btn_Reset.TabIndex = 31;
            this.btn_Reset.Text = "RESET";
            this.btn_Reset.UseVisualStyleBackColor = true;
            this.btn_Reset.Click += new System.EventHandler(btn_Reset_Click);
            // 
            // btn_ZeroSet
            // 
            this.btn_ZeroSet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_ZeroSet.Location = new System.Drawing.Point(14, 153);
            this.btn_ZeroSet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_ZeroSet.Name = "btn_ZeroSet";
            this.btn_ZeroSet.Size = new System.Drawing.Size(78, 31);
            this.btn_ZeroSet.TabIndex = 28;
            this.btn_ZeroSet.Text = "SET";
            this.btn_ZeroSet.UseVisualStyleBackColor = true;
            this.btn_ZeroSet.Click += new System.EventHandler(btn_ZeroSet_Click);
            // 
            // txtCenterY
            // 
            this.txtCenterY.Location = new System.Drawing.Point(80, 104);
            this.txtCenterY.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtCenterY.Name = "txtCenterY";
            this.txtCenterY.Size = new System.Drawing.Size(74, 22);
            this.txtCenterY.TabIndex = 24;
            this.txtCenterY.Text = "512";
            this.txtCenterY.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // FormZeroSet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(197, 197);
            this.Controls.Add(this.txtCenterY);
            this.Controls.Add(this.btn_ZeroSet);
            this.Controls.Add(this.btn_Reset);
            this.Controls.Add(this.cbSpotNo);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtCenterX);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "FormZeroSet";
            this.Text = "Zero point";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtCenterX;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox cbSpotNo;
        private System.Windows.Forms.Button btn_Reset;
        private System.Windows.Forms.Button btn_ZeroSet;
        private System.Windows.Forms.TextBox txtCenterY;
    }
}