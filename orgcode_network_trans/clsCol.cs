using OpenCvSharp;

namespace WikiOptics_Collimator
{
    internal class clsColor
    {
        public static CvColor SelectColor(string ColorNumber)
        {
            CvColor white = CvColor.White;

            switch (ColorNumber.ToUpper())
            {
                case "WHITE":
                    return CvColor.White;
                case "RED":
                    return CvColor.Red;
                case "BLUE":
                    return CvColor.Blue;
                case "GREEN":
                    return CvColor.Green;
                case "YELLOW":
                    return CvColor.Yellow;
                default:
                    return CvColor.White;
            }
        }
    }
}
