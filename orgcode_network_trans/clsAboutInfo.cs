using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    internal class clsAboutInfo
    {
        public static string m_szAboutSection_name = "About";

        public static string m_szAboutTitleKey = "ABOUT_TITLE";
        public static string m_szAboutTitle = "WAC-05 Laser Autocollimator Application Software";

        public static string m_szAboutSerial = "..";
        //public static string m_szAboutSerialKey = "Serial_Number";

        public static string m_szAboutConnectKey = "ABOUT_CONNECT";
        public static string m_szAboutConnect = "contact@wikitoptics.com";

        public static string m_szAboutTelKey = "ABOUT_TEL";
        public static string m_szAboutTel = "82-70-4667-4270~1";

        public static string m_szAboutFactoryKey = "ABOUT_FACTORY";
        public static int m_nAboutFactory = 1;

        public static void LoadAboutInfo()
        {
            try
            {
                // 현재 프로그램 실행 위치
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
                bool bExist = System.IO.File.Exists(filepath);

                StringBuilder retVal1 = new StringBuilder(256);
                int result1 = 0;
                int nSize = 256;
                string defValue = "";

                if (false == bExist)
                {
                    WriteDefault(filepath);
                }
                else
                {
                    bExist = false;
                    string[] sec_names = CUtil.GetSectionNames(filepath);
                    int num_sections = sec_names.Length;
                    for (int i = 0; i < num_sections; i++)
                    {
                        bExist = sec_names[i].Equals(m_szAboutSection_name);
                        if (bExist == true)
                            break;
                    }
                    if (false == bExist)
                    {
                        WriteDefault(filepath);
                    }

                    defValue = "WAC-05 Laser Autocollimator Application Software";
                    NativeMethods.GetPrivateProfileString(m_szAboutSection_name, m_szAboutTitleKey, defValue, retVal1, nSize, filepath);
                    m_szAboutTitle = retVal1.ToString();

                    defValue = "connect@wikioptics.com";
                    NativeMethods.GetPrivateProfileString(m_szAboutSection_name, m_szAboutConnectKey, defValue, retVal1, nSize, filepath);
                    m_szAboutConnect = retVal1.ToString();

                    defValue = "82-70-4667-4270~1";
                    NativeMethods.GetPrivateProfileString(m_szAboutSection_name, m_szAboutTelKey, defValue, retVal1, nSize, filepath);
                    m_szAboutTel = retVal1.ToString();

                    defValue = "1";
                    m_nAboutFactory = 1;
                    NativeMethods.GetPrivateProfileString(m_szAboutSection_name, m_szAboutFactoryKey, defValue, retVal1, nSize, filepath);
                    if (true == int.TryParse(retVal1.ToString(), out result1))
                    {
                        m_nAboutFactory = result1;
                    }
                }
                m_szAboutSerial = CPGCamera.CameraSerial.ToString();
            }
            catch
            {
                Form form = new Form();
                form.TopMost = true;
                MessageBox.Show(form, "Can't find Aboutinfo information file.");
            }
        }

        private static void WriteDefault(string filepath)
        {            
            NativeMethods.WritePrivateProfileString(m_szAboutSection_name, m_szAboutTitleKey, m_szAboutTitle, filepath);
            NativeMethods.WritePrivateProfileString(m_szAboutSection_name, m_szAboutConnectKey, m_szAboutConnect, filepath);
            NativeMethods.WritePrivateProfileString(m_szAboutSection_name, m_szAboutTelKey, m_szAboutTel, filepath);
            NativeMethods.WritePrivateProfileString(m_szAboutSection_name, m_szAboutFactoryKey, "1", filepath);
        }
    }
}
