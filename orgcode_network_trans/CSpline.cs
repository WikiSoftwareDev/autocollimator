﻿using System;


namespace WikiOptics_Collimator
{
    internal class CSpline
    {
        public class CubicSpline
        {
            private float[] a;
            private float[] b;
            private float[] xOrig;
            private float[] yOrig;
            private int _lastIndex;

            public CubicSpline()
            {
            }

            public CubicSpline(float[] x, float[] y, float startSlope = float.NaN, float endSlope = float.NaN, bool debug = false)
            {
                this.Fit(x, y, startSlope, endSlope, debug);
            }

            private void CheckAlreadyFitted()
            {
                if (this.a == null)
                {
                    throw new Exception("Fit must be called before you can evaluate.");
                }
            }

            private int GetNextXIndex(float x)
            {
                if (x < this.xOrig[this._lastIndex])
                {
                    throw new ArgumentException("The X values to evaluate must be sorted.");
                }
                while (this._lastIndex < this.xOrig.Length - 2 && x > this.xOrig[this._lastIndex + 1])
                {
                    this._lastIndex++;
                }
                return this._lastIndex;
            }

            private float EvalSpline(float x, int j, bool debug = false)
            {
                float num = this.xOrig[j + 1] - this.xOrig[j];
                float num2 = (x - this.xOrig[j]) / num;
                float result = (1f - num2) * this.yOrig[j] + num2 * this.yOrig[j + 1] + num2 * (1f - num2) * (this.a[j] * (1f - num2) + this.b[j] * num2);
                if (debug)
                {
                    //Console.WriteLine("xs = {0}, j = {1}, t = {2}", x, j, num2);
                }
                return result;
            }
            
            public float[] FitAndEval(float[] x, float[] y, float[] xs, float startSlope = float.NaN, float endSlope = float.NaN, bool debug = false)
            {
                this.Fit(x, y, startSlope, endSlope, debug);
                return this.Eval(xs, debug);
            }

            public void Fit(float[] x, float[] y, float startSlope = float.NaN, float endSlope = float.NaN, bool debug = false)
            {
                if (float.IsInfinity(startSlope) || float.IsInfinity(endSlope))
                {
                    throw new Exception("startSlope and endSlope cannot be infinity.");
                }
                this.xOrig = x;
                this.yOrig = y;
                int num = x.Length;
                float[] array = new float[num];
                CTriDiagonalMatrixCal clsTriDiagonalMatrixF = new CTriDiagonalMatrixCal(num);
                if (float.IsNaN(startSlope))
                {
                    float num2 = x[1] - x[0];
                    clsTriDiagonalMatrixF.C[0] = 1f / num2;
                    clsTriDiagonalMatrixF.B[0] = 2f * clsTriDiagonalMatrixF.C[0];
                    array[0] = 3f * (y[1] - y[0]) / (num2 * num2);
                }
                else
                {
                    clsTriDiagonalMatrixF.B[0] = 1f;
                    array[0] = startSlope;
                }

                for (int i = 1; i < num - 1; i++)
                {
                    float num2 = x[i] - x[i - 1];
                    float num3 = x[i + 1] - x[i];
                    clsTriDiagonalMatrixF.A[i] = 1f / num2;
                    clsTriDiagonalMatrixF.C[i] = 1f / num3;
                    clsTriDiagonalMatrixF.B[i] = 2f * (clsTriDiagonalMatrixF.A[i] + clsTriDiagonalMatrixF.C[i]);
                    float num4 = y[i] - y[i - 1];
                    float num5 = y[i + 1] - y[i];
                    array[i] = 3f * (num4 / (num2 * num2) + num5 / (num3 * num3));
                }

                if (float.IsNaN(endSlope))
                {
                    float num2 = x[num - 1] - x[num - 2];
                    float num4 = y[num - 1] - y[num - 2];
                    clsTriDiagonalMatrixF.A[num - 1] = 1f / num2;
                    clsTriDiagonalMatrixF.B[num - 1] = 2f * clsTriDiagonalMatrixF.A[num - 1];
                    array[num - 1] = 3f * (num4 / (num2 * num2));
                }
                else
                {
                    clsTriDiagonalMatrixF.B[num - 1] = 1f;
                    array[num - 1] = endSlope;
                }

                if (debug)
                {
                    //Console.WriteLine("Tri-diagonal matrix:\n{0}", clsTriDiagonalMatrixF.ToDisplayString(":0.0000", "  "));
                }
                if (debug)
                {
                    //Console.WriteLine("r: {0}", clsArrayUtil.ToString<float>(array, ""));
                }

                float[] array2 = clsTriDiagonalMatrixF.Solve(array);
                if (debug)
                {
                    //Console.WriteLine("k = {0}", clsArrayUtil.ToString<float>(array2, ""));
                }
                this.a = new float[num - 1];
                this.b = new float[num - 1];
                for (int j = 1; j < num; j++)
                {
                    float num2 = x[j] - x[j - 1];
                    float num4 = y[j] - y[j - 1];
                    this.a[j - 1] = array2[j - 1] * num2 - num4;
                    this.b[j - 1] = -array2[j] * num2 + num4;
                }
                if (debug)
                {
                    //Console.WriteLine("a: {0}", clsArrayUtil.ToString<float>(this.a, ""));
                }
                if (debug)
                {
                    //Console.WriteLine("b: {0}", clsArrayUtil.ToString<float>(this.b, ""));
                }
            }

            public float[] Eval(float[] x, bool debug = false)
            {
                this.CheckAlreadyFitted();
                int num = x.Length;
                float[] array = new float[num];
                this._lastIndex = 0;
                for (int i = 0; i < num; i++)
                {
                    int nextXIndex = this.GetNextXIndex(x[i]);
                    array[i] = this.EvalSpline(x[i], nextXIndex, debug);
                }
                return array;
            }

            public float[] EvalSlope(float[] x, bool debug = false)
            {
                this.CheckAlreadyFitted();
                int num = x.Length;
                float[] array = new float[num];
                this._lastIndex = 0;
                for (int i = 0; i < num; i++)
                {
                    int nextXIndex = this.GetNextXIndex(x[i]);
                    float num2 = this.xOrig[nextXIndex + 1] - this.xOrig[nextXIndex];
                    float num3 = this.yOrig[nextXIndex + 1] - this.yOrig[nextXIndex];
                    float num4 = (x[i] - this.xOrig[nextXIndex]) / num2;
                    array[i] = num3 / num2 + (1f - 2f * num4) * (this.a[nextXIndex] * (1f - num4) + this.b[nextXIndex] * num4) / num2 + num4 * (1f - num4) * (this.b[nextXIndex] - this.a[nextXIndex]) / num2;
                    if (debug)
                    {
                        //Console.WriteLine("[{0}]: xs = {1}, j = {2}, t = {3}", new object[]
                        //{
                        //    i,
                        //    x[i],
                        //    nextXIndex,
                        //    num4
                        //});
                    }
                }
                return array;
            }

            public static float[] Compute(float[] x, float[] y, float[] xs, float startSlope = float.NaN, float endSlope = float.NaN, bool debug = false)
            {
                CSpline.CubicSpline cubicSpline = new CSpline.CubicSpline();
                return cubicSpline.FitAndEval(x, y, xs, startSlope, endSlope, debug);
            }

            public static void FitParametric(
                float[] x, 
                float[] y, 
                int nOutputPoints, 
                out float[] xs, 
                out float[] ys, 
                float firstDx = float.NaN, 
                float firstDy = float.NaN, 
                float lastDx = float.NaN, 
                float lastDy = float.NaN)
            {
                int num = x.Length;
                float[] array = new float[num];
                array[0] = 0f;
                float num2 = 0f;
                for (int i = 1; i < num; i++)
                {
                    float num3 = x[i] - x[i - 1];
                    float num4 = y[i] - y[i - 1];
                    float num5 = (float)Math.Sqrt((double)(num3 * num3 + num4 * num4));
                    num2 += num5;
                    array[i] = num2;
                }
                float num6 = num2 / (float)(nOutputPoints - 1);
                float[] array2 = new float[nOutputPoints];
                array2[0] = 0f;
                for (int j = 1; j < nOutputPoints; j++)
                {
                    array2[j] = array2[j - 1] + num6;
                }
                CSpline.CubicSpline.NormalizeVector(ref firstDx, ref firstDy);
                CSpline.CubicSpline.NormalizeVector(ref lastDx, ref lastDy);
                CSpline.CubicSpline cubicSpline = new CSpline.CubicSpline();
                xs = cubicSpline.FitAndEval(array, x, array2, firstDx / num6, lastDx / num6, false);
                CSpline.CubicSpline cubicSpline2 = new CSpline.CubicSpline();
                ys = cubicSpline2.FitAndEval(array, y, array2, firstDy / num6, lastDy / num6, false);
            }

            private static void NormalizeVector(ref float dx, ref float dy)
            {
                if (float.IsNaN(dx) || float.IsNaN(dy))
                {
                    dx = (dy = float.NaN);
                    return;
                }
                float num = (float)Math.Sqrt((double)(dx * dx + dy * dy));
                if (num > 1E-45f)
                {
                    dx /= num;
                    dy /= num;
                    return;
                }
                throw new ArgumentException("The input vector is too small to be normalized.");
            }
            
        }
    }
}
