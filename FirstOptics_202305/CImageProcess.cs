﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using OpenCvSharp;
using OpenCvSharp.Blob;
using OpenCvSharp.Extensions;


namespace WikiOptics_Collimator
{
    internal class CImageProcess
    {
        #region variables
        public static bool AnalysisToleranceFlag = false;
        public static bool AnalysisDisplayFlag = false;
        public static int nAnalysisAverage = 0;
        public static int nAnalysisCurrent = 0;
        public static double[] AnalysisAreaSize = new double[3];
        public static double[] sumAnalysisSize = new double[3];
        public static float[] sumAnalysisX = new float[3];
        public static float[] sumAnalysisY = new float[3];
        public static double[] AngleHourX = new double[3];
        public static double[] AngleMinuteX = new double[3];
        public static double[] AngleSecondX = new double[3];
        public static double[] AngleHourY = new double[3];
        public static double[] AngleMinuteY = new double[3];
        public static double[] AngleSecondY = new double[3];
        public static double[] AngleHourXY = new double[3];
        public static double[] AngleMinuteXY = new double[3];
        public static double[] AngleSecondXY = new double[3];
        public static double[] AngleX = new double[3];
        public static double[] AngleY = new double[3];
        public static double[] AngleD = new double[3];
        private static Bitmap[] originalBitmap = new Bitmap[3];
        private static Bitmap[] previewBitmap = new Bitmap[3];
        private static Bitmap[] resultBitmap = new Bitmap[3];
        public static IplImage OriginalImage;
        public static IplImage DisplayImage;
        public static IplImage GrayImage;
        public static IplImage[] RoiResizeImage = new IplImage[3];
        public static IplImage[] RoiResizeGrayImage = new IplImage[3];
        public static IplImage[] RoiImage = new IplImage[3];
        public static IplImage[] RoiKernelImage = new IplImage[3];
        public static bool CalibrationFlag = false;
        public static double[] CenterPointX = new double[3];
        public static double[] CenterPointY = new double[3];
        public static int SearchSpotNumber = 0;
        public static bool HistoSearchSpotFlag = false;

        public static int[] HistoSearchPointX = new int[3];
        public static int[] HistoSearchPointY = new int[3];

        public static int[] HistoSearchStartX = new int[3];
        public static int[] HistoSearchEndX = new int[3];

        public static int[] HistoSearchStartY = new int[3];
        public static int[] HistoSearchEndY = new int[3];

        public static int[] HistoCenterPointX = new int[3];
        public static int[] HistoCenterPointY = new int[3];

        public static int[] HistoCircleXLength = new int[3];
        public static int[] HistoCircleYLength = new int[3];

        public static int[] HistoCircleStartX = new int[3];
        public static int[] HistoCircleEndX = new int[3];

        public static int[] HistoCircleStartY = new int[3];
        public static int[] HistoCircleEndY = new int[3];

        public static float[] AnalysisResultX = new float[3];
        public static float[] AnalysisResultY = new float[3];

        public static double[] SpotDistance = new double[3];

        public static float[] AnalysisPeakX = new float[3];
        public static float[] AnalysisPeakY = new float[3];

        public static float[] AnalysisAreaX = new float[3];
        public static float[] AnalysisAreaY = new float[3];

        public static float[] AnalysisBrightnessX = new float[3];
        public static float[] AnalysisBrightnessY = new float[3];



        public static IplImage WobblePlottingImage = null;

        private static bool[] SpotInZoomFlag = new bool[3];

        public static float[] ZoomX = new float[3];
        public static float[] ZoomY = new float[3];

        public static double squareSizeWidth { get; set; }
        public static double squareSizeHeight { get; set; }


        #endregion

        public CImageProcess()
        {
        }

        public static void CheckCoordinate()
        {

        }

        public static void ResetSumAnalysisData()
        {
            nAnalysisCurrent = 0;
            for (int i = 0; i < 3; i++)
            {
                sumAnalysisSize[i] = 0.0;
                sumAnalysisX[i] = 0f;
                sumAnalysisY[i] = 0f;
            }
        }

        public static void CheckCalibration()
        {
         
            DisplayImage = OriginalImage.Clone();
          
           
            if (CalibrationFlag == true )
            {
                    nAnalysisCurrent = 0;
                   
                {
                    DisplayToleranceView();
                   
                }
                AnalysisDisplayFlag = true;
            }
        }



     

     
        private static void DisplayToleranceView()
        {
            CvFont font;
            Cv.InitFont(out font, FontFace.HersheySimplex, 1.0, 1.0, 0.0, 2);
            Int32 thickness = 2;

            Cv.DrawRect(
            DisplayImage,
            new CvPoint((int)(CParameter.CenterXPos - CSetInfo.ToleranceDistanceX), (int)(CParameter.CenterYPos - CSetInfo.ToleranceDistanceY)),
            new CvPoint((int)(CParameter.CenterXPos + CSetInfo.ToleranceDistanceX), (int)(CParameter.CenterYPos + CSetInfo.ToleranceDistanceY)),
            new CvScalar(0.0, 0.0, 255.0),
            thickness
        );

            // x, y 축 그리기
            int maxLength = 500;
            Cv.Line(CImageProcess.DisplayImage, new CvPoint((int)CParameter.CenterXPos - maxLength, (int)CParameter.CenterYPos), new CvPoint((int)CParameter.CenterXPos + maxLength, (int)CParameter.CenterYPos), new CvScalar(0, 0, 255), 2);
            Cv.Line(CImageProcess.DisplayImage, new CvPoint((int)CParameter.CenterXPos, (int)CParameter.CenterYPos - maxLength), new CvPoint((int)CParameter.CenterXPos, (int)CParameter.CenterYPos + maxLength), new CvScalar(0, 0, 255), 2);


            float alpha = 0.5f;
            if (CImageProcess.DisplayImage != null && CPGCamera.m_processedImage.bitmap != null)
            {
                IplImage blendedImageResult = AlphaBlend(CImageProcess.DisplayImage, CPGCamera.m_processedImage.bitmap.ToIplImage(), alpha);


                CImageProcess.DisplayImage = blendedImageResult;
            }
            else
            {

            }

            font.Dispose();
        }




        private static IplImage AlphaBlend(IplImage image1, IplImage image2, float alpha)
        {
            if (image1.Size != image2.Size)
            {
                throw new ArgumentException("Images must have the same size.");
            }

            IplImage blendedImage = new IplImage(image1.Size, BitDepth.U8, 3);

            Cv.AddWeighted(image1, 1 - alpha, image2, alpha, 0, blendedImage);

            return blendedImage;
        }

    }
}
