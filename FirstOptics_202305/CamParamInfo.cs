﻿using System.Text;
using System.Windows.Forms;
//ParamInfo Save 파일
namespace WikiOptics_Collimator
{
    internal class CamParamInfo
    {
        #region

        private static string   strAbsoluteMode = "ON";

        public static bool      AbsoluteMode = true;
        public static uint      Brightness = 0;
        public static float     Exposure = -2.585f;
        public static uint      Sharpness = 1270;
        public static uint      Hue = 0;
        public static uint      Saturation = 0;
        public static float     Gamma = 1.7f;
        public static float     ShutterSpeed = 20f;
        public static float     Gain = 15;
        public static uint      FrameRate = 40;
        public static uint      WhiteBalanceRed = 0;
        public static uint      WhiteBalanceBlue = 0;
        public static string    Power = "ON";
        public static string    PixelFormat = "Mono8";
        public static uint      ImageLeft = 128;
        public static uint      ImageTop = 0;
        public static uint      ImageWidth = 1024;
        public static uint      ImageHeight = 1024;
        public static uint      PacketSize = 6560;
      

        const string m_szSectionName           = "CAMERAINFO_SECTION";
        const string m_szKey_AbsoluteMode      = "AbsoluteMode(0(OFF),1(ON))";
        const string m_szKey_Brightness        = "Brightness(0)";
        const string m_szKey_Exposure          = "Exposure(-2.585)";
        const string m_szKey_Sharpness         = "Sharpness(1270)";
        const string m_szKey_Gamma             = "Gamma(1.7)";
        const string m_szKey_ShutterSpeed      = "ShutterSpeed(20)";
        const string m_szKey_Gain              = "Gain(15)";
        const string m_szKey_FPS               = "FrameRate(40)";
        const string m_szKey_Image_Left_Offset = "Image_Left_Offset(128)";
        const string m_szKey_Image_Top_Offset  = "Image_Top_Offset(0)";
        const string m_szKey_Image_Width       = "ImageWidth(1024)";
        const string m_szKey_Image_Height      = "ImageHeight(1024)";

        #endregion

        public CamParamInfo()
        {
        }

        static void SaveDefaultCameraInfo(string filepath)
        {
            bool writeResult = false;
            string defValue;

            defValue = "20";
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ShutterSpeed, defValue, filepath);

        }

        public static void LoadCameraInfo()
        {
            try
            {
                // 현재 프로그램 실행 위치
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";
                bool bExist = System.IO.File.Exists(filepath);
                if (false == bExist)
                {
                    SaveDefaultCameraInfo(filepath);
                }
                else
                {
                    bExist = false;
                    string[] sec_names = CUtil.GetSectionNames(filepath);
                    int num_sections = sec_names.Length;
                    for (int i = 0; i < num_sections; i++)
                    {
                        bExist = sec_names[i].Equals(m_szSectionName);
                        if (bExist == true)
                            break;
                    }
                    if (false == bExist)
                    {
                        SaveDefaultCameraInfo(filepath);
                    }
                }

                string defValue;
                StringBuilder retVal1 = new StringBuilder();
                int nSize = 128;
                int result1 = 0;
               
                float temp2 = 0;
               


                defValue = "20";
                result1 = NativeMethods.GetPrivateProfileString(m_szSectionName, m_szKey_ShutterSpeed, defValue, retVal1, nSize, filepath);
                if (true == float.TryParse(retVal1.ToString(), out temp2))
                {
                    ShutterSpeed = temp2;
                }

             
           
            }
            catch
            {
                MessageBox.Show(new Form
                {
                    TopMost = true
                }, "Camera Info. FILE LOAD FAIL");
            }
        }

        public static void SaveCameraInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";

            bool writeResult = false;
            string value;

           

            value = string.Format("{0:#.#####}", ShutterSpeed);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSectionName, m_szKey_ShutterSpeed, value, filepath);

       
        }
    }
}
