﻿using System;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Text;
using log4net;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net_collimator.cfg", Watch = true)]
//Program.log.Info(dbg_str);

namespace WikiOptics_Collimator
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        ///

        public static ILog log = LogManager.GetLogger("Program");

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            Application.Run(new MainForm());
        }
    }

    internal static class NativeMethods
    {
        // 출처: https://crazykim2.tistory.com/359 [차근차근 개발일기+일상:티스토리]
        #region ini 입력 메소드
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool WritePrivateProfileString(string section, string key, string val, string filePath);
        [DllImport("kernel32.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retVal, int size, string filePath);
        [DllImport("setupapi.dll", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool SetupCopyOEMInf(
                string SourceInfFileName,
                string OEMSourceMediaLocation,
                OemSourceMediaType OEMSourceMediaType,
                OemCopyStyle CopyStyle,
                string DestinationInfFileName,
                int DestinationInfFileNameSize,
                ref int RequiredSize,
                string DestinationInfFileNameComponent
            );
        #endregion
        [DllImport("setupapi.dll", SetLastError = true, CharSet = CharSet.Unicode)]
        public static extern bool InstallHinfSection(
        IntPtr hwnd,
        IntPtr moduleHandle,
        string sectionName,
        int flags,
        IntPtr relativeKeyRoot,
        IntPtr infHandle,
        int copyFlags,
        INSTALLERINFO installerInfo);

        [StructLayout(LayoutKind.Sequential)]
        public class INSTALLERINFO
        {
            public int cbSize;
            public IntPtr hwndParent;
            public IntPtr pExeFile;
            public int dwFlags;
            public IntPtr lpInstallMsgHandler;
            public IntPtr context;
        }

        public const int INSTALLFLAG_FORCE_IFHUNG = 0x00002000;
    }
    public enum OemSourceMediaType
    {
        SPOST_NONE = 0,
        //Only use the following if you have a pnf file as well
        SPOST_PATH = 1,
        SPOST_URL = 2,
        SPOST_MAX = 3
    }
    public enum OemCopyStyle
    {
        SP_COPY_NEWER = 0x0000004,   // copy only if source newer than or same as target
        SP_COPY_NEWER_ONLY = 0x0010000,   // copy only if source file newer than target
        SP_COPY_OEMINF_CATALOG_ONLY = 0x0040000,   // (SetupCopyOEMInf only) don't copy INF--just catalog
        SP_COPY_SOURCE_ABSOLUTE = 0x0000040
    }
   
   
}
