﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Windows.Forms;
using OpenCvSharp;


namespace WikiOptics_Collimator
{
    public partial class FormZeroSet : Form
    {
        public FormZeroSet()
        {
            InitializeComponent();
        }

        private void FrmZeroSet_Load(object sender, EventArgs e)
        {
            txtCenterX.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterX);
            txtCenterY.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterY);
        }

        private void btn_Reset_Click(object sender, EventArgs e)
        {
            CZeroSetInfo.ZeroSetCenterX = CParameter.CenterXPos;
            CZeroSetInfo.ZeroSetOriginalCenterX = CParameter.CenterXPos;
            CZeroSetInfo.ZeroSetCenterY = CParameter.CenterYPos;
            CZeroSetInfo.ZeroSetOriginalCenterY = CParameter.CenterYPos;
            txtCenterX.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterX);
            txtCenterY.Text = string.Format("{0:f4}", CZeroSetInfo.ZeroSetCenterY);
        }

        private void btn_ZeroSet_Click(object sender, EventArgs e)
        {

        }
    }
}
