﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FlyCapture2Managed;
using FlyCapture2Managed.Gui;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Threading;

namespace WikiOptics_Collimator
{
    public partial class MainForm : Form
    {


        public object m_LockObj = new object();

        static AutoResetEvent m_grabThreadExited;
        static BackgroundWorker m_grabThread;

        Graphics pic_camera_graphic;

        int PicOffsetX;
        int PicOffsetY;
        //string strFPS;
        //int nCount = 20;

        bool UpdateUIFlag = false;
       
        private delegate void TimerEventFiredDelegate();
        private delegate void SaveTimerEventFiredDelegate();

        PictureBox[] PicBrightness;

        public MainForm()
        {
            InitializeComponent();

            CamParamInfo.LoadCameraInfo();

            CImageProcess.OriginalImage = new IplImage((int)CamParamInfo.ImageWidth, (int)CamParamInfo.ImageHeight, BitDepth.U8, 3);
            CImageProcess.GrayImage = new IplImage((int)CamParamInfo.ImageWidth, (int)CamParamInfo.ImageHeight, BitDepth.U8, 1);


            this.PicBrightness = new PictureBox[]
            {
                this.pic_Status1,
                this.pic_Status2,
                this.pic_Status3,
                this.pic_Status4,
                this.pic_Status5,
                this.pic_Status6,
                this.pic_Status7,
                this.pic_Status8,
                this.pic_Status9,
                this.pic_Status10,
                this.pic_Status11,
                this.pic_Status12,
                this.pic_Status13,
                this.pic_Status14,
                this.pic_Status15
            };
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            pic_camera_graphic = pic_Camera.CreateGraphics();

            // 이미지 위치 조정
            if (pic_Camera.Width > 512)
            {
                PicOffsetX = (pic_Camera.Width - 512) / 2;
            }
            else
            {
                PicOffsetX = 0;
            }

            if (pic_Camera.Height > 512)
            {
                PicOffsetY = (pic_Camera.Height - 512) / 2;
            }
            else
            {
                PicOffsetY = 0;
            }

            frmDevice frmDevice2 = new frmDevice(this);
            DialogResult dialogResult = frmDevice2.ShowDialog();

            // 다이얼로그 결과 확인
            if (dialogResult != DialogResult.OK)
            {
                Application.ExitThread();
                Environment.Exit(0);
            }

            ConnectCamera();

            // 설정 정보 로드
            CSetInfo.LoadSettingInfo();
            
            clsAboutInfo.LoadAboutInfo();
            Track_ShutterSpeed.Value = (int)(CamParamInfo.ShutterSpeed * 1000f);

            TextBox_ShutterSpeed.Text = string.Format("{0:f3}", CamParamInfo.ShutterSpeed);
            CPGCamera.SetShutterSpeed(CamParamInfo.ShutterSpeed);
            UpdateCheckBox();
           
        }


        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            CPGCamera.m_grabImages = false;
            if (m_grabThreadExited != null)
            {
                // WaitOne(int millisecondsTimeout);
                m_grabThreadExited.WaitOne(2 * 1000);      // 2 seconds
                m_grabThreadExited.Dispose();
                m_grabThreadExited = null;
            }

            if (CPGCamera.camera == null)
                return;

            CPGCamera.StopCameraCapture();
            try
            {
                CPGCamera.camera.Disconnect();
            }
            catch
            {
            }
        }

        private void InitCamera()
        {
            CPGCamera.initCamera();
            CPGCamera.StartCameraCapture();
        }

        private void ConnectCamera()
        {
            if (!CPGCamera.initCamera())
            {
                MessageBox.Show("There is no camera, check camera connection state.");
                return;
            }

            if (!CPGCamera.SetCustomVideoMode())
            {
                MessageBox.Show("SetCustomVideoMode error");
                return;
            }

            if (!CPGCamera.SetCameraSetting())
            {
                MessageBox.Show("SetCameraSetting error");
                return;
            }

            CPGCamera.m_rawImage = new ManagedImage();
            CPGCamera.m_processedImage = new ManagedImage();
            CPGCamera.m_AnalysisImage = new ManagedImage();
            CPGCamera.m_camCtlDlg = new CameraControlDialog();
            m_grabThreadExited = new AutoResetEvent(false);
            UpdateUIFlag = true;
            RunCamera();
        }
        private object _lockObject = new object();
        public void RunCamera()
        {


            try
            {
                EmbeddedImageInfo embeddedImageInfo = CPGCamera.camera.GetEmbeddedImageInfo();
                embeddedImageInfo.timestamp.onOff = true;
                CPGCamera.camera.SetEmbeddedImageInfo(embeddedImageInfo);
                CPGCamera.StartCameraCapture();
                CPGCamera.m_grabImages = true;

                m_grabThread = new BackgroundWorker();
                m_grabThread.DoWork += GrabLoop;
                m_grabThread.WorkerReportsProgress = true;
                m_grabThread.ProgressChanged += UpdateUI;
                m_grabThread.RunWorkerAsync();
            }
            catch (FC2Exception)
            {
                Environment.ExitCode = -1;
                Application.Exit();
            }

        }




        private void GrabLoop(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker backgroundWorker = sender as BackgroundWorker;
            int nFailure_count = 0;
            Bitmap DisplayImage = new Bitmap(1024, 1024);
            float alpha = 0.5f;
            float alphaFloat = (float)alpha;

            while (CPGCamera.m_grabImages)
            {
                if (!UpdateUIFlag)
                {
                    continue;
                }
                Application.DoEvents();

                if (nFailure_count > 10)
                {
                    DialogResult result = MessageBox.Show("USB Camera is not compatible with this system. This device require USB 3.0 port.", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    if (result == DialogResult.OK)
                    {
                        Application.Exit();
                    }
                    return;
                }

                try
                {
                    CPGCamera.camera.RetrieveBuffer(CPGCamera.m_rawImage);
                    lock (_lockObject)
                    {
                        CPGCamera.m_rawImage.Convert(PixelFormat.PixelFormatBgr, CPGCamera.m_processedImage);
                        CImageProcess.OriginalImage.CopyFrom((Bitmap)(object)CPGCamera.m_processedImage.bitmap);
                        CImageProcess.CheckCalibration();
                    }
                    nFailure_count = 0;
                }
                catch (FC2Exception)
                {
                    InitCamera();
                    nFailure_count++;
                    continue;
                }

                clsETC.Time_Delay(70L);
                nFailure_count = 0;
                backgroundWorker.ReportProgress(0);
            }
            m_grabThreadExited.Set();
        }


        private void UpdateUI(object sender, ProgressChangedEventArgs e)
        {
            this.UpdateUIFlag = false;
           

           

            if (CImageProcess.AnalysisDisplayFlag)
            {
               // UpdateBrightnessStatus();
                DrawImage();

                CImageProcess.AnalysisDisplayFlag = false;
            }

            if (!CImageProcess.CalibrationFlag)
            {
                this.pic_Camera.Image = CImageProcess.DisplayImage.ToBitmap();
                this.pic_Camera.Invalidate();
            }


            this.UpdateUIFlag = true;
        }

        private void DrawImage()
        {
            if (pic_Camera.Width != pic_Camera.Height)
            {
                if (pic_Camera.Width < pic_Camera.Height)
                {
                    pic_camera_graphic.DrawImage((Image)(object)CImageProcess.DisplayImage.ToBitmap(), PicOffsetX, PicOffsetY, pic_Camera.Width, pic_Camera.Width);
                }
                else
                {
                    pic_camera_graphic.DrawImage((Image)(object)CImageProcess.DisplayImage.ToBitmap(), PicOffsetX, PicOffsetY, pic_Camera.Height, pic_Camera.Height);
                }
            }
            else
            {
                pic_camera_graphic.DrawImage((Image)(object)CImageProcess.DisplayImage.ToBitmap(), 0, 0, pic_Camera.Height, pic_Camera.Height);
            }

        }

        private void UpdateCheckBox()
        {

            txtToleranceDoX.Text = CSetInfo.Setting_Tolerance_HourX.ToString();
            txtToleranceMinX.Text = CSetInfo.Setting_Tolerance_MinX.ToString();
            txtToleranceSecX.Text = CSetInfo.Setting_Tolerance_SecX.ToString();
            txtToleranceDoY.Text = CSetInfo.Setting_Tolerance_HourY.ToString();
            txtToleranceMinY.Text = CSetInfo.Setting_Tolerance_MinY.ToString();
            txtToleranceSecY.Text = CSetInfo.Setting_Tolerance_SecY.ToString();

        }

        private void UpdateBrightnessStatus()
        {


        }

        private void btn_SettingFileSave_Click(object sender, EventArgs e)
        {

            CSetInfo.SaveSettingInfo();
            CamParamInfo.SaveCameraInfo();

        }



        private void btn_Capture_Click(object sender, EventArgs e) // 화면 캡쳐
        {
            clsETC.ScreenCapture(base.Width, base.Height, base.Location);
        }

        private void Track_ShutterSpeed_Scroll(object sender, EventArgs e)//Camera Shot 속도 스크롤 
        {
            float num = Track_ShutterSpeed.Value / 1000f;
            TextBox_ShutterSpeed.Text = num.ToString("F3");
            CamParamInfo.ShutterSpeed = num;
            CPGCamera.SetShutterSpeed(num);
        }

        private void OnShutterSpeedTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int num = Track_ShutterSpeed.Value;
                string szShutter = string.Format("{0:f3}", (float)num / 1000f);

                string szSpeed = TextBox_ShutterSpeed.Text;
                double nSpeed;

                bool tf = double.TryParse(szSpeed, out nSpeed);

                if (tf == true)
                {
                    nSpeed *= 1000;
                    if (nSpeed > Track_ShutterSpeed.Maximum)
                    {
                        MessageBox.Show("Shutter Max Speed 30ms Value");
                        TextBox_ShutterSpeed.Text = szShutter;
                        return;
                    }
                    if (nSpeed < Track_ShutterSpeed.Minimum)
                        nSpeed = Track_ShutterSpeed.Minimum;

                    Track_ShutterSpeed.Value = (int)nSpeed;

                    float ShutterValue = (float)nSpeed / 1000f;
                    CamParamInfo.ShutterSpeed = ShutterValue;
                    CPGCamera.SetShutterSpeed(ShutterValue);
                }
                else
                {
                    MessageBox.Show("Invalid shutter speed value");
                    TextBox_ShutterSpeed.Text = szShutter;
                }
            }
        }

        /*      private void menu_Tools_Coordinate_Mirroring_Xon_Click(object sender, EventArgs e) // X축 대칭 좌표계로 변경
              {
                  if (CSetInfo.Tool_Coordinate_Mirroring_State != 1)
                  {
                      CImageProcess.ResetZeroPoint(); //0323
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 2;
                  CSetInfo.Tool_Coordinate_Mirroring_State = 1;
                  UpdateCheckBox();
              }

              private void menu_Tools_Coordinate_Mirroring_Yon_Click(object sender, EventArgs e) // Y축 대칭 좌표계로 변경
              {
                  if (CSetInfo.Tool_Coordinate_Mirroring_State != 2)
                  {
                      CImageProcess.ResetZeroPoint();
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 2;
                  CSetInfo.Tool_Coordinate_Mirroring_State = 2;
                  UpdateCheckBox();
              }

              private void menu_Tools_Coordinate_Mirroring_Off_Click(object sender, EventArgs e) //대칭 좌표계를 해제
              {
                  if (CSetInfo.Tool_Coordinate_Mirroring_State != 3)
                  {
                      CImageProcess.ResetZeroPoint();
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 0;
                  CSetInfo.Tool_Coordinate_Mirroring_State = 3;
                  UpdateCheckBox();
              }

              private void menu_Tools_Coordinate_Rotating_L90_Click(object sender, EventArgs e) //90도 왼쪽 방향으로 회전하는 좌표계를 설정
              {
                  if (CSetInfo.Tool_Coordinate_Rotating_State != 2)
                  {
                      CImageProcess.ResetZeroPoint();
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 1;
                  CSetInfo.Tool_Coordinate_Rotating_State = 2;
                  UpdateCheckBox();
              }

              private void menu_Tools_Coordinate_Rotating_R90_Click(object sender, EventArgs e) //90도 오른쪽 방향으로 회전하는 좌표계를 설정
              {
                  if (CSetInfo.Tool_Coordinate_Rotating_State != 1)
                  {
                      CImageProcess.ResetZeroPoint();
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 1;
                  CSetInfo.Tool_Coordinate_Rotating_State = 1;
                  UpdateCheckBox();
              }

              private void menu_Tools_Coordinate_Rotating_Off_Click(object sender, EventArgs e) //좌표 회전 기능 off
              {
                  if (CSetInfo.Tool_Coordinate_Rotating_State != 3)
                  {
                      CImageProcess.ResetZeroPoint();
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 0;
                  CSetInfo.Tool_Coordinate_Rotating_State = 3;
                  UpdateCheckBox(); ;
              }

              private void menu_Tools_Coordinate_Original_Click(object sender, EventArgs e) //현재 좌표계 설정이 원점 기준인지 확인한 다음, 원점 기준 좌표계로 설정
              {
                  if (CSetInfo.Tool_Coordinate != 0)
                  {
                      CSetInfo.Tool_Coordinate_ChangedFlag = true;
                  }
                  CSetInfo.Tool_Coordinate = 0;
                  UpdateCheckBox();
              }

              private void menu_Tools_Zoom_4_Click(object sender, EventArgs e) //4 배 확대
              {
                  if (CSetInfo.Tool_Zoom_State != 4)
                  {
                      CSetInfo.Tool_Zoom_Flag = 1;
                      CSetInfo.Tool_Zoom_State = 4;
                      this.UpdateCheckBox();
                  }
              }

              private void menu_Tools_Zoom_8_Click(object sender, EventArgs e) //8배 확대
              {
                  if (CSetInfo.Tool_Zoom_State != 8)
                  {
                      CSetInfo.Tool_Zoom_Flag = 1;
                      CSetInfo.Tool_Zoom_State = 8;
                      this.UpdateCheckBox();
                  }
              }

              private void menu_Tools_Zoom_16_Click(object sender, EventArgs e)//16배 확대
              {
                  if (CSetInfo.Tool_Zoom_State != 16)
                  {
                      CSetInfo.Tool_Zoom_Flag = 1;
                      CSetInfo.Tool_Zoom_State = 16;
                      this.UpdateCheckBox();
                  }
              }

              private void menu_Tools_Zoom_32_Click(object sender, EventArgs e)//32배 확대
              {
                  if (CSetInfo.Tool_Zoom_State != 32)
                  {
                      CSetInfo.Tool_Zoom_Flag = 1;
                      CSetInfo.Tool_Zoom_State = 32;
                      this.UpdateCheckBox();
                  }
              }

              private void menu_Tools_Zoom_Off_Click(object sender, EventArgs e) //Zoom 초기화
              {
                  if (CSetInfo.Tool_Zoom_State != 0)
                  {
                      CSetInfo.Tool_Zoom_Flag = 0;
                      CSetInfo.Tool_Zoom_State = 0;
                      this.UpdateCheckBox();
                  }
              }*/



        private void menu_Information_About_Click(object sender, EventArgs e) //INFO
        {
            foreach (Form openForm in Application.OpenForms)
            {
                if (openForm.Name == "frmInfo")
                {
                    if (openForm.WindowState == FormWindowState.Minimized)
                    {
                        openForm.WindowState = FormWindowState.Normal;
                    }
                    openForm.Activate();
                    openForm.Location = new Point(base.Location.X + base.Width / 2 - openForm.Width / 2, base.Location.Y + base.Height / 2 - openForm.Height / 2);
                    return;
                }
            }
            frmInfo frmInfo2 = new frmInfo();
            frmInfo2.StartPosition = FormStartPosition.Manual;
            frmInfo2.Location = new Point(base.Location.X + base.Width / 2 - frmInfo2.Width / 2, base.Location.Y + base.Height / 2 - frmInfo2.Height / 2);
            frmInfo2.Show();
        }

       

        //Start 버튼
        private void cmdAnalysis_Click(object sender, EventArgs e)
        {
            if (CImageProcess.CalibrationFlag)
            {
                CImageProcess.CalibrationFlag = false;
                cmdAnalysis.ImageIndex = 0;
                cmdAnalysis.Text = "START";
            }
            else
            {
                CImageProcess.CalibrationFlag = true;
                cmdAnalysis.ImageIndex = 1;
                cmdAnalysis.Text = "STOP";
            }
        }
        private void drawButton_Click(object sender, EventArgs e)
        {
            if (this.txtToleranceDoX.Text == "")
            {
                this.txtToleranceDoX.Text = "0";
            }
            if (!clsETC.ISNumeric(this.txtToleranceDoX.Text))
            {
                MessageBox.Show("Please, Check Angle Value!!");
                return;
            }
            if (this.txtToleranceMinX.Text == "")
            {
                this.txtToleranceMinX.Text = "0";
            }
            if (!clsETC.ISNumeric(this.txtToleranceMinX.Text))
            {
                MessageBox.Show("Please, Check Angle Value!!");
                return;
            }
            if (this.txtToleranceSecX.Text == "")
            {
                this.txtToleranceSecX.Text = "0";
            }
            if (!clsETC.ISNumeric(this.txtToleranceSecX.Text))
            {
                MessageBox.Show("Please, Check Angle Value!!");
                return;
            }

            if (this.txtToleranceDoY.Text == "")
            {
                this.txtToleranceDoY.Text = "0";
            }
            if (!clsETC.ISNumeric(this.txtToleranceDoY.Text))
            {
                MessageBox.Show("Please, Check Angle Value!!");
                return;
            }
            if (this.txtToleranceMinY.Text == "")
            {
                this.txtToleranceMinY.Text = "0";
            }
            if (!clsETC.ISNumeric(this.txtToleranceMinY.Text))
            {
                MessageBox.Show("Please, Check Angle Value!!");
                return;
            }
            if (this.txtToleranceSecY.Text == "")
            {
                this.txtToleranceSecY.Text = "0";
            }
            if (!clsETC.ISNumeric(this.txtToleranceSecY.Text))
            {
                MessageBox.Show("Please, Check Angle Value!!");
                return;
            }
            CSetInfo.Setting_Tolerance_HourX = (int)Convert.ToInt16(this.txtToleranceDoX.Text);
            CSetInfo.Setting_Tolerance_MinX = (int)Convert.ToInt16(this.txtToleranceMinX.Text);
            CSetInfo.Setting_Tolerance_SecX = (int)Convert.ToInt16(this.txtToleranceSecX.Text);
            CSetInfo.Setting_Tolerance_HourY = (int)Convert.ToInt16(this.txtToleranceDoY.Text);
            CSetInfo.Setting_Tolerance_MinY = (int)Convert.ToInt16(this.txtToleranceMinY.Text);
            CSetInfo.Setting_Tolerance_SecY = (int)Convert.ToInt16(this.txtToleranceSecY.Text);
            CSetInfo.CalToleranceDistance();
            CSetInfo.CalToleranceDistanceY();

        }

        private void ToleranceSize_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                drawButton_Click(sender, e);
            }
        }

    }
}
  


