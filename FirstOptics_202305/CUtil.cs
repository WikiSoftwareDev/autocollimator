﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.IO;

namespace WikiOptics_Collimator
{
    internal class CUtil
    {
        // ini 파일 안의 모든 section 이름 가져오기
        public static string[] GetSectionNames(string path)
        {
            List<string> sectionNames = new List<string>();
            string[] lines = File.ReadAllLines(path);

            foreach (string line in lines)
            {
                if (line.Trim().StartsWith("[") && line.Trim().EndsWith("]"))
                {
                    string sectionName = line.Trim().Substring(1, line.Length - 2);
                    sectionNames.Add(sectionName);
                }
            }

            return sectionNames.ToArray();
        }


        // 해당 section 안의 모든 키 값 가져오기
        public static string[] GetEntryNames(string path, string section)
        {
            //for (int maxsize = 500; true; maxsize *= 2)
            {
                int maxsize = 1024 * 10;
                StringBuilder section_names = new StringBuilder();
                //byte[] bytes = new byte[maxsize];
                int size = NativeMethods.GetPrivateProfileString(section, null, "", section_names, maxsize, path);
                if (size < maxsize - 2)
                {
                    byte[] bytes = Encoding.ASCII.GetBytes(section_names.ToString());
                    string entries = Encoding.ASCII.GetString(bytes, 0, size - (size > 0 ? 1 : 0));
                    return entries.Split(new char[] { '\0' });
                }
            }
            return null;
        }
    }
}
