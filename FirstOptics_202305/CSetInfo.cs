﻿using System.Text;
using System;

namespace WikiOptics_Collimator
{
    internal class CSetInfo
    {
        #region
        public const int Tool_Mode_AreaCenter               = 1;
        public const int Tool_Mode_BrightnessCenter         = 2;
        public const int Tool_Mode_PeakCenter               = 3;
        public const int Tool_Angle_Definition_Ext          = 1;
        public const int Tool_Angle_Definition_Int          = 2;
        public const int Tool_Coordinate_Original           = 0;
        public const int Tool_Coordinate_Rotating           = 1;
        public const int Tool_Coordinate_Mirroring          = 2;
        public const int Tool_Coordinate_Mirroring_X        = 1;
        public const int Tool_Coordinate_Mirroring_Y        = 2;
        public const int Tool_Coordinate_Mirroring_Off      = 3;

        public const int Tool_Coordinate_Rotating_R         = 1;
        public const int Tool_Coordinate_Rotating_L         = 2;
        public const int Tool_Coordinate_Rotating_Off       = 3;
        public const int Tool_Numbering_Angle               = 1;
        public const int Tool_Numbering_Size                = 2;
        public const int Tool_Zoom_Off                      = 0;
        public const int Tool_Zoom_4                        = 4;
        public const int Tool_Zoom_8                        = 8;
        public const int Tool_Zoom_16                       = 16;
        public const int Tool_Zoom_32                       = 32;
        public const int Setting_Unit_Degree                = 1;
        public const int Setting_Unit_MinSec                = 2;
        public const int Setting_Tolerance_Circle           = 1;
        public const int Setting_Tolerance_Square           = 2;

        public static int Tool_Angle_Definition_Value;
        public static bool Tool_Coordinate_ChangedFlag = false;
        public static int Tool_Coordinate_Mirroring_State = 0;
        public static int Tool_Coordinate_Rotating_State = 0;
        public static int Tool_Zoom_State = 0;
        public static double ToleranceDistanceX;
        public static double ToleranceDistanceY;
        // public static int Setting_Tolerance_Mode;
        public static int Setting_Tolerance_HourX;
        public static int Setting_Tolerance_MinX;
        public static int Setting_Tolerance_SecX;
        public static int Setting_Tolerance_HourY;
        public static int Setting_Tolerance_MinY;
        public static int Setting_Tolerance_SecY;
       // public static int Setting_Gray_MinLevel;
       // public static int Setting_Gray_MaxLevel;
        public static int SquareSize_Height;
        public static int SquareSize_Width;
        public static float ShutterSpeed = 20f;
        const string m_szSetInfoSectionName = "SET_INFO";
        const string m_szKey_Tool_Mode = "Tool_Mode";
        const string m_szKey_Setting_Tolerance_HourX = "Setting_Tolerance_HourX";
        const string m_szKey_Setting_Tolerance_MinX = "Setting_Tolerance_MinX";
        const string m_szKey_Setting_Tolerance_SecX = "Setting_Tolerance_SecX";
        const string m_szKey_Setting_Tolerance_HourY = "Setting_Tolerance_Hour_Y";
        const string m_szKey_Setting_Tolerance_MinY = "Setting_Tolerance_Min_Y";
        const string m_szKey_Setting_Tolerance_SecY = "Setting_Tolerance_Sec_Y";
        const string m_szKey_SquareSize_Height = "SquareSize_Height";
        const string m_szKey_SquareSize_Width = "SquareSize_Width";
        const string m_szKey_ShutterSpeed = "ShutterSpeed";
        #endregion

        public CSetInfo()
        {
       
            
        }

        static void SaveDefaultSettingInfo(string filepath)
        {
            string value;

            value = "100";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_SquareSize_Height,
                value,
                filepath);

            value = "100";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_SquareSize_Width,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_HourX,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_MinX,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_SecX,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_HourY,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_MinY,
                value,
                filepath);

            value = "1";
            NativeMethods.WritePrivateProfileString( 
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_SecY,
                value,
                filepath);

            value = "20";
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_ShutterSpeed,
                value,
                filepath);

        }

        public static void LoadSettingInfo()
        {
            // 현재 프로그램 실행 위치
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";            
            bool bExist = System.IO.File.Exists(filepath);
            if (false == bExist)
            {
                SaveDefaultSettingInfo(filepath);
            }
            else
            {
                bExist = false;
                string[] sec_names = CUtil.GetSectionNames(filepath);
                int num_sections = sec_names.Length;
                for (int i = 0; i < num_sections; i++)
                {
                    bExist = sec_names[i].Equals(m_szSetInfoSectionName);
                    if (bExist == true)
                        break;
                }
                if (false == bExist)
                {
                    SaveDefaultSettingInfo(filepath);
                }
            }
             LoadInfoFile(filepath);
        }

        public static void LoadInfoFile(string filepath)
        {
            int num = 0;
            string defValue;
            StringBuilder retVal1 = new StringBuilder();
            int nSize = 128;

         
                  defValue = "0";
                  NativeMethods.GetPrivateProfileString(
                      m_szSetInfoSectionName,
                      m_szKey_Setting_Tolerance_HourX,
                      defValue,
                      retVal1,
                      nSize,
                      filepath
                      );
                  if (true == int.TryParse(retVal1.ToString(), out num))
                  {
                      Setting_Tolerance_HourX = num;
                  }

                  defValue = "2";
                  NativeMethods.GetPrivateProfileString(
                      m_szSetInfoSectionName,
                      m_szKey_Setting_Tolerance_MinX,
                      defValue,
                      retVal1,
                      nSize,
                      filepath
                      );
                  if (true == int.TryParse(retVal1.ToString(), out num))
                  {
                      Setting_Tolerance_MinX = num;
                  }

                  defValue = "32";
                  NativeMethods.GetPrivateProfileString(
                      m_szSetInfoSectionName,
                      m_szKey_Setting_Tolerance_SecX,
                      defValue,
                      retVal1,
                      nSize,
                      filepath
                      );
                  if (true == int.TryParse(retVal1.ToString(), out num))
                  {
                      Setting_Tolerance_SecX = num;
                  }

                   defValue = "0";
                  NativeMethods.GetPrivateProfileString(
                      m_szSetInfoSectionName,
                      m_szKey_Setting_Tolerance_HourY,
                      defValue,
                      retVal1,
                      nSize,
                      filepath
                      );
                  if (true == int.TryParse(retVal1.ToString(), out num))
                  {
                      Setting_Tolerance_HourY = num;
                  }

                  defValue = "2";
                  NativeMethods.GetPrivateProfileString(
                      m_szSetInfoSectionName,
                      m_szKey_Setting_Tolerance_MinY,
                      defValue,
                      retVal1,
                      nSize,
                      filepath
                      );
                  if (true == int.TryParse(retVal1.ToString(), out num))
                  {
                      Setting_Tolerance_MinY = num;
                  }

                   defValue = "32";
                  NativeMethods.GetPrivateProfileString(
                      m_szSetInfoSectionName,
                      m_szKey_Setting_Tolerance_SecY,
                      defValue,
                      retVal1,
                      nSize,
                      filepath
                      );
                  if (true == int.TryParse(retVal1.ToString(), out num))
                  {
                      Setting_Tolerance_SecY = num;
                  }
              
                 defValue = "100";
                NativeMethods.GetPrivateProfileString(
                    m_szSetInfoSectionName,
                    m_szKey_SquareSize_Width,
                    defValue,
                    retVal1,
                    nSize,
                    filepath
                    );
                    if (true == int.TryParse(retVal1.ToString(), out num))
                 {
                     SquareSize_Width = num;
                 }

            defValue = "100";
            NativeMethods.GetPrivateProfileString(
                    m_szSetInfoSectionName,
                    m_szKey_SquareSize_Height,
                    defValue,
                    retVal1,
                    nSize,
                    filepath
                );
                    if (true == int.TryParse(retVal1.ToString(), out num))
                {
                    SquareSize_Height = num;
                }

            defValue = "20";
            NativeMethods.GetPrivateProfileString(
                    m_szSetInfoSectionName,
                    m_szKey_ShutterSpeed,
                    defValue,
                    retVal1,
                    nSize,
                    filepath
                );
            if (true == int.TryParse(retVal1.ToString(), out num))
            {
                SquareSize_Height = num;
            }


            Tool_Angle_Definition_Value = 2;

            CalToleranceDistance();
            CalToleranceDistanceY();
        }          

        public static void SaveSettingInfo()
        {
            // 현재 프로그램 실행 위치
            
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
            string filepath = System.IO.Path.GetDirectoryName(path) + "\\wiki_param.ini";

            bool writeResult = false;
            string value;




            value = string.Format("{0}", Setting_Tolerance_HourX);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_HourX, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_MinX);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_MinX, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_SecX);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_SecX, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_HourY);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_HourY, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_MinY);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_MinY, value, filepath);

            value = string.Format("{0}", Setting_Tolerance_SecY);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_Setting_Tolerance_SecY, value, filepath);

            value = string.Format("{0}", SquareSize_Height);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_SquareSize_Height, value, filepath);

            value = string.Format("{0}", SquareSize_Width);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_SquareSize_Width, value, filepath);

            value = string.Format("{0}", ShutterSpeed);
            writeResult = NativeMethods.WritePrivateProfileString(m_szSetInfoSectionName, m_szKey_ShutterSpeed, value, filepath);

            SaveInfoFile(filepath);
        }

        public static void SaveInfoFile(string filepath)
        {
            string value;

            value = string.Format("{0}", Setting_Tolerance_HourX);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_HourX,
                value,
                filepath);

            value = string.Format("{0}", Setting_Tolerance_MinX);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_MinX,
                value,
                filepath);

            value = string.Format("{0}", Setting_Tolerance_SecX);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_SecX,
                value,
                filepath);

            value = string.Format("{0}", Setting_Tolerance_HourY);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_HourY,
                value,
                filepath);

            value = string.Format("{0}", Setting_Tolerance_MinY);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_MinY,
                value,
                filepath);

            value = string.Format("{0}", Setting_Tolerance_SecY);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_Setting_Tolerance_SecY,
                value,
                filepath);

            value = string.Format("{0}", SquareSize_Height);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_SquareSize_Height,
                value,
                filepath);

            value = string.Format("{0}", SquareSize_Width);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_SquareSize_Width,
                value,
                filepath);

            value = string.Format("{0}", ShutterSpeed);
            NativeMethods.WritePrivateProfileString(
                m_szSetInfoSectionName,
                m_szKey_ShutterSpeed,
                value,
                filepath);
        }

        public static void CalToleranceDistance()
        {
            double num = (double)CSetInfo.Setting_Tolerance_HourX + (double)CSetInfo.Setting_Tolerance_MinX / 60.0 + (double)CSetInfo.Setting_Tolerance_SecX / 3600.0;
            double num2 = Math.Tan(num * 2.0 / (double)CSetInfo.Tool_Angle_Definition_Value * Math.PI / 180.0) * CParameter.FocalLength / CParameter.PixelPerDistance;
            double num3 = num2 / (Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0));
            double toleranceDistance = num3;
            ToleranceDistanceX = toleranceDistance;
        }
        public static void CalToleranceDistanceY()
        {
            double num = (double)CSetInfo.Setting_Tolerance_HourY + (double)CSetInfo.Setting_Tolerance_MinY / 60.0 + (double)CSetInfo.Setting_Tolerance_SecY / 3600.0;
            double num2 = Math.Tan(num * 2.0 / (double)CSetInfo.Tool_Angle_Definition_Value * Math.PI / 180.0) * CParameter.FocalLength / CParameter.PixelPerDistance;
            double num3 = num2 / (Math.Cos(Math.PI * CParameter.TiltAngle / 180.0) + Math.Sin(Math.PI * CParameter.TiltAngle / 180.0));
            double toleranceDistanceY = num3;
            ToleranceDistanceY = toleranceDistanceY;
        }
    }
}
