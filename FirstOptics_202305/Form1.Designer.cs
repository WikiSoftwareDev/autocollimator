﻿using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menuBar = new System.Windows.Forms.MenuStrip();
            this.menu_Measurement = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_ZeroSet = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Area = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Brightness = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Mode_Peak = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angledefinition = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angle_Ext = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Angle_Int = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Xon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Yon = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Mirroring_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_L90 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_R90 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Rotating_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Coordinate_Original = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbering = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbring_Angle = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Numbring_Size = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_4 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_8 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_16 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_32 = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Tools_Zoom_Off = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_Unit = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Unit_Degree = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Unit_Sec = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_Tolerance = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Circle = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Square = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Settings_Tolerance_Value = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Setting_GrayLevel = new System.Windows.Forms.ToolStripMenuItem();
            this.menu_Information = new System.Windows.Forms.ToolStripMenuItem();
            this.picWikiLogo = new System.Windows.Forms.PictureBox();
            this.panel_Icon = new System.Windows.Forms.Panel();
            this.InstallSerialDriver = new System.Windows.Forms.Button();
            this.AutoShutter = new System.Windows.Forms.Button();
            this.SaveAllConfig = new System.Windows.Forms.Button();
            this.cmdClear = new System.Windows.Forms.Button();
            this.cmdPrint = new System.Windows.Forms.Button();
            this.cmdExport = new System.Windows.Forms.Button();
            this.btn_SettingFileOpen = new System.Windows.Forms.Button();
            this.cmdFactorySet = new System.Windows.Forms.Button();
            this.cmdAnalysis = new System.Windows.Forms.Button();
            this.btn_Capture = new System.Windows.Forms.Button();
            this.btn_SettingFileSave = new System.Windows.Forms.Button();
            this.panel_Set = new System.Windows.Forms.Panel();
            this.txtSamplingFreq = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.cmdMeasureData = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel_Status = new System.Windows.Forms.Panel();
            this.lbStatusDescription = new System.Windows.Forms.Label();
            this.lbStatusResult = new System.Windows.Forms.Label();
            this.pic_Status15 = new System.Windows.Forms.PictureBox();
            this.pic_Status14 = new System.Windows.Forms.PictureBox();
            this.pic_Status13 = new System.Windows.Forms.PictureBox();
            this.pic_Status12 = new System.Windows.Forms.PictureBox();
            this.pic_Status11 = new System.Windows.Forms.PictureBox();
            this.pic_Status10 = new System.Windows.Forms.PictureBox();
            this.pic_Status9 = new System.Windows.Forms.PictureBox();
            this.pic_Status8 = new System.Windows.Forms.PictureBox();
            this.pic_Status7 = new System.Windows.Forms.PictureBox();
            this.pic_Status6 = new System.Windows.Forms.PictureBox();
            this.pic_Status5 = new System.Windows.Forms.PictureBox();
            this.pic_Status4 = new System.Windows.Forms.PictureBox();
            this.pic_Status3 = new System.Windows.Forms.PictureBox();
            this.pic_Status2 = new System.Windows.Forms.PictureBox();
            this.pic_Status1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.TextBox_ShutterSpeed = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.lbShutter = new System.Windows.Forms.Label();
            this.Track_ShutterSpeed = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.panel_image = new System.Windows.Forms.Panel();
            this.pic_Camera = new System.Windows.Forms.PictureBox();
            this.panel_Info = new System.Windows.Forms.Panel();
            this.lbFPS = new System.Windows.Forms.Label();
            this.lbCurrentSpotNo = new System.Windows.Forms.Label();
            this.lbCurrentMode = new System.Windows.Forms.Label();
            this.lbTolerance = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbFPSb = new System.Windows.Forms.Label();
            this.SaveProgressbar = new System.Windows.Forms.ProgressBar();
            this.txtPixelPerDistance = new System.Windows.Forms.TextBox();
            this.imageListButton = new System.Windows.Forms.ImageList(this.components);
            this.squareSizeWidth = new System.Windows.Forms.TextBox();
            this.squareSizeHeight = new System.Windows.Forms.TextBox();
            this.squareSizeSet = new System.Windows.Forms.Button();
            this.squareWidth = new System.Windows.Forms.Label();
            this.squareHeight = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtToleranceDoX = new System.Windows.Forms.TextBox();
            this.txtToleranceMinX = new System.Windows.Forms.TextBox();
            this.txtToleranceSecX = new System.Windows.Forms.TextBox();
            this.txtToleranceDoY = new System.Windows.Forms.TextBox();
            this.txtToleranceMinY = new System.Windows.Forms.TextBox();
            this.txtToleranceSecY = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.menuBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).BeginInit();
            this.panel_Icon.SuspendLayout();
            this.panel_Set.SuspendLayout();
            this.panel_Status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Track_ShutterSpeed)).BeginInit();
            this.panel_image.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Camera)).BeginInit();
            this.panel_Info.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuBar
            // 
            this.menuBar.BackColor = System.Drawing.Color.White;
            this.menuBar.Dock = System.Windows.Forms.DockStyle.None;
            this.menuBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Measurement,
            this.menu_Tools,
            this.menu_Settings,
            this.menu_Information});
            this.menuBar.Location = new System.Drawing.Point(9, 5);
            this.menuBar.Name = "menuBar";
            this.menuBar.Size = new System.Drawing.Size(63, 24);
            this.menuBar.TabIndex = 0;
            this.menuBar.Text = "menuBar";
            // 
            // menu_Measurement
            // 
            this.menu_Measurement.MergeIndex = 10;
            this.menu_Measurement.Name = "menu_Measurement";
            this.menu_Measurement.Size = new System.Drawing.Size(104, 20);
            this.menu_Measurement.Text = "MEASUREMENT";
            this.menu_Measurement.Visible = false;
            // 
            // menu_Tools
            // 
            this.menu_Tools.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_ZeroSet,
            this.menu_Tools_Mode,
            this.menu_Tools_Angledefinition,
            this.menu_Tools_Coordinate,
            this.menu_Tools_Numbering,
            this.menu_Tools_Zoom});
            this.menu_Tools.MergeIndex = 10;
            this.menu_Tools.Name = "menu_Tools";
            this.menu_Tools.Size = new System.Drawing.Size(56, 20);
            this.menu_Tools.Text = "TOOLS";
            this.menu_Tools.Visible = false;
            // 
            // menu_Tools_ZeroSet
            // 
            this.menu_Tools_ZeroSet.BackColor = System.Drawing.Color.White;
            this.menu_Tools_ZeroSet.Name = "menu_Tools_ZeroSet";
            this.menu_Tools_ZeroSet.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_ZeroSet.Text = "ZERO SET";
            this.menu_Tools_ZeroSet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Mode
            // 
            this.menu_Tools_Mode.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Mode.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Mode_Area,
            this.menu_Tools_Mode_Brightness,
            this.menu_Tools_Mode_Peak});
            this.menu_Tools_Mode.Name = "menu_Tools_Mode";
            this.menu_Tools_Mode.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Mode.Text = "MODE";
            this.menu_Tools_Mode.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Mode_Area
            // 
            this.menu_Tools_Mode_Area.CheckOnClick = true;
            this.menu_Tools_Mode_Area.Name = "menu_Tools_Mode_Area";
            this.menu_Tools_Mode_Area.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode_Area.Text = "AREA CENTER";
            // 
            // menu_Tools_Mode_Brightness
            // 
            this.menu_Tools_Mode_Brightness.CheckOnClick = true;
            this.menu_Tools_Mode_Brightness.Name = "menu_Tools_Mode_Brightness";
            this.menu_Tools_Mode_Brightness.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode_Brightness.Text = "BRIGHTNESS CENTER";
            // 
            // menu_Tools_Mode_Peak
            // 
            this.menu_Tools_Mode_Peak.CheckOnClick = true;
            this.menu_Tools_Mode_Peak.Name = "menu_Tools_Mode_Peak";
            this.menu_Tools_Mode_Peak.Size = new System.Drawing.Size(189, 22);
            this.menu_Tools_Mode_Peak.Text = "PEAK";
            // 
            // menu_Tools_Angledefinition
            // 
            this.menu_Tools_Angledefinition.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Angledefinition.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Angle_Ext,
            this.menu_Tools_Angle_Int});
            this.menu_Tools_Angledefinition.Name = "menu_Tools_Angledefinition";
            this.menu_Tools_Angledefinition.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Angledefinition.Text = "ANGLE DEFINITION";
            this.menu_Tools_Angledefinition.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Angle_Ext
            // 
            this.menu_Tools_Angle_Ext.CheckOnClick = true;
            this.menu_Tools_Angle_Ext.Name = "menu_Tools_Angle_Ext";
            this.menu_Tools_Angle_Ext.Size = new System.Drawing.Size(227, 22);
            this.menu_Tools_Angle_Ext.Text = "EXTERNAL LIGHT MODE(2α)";
            // 
            // menu_Tools_Angle_Int
            // 
            this.menu_Tools_Angle_Int.CheckOnClick = true;
            this.menu_Tools_Angle_Int.Name = "menu_Tools_Angle_Int";
            this.menu_Tools_Angle_Int.Size = new System.Drawing.Size(227, 22);
            this.menu_Tools_Angle_Int.Text = "INTERNAL LIGHT MODE(α)";
            // 
            // menu_Tools_Coordinate
            // 
            this.menu_Tools_Coordinate.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Coordinate.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Mirroring,
            this.menu_Tools_Coordinate_Rotating,
            this.menu_Tools_Coordinate_Original});
            this.menu_Tools_Coordinate.Name = "menu_Tools_Coordinate";
            this.menu_Tools_Coordinate.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Coordinate.Text = "COORDINATE";
            this.menu_Tools_Coordinate.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Coordinate_Mirroring
            // 
            this.menu_Tools_Coordinate_Mirroring.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Mirroring_Xon,
            this.menu_Tools_Coordinate_Mirroring_Yon,
            this.menu_Tools_Coordinate_Mirroring_Off});
            this.menu_Tools_Coordinate_Mirroring.Name = "menu_Tools_Coordinate_Mirroring";
            this.menu_Tools_Coordinate_Mirroring.Size = new System.Drawing.Size(138, 22);
            this.menu_Tools_Coordinate_Mirroring.Text = "MIRRORING";
            // 
            // menu_Tools_Coordinate_Mirroring_Xon
            // 
            this.menu_Tools_Coordinate_Mirroring_Xon.Name = "menu_Tools_Coordinate_Mirroring_Xon";
            this.menu_Tools_Coordinate_Mirroring_Xon.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Coordinate_Mirroring_Xon.Text = "X-on";
            // 
            // menu_Tools_Coordinate_Mirroring_Yon
            // 
            this.menu_Tools_Coordinate_Mirroring_Yon.Name = "menu_Tools_Coordinate_Mirroring_Yon";
            this.menu_Tools_Coordinate_Mirroring_Yon.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Coordinate_Mirroring_Yon.Text = "Y-on";
            // 
            // menu_Tools_Coordinate_Mirroring_Off
            // 
            this.menu_Tools_Coordinate_Mirroring_Off.Name = "menu_Tools_Coordinate_Mirroring_Off";
            this.menu_Tools_Coordinate_Mirroring_Off.Size = new System.Drawing.Size(100, 22);
            this.menu_Tools_Coordinate_Mirroring_Off.Text = "OFF";
            // 
            // menu_Tools_Coordinate_Rotating
            // 
            this.menu_Tools_Coordinate_Rotating.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Coordinate_Rotating_L90,
            this.menu_Tools_Coordinate_Rotating_R90,
            this.menu_Tools_Coordinate_Rotating_Off});
            this.menu_Tools_Coordinate_Rotating.Name = "menu_Tools_Coordinate_Rotating";
            this.menu_Tools_Coordinate_Rotating.Size = new System.Drawing.Size(138, 22);
            this.menu_Tools_Coordinate_Rotating.Text = "ROTATING";
            // 
            // menu_Tools_Coordinate_Rotating_L90
            // 
            this.menu_Tools_Coordinate_Rotating_L90.Name = "menu_Tools_Coordinate_Rotating_L90";
            this.menu_Tools_Coordinate_Rotating_L90.Size = new System.Drawing.Size(95, 22);
            this.menu_Tools_Coordinate_Rotating_L90.Text = "L90";
            // 
            // menu_Tools_Coordinate_Rotating_R90
            // 
            this.menu_Tools_Coordinate_Rotating_R90.Name = "menu_Tools_Coordinate_Rotating_R90";
            this.menu_Tools_Coordinate_Rotating_R90.Size = new System.Drawing.Size(95, 22);
            this.menu_Tools_Coordinate_Rotating_R90.Text = "R90";
            // 
            // menu_Tools_Coordinate_Rotating_Off
            // 
            this.menu_Tools_Coordinate_Rotating_Off.Name = "menu_Tools_Coordinate_Rotating_Off";
            this.menu_Tools_Coordinate_Rotating_Off.Size = new System.Drawing.Size(95, 22);
            this.menu_Tools_Coordinate_Rotating_Off.Text = "OFF";
            // 
            // menu_Tools_Coordinate_Original
            // 
            this.menu_Tools_Coordinate_Original.Name = "menu_Tools_Coordinate_Original";
            this.menu_Tools_Coordinate_Original.Size = new System.Drawing.Size(138, 22);
            this.menu_Tools_Coordinate_Original.Text = "ORIGINAL";
            this.menu_Tools_Coordinate_Original.Visible = false;
            // 
            // menu_Tools_Numbering
            // 
            this.menu_Tools_Numbering.BackColor = System.Drawing.Color.White;
            this.menu_Tools_Numbering.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Numbring_Angle,
            this.menu_Tools_Numbring_Size});
            this.menu_Tools_Numbering.Name = "menu_Tools_Numbering";
            this.menu_Tools_Numbering.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Numbering.Text = "NUMBERING";
            this.menu_Tools_Numbering.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // menu_Tools_Numbring_Angle
            // 
            this.menu_Tools_Numbring_Angle.CheckOnClick = true;
            this.menu_Tools_Numbring_Angle.Name = "menu_Tools_Numbring_Angle";
            this.menu_Tools_Numbring_Angle.Size = new System.Drawing.Size(111, 22);
            this.menu_Tools_Numbring_Angle.Text = "ANGLE";
            // 
            // menu_Tools_Numbring_Size
            // 
            this.menu_Tools_Numbring_Size.CheckOnClick = true;
            this.menu_Tools_Numbring_Size.Name = "menu_Tools_Numbring_Size";
            this.menu_Tools_Numbring_Size.Size = new System.Drawing.Size(111, 22);
            this.menu_Tools_Numbring_Size.Text = "SIZE";
            // 
            // menu_Tools_Zoom
            // 
            this.menu_Tools_Zoom.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Tools_Zoom_4,
            this.menu_Tools_Zoom_8,
            this.menu_Tools_Zoom_16,
            this.menu_Tools_Zoom_32,
            this.menu_Tools_Zoom_Off});
            this.menu_Tools_Zoom.Name = "menu_Tools_Zoom";
            this.menu_Tools_Zoom.Size = new System.Drawing.Size(178, 22);
            this.menu_Tools_Zoom.Text = "ZOOM";
            // 
            // menu_Tools_Zoom_4
            // 
            this.menu_Tools_Zoom_4.Name = "menu_Tools_Zoom_4";
            this.menu_Tools_Zoom_4.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_4.Text = "x 4";
            // 
            // menu_Tools_Zoom_8
            // 
            this.menu_Tools_Zoom_8.Name = "menu_Tools_Zoom_8";
            this.menu_Tools_Zoom_8.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_8.Text = "x 8";
            // 
            // menu_Tools_Zoom_16
            // 
            this.menu_Tools_Zoom_16.Name = "menu_Tools_Zoom_16";
            this.menu_Tools_Zoom_16.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_16.Text = "x 16";
            // 
            // menu_Tools_Zoom_32
            // 
            this.menu_Tools_Zoom_32.Name = "menu_Tools_Zoom_32";
            this.menu_Tools_Zoom_32.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_32.Text = "x 32";
            // 
            // menu_Tools_Zoom_Off
            // 
            this.menu_Tools_Zoom_Off.Name = "menu_Tools_Zoom_Off";
            this.menu_Tools_Zoom_Off.Size = new System.Drawing.Size(98, 22);
            this.menu_Tools_Zoom_Off.Text = "Off";
            // 
            // menu_Settings
            // 
            this.menu_Settings.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Setting_Unit,
            this.menu_Setting_Tolerance,
            this.menu_Setting_GrayLevel});
            this.menu_Settings.Name = "menu_Settings";
            this.menu_Settings.Size = new System.Drawing.Size(71, 20);
            this.menu_Settings.Text = "SETTINGS";
            this.menu_Settings.Visible = false;
            // 
            // menu_Setting_Unit
            // 
            this.menu_Setting_Unit.BackColor = System.Drawing.Color.White;
            this.menu_Setting_Unit.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Settings_Unit_Degree,
            this.menu_Settings_Unit_Sec});
            this.menu_Setting_Unit.Name = "menu_Setting_Unit";
            this.menu_Setting_Unit.Size = new System.Drawing.Size(139, 22);
            this.menu_Setting_Unit.Text = "UNIT";
            // 
            // menu_Settings_Unit_Degree
            // 
            this.menu_Settings_Unit_Degree.CheckOnClick = true;
            this.menu_Settings_Unit_Degree.Name = "menu_Settings_Unit_Degree";
            this.menu_Settings_Unit_Degree.Size = new System.Drawing.Size(126, 22);
            this.menu_Settings_Unit_Degree.Text = "DEGREE";
            // 
            // menu_Settings_Unit_Sec
            // 
            this.menu_Settings_Unit_Sec.CheckOnClick = true;
            this.menu_Settings_Unit_Sec.Name = "menu_Settings_Unit_Sec";
            this.menu_Settings_Unit_Sec.Size = new System.Drawing.Size(126, 22);
            this.menu_Settings_Unit_Sec.Text = "MIN+SEC";
            // 
            // menu_Setting_Tolerance
            // 
            this.menu_Setting_Tolerance.BackColor = System.Drawing.Color.White;
            this.menu_Setting_Tolerance.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menu_Settings_Tolerance_Circle,
            this.menu_Settings_Tolerance_Square,
            this.menu_Settings_Tolerance_Value});
            this.menu_Setting_Tolerance.Name = "menu_Setting_Tolerance";
            this.menu_Setting_Tolerance.Size = new System.Drawing.Size(139, 22);
            this.menu_Setting_Tolerance.Text = "TOLERANCE";
            // 
            // menu_Settings_Tolerance_Circle
            // 
            this.menu_Settings_Tolerance_Circle.CheckOnClick = true;
            this.menu_Settings_Tolerance_Circle.Name = "menu_Settings_Tolerance_Circle";
            this.menu_Settings_Tolerance_Circle.Size = new System.Drawing.Size(159, 22);
            this.menu_Settings_Tolerance_Circle.Text = "Circle";
            // 
            // menu_Settings_Tolerance_Square
            // 
            this.menu_Settings_Tolerance_Square.CheckOnClick = true;
            this.menu_Settings_Tolerance_Square.Name = "menu_Settings_Tolerance_Square";
            this.menu_Settings_Tolerance_Square.Size = new System.Drawing.Size(159, 22);
            this.menu_Settings_Tolerance_Square.Text = "Square";
            // 
            // menu_Settings_Tolerance_Value
            // 
            this.menu_Settings_Tolerance_Value.Name = "menu_Settings_Tolerance_Value";
            this.menu_Settings_Tolerance_Value.Size = new System.Drawing.Size(159, 22);
            this.menu_Settings_Tolerance_Value.Text = "Tolerance Value";
            // 
            // menu_Setting_GrayLevel
            // 
            this.menu_Setting_GrayLevel.BackColor = System.Drawing.Color.White;
            this.menu_Setting_GrayLevel.Name = "menu_Setting_GrayLevel";
            this.menu_Setting_GrayLevel.Size = new System.Drawing.Size(139, 22);
            this.menu_Setting_GrayLevel.Text = "GREY LEVEL";
            // 
            // menu_Information
            // 
            this.menu_Information.Name = "menu_Information";
            this.menu_Information.Size = new System.Drawing.Size(55, 20);
            this.menu_Information.Text = "About.";
            this.menu_Information.Click += new System.EventHandler(this.menu_Information_About_Click);
            // 
            // picWikiLogo
            // 
            this.picWikiLogo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picWikiLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.picWikiLogo.Image = global::WikiOptics_Collimator.Properties.Resources.picWikiLogo;
            this.picWikiLogo.Location = new System.Drawing.Point(771, 4);
            this.picWikiLogo.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.picWikiLogo.Name = "picWikiLogo";
            this.picWikiLogo.Size = new System.Drawing.Size(142, 25);
            this.picWikiLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picWikiLogo.TabIndex = 1;
            this.picWikiLogo.TabStop = false;
            // 
            // panel_Icon
            // 
            this.panel_Icon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel_Icon.BackColor = System.Drawing.Color.White;
            this.panel_Icon.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Icon.Controls.Add(this.InstallSerialDriver);
            this.panel_Icon.Controls.Add(this.AutoShutter);
            this.panel_Icon.Controls.Add(this.SaveAllConfig);
            this.panel_Icon.Controls.Add(this.cmdClear);
            this.panel_Icon.Controls.Add(this.cmdPrint);
            this.panel_Icon.Controls.Add(this.cmdExport);
            this.panel_Icon.Controls.Add(this.btn_SettingFileOpen);
            this.panel_Icon.Location = new System.Drawing.Point(95, 90);
            this.panel_Icon.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Icon.Name = "panel_Icon";
            this.panel_Icon.Size = new System.Drawing.Size(96, 174);
            this.panel_Icon.TabIndex = 2;
            this.panel_Icon.Visible = false;
            // 
            // InstallSerialDriver
            // 
            this.InstallSerialDriver.ForeColor = System.Drawing.Color.Red;
            this.InstallSerialDriver.Location = new System.Drawing.Point(3, 388);
            this.InstallSerialDriver.Name = "InstallSerialDriver";
            this.InstallSerialDriver.Size = new System.Drawing.Size(87, 50);
            this.InstallSerialDriver.TabIndex = 11;
            this.InstallSerialDriver.Text = "Install Serial Driver";
            this.InstallSerialDriver.UseVisualStyleBackColor = true;
            this.InstallSerialDriver.Visible = false;
            // 
            // AutoShutter
            // 
            this.AutoShutter.Location = new System.Drawing.Point(3, 348);
            this.AutoShutter.Name = "AutoShutter";
            this.AutoShutter.Size = new System.Drawing.Size(88, 34);
            this.AutoShutter.TabIndex = 10;
            this.AutoShutter.Text = "AutoShutter";
            this.AutoShutter.UseVisualStyleBackColor = true;
            this.AutoShutter.Visible = false;
            // 
            // SaveAllConfig
            // 
            this.SaveAllConfig.Location = new System.Drawing.Point(-1, 95);
            this.SaveAllConfig.Name = "SaveAllConfig";
            this.SaveAllConfig.Size = new System.Drawing.Size(96, 42);
            this.SaveAllConfig.TabIndex = 9;
            this.SaveAllConfig.Text = "SAVE";
            this.SaveAllConfig.UseVisualStyleBackColor = true;
            this.SaveAllConfig.Visible = false;
            // 
            // cmdClear
            // 
            this.cmdClear.FlatAppearance.BorderSize = 0;
            this.cmdClear.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdClear.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdClear.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdClear.ImageIndex = 5;
            this.cmdClear.Location = new System.Drawing.Point(-1, 191);
            this.cmdClear.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdClear.Name = "cmdClear";
            this.cmdClear.Size = new System.Drawing.Size(96, 42);
            this.cmdClear.TabIndex = 4;
            this.cmdClear.Text = "CLEAR";
            this.cmdClear.UseVisualStyleBackColor = true;
            this.cmdClear.Visible = false;
            // 
            // cmdPrint
            // 
            this.cmdPrint.FlatAppearance.BorderSize = 0;
            this.cmdPrint.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdPrint.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdPrint.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdPrint.ImageIndex = 7;
            this.cmdPrint.Location = new System.Drawing.Point(0, 299);
            this.cmdPrint.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdPrint.Name = "cmdPrint";
            this.cmdPrint.Size = new System.Drawing.Size(96, 42);
            this.cmdPrint.TabIndex = 6;
            this.cmdPrint.Text = "PRINT";
            this.cmdPrint.UseVisualStyleBackColor = true;
            this.cmdPrint.Visible = false;
            // 
            // cmdExport
            // 
            this.cmdExport.FlatAppearance.BorderSize = 0;
            this.cmdExport.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdExport.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdExport.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdExport.ImageIndex = 4;
            this.cmdExport.Location = new System.Drawing.Point(-1, 143);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(96, 42);
            this.cmdExport.TabIndex = 3;
            this.cmdExport.Text = "EXPORT";
            this.cmdExport.UseVisualStyleBackColor = true;
            this.cmdExport.Visible = false;
            // 
            // btn_SettingFileOpen
            // 
            this.btn_SettingFileOpen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_SettingFileOpen.FlatAppearance.BorderSize = 0;
            this.btn_SettingFileOpen.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_SettingFileOpen.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btn_SettingFileOpen.ImageIndex = 2;
            this.btn_SettingFileOpen.Location = new System.Drawing.Point(0, 238);
            this.btn_SettingFileOpen.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_SettingFileOpen.Name = "btn_SettingFileOpen";
            this.btn_SettingFileOpen.Size = new System.Drawing.Size(96, 42);
            this.btn_SettingFileOpen.TabIndex = 1;
            this.btn_SettingFileOpen.Text = "OPEN";
            this.btn_SettingFileOpen.UseVisualStyleBackColor = true;
            this.btn_SettingFileOpen.Visible = false;
            // 
            // cmdFactorySet
            // 
            this.cmdFactorySet.BackColor = System.Drawing.Color.Red;
            this.cmdFactorySet.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.cmdFactorySet.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Fuchsia;
            this.cmdFactorySet.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Fuchsia;
            this.cmdFactorySet.Font = new System.Drawing.Font("Arial", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFactorySet.ForeColor = System.Drawing.Color.White;
            this.cmdFactorySet.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdFactorySet.Location = new System.Drawing.Point(1055, 335);
            this.cmdFactorySet.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdFactorySet.Name = "cmdFactorySet";
            this.cmdFactorySet.Size = new System.Drawing.Size(96, 58);
            this.cmdFactorySet.TabIndex = 8;
            this.cmdFactorySet.Text = "FACTORY SET";
            this.cmdFactorySet.UseVisualStyleBackColor = false;
            this.cmdFactorySet.Visible = false;
            // 
            // cmdAnalysis
            // 
            this.cmdAnalysis.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.cmdAnalysis.FlatAppearance.BorderSize = 0;
            this.cmdAnalysis.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.cmdAnalysis.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.cmdAnalysis.Font = new System.Drawing.Font("Arial Narrow", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnalysis.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdAnalysis.ImageIndex = 0;
            this.cmdAnalysis.Location = new System.Drawing.Point(8, 45);
            this.cmdAnalysis.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdAnalysis.Name = "cmdAnalysis";
            this.cmdAnalysis.Size = new System.Drawing.Size(96, 42);
            this.cmdAnalysis.TabIndex = 0;
            this.cmdAnalysis.Text = "START";
            this.cmdAnalysis.UseVisualStyleBackColor = true;
            this.cmdAnalysis.Click += new System.EventHandler(this.cmdAnalysis_Click);
            // 
            // btn_Capture
            // 
            this.btn_Capture.FlatAppearance.BorderSize = 0;
            this.btn_Capture.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_Capture.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btn_Capture.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_Capture.ImageIndex = 1;
            this.btn_Capture.Location = new System.Drawing.Point(7, 97);
            this.btn_Capture.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_Capture.Name = "btn_Capture";
            this.btn_Capture.Size = new System.Drawing.Size(96, 42);
            this.btn_Capture.TabIndex = 5;
            this.btn_Capture.Text = "CAPTURE";
            this.btn_Capture.UseVisualStyleBackColor = true;
            this.btn_Capture.Click += new System.EventHandler(this.btn_Capture_Click);
            // 
            // btn_SettingFileSave
            // 
            this.btn_SettingFileSave.FlatAppearance.BorderSize = 0;
            this.btn_SettingFileSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DimGray;
            this.btn_SettingFileSave.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkGray;
            this.btn_SettingFileSave.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btn_SettingFileSave.ImageIndex = 3;
            this.btn_SettingFileSave.Location = new System.Drawing.Point(689, 154);
            this.btn_SettingFileSave.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btn_SettingFileSave.Name = "btn_SettingFileSave";
            this.btn_SettingFileSave.Size = new System.Drawing.Size(83, 48);
            this.btn_SettingFileSave.TabIndex = 2;
            this.btn_SettingFileSave.Text = "SAVE";
            this.btn_SettingFileSave.UseVisualStyleBackColor = true;
            this.btn_SettingFileSave.Click += new System.EventHandler(this.btn_SettingFileSave_Click);
            // 
            // panel_Set
            // 
            this.panel_Set.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Set.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Set.Controls.Add(this.txtSamplingFreq);
            this.panel_Set.Controls.Add(this.label37);
            this.panel_Set.Controls.Add(this.cmdMeasureData);
            this.panel_Set.Controls.Add(this.label5);
            this.panel_Set.Location = new System.Drawing.Point(885, 313);
            this.panel_Set.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Set.Name = "panel_Set";
            this.panel_Set.Size = new System.Drawing.Size(403, 48);
            this.panel_Set.TabIndex = 3;
            this.panel_Set.Visible = false;
            // 
            // txtSamplingFreq
            // 
            this.txtSamplingFreq.Location = new System.Drawing.Point(149, 15);
            this.txtSamplingFreq.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtSamplingFreq.Name = "txtSamplingFreq";
            this.txtSamplingFreq.Size = new System.Drawing.Size(100, 21);
            this.txtSamplingFreq.TabIndex = 12;
            this.txtSamplingFreq.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(255, 18);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(110, 15);
            this.label37.TabIndex = 11;
            this.label37.Text = "sec (0.1s ~ 3600s)";
            // 
            // cmdMeasureData
            // 
            this.cmdMeasureData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdMeasureData.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMeasureData.Font = new System.Drawing.Font("Gulim", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.cmdMeasureData.Location = new System.Drawing.Point(362, 10);
            this.cmdMeasureData.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmdMeasureData.Name = "cmdMeasureData";
            this.cmdMeasureData.Size = new System.Drawing.Size(206, 26);
            this.cmdMeasureData.TabIndex = 10;
            this.cmdMeasureData.Text = "Start measurement";
            this.cmdMeasureData.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(28, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Sampling frequency";
            // 
            // panel_Status
            // 
            this.panel_Status.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Status.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Status.Controls.Add(this.lbStatusDescription);
            this.panel_Status.Controls.Add(this.lbStatusResult);
            this.panel_Status.Controls.Add(this.pic_Status15);
            this.panel_Status.Controls.Add(this.pic_Status14);
            this.panel_Status.Controls.Add(this.pic_Status13);
            this.panel_Status.Controls.Add(this.pic_Status12);
            this.panel_Status.Controls.Add(this.pic_Status11);
            this.panel_Status.Controls.Add(this.pic_Status10);
            this.panel_Status.Controls.Add(this.pic_Status9);
            this.panel_Status.Controls.Add(this.pic_Status8);
            this.panel_Status.Controls.Add(this.pic_Status7);
            this.panel_Status.Controls.Add(this.pic_Status6);
            this.panel_Status.Controls.Add(this.pic_Status5);
            this.panel_Status.Controls.Add(this.pic_Status4);
            this.panel_Status.Controls.Add(this.pic_Status3);
            this.panel_Status.Controls.Add(this.pic_Status2);
            this.panel_Status.Controls.Add(this.pic_Status1);
            this.panel_Status.Location = new System.Drawing.Point(885, 271);
            this.panel_Status.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Status.Name = "panel_Status";
            this.panel_Status.Size = new System.Drawing.Size(403, 34);
            this.panel_Status.TabIndex = 5;
            this.panel_Status.Visible = false;
            // 
            // lbStatusDescription
            // 
            this.lbStatusDescription.AutoSize = true;
            this.lbStatusDescription.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatusDescription.ForeColor = System.Drawing.Color.Green;
            this.lbStatusDescription.Location = new System.Drawing.Point(449, 10);
            this.lbStatusDescription.Name = "lbStatusDescription";
            this.lbStatusDescription.Size = new System.Drawing.Size(12, 17);
            this.lbStatusDescription.TabIndex = 16;
            this.lbStatusDescription.Text = ".";
            this.lbStatusDescription.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.lbStatusDescription.Visible = false;
            // 
            // lbStatusResult
            // 
            this.lbStatusResult.AutoSize = true;
            this.lbStatusResult.Font = new System.Drawing.Font("Arial Black", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbStatusResult.ForeColor = System.Drawing.Color.Green;
            this.lbStatusResult.Location = new System.Drawing.Point(349, 5);
            this.lbStatusResult.Name = "lbStatusResult";
            this.lbStatusResult.Size = new System.Drawing.Size(36, 22);
            this.lbStatusResult.TabIndex = 15;
            this.lbStatusResult.Text = "OK";
            this.lbStatusResult.Visible = false;
            // 
            // pic_Status15
            // 
            this.pic_Status15.BackColor = System.Drawing.Color.Red;
            this.pic_Status15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status15.Location = new System.Drawing.Point(290, 4);
            this.pic_Status15.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status15.Name = "pic_Status15";
            this.pic_Status15.Size = new System.Drawing.Size(19, 23);
            this.pic_Status15.TabIndex = 14;
            this.pic_Status15.TabStop = false;
            this.pic_Status15.Visible = false;
            // 
            // pic_Status14
            // 
            this.pic_Status14.BackColor = System.Drawing.Color.Red;
            this.pic_Status14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status14.Location = new System.Drawing.Point(270, 4);
            this.pic_Status14.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status14.Name = "pic_Status14";
            this.pic_Status14.Size = new System.Drawing.Size(19, 23);
            this.pic_Status14.TabIndex = 13;
            this.pic_Status14.TabStop = false;
            this.pic_Status14.Visible = false;
            // 
            // pic_Status13
            // 
            this.pic_Status13.BackColor = System.Drawing.Color.Red;
            this.pic_Status13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status13.Location = new System.Drawing.Point(250, 4);
            this.pic_Status13.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status13.Name = "pic_Status13";
            this.pic_Status13.Size = new System.Drawing.Size(19, 23);
            this.pic_Status13.TabIndex = 12;
            this.pic_Status13.TabStop = false;
            this.pic_Status13.Visible = false;
            // 
            // pic_Status12
            // 
            this.pic_Status12.BackColor = System.Drawing.Color.Green;
            this.pic_Status12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status12.Location = new System.Drawing.Point(230, 4);
            this.pic_Status12.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status12.Name = "pic_Status12";
            this.pic_Status12.Size = new System.Drawing.Size(19, 23);
            this.pic_Status12.TabIndex = 11;
            this.pic_Status12.TabStop = false;
            this.pic_Status12.Visible = false;
            // 
            // pic_Status11
            // 
            this.pic_Status11.BackColor = System.Drawing.Color.Green;
            this.pic_Status11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status11.Location = new System.Drawing.Point(210, 4);
            this.pic_Status11.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status11.Name = "pic_Status11";
            this.pic_Status11.Size = new System.Drawing.Size(19, 23);
            this.pic_Status11.TabIndex = 10;
            this.pic_Status11.TabStop = false;
            this.pic_Status11.Visible = false;
            // 
            // pic_Status10
            // 
            this.pic_Status10.BackColor = System.Drawing.Color.Green;
            this.pic_Status10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status10.Location = new System.Drawing.Point(190, 4);
            this.pic_Status10.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status10.Name = "pic_Status10";
            this.pic_Status10.Size = new System.Drawing.Size(19, 23);
            this.pic_Status10.TabIndex = 9;
            this.pic_Status10.TabStop = false;
            this.pic_Status10.Visible = false;
            // 
            // pic_Status9
            // 
            this.pic_Status9.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status9.Location = new System.Drawing.Point(170, 4);
            this.pic_Status9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status9.Name = "pic_Status9";
            this.pic_Status9.Size = new System.Drawing.Size(19, 23);
            this.pic_Status9.TabIndex = 8;
            this.pic_Status9.TabStop = false;
            this.pic_Status9.Visible = false;
            // 
            // pic_Status8
            // 
            this.pic_Status8.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status8.Location = new System.Drawing.Point(150, 4);
            this.pic_Status8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status8.Name = "pic_Status8";
            this.pic_Status8.Size = new System.Drawing.Size(19, 23);
            this.pic_Status8.TabIndex = 7;
            this.pic_Status8.TabStop = false;
            this.pic_Status8.Visible = false;
            // 
            // pic_Status7
            // 
            this.pic_Status7.BackColor = System.Drawing.Color.GreenYellow;
            this.pic_Status7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status7.Location = new System.Drawing.Point(130, 4);
            this.pic_Status7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status7.Name = "pic_Status7";
            this.pic_Status7.Size = new System.Drawing.Size(19, 23);
            this.pic_Status7.TabIndex = 6;
            this.pic_Status7.TabStop = false;
            this.pic_Status7.Visible = false;
            // 
            // pic_Status6
            // 
            this.pic_Status6.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status6.Location = new System.Drawing.Point(110, 4);
            this.pic_Status6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status6.Name = "pic_Status6";
            this.pic_Status6.Size = new System.Drawing.Size(19, 23);
            this.pic_Status6.TabIndex = 5;
            this.pic_Status6.TabStop = false;
            this.pic_Status6.Visible = false;
            // 
            // pic_Status5
            // 
            this.pic_Status5.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status5.Location = new System.Drawing.Point(90, 4);
            this.pic_Status5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status5.Name = "pic_Status5";
            this.pic_Status5.Size = new System.Drawing.Size(19, 23);
            this.pic_Status5.TabIndex = 4;
            this.pic_Status5.TabStop = false;
            this.pic_Status5.Visible = false;
            // 
            // pic_Status4
            // 
            this.pic_Status4.BackColor = System.Drawing.Color.Yellow;
            this.pic_Status4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status4.Location = new System.Drawing.Point(70, 4);
            this.pic_Status4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status4.Name = "pic_Status4";
            this.pic_Status4.Size = new System.Drawing.Size(19, 23);
            this.pic_Status4.TabIndex = 3;
            this.pic_Status4.TabStop = false;
            this.pic_Status4.Visible = false;
            // 
            // pic_Status3
            // 
            this.pic_Status3.BackColor = System.Drawing.Color.Red;
            this.pic_Status3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status3.Location = new System.Drawing.Point(50, 4);
            this.pic_Status3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status3.Name = "pic_Status3";
            this.pic_Status3.Size = new System.Drawing.Size(19, 23);
            this.pic_Status3.TabIndex = 2;
            this.pic_Status3.TabStop = false;
            this.pic_Status3.Visible = false;
            // 
            // pic_Status2
            // 
            this.pic_Status2.BackColor = System.Drawing.Color.Red;
            this.pic_Status2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status2.Location = new System.Drawing.Point(30, 4);
            this.pic_Status2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status2.Name = "pic_Status2";
            this.pic_Status2.Size = new System.Drawing.Size(19, 23);
            this.pic_Status2.TabIndex = 1;
            this.pic_Status2.TabStop = false;
            this.pic_Status2.Visible = false;
            // 
            // pic_Status1
            // 
            this.pic_Status1.BackColor = System.Drawing.Color.Red;
            this.pic_Status1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pic_Status1.Location = new System.Drawing.Point(10, 4);
            this.pic_Status1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pic_Status1.Name = "pic_Status1";
            this.pic_Status1.Size = new System.Drawing.Size(19, 23);
            this.pic_Status1.TabIndex = 0;
            this.pic_Status1.TabStop = false;
            this.pic_Status1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.TextBox_ShutterSpeed);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.lbShutter);
            this.panel1.Controls.Add(this.Track_ShutterSpeed);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Location = new System.Drawing.Point(105, 552);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(538, 35);
            this.panel1.TabIndex = 6;
            // 
            // TextBox_ShutterSpeed
            // 
            this.TextBox_ShutterSpeed.BackColor = System.Drawing.Color.Black;
            this.TextBox_ShutterSpeed.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.TextBox_ShutterSpeed.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TextBox_ShutterSpeed.ForeColor = System.Drawing.Color.Lime;
            this.TextBox_ShutterSpeed.Location = new System.Drawing.Point(444, 8);
            this.TextBox_ShutterSpeed.MinimumSize = new System.Drawing.Size(70, 18);
            this.TextBox_ShutterSpeed.Name = "TextBox_ShutterSpeed";
            this.TextBox_ShutterSpeed.Size = new System.Drawing.Size(70, 15);
            this.TextBox_ShutterSpeed.TabIndex = 21;
            this.TextBox_ShutterSpeed.Text = "0.00";
            this.TextBox_ShutterSpeed.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.TextBox_ShutterSpeed.KeyUp += new System.Windows.Forms.KeyEventHandler(this.OnShutterSpeedTextBox_KeyUp);
            // 
            // label17
            // 
            this.label17.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label17.Location = new System.Drawing.Point(512, -2);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(28, 24);
            this.label17.TabIndex = 17;
            this.label17.Text = "ms";
            this.label17.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // lbShutter
            // 
            this.lbShutter.BackColor = System.Drawing.Color.Black;
            this.lbShutter.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbShutter.ForeColor = System.Drawing.Color.Lime;
            this.lbShutter.Location = new System.Drawing.Point(566, 4);
            this.lbShutter.Name = "lbShutter";
            this.lbShutter.Size = new System.Drawing.Size(70, 26);
            this.lbShutter.TabIndex = 16;
            this.lbShutter.Text = "10.000";
            this.lbShutter.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lbShutter.Visible = false;
            // 
            // Track_ShutterSpeed
            // 
            this.Track_ShutterSpeed.Location = new System.Drawing.Point(87, 4);
            this.Track_ShutterSpeed.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Track_ShutterSpeed.Maximum = 30000;
            this.Track_ShutterSpeed.Name = "Track_ShutterSpeed";
            this.Track_ShutterSpeed.Size = new System.Drawing.Size(347, 45);
            this.Track_ShutterSpeed.TabIndex = 2;
            this.Track_ShutterSpeed.TickStyle = System.Windows.Forms.TickStyle.None;
            this.Track_ShutterSpeed.Scroll += new System.EventHandler(this.Track_ShutterSpeed_Scroll);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(84, 15);
            this.label6.TabIndex = 1;
            this.label6.Text = "Shutter speed";
            // 
            // panel_image
            // 
            this.panel_image.AutoScroll = true;
            this.panel_image.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_image.Controls.Add(this.pic_Camera);
            this.panel_image.Location = new System.Drawing.Point(117, 36);
            this.panel_image.Margin = new System.Windows.Forms.Padding(0);
            this.panel_image.Name = "panel_image";
            this.panel_image.Size = new System.Drawing.Size(512, 512);
            this.panel_image.TabIndex = 0;
            // 
            // pic_Camera
            // 
            this.pic_Camera.BackColor = System.Drawing.Color.Transparent;
            this.pic_Camera.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.pic_Camera.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pic_Camera.Location = new System.Drawing.Point(0, 0);
            this.pic_Camera.Margin = new System.Windows.Forms.Padding(0);
            this.pic_Camera.Name = "pic_Camera";
            this.pic_Camera.Size = new System.Drawing.Size(510, 510);
            this.pic_Camera.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pic_Camera.TabIndex = 0;
            this.pic_Camera.TabStop = false;
            // 
            // panel_Info
            // 
            this.panel_Info.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel_Info.BackColor = System.Drawing.Color.White;
            this.panel_Info.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel_Info.Controls.Add(this.lbFPS);
            this.panel_Info.Controls.Add(this.lbCurrentSpotNo);
            this.panel_Info.Controls.Add(this.lbCurrentMode);
            this.panel_Info.Controls.Add(this.lbTolerance);
            this.panel_Info.Controls.Add(this.label12);
            this.panel_Info.Controls.Add(this.label11);
            this.panel_Info.Controls.Add(this.label10);
            this.panel_Info.Controls.Add(this.lbFPSb);
            this.panel_Info.Controls.Add(this.panel_Icon);
            this.panel_Info.Location = new System.Drawing.Point(653, 127);
            this.panel_Info.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.panel_Info.Name = "panel_Info";
            this.panel_Info.Size = new System.Drawing.Size(116, 391);
            this.panel_Info.TabIndex = 8;
            this.panel_Info.Visible = false;
            // 
            // lbFPS
            // 
            this.lbFPS.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbFPS.Location = new System.Drawing.Point(42, 9);
            this.lbFPS.Name = "lbFPS";
            this.lbFPS.Size = new System.Drawing.Size(62, 19);
            this.lbFPS.TabIndex = 10;
            this.lbFPS.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentSpotNo
            // 
            this.lbCurrentSpotNo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCurrentSpotNo.Location = new System.Drawing.Point(78, 50);
            this.lbCurrentSpotNo.Name = "lbCurrentSpotNo";
            this.lbCurrentSpotNo.Size = new System.Drawing.Size(26, 19);
            this.lbCurrentSpotNo.TabIndex = 9;
            this.lbCurrentSpotNo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCurrentMode
            // 
            this.lbCurrentMode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCurrentMode.Font = new System.Drawing.Font("Arial Black", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCurrentMode.Location = new System.Drawing.Point(6, 112);
            this.lbCurrentMode.Name = "lbCurrentMode";
            this.lbCurrentMode.Size = new System.Drawing.Size(98, 37);
            this.lbCurrentMode.TabIndex = 8;
            this.lbCurrentMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTolerance
            // 
            this.lbTolerance.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTolerance.Location = new System.Drawing.Point(6, 189);
            this.lbTolerance.Name = "lbTolerance";
            this.lbTolerance.Size = new System.Drawing.Size(98, 23);
            this.lbTolerance.TabIndex = 7;
            this.lbTolerance.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(6, 169);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 15);
            this.label12.TabIndex = 3;
            this.label12.Text = "TOLERANCE";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.ForeColor = System.Drawing.Color.Black;
            this.label11.Location = new System.Drawing.Point(6, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(42, 15);
            this.label11.TabIndex = 2;
            this.label11.Text = "MODE";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(6, 53);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(63, 15);
            this.label10.TabIndex = 1;
            this.label10.Text = "SPOT NO.";
            // 
            // lbFPSb
            // 
            this.lbFPSb.AutoSize = true;
            this.lbFPSb.ForeColor = System.Drawing.Color.Black;
            this.lbFPSb.Location = new System.Drawing.Point(6, 15);
            this.lbFPSb.Name = "lbFPSb";
            this.lbFPSb.Size = new System.Drawing.Size(30, 15);
            this.lbFPSb.TabIndex = 0;
            this.lbFPSb.Text = "FPS";
            // 
            // SaveProgressbar
            // 
            this.SaveProgressbar.Location = new System.Drawing.Point(288, 9);
            this.SaveProgressbar.Name = "SaveProgressbar";
            this.SaveProgressbar.Size = new System.Drawing.Size(667, 20);
            this.SaveProgressbar.Step = 1;
            this.SaveProgressbar.TabIndex = 10;
            this.SaveProgressbar.Visible = false;
            // 
            // txtPixelPerDistance
            // 
            this.txtPixelPerDistance.Location = new System.Drawing.Point(225, 8);
            this.txtPixelPerDistance.Name = "txtPixelPerDistance";
            this.txtPixelPerDistance.Size = new System.Drawing.Size(74, 21);
            this.txtPixelPerDistance.TabIndex = 34;
            this.txtPixelPerDistance.Text = "4.8";
            this.txtPixelPerDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtPixelPerDistance.Visible = false;
            // 
            // imageListButton
            // 
            this.imageListButton.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListButton.ImageStream")));
            this.imageListButton.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListButton.Images.SetKeyName(0, "icoCapture.png");
            this.imageListButton.Images.SetKeyName(1, "icoExport.png");
            this.imageListButton.Images.SetKeyName(2, "icoOpen.png");
            this.imageListButton.Images.SetKeyName(3, "IcoSave.png");
            this.imageListButton.Images.SetKeyName(4, "icoStart.png");
            this.imageListButton.Images.SetKeyName(5, "icoStop.png");
            this.imageListButton.Images.SetKeyName(6, "icoTrashcan.png");
            // 
            // squareSizeWidth
            // 
            this.squareSizeWidth.Location = new System.Drawing.Point(736, 351);
            this.squareSizeWidth.Name = "squareSizeWidth";
            this.squareSizeWidth.Size = new System.Drawing.Size(70, 21);
            this.squareSizeWidth.TabIndex = 35;
            this.squareSizeWidth.Visible = false;
            // 
            // squareSizeHeight
            // 
            this.squareSizeHeight.Location = new System.Drawing.Point(735, 384);
            this.squareSizeHeight.Name = "squareSizeHeight";
            this.squareSizeHeight.Size = new System.Drawing.Size(71, 21);
            this.squareSizeHeight.TabIndex = 36;
            this.squareSizeHeight.Visible = false;
            // 
            // squareSizeSet
            // 
            this.squareSizeSet.Location = new System.Drawing.Point(796, 154);
            this.squareSizeSet.Name = "squareSizeSet";
            this.squareSizeSet.Size = new System.Drawing.Size(83, 48);
            this.squareSizeSet.TabIndex = 37;
            this.squareSizeSet.Text = "Set";
            this.squareSizeSet.UseVisualStyleBackColor = true;
            this.squareSizeSet.Click += new System.EventHandler(this.drawButton_Click);
            // 
            // squareWidth
            // 
            this.squareWidth.AutoSize = true;
            this.squareWidth.Location = new System.Drawing.Point(650, 111);
            this.squareWidth.Name = "squareWidth";
            this.squareWidth.Size = new System.Drawing.Size(38, 15);
            this.squareWidth.TabIndex = 38;
            this.squareWidth.Text = "Width";
            // 
            // squareHeight
            // 
            this.squareHeight.AutoSize = true;
            this.squareHeight.Location = new System.Drawing.Point(645, 74);
            this.squareHeight.Name = "squareHeight";
            this.squareHeight.Size = new System.Drawing.Size(43, 15);
            this.squareHeight.TabIndex = 39;
            this.squareHeight.Text = "Height";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(811, 388);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 15);
            this.label3.TabIndex = 42;
            this.label3.Text = "(range : 1~999)";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(811, 354);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 15);
            this.label4.TabIndex = 43;
            this.label4.Text = "(range : 1~999)";
            this.label4.Visible = false;
            // 
            // txtToleranceDoX
            // 
            this.txtToleranceDoX.Location = new System.Drawing.Point(705, 71);
            this.txtToleranceDoX.Name = "txtToleranceDoX";
            this.txtToleranceDoX.Size = new System.Drawing.Size(43, 21);
            this.txtToleranceDoX.TabIndex = 44;
            this.txtToleranceDoX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ToleranceSize_KeyDown);
            // 
            // txtToleranceMinX
            // 
            this.txtToleranceMinX.Location = new System.Drawing.Point(773, 71);
            this.txtToleranceMinX.Name = "txtToleranceMinX";
            this.txtToleranceMinX.Size = new System.Drawing.Size(43, 21);
            this.txtToleranceMinX.TabIndex = 45;
            this.txtToleranceMinX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ToleranceSize_KeyDown);
            // 
            // txtToleranceSecX
            // 
            this.txtToleranceSecX.Location = new System.Drawing.Point(843, 72);
            this.txtToleranceSecX.Name = "txtToleranceSecX";
            this.txtToleranceSecX.Size = new System.Drawing.Size(43, 21);
            this.txtToleranceSecX.TabIndex = 46;
            this.txtToleranceSecX.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ToleranceSize_KeyDown);
            // 
            // txtToleranceDoY
            // 
            this.txtToleranceDoY.Location = new System.Drawing.Point(705, 108);
            this.txtToleranceDoY.Name = "txtToleranceDoY";
            this.txtToleranceDoY.Size = new System.Drawing.Size(43, 21);
            this.txtToleranceDoY.TabIndex = 47;
            this.txtToleranceDoY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ToleranceSize_KeyDown);
            // 
            // txtToleranceMinY
            // 
            this.txtToleranceMinY.Location = new System.Drawing.Point(773, 108);
            this.txtToleranceMinY.Name = "txtToleranceMinY";
            this.txtToleranceMinY.Size = new System.Drawing.Size(43, 21);
            this.txtToleranceMinY.TabIndex = 48;
            this.txtToleranceMinY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ToleranceSize_KeyDown);
            // 
            // txtToleranceSecY
            // 
            this.txtToleranceSecY.Location = new System.Drawing.Point(843, 108);
            this.txtToleranceSecY.Name = "txtToleranceSecY";
            this.txtToleranceSecY.Size = new System.Drawing.Size(43, 21);
            this.txtToleranceSecY.TabIndex = 49;
            this.txtToleranceSecY.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ToleranceSize_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(746, 71);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(12, 15);
            this.label1.TabIndex = 50;
            this.label1.Text = "°";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(816, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(9, 15);
            this.label2.TabIndex = 51;
            this.label2.Text = "\'";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(887, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 15);
            this.label7.TabIndex = 52;
            this.label7.Text = "\'\'";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(887, 108);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 15);
            this.label8.TabIndex = 53;
            this.label8.Text = "\'\'";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(816, 108);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(9, 15);
            this.label9.TabIndex = 54;
            this.label9.Text = "\'";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(746, 108);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(12, 15);
            this.label13.TabIndex = 55;
            this.label13.Text = "°";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(919, 590);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtToleranceSecY);
            this.Controls.Add(this.txtToleranceMinY);
            this.Controls.Add(this.txtToleranceDoY);
            this.Controls.Add(this.txtToleranceSecX);
            this.Controls.Add(this.txtToleranceMinX);
            this.Controls.Add(this.txtToleranceDoX);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.squareHeight);
            this.Controls.Add(this.menuBar);
            this.Controls.Add(this.squareWidth);
            this.Controls.Add(this.squareSizeSet);
            this.Controls.Add(this.cmdFactorySet);
            this.Controls.Add(this.squareSizeHeight);
            this.Controls.Add(this.btn_Capture);
            this.Controls.Add(this.squareSizeWidth);
            this.Controls.Add(this.SaveProgressbar);
            this.Controls.Add(this.cmdAnalysis);
            this.Controls.Add(this.panel_Info);
            this.Controls.Add(this.panel_image);
            this.Controls.Add(this.btn_SettingFileSave);
            this.Controls.Add(this.panel_Status);
            this.Controls.Add(this.panel_Set);
            this.Controls.Add(this.picWikiLogo);
            this.Controls.Add(this.txtPixelPerDistance);
            this.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1173, 750);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "* WikiOptics FirstOptics *";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.menuBar.ResumeLayout(false);
            this.menuBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picWikiLogo)).EndInit();
            this.panel_Icon.ResumeLayout(false);
            this.panel_Set.ResumeLayout(false);
            this.panel_Set.PerformLayout();
            this.panel_Status.ResumeLayout(false);
            this.panel_Status.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pic_Status1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Track_ShutterSpeed)).EndInit();
            this.panel_image.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pic_Camera)).EndInit();
            this.panel_Info.ResumeLayout(false);
            this.panel_Info.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private ImageList imageListButton;
        private TextBox txtPixelPerDistance;

        MenuStrip menuBar;
        ToolStripMenuItem menu_Measurement;
        ToolStripMenuItem menu_Tools;
        ToolStripMenuItem menu_Settings;
        ToolStripMenuItem menu_Tools_ZeroSet;
        ToolStripMenuItem menu_Tools_Mode;
        ToolStripMenuItem menu_Tools_Angledefinition;
        ToolStripMenuItem menu_Tools_Coordinate;
        ToolStripMenuItem menu_Tools_Numbering;
        ToolStripMenuItem menu_Setting_Unit;
        ToolStripMenuItem menu_Setting_Tolerance;
        ToolStripMenuItem menu_Setting_GrayLevel;
        ToolStripMenuItem menu_Information;
        //ToolStripMenuItem menu_Information_About;

        ToolStripMenuItem menu_Tools_Mode_Area;
        ToolStripMenuItem menu_Tools_Mode_Brightness;
        ToolStripMenuItem menu_Tools_Mode_Peak;

        ToolStripMenuItem menu_Tools_Angle_Ext;
        ToolStripMenuItem menu_Tools_Angle_Int;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring;
        ToolStripMenuItem menu_Tools_Numbring_Angle;
        ToolStripMenuItem menu_Tools_Numbring_Size;
        ToolStripMenuItem menu_Settings_Unit_Degree;
        ToolStripMenuItem menu_Settings_Unit_Sec;
        ToolStripMenuItem menu_Settings_Tolerance_Circle;
        ToolStripMenuItem menu_Settings_Tolerance_Square;

        ToolStripMenuItem menu_Tools_Zoom;
        ToolStripMenuItem menu_Tools_Zoom_4;
        ToolStripMenuItem menu_Tools_Zoom_8;
        ToolStripMenuItem menu_Tools_Zoom_16;

        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Xon;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Yon;
        ToolStripMenuItem menu_Tools_Coordinate_Mirroring_Off;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_L90;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_R90;
        ToolStripMenuItem menu_Tools_Coordinate_Rotating_Off;
        ToolStripMenuItem menu_Tools_Zoom_Off;
        ToolStripMenuItem menu_Tools_Zoom_32;
        ToolStripMenuItem menu_Tools_Coordinate_Original;
        ToolStripMenuItem menu_Settings_Tolerance_Value;
        Label label5;
        Label label6;
        Label label10;
        Label label11;
        Label label12;
        Label label17;
        Label label37;

        Label lbCurrentSpotNo;
        Label lbCurrentMode;
        Label lbTolerance;
        Label lbFPSb;
        Label lbFPS;
        Label lbShutter;
        Label lbStatusDescription;
        Label lbStatusResult;

        PictureBox pic_Status1;
        PictureBox pic_Status2;
        PictureBox pic_Status3;
        PictureBox pic_Status4;
        PictureBox pic_Status5;
        PictureBox pic_Status6;
        PictureBox pic_Status7;
        PictureBox pic_Status8;
        PictureBox pic_Status9;
        PictureBox pic_Status10;
        PictureBox pic_Status11;
        PictureBox pic_Status12;
        PictureBox pic_Status13;
        PictureBox pic_Status14;
        PictureBox pic_Status15;

        PictureBox pic_Camera;

        Panel panel1;
       // Panel panel3;
       // Panel panel4;
        //Panel panel5;

        Panel panel_Status;
        Panel panel_image;
        Panel panel_Info;
        Panel panel_Icon;
        Panel panel_Set;

        Button cmdFactorySet;
        Button cmdAnalysis;
        Button btn_Capture;
        Button cmdClear;
        Button cmdPrint;
        Button cmdExport;
        Button btn_SettingFileSave;
        Button btn_SettingFileOpen;
        Button cmdMeasureData;

        TextBox txtSamplingFreq;

        TrackBar Track_ShutterSpeed;

        ProgressBar SaveProgressbar;

        PictureBox picWikiLogo;

        private TextBox TextBox_ShutterSpeed;
        private Button SaveAllConfig;
        private Button AutoShutter;
        private Button InstallSerialDriver;

        #endregion

        private TextBox squareSizeWidth;
        private TextBox squareSizeHeight;
        private Button squareSizeSet;
        private Label squareWidth;
        private Label squareHeight;
        private Label label3;
        private Label label4;
        private TextBox txtToleranceDoX;
        private TextBox txtToleranceMinX;
        private TextBox txtToleranceSecX;
        private TextBox txtToleranceDoY;
        private TextBox txtToleranceMinY;
        private TextBox txtToleranceSecY;
        private Label label1;
        private Label label2;
        private Label label7;
        private Label label8;
        private Label label9;
        private Label label13;
    }
}

