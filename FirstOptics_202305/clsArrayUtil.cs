using System;
using System.Text;

namespace WikiAngleAutoCollimator
{
	// Token: 0x02000014 RID: 20
	internal class clsArrayUtil
	{
		// Token: 0x06000079 RID: 121 RVA: 0x0000D810 File Offset: 0x0000BA10
		public static string ToString<T>(T[] array, string format = "")
		{
			StringBuilder stringBuilder = new StringBuilder();
			string text = "{0" + format + "}";
			for (int i = 0; i < array.Length; i++)
			{
				if (i < array.Length - 1)
				{
					stringBuilder.AppendFormat(text + ", ", array[i]);
				}
				else
				{
					stringBuilder.AppendFormat(text, array[i]);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600007A RID: 122 RVA: 0x0000D884 File Offset: 0x0000BA84
		public clsArrayUtil()
		{
		}
	}
}
