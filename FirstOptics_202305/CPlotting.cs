﻿using System;

namespace WikiOptics_Collimator
{
    internal class CPlotting
    {
        public static int PlottingSpotNumber;        
        public static bool WobblePlottingFlag;        
        public static bool WobblePlottingClearFlag;

        public CPlotting()
        {
            PlottingSpotNumber = 1;
            WobblePlottingFlag = false;
            WobblePlottingClearFlag = false;
        }
    }
}

