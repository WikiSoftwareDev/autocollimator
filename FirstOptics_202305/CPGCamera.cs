﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlyCapture2Managed;
using FlyCapture2Managed.Gui;
using System.Diagnostics;


namespace WikiOptics_Collimator
{
    internal class CPGCamera
    {
        #region variables

        public const Mode CustomMode = Mode.Mode0;      // FlyCapture2Managed
        public static uint[] DeviceSerial;
        public static uint DeviceNumer;
        public static uint SelectDeviceNumber;
        public static string CameraName;
        public static uint CameraSerial;
        public static CameraControlDialog m_camCtlDlg;
        public static ManagedCameraBase m_camera = null;
        public static ManagedImage m_rawImage;                  // FlyCapture2Managed
        public static ManagedImage m_processedImage;
        public static ManagedImage m_AnalysisImage;
        public static ManagedImage m_RedImage = null;
        public static ManagedBusManager m_busMgr = null;
        public static bool m_grabImages;
        public static uint numCameras;
        //private static IntPtr m_busResetHandle;
        public static ManagedPGRGuid guid;
        public static InterfaceType currInterface;
        public static ManagedCamera camera;
        private static CameraInfo camInfo;
        //private static CameraProperty camProp;
       // private static CameraPropertyInfo camPropInfo;
        private static Format7ImageSettings Imagesettings;
        private static Format7Info ImageFormat7Info;
        private static PixelFormat CustomPixelFormat;
        private static CameraProperty camProp;
        public static bool StartCameraFlag = false;
        public static bool CameraConnectFlag = false;

        #endregion

        public CPGCamera()
        {
            m_busMgr = null;
            camera = null;
        }

        ~CPGCamera()
        {
            if (m_busMgr != null)
            {
                m_busMgr.Dispose();
                m_busMgr = null;
            }

            if (camera != null)
            {
                camera.Dispose();
                camera = null;
            }
        }

        public static bool initCamera()
        {
            if (m_busMgr == null)
                m_busMgr = new ManagedBusManager();

            try
            {
                numCameras = m_busMgr.GetNumOfCameras();
            }
            catch (FC2Exception ex)
            {
                ex.Dispose();
                CameraConnectFlag = false;
                return false;
            }
            if (numCameras == 0U)
            {
                CameraConnectFlag = false;
                return false;
            }

            guid = m_busMgr.GetCameraFromIndex(SelectDeviceNumber);
            currInterface = m_busMgr.GetInterfaceTypeFromGuid(guid);
            camera = new ManagedCamera();
            try
            {
                Program.log.Info("call camera.Connect(guid)");
                camera.Connect(guid);
            }
            catch (FC2Exception ex2)
            {
                Program.log.Info("call camera.Connect(guid) exception");
                Program.log.Info(ex2.ToString());
                ex2.Dispose();
                CameraConnectFlag = false;
                return false;
            }

            camInfo = camera.GetCameraInfo();
            CameraName = camInfo.modelName;
            CameraSerial = camInfo.serialNumber;
            m_rawImage = new ManagedImage();
            CameraConnectFlag = true;
            return true;
        }

        public static void StartCameraCapture()
        {
            try
            {
                camera.StartCapture();
                StartCameraFlag = true;
            }
            catch (Exception e)
            {
                Trace.WriteLine("StartCapture : exception : " + e.ToString());
                Trace.WriteLine("StartCapture : stack : " + e.StackTrace);
            }
        }

        public static bool ShowDeviceList()
        {
            try
            {
                if (m_busMgr == null)
                    m_busMgr = new ManagedBusManager();

                DeviceNumer = m_busMgr.GetNumOfCameras();
                DeviceSerial = new uint[DeviceNumer];
                ManagedCamera[] array = new ManagedCamera[DeviceNumer];
                for (uint num = 0U; num < DeviceNumer; num += 1U)
                {
                    array[(int)((UIntPtr)num)] = new ManagedCamera();
                    ManagedPGRGuid cameraFromIndex = m_busMgr.GetCameraFromIndex(num);
                    array[(int)((UIntPtr)num)].Connect(cameraFromIndex);
                    CameraInfo cameraInfo = array[(int)((UIntPtr)num)].GetCameraInfo();
                    DeviceSerial[(int)((UIntPtr)num)] = cameraInfo.serialNumber;
                }
            }
            catch (FC2Exception ex)
            {
                ex.Dispose();
                DeviceNumer = 0U;
                CameraConnectFlag = false;
                return true;
            }
            return true;
        }

        public static void StopCameraCapture()
        {
            try
            {
                camera.StopCapture();
            }
            catch (FC2Exception)
            {
            }
            StartCameraFlag = false;
        }

        public static bool SetCustomVideoMode()
        {
            bool flag = false;
            Imagesettings = new Format7ImageSettings();
            ImageFormat7Info = camera.GetFormat7Info(Mode.Mode0, ref flag);
            Imagesettings.mode = Mode.Mode0;
            Imagesettings.offsetX = CamParamInfo.ImageLeft;
            Imagesettings.offsetY = CamParamInfo.ImageTop;
            Imagesettings.width = CamParamInfo.ImageWidth;
            Imagesettings.height = CamParamInfo.ImageHeight;
            string pixelFormat;
            if ((pixelFormat = CamParamInfo.PixelFormat) != null)
            {
                if (!(pixelFormat == "Mono8"))
                {
                    if (!(pixelFormat == "Mono12"))
                    {
                        if (pixelFormat == "Mono16")
                        {
                            CustomPixelFormat = PixelFormat.PixelFormatMono16;
                        }
                    }
                    else
                    {
                        CustomPixelFormat = PixelFormat.PixelFormatMono12;
                    }
                }
                else
                {
                    CustomPixelFormat = PixelFormat.PixelFormatMono8;
                }
            }
            Imagesettings.pixelFormat = CustomPixelFormat;
            bool flag2 = false;
            bool result;
            try
            {
                Format7PacketInfo format7PacketInfo = camera.ValidateFormat7Settings(Imagesettings, ref flag2);
                if (!flag2)
                {
                    result = false;
                }
                else
                {
                    format7PacketInfo.maxBytesPerPacket = CamParamInfo.PacketSize;
                    camera.SetFormat7Configuration(Imagesettings, format7PacketInfo.recommendedBytesPerPacket);
                    result = true;
                }
            }
            catch (FC2Exception ex)
            {
                Console.Write(ex.Message);
                result = false;
            }
            return result;
        }

        public static bool SetCameraSetting()
        {
            CameraProperty property = camera.GetProperty(PropertyType.Brightness);
            property.absControl = CamParamInfo.AbsoluteMode;
            property.autoManualMode = false;
            property.valueA = CamParamInfo.Brightness;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex)
            {
                ex.Dispose();
                return false;
            }
            property = camera.GetProperty(PropertyType.AutoExposure);
            property.absControl = CamParamInfo.AbsoluteMode;
            property.autoManualMode = true;
            property.onOff = true;
            property.absValue = CamParamInfo.Exposure;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex2)
            {
                ex2.Dispose();
                return false;
            }
            property = camera.GetProperty(PropertyType.Sharpness);
            property.absControl = false;
            property.autoManualMode = true;
            property.onOff = true;
            property.valueA = CamParamInfo.Sharpness;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex3)
            {
                ex3.Dispose();
                return false;
            }
            property = camera.GetProperty(PropertyType.Gamma);
            property.absControl = CamParamInfo.AbsoluteMode;
            property.autoManualMode = true;
            property.absValue = CamParamInfo.Gamma;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex4)
            {
                ex4.Dispose();
                return false;
            }
            property = camera.GetProperty(PropertyType.Shutter);
            property.absControl = CamParamInfo.AbsoluteMode;
            property.autoManualMode = true;
            property.absValue = CamParamInfo.ShutterSpeed;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex5)
            {
                ex5.Dispose();
                return false;
            }
            property = camera.GetProperty(PropertyType.Gain);
            property.absControl = CamParamInfo.AbsoluteMode;
            property.autoManualMode = false;
            property.absValue = CamParamInfo.Gain;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex6)
            {
                ex6.Dispose();
                return false;
            }
            property = camera.GetProperty(PropertyType.FrameRate);
            property.absControl = CamParamInfo.AbsoluteMode;
            property.autoManualMode = false;
            property.onOff = true;
            property.valueA = CamParamInfo.FrameRate;
            try
            {
                camera.SetProperty(property);
            }
            catch (FC2Exception ex7)
            {
                ex7.Dispose();
                return false;
            }
            return true;
        }
        public static void SetShutterSpeed(float ShutterValue)
        {
            if (null == camera)
                return;

            camProp = camera.GetProperty(PropertyType.Shutter);
            camProp.absControl = CamParamInfo.AbsoluteMode;
            camProp.autoManualMode = false;
            camProp.absValue = ShutterValue;
            try
            {
                camera.SetProperty(camProp);
            }
            catch (FC2Exception ex)
            {
                ex.Dispose();
            }
        }




    }
}
    

