// WikiAngleAutoCollimator.frmFactorySetting
using System;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;

namespace WikiOptics_Collimator
{
    public class frmFactorySetting : Form
    {
        private IContainer components ;

        private TextBox txtThreshold;

        private Label label34;

        private TextBox txtRefAngle;

        private Label label35;

        private Button cmdMeasureFocalLength;

        private TextBox txtFocalLength;

        private Label label11;

        private Label label8;

        private TextBox txtCenterY;

        private Label label9;

        private Label label6;

        private TextBox txtCenterX;

        private Label label7;

        private Label label4;

        private TextBox txttiltAngle;

        private Label label5;

        private Label label3;

        private TextBox txtPixelPerDistance;

        private Label label2;

        private Button cmdFactorySet;

        private TextBox txtAverageNumber;

        private Label label1;

        private TextBox txtAlpha;

        private Label label10;

        private Label label12;

        public frmFactorySetting()
        {
            InitializeComponent();
        }

        private void frmFactorySetting_Load(object sender, EventArgs e)
        {
            txtPixelPerDistance.Text = CParameter.PixelPerDistance.ToString();
            txtAverageNumber.Text = CParameter.AverageNumber.ToString();
            txtCenterX.Text = $"{CParameter.CenterXPos:f4}";
            txtCenterY.Text = $"{CParameter.CenterYPos:f4}";
            txtFocalLength.Text = $"{CParameter.FocalLength:f4}";
            txtThreshold.Text = CParameter.ReferenceThreshold.ToString();
            txttiltAngle.Text = $"{CParameter.TiltAngle:f4}";
            txtRefAngle.Text = CParameter.ReferenceAngle.ToString();
        }

        private void cmdMeasureFocalLength_Click(object sender, EventArgs e)
        {
            CParameter.PixelPerDistance = Convert.ToDouble(txtPixelPerDistance.Text);
            CParameter.ReferenceThreshold = Convert.ToInt16(txtThreshold.Text);
            CParameter.ReferenceAngle = Convert.ToDouble(txtRefAngle.Text);
            CParameter.AlphaValue = Convert.ToInt16(txtAlpha.Text);
            CParameter.AverageNumber = Convert.ToInt16(txtAverageNumber.Text);
            CParameter.FactorySetFlag = true;
            CParameter.FactorySetStartFlag = true;
            while (CParameter.FactorySetFlag)
            {
                clsETC.Time_Delay(100L);
            }
            txtCenterX.Text = $"{CParameter.CenterXPos:f4}";
            txtCenterY.Text = $"{CParameter.CenterYPos:f4}";
            txttiltAngle.Text = $"{CParameter.TiltAngle:f4}";
            txtFocalLength.Text = $"{CParameter.FocalLength:f4}";
        }

        private void cmdFactorySet_Click(object sender, EventArgs e)
        {
            CParameter.PixelPerDistance = Convert.ToDouble(txtPixelPerDistance.Text);
            CParameter.ReferenceThreshold = Convert.ToInt16(txtThreshold.Text);
            CParameter.ReferenceAngle = Convert.ToDouble(txtRefAngle.Text);
            CParameter.AlphaValue = Convert.ToInt16(txtAlpha.Text);
            CParameter.AverageNumber = Convert.ToInt16(txtAverageNumber.Text);
            CImageProcess.nAnalysisAverage = CParameter.AverageNumber;
            CImageProcess.nAnalysisCurrent = 0;
            CParameter.SaveParameterInfo();
            
            MessageBox.Show(" Factory Set Success..!!");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && components != null)
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(WikiOptics_Collimator.frmFactorySetting));
            this.components = new System.ComponentModel.Container();
            this.txtThreshold = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtRefAngle = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.cmdMeasureFocalLength = new System.Windows.Forms.Button();
            this.txtFocalLength = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCenterY = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtCenterX = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txttiltAngle = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPixelPerDistance = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdFactorySet = new System.Windows.Forms.Button();
            this.txtAverageNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAlpha = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            base.SuspendLayout();
            this.txtThreshold.Location = new System.Drawing.Point(131, 37);
            this.txtThreshold.Name = "txtThreshold";
            this.txtThreshold.Size = new System.Drawing.Size(74, 21);
            this.txtThreshold.TabIndex = 51;
            this.txtThreshold.Text = "30";
            this.txtThreshold.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(55, 40);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(70, 12);
            this.label34.TabIndex = 50;
            this.label34.Text = "Threshold :";
            this.txtRefAngle.Location = new System.Drawing.Point(131, 64);
            this.txtRefAngle.Name = "txtRefAngle";
            this.txtRefAngle.Size = new System.Drawing.Size(74, 21);
            this.txtRefAngle.TabIndex = 49;
            this.txtRefAngle.Text = "0.1";
            this.txtRefAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(58, 67);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(67, 12);
            this.label35.TabIndex = 48;
            this.label35.Text = "Ref Angle :";
            this.cmdMeasureFocalLength.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMeasureFocalLength.Location = new System.Drawing.Point(60, 144);
            this.cmdMeasureFocalLength.Name = "cmdMeasureFocalLength";
            this.cmdMeasureFocalLength.Size = new System.Drawing.Size(145, 30);
            this.cmdMeasureFocalLength.TabIndex = 47;
            this.cmdMeasureFocalLength.Text = "Measure FocalLength";
            this.cmdMeasureFocalLength.UseVisualStyleBackColor = true;
            this.cmdMeasureFocalLength.Click += new System.EventHandler(cmdMeasureFocalLength_Click);
            this.txtFocalLength.Location = new System.Drawing.Point(340, 91);
            this.txtFocalLength.Name = "txtFocalLength";
            this.txtFocalLength.Size = new System.Drawing.Size(74, 21);
            this.txtFocalLength.TabIndex = 46;
            this.txtFocalLength.Text = "50";
            this.txtFocalLength.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(318, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(16, 12);
            this.label11.TabIndex = 45;
            this.label11.Text = "f :";
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(415, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 12);
            this.label8.TabIndex = 44;
            this.label8.Text = "pixel";
            this.txtCenterY.Location = new System.Drawing.Point(340, 37);
            this.txtCenterY.Name = "txtCenterY";
            this.txtCenterY.Size = new System.Drawing.Size(74, 21);
            this.txtCenterY.TabIndex = 43;
            this.txtCenterY.Text = "512";
            this.txtCenterY.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(272, 46);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 12);
            this.label9.TabIndex = 42;
            this.label9.Text = "Center Y :";
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(415, 19);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 12);
            this.label6.TabIndex = 41;
            this.label6.Text = "pixel";
            this.txtCenterX.Location = new System.Drawing.Point(340, 10);
            this.txtCenterX.Name = "txtCenterX";
            this.txtCenterX.Size = new System.Drawing.Size(74, 21);
            this.txtCenterX.TabIndex = 40;
            this.txtCenterX.Text = "650";
            this.txtCenterX.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(272, 19);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(62, 12);
            this.label7.TabIndex = 39;
            this.label7.Text = "Center X :";
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(415, 73);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 12);
            this.label4.TabIndex = 38;
            this.label4.Text = "degree";
            this.txttiltAngle.Location = new System.Drawing.Point(340, 64);
            this.txttiltAngle.Name = "txttiltAngle";
            this.txttiltAngle.Size = new System.Drawing.Size(74, 21);
            this.txttiltAngle.TabIndex = 37;
            this.txttiltAngle.Text = "0";
            this.txttiltAngle.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(289, 73);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(45, 12);
            this.label5.TabIndex = 36;
            this.label5.Text = "Angle :";
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(206, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 12);
            this.label3.TabIndex = 35;
            this.label3.Text = "um";
            this.txtPixelPerDistance.Location = new System.Drawing.Point(131, 10);
            this.txtPixelPerDistance.Name = "txtPixelPerDistance";
            this.txtPixelPerDistance.Size = new System.Drawing.Size(74, 21);
            this.txtPixelPerDistance.TabIndex = 34;
            this.txtPixelPerDistance.Text = "4.8";
            this.txtPixelPerDistance.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 12);
            this.label2.TabIndex = 33;
            this.label2.Text = "Pixel Per Distance :";
            this.cmdFactorySet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFactorySet.Location = new System.Drawing.Point(269, 144);
            this.cmdFactorySet.Name = "cmdFactorySet";
            this.cmdFactorySet.Size = new System.Drawing.Size(145, 30);
            this.cmdFactorySet.TabIndex = 52;
            this.cmdFactorySet.Text = "SET";
            this.cmdFactorySet.UseVisualStyleBackColor = true;
            this.cmdFactorySet.Click += new System.EventHandler(cmdFactorySet_Click);
            this.txtAverageNumber.Location = new System.Drawing.Point(131, 118);
            this.txtAverageNumber.Name = "txtAverageNumber";
            this.txtAverageNumber.Size = new System.Drawing.Size(74, 21);
            this.txtAverageNumber.TabIndex = 54;
            this.txtAverageNumber.Text = "20";
            this.txtAverageNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 124);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(108, 12);
            this.label1.TabIndex = 53;
            this.label1.Text = "Average Number :";
            this.txtAlpha.Location = new System.Drawing.Point(131, 91);
            this.txtAlpha.Name = "txtAlpha";
            this.txtAlpha.Size = new System.Drawing.Size(74, 21);
            this.txtAlpha.TabIndex = 56;
            this.txtAlpha.Text = "2";
            this.txtAlpha.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(80, 94);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(45, 12);
            this.label10.TabIndex = 55;
            this.label10.Text = "Alpha :";
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(415, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(23, 12);
            this.label12.TabIndex = 57;
            this.label12.Text = "um";
            base.AutoScaleDimensions = new System.Drawing.SizeF(7f, 12f);
            base.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            base.ClientSize = new System.Drawing.Size(462, 186);
            base.Controls.Add(this.label12);
            base.Controls.Add(this.txtAlpha);
            base.Controls.Add(this.label10);
            base.Controls.Add(this.txtAverageNumber);
            base.Controls.Add(this.label1);
            base.Controls.Add(this.cmdFactorySet);
            base.Controls.Add(this.txtThreshold);
            base.Controls.Add(this.label34);
            base.Controls.Add(this.txtRefAngle);
            base.Controls.Add(this.label35);
            base.Controls.Add(this.cmdMeasureFocalLength);
            base.Controls.Add(this.txtFocalLength);
            base.Controls.Add(this.label11);
            base.Controls.Add(this.label8);
            base.Controls.Add(this.txtCenterY);
            base.Controls.Add(this.label9);
            base.Controls.Add(this.label6);
            base.Controls.Add(this.txtCenterX);
            base.Controls.Add(this.label7);
            base.Controls.Add(this.label4);
            base.Controls.Add(this.txttiltAngle);
            base.Controls.Add(this.label5);
            base.Controls.Add(this.label3);
            base.Controls.Add(this.txtPixelPerDistance);
            base.Controls.Add(this.label2);
          //  base.Icon = (System.Drawing.Icon)resources.GetObject("$this.Icon");
            base.Name = "frmFactorySetting";
            base.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FactorySetting";
            base.Load += new System.EventHandler(frmFactorySetting_Load);
            base.ResumeLayout(false);
            base.PerformLayout();
        }
    }
}
